package de.crysxd.octoapp.utils

import android.widget.Toast
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.AutoConnectPrinterUseCase
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.base.usecase.GetPrinterConnectionUseCase
import de.crysxd.octoapp.base.usecase.UseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.exceptions.PrintBootingException
import de.crysxd.octoapp.engine.models.connection.Connection
import de.crysxd.octoapp.engine.models.connection.ConnectionState
import de.crysxd.octoapp.engine.models.power.PowerDevice
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flattenMerge
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
class ConnectPrinterUseCase @Inject constructor(
    private val autoConnectPrinterUseCase: AutoConnectPrinterUseCase,
    private val getPrinterConnectionUseCase: GetPrinterConnectionUseCase,
    private val getPowerDevicesUseCase: GetPowerDevicesUseCase,
    private val octoPreferences: OctoPreferences,
    private val octoPrintRepository: OctoPrintRepository,
) : UseCase<ConnectPrinterUseCase.Params, Flow<ConnectPrinterUseCase.UiState>>() {

    companion object {
        private const val MIN_LOADING_DELAY = 2000L
        private const val ERROR_RETRY_DELAY = 2000L
        private const val PORT_POLLING_INTERVAL = 1000L
        private const val PSU_POLLING_INTERVAL = 3000L
    }

    private val connectionTimeoutMs = TimeUnit.SECONDS.toNanos(Firebase.remoteConfig.getLong("printer_connection_timeout_sec"))
    private var lastConnectionAttempt = 0L
    private var startedAt = System.currentTimeMillis()

    private var supervisorJob: Job? = null
    private var scope: CoroutineScope? = null

    private val instanceIdFlow = MutableStateFlow<String?>(null)
    private val userAllowedConnectionAtFlow = MutableStateFlow(0L)
    private val manualTriggerFlow = MutableSharedFlow<Unit>()
    private val psuCycledStateFlow = MutableStateFlow<PsuCyclingState>(PsuCyclingState.NotCycled)
    private val manualPsuStateFlow = MutableStateFlow<Boolean?>(null)

    private val psuSupportedFlow = instanceIdFlow.filterNotNull().flatMapLatest { instanceId ->
        octoPrintRepository.instanceInformationFlow(instanceId)
    }.flatMapLatest { instance ->
        flow {
            while (currentCoroutineContext().isActive) {
                // Check if the default device is turned on or off
                val default = instance?.appSettings?.defaultPowerDevices?.get(AppSettings.DEFAULT_POWER_DEVICE_PSU)
                Timber.i("Default power device: $default")
                default?.let { deviceId ->
                    val params = GetPowerDevicesUseCase.Params(queryState = true, onlyGetDeviceWithUniqueId = deviceId, instanceId = instance.id)
                    when (getPowerDevicesUseCase.execute(params).firstOrNull()?.second) {
                        GetPowerDevicesUseCase.PowerState.Off -> emit(false)
                        GetPowerDevicesUseCase.PowerState.On -> emit(true)
                        else -> emit(null)
                    }

                    // Reset manual, we now updated the real one
                    manualPsuStateFlow.emit(null)
                } ?: emit(null)

                delay(PSU_POLLING_INTERVAL)
            }
        }.onCompletion {
            Timber.i("Finish PSU check")
        }.onStart {
            Timber.i("Start PSU check")
        }
    }.retry {
        Timber.e(it)
        delay(ERROR_RETRY_DELAY)
        true
    }.flowOn(Dispatchers.IO)

    private val availableSerialConnectionsFlow = instanceIdFlow.filterNotNull().flatMapLatest { instanceId ->
        flow {
            emit(null)
            var lastWasSuccess = false
            while (currentCoroutineContext().isActive) {
                try {
                    // It's not unused...
                    @Suppress("UNUSED_VALUE")
                    lastWasSuccess = false

                    emit(ConnectionResult.Success(getPrinterConnectionUseCase.execute(GetPrinterConnectionUseCase.Params(instanceId))))
                    lastWasSuccess = true
                    delay(PORT_POLLING_INTERVAL)
                } catch (e: CancellationException) {
                    // Ignore, usually the child got cancelled randomly
                    // If the main flow got cancelled the while loop will exit
                    Timber.w("Ignoring exception in connection result: ${e::class.java.name}: ${e.message} ")
                } catch (e: Exception) {
                    // Do not log full exception to not litter logs!
                    Timber.e("Connection result failed with: ${e::class.java.name}: ${e.message}")

                    // Only delay and emit failure if last attempt was not successful, so we get one
                    // "free" failure
                    if (!lastWasSuccess) {
                        emit(ConnectionResult.Failure(e))
                        delay(ERROR_RETRY_DELAY)
                    }
                }
            }
        }
    }.retry {
        Timber.e(it)
        delay(ERROR_RETRY_DELAY)
        true
    }.flowOn(Dispatchers.IO)

    override suspend fun doExecute(param: Params, timber: Timber.Tree): Flow<UiState> {
        instanceIdFlow.emit(param.instanceId)
        return combine(
            listOf(
                flowOf(flowOf(Unit), octoPreferences.updatedFlow, manualTriggerFlow).flattenMerge(),
                availableSerialConnectionsFlow,
                psuCycledStateFlow,
                psuSupportedFlow,
                userAllowedConnectionAtFlow,
                manualPsuStateFlow,
                instanceIdFlow,
            )
        ) {
            computeUiState(
                connectionResult = it[1] as ConnectionResult?,
                psuCyclingState = it[2] as PsuCyclingState,
                psuState = it[3] as Boolean?,
                userAllowedConnectionAt = it[4] as Long,
                manualPsuState = it[5] as Boolean?,
                instanceId = it[6] as String,
            )
        }.onStart {
            rebuildCoroutineScope()
        }.onCompletion {
            supervisorJob?.cancel()
        }
    }

    private fun rebuildCoroutineScope() {
        supervisorJob?.cancel()
        supervisorJob = SupervisorJob()
        scope = CoroutineScope(supervisorJob!! + Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
            Timber.e(throwable, "Caught NON-CONTAINED exception in ConnectPrinterUseCase#scope!")
            ExceptionReceivers.dispatchException(throwable)
        })
    }

    // ===================
    // State determination
    // ===================

    private fun computeUiState(
        connectionResult: ConnectionResult?,
        psuCyclingState: PsuCyclingState,
        psuState: Boolean?,
        userAllowedConnectionAt: Long,
        manualPsuState: Boolean?,
        instanceId: String,
    ): UiState {
        try {
            // Connection
            val connectionResponse = (connectionResult as? ConnectionResult.Success)?.response

            // PSU
            val isPsuSupported = psuState != null
            val isPsuTurnedOn = manualPsuState ?: psuState

            // Are we allowed to automatically connect the printer?
            val isAutoConnect = octoPreferences.isAutoConnectPrinter ||
                    TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - userAllowedConnectionAt) < 3

            val activeSince = System.currentTimeMillis() - startedAt
            Timber.d("-----")
            Timber.d("ConnectionResponse: $connectionResponse")
            Timber.d("PsuSupported: $isPsuSupported")
            Timber.d("PsuCycled: $psuCyclingState")
            Timber.d("PsuState: $isPsuTurnedOn")
            Timber.d("activeSince: ${activeSince}ms")
            Timber.d("isAutoConnect: $isAutoConnect")

            if (activeSince > MIN_LOADING_DELAY) {
                return when {
                    isOctoPrintUnavailable(connectionResult) -> UiState.OctoPrintNotAvailable
                    isOctoPrintStarting(connectionResult) -> UiState.OctoPrintStarting
                    isPsuBeingCycled(psuCyclingState) -> UiState.PrinterPsuCycling
                    connectionResponse == null -> UiState.Initializing
                    isPrinterOffline(connectionResponse, psuCyclingState) -> UiState.PrinterOffline(isPsuSupported)
                    isPrinterConnecting(connectionResponse) -> UiState.PrinterConnecting
                    isPrinterConnected(connectionResponse) -> UiState.PrinterConnected
                    !isAutoConnect -> UiState.WaitingForUser
                    isNoPrinterAvailable(connectionResponse) -> UiState.WaitingForPrinterToComeOnline(isPsuTurnedOn)
                    else -> {
                        // Printer ready to connect
                        autoConnect(connectionResponse, instanceId = instanceId)
                        UiState.WaitingForPrinterToComeOnline(isPsuTurnedOn)
                    }

                }
            }
        } catch (e: Exception) {
            Timber.e(e)
        }

        return UiState.Initializing
    }

    private fun isPsuBeingCycled(psuCycledState: PsuCyclingState) =
        psuCycledState == PsuCyclingState.Cycling

    private fun isOctoPrintUnavailable(result: ConnectionResult?) =
        result is ConnectionResult.Failure

    private fun isOctoPrintStarting(result: ConnectionResult?) =
        (result as? ConnectionResult.Failure)?.exception is PrintBootingException

    private fun isNoPrinterAvailable(connectionResponse: ConnectionState) =
        connectionResponse.options.ports.isEmpty()

    private fun isPrinterOffline(
        connectionResponse: ConnectionState,
        psuState: PsuCyclingState
    ) = connectionResponse.options.ports.isNotEmpty() &&
            (isInErrorState(connectionResponse) || isConnectionAttemptTimedOut(connectionResponse)) &&
            psuState != PsuCyclingState.Cycled

    private fun isConnectionAttemptTimedOut(connectionResponse: ConnectionState) = isPrinterConnecting(connectionResponse) &&
            System.nanoTime() - lastConnectionAttempt > connectionTimeoutMs

    private fun isInErrorState(connectionResponse: ConnectionState) = listOf(
        Connection.State.MAYBE_UNKNOWN_ERROR,
        Connection.State.MAYBE_CONNECTION_ERROR
    ).contains(connectionResponse.current.state)

    private fun isPrinterConnecting(connectionResponse: ConnectionState) = listOf(
        Connection.State.MAYBE_CONNECTING,
        Connection.State.MAYBE_DETECTING_SERIAL_PORT,
        Connection.State.MAYBE_DETECTING_SERIAL_CONNECTION,
        Connection.State.MAYBE_DETECTING_BAUDRATE
    ).contains(connectionResponse.current.state)


    // =============
    // Auto connect
    // =============

    private fun autoConnect(connectionResponse: ConnectionState, instanceId: String) {
        if (connectionResponse.options.ports.isNotEmpty() && !didJustAttemptToConnect() && !isPrinterConnecting(connectionResponse)) {
            recordConnectionAttempt()
            Timber.i("Attempting auto connect")
            requireNotNull(scope) { "Coroutine scope not ready!" }.launch(Dispatchers.IO) {
                autoConnectPrinterUseCase.execute(
                    if (connectionResponse.current.state == Connection.State.MAYBE_ERROR_FAILED_TO_AUTODETECT_SERIAL_PORT) {
                        // TODO Ask user which port to select
                        val app = BaseInjector.get().app()
                        Toast.makeText(app, app.getString(R.string.connect_printer___auto_selection_failed), Toast.LENGTH_SHORT).show()
                        OctoAnalytics.logEvent(OctoAnalytics.Event.PrinterAutoConnectFailed)
                        AutoConnectPrinterUseCase.Params(instanceId = instanceId, port = connectionResponse.options.ports.first())
                    } else {
                        AutoConnectPrinterUseCase.Params(instanceId = instanceId)
                    }
                )

                psuCycledStateFlow.emit(PsuCyclingState.NotCycled)
            }
        }
    }

    private fun didJustAttemptToConnect() =
        (System.nanoTime() - lastConnectionAttempt) < TimeUnit.SECONDS.toNanos(10)

    private fun recordConnectionAttempt() {
        lastConnectionAttempt = System.nanoTime()
    }

    private fun isPrinterConnected(connectionResponse: ConnectionState) = connectionResponse.current.port != null ||
            connectionResponse.current.baudrate != null

    private fun resetConnectionAttempt() {
        lastConnectionAttempt = 0
    }

    // ======
    // Triggers
    // ======

    private suspend fun getDefaultPowerDevice(): PowerDevice? {
        val id = octoPrintRepository.get(instanceIdFlow.value)?.appSettings?.defaultPowerDevices?.get(AppSettings.DEFAULT_POWER_DEVICE_PSU)
        val params = GetPowerDevicesUseCase.Params(instanceId = instanceIdFlow.value, queryState = false, onlyGetDeviceWithUniqueId = id)
        return getPowerDevicesUseCase.execute(params).firstOrNull()?.first
    }

    suspend fun cyclePsu() {
        getDefaultPowerDevice()?.turnOn()
        manualPsuStateFlow.emit(true)
        psuCycledStateFlow.emit(PsuCyclingState.Cycled)
        resetConnectionAttempt()
    }

    suspend fun turnPsuOff() {
        getDefaultPowerDevice()?.turnOff()
        manualPsuStateFlow.emit(false)
        resetConnectionAttempt()
    }

    suspend fun turnPsuOn() {
        getDefaultPowerDevice()?.turnOn()
        manualPsuStateFlow.emit(true)
        resetConnectionAttempt()
    }

    suspend fun retryConnectionFromOfflineState() {
        lastConnectionAttempt = 0L
        psuCycledStateFlow.emit(PsuCyclingState.Cycled)
        userAllowedConnectionAtFlow.emit(System.currentTimeMillis())
    }

    suspend fun beginConnect() {
        Timber.i("Connection initiated")
        userAllowedConnectionAtFlow.emit(System.currentTimeMillis())
    }

    // ======
    // Models
    // ======

    private sealed class ConnectionResult {
        data class Failure(val exception: Throwable) : ConnectionResult()
        data class Success(val response: ConnectionState) : ConnectionResult()
    }

    private sealed class PsuCyclingState {
        object NotCycled : PsuCyclingState()
        object Cycled : PsuCyclingState()
        object Cycling : PsuCyclingState()
    }

    data class Params(
        val instanceId: String,
    )

    sealed class UiState {

        object Initializing : UiState()

        object OctoPrintStarting : UiState()
        object OctoPrintNotAvailable : UiState()

        data class WaitingForPrinterToComeOnline(val psuIsOn: Boolean?) : UiState()
        object WaitingForUser : UiState()
        object PrinterConnecting : UiState()
        data class PrinterOffline(val psuSupported: Boolean) : UiState()
        object PrinterPsuCycling : UiState()
        object PrinterConnected : UiState()
    }
}