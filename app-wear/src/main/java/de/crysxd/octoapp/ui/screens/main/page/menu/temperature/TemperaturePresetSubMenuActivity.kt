package de.crysxd.octoapp.ui.screens.main.page.menu.temperature

import android.app.Activity
import android.os.Bundle
import android.os.Parcelable
import androidx.activity.compose.setContent
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.LocalFireDepartment
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.res.stringResource
import androidx.core.os.bundleOf
import androidx.wear.compose.material.ScalingLazyListScope
import androidx.wear.compose.material.rememberScalingLazyListState
import de.crysxd.octoapp.OctoPrintInstanceActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.BaseChangeTemperaturesUseCase
import de.crysxd.octoapp.ext.showSuccess
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuHeader
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import kotlinx.parcelize.Parcelize

class TemperaturePresetSubMenuActivity : OctoPrintInstanceActivity() {

    private val args get() = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))
    override val instanceId get() = args.instanceId

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val args = requireNotNull(Args.fromBundle(requireNotNull(intent.extras)))

        setContent {
            val instance by BaseInjector.get().octorPrintRepository()
                .instanceInformationFlow(args.instanceId).collectAsState(initial = null)

            OctoAppTheme.ForOctoPrint(instance = instance) {
                OctoAppTheme.ForMenuStyle(menuStyle = OctoAppTheme.MenuStyle.Printer) {
                    MainPageScaffold {
                        OctoScalingLazyColumn(state = rememberScalingLazyListState()) {
                            content()
                        }
                    }
                }
            }
        }
    }

    private suspend fun applyTemps(temps: List<BaseChangeTemperaturesUseCase.Temperature>) {
        BaseInjector.get().setTargetTemperatureUseCase().execute(
            BaseChangeTemperaturesUseCase.Params(
                temps = temps,
                instanceId = args.instanceId
            )
        )
        showSuccess()
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun ScalingLazyListScope.content() {
        item {
            MenuHeader(text = args.presetName)
        }

        if (args.temperatures.any { it.isTool }) {
            item {
                MenuItemChip(
                    icon = Icons.Rounded.LocalFireDepartment,
                    text = stringResource(id = R.string.temperature_menu___item_preheat_hotend, args.presetName),
                    onClick = {
                        applyTemps(args.temperatures.filter { it.isTool })
                    }
                )
            }
        }

        if (args.temperatures.any { it.isBed }) {
            item {
                MenuItemChip(
                    icon = Icons.Rounded.LocalFireDepartment,
                    text = stringResource(id = R.string.temperature_menu___item_preheat_bed, args.presetName),
                    onClick = {
                        applyTemps(args.temperatures.filter { it.isBed })
                    }
                )
            }
        }

        if (args.temperatures.any { it.isChamber }) {
            item {
                MenuItemChip(
                    icon = Icons.Rounded.LocalFireDepartment,
                    text = stringResource(id = R.string.temperature_menu___item_preheat_chamber, args.presetName),
                    onClick = {
                        applyTemps(args.temperatures.filter { it.isChamber })
                    }
                )
            }
        }
    }

    private val BaseChangeTemperaturesUseCase.Temperature.isTool get() = component.startsWith("tool")
    private val BaseChangeTemperaturesUseCase.Temperature.isBed get() = component.startsWith("bed")
    private val BaseChangeTemperaturesUseCase.Temperature.isChamber get() = component.startsWith("chamber")

    @Parcelize
    data class Args(
        val presetName: String,
        val instanceId: String,
        val temperatures: List<BaseChangeTemperaturesUseCase.Temperature>,
    ) : Parcelable {
        companion object {
            fun fromBundle(bundle: Bundle) = bundle.getParcelable<Args>("args")
        }

        fun toBundle() = bundleOf(
            "args" to this
        )
    }
}