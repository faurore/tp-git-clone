package de.crysxd.octoapp.ui.screens.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.fragment.app.Fragment
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.R
import de.crysxd.octoapp.ext.ComposeView
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.utils.Constants.PREVIEW_SIZE_DP

class InitialFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ComposeView {
        InitialFragmentContent()
    }

    @Composable
    fun InitialFragmentContent() = OctoAppTheme.WithTheme {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier.fillMaxSize()
        ) {
            Image(
                painter = painterResource(id = R.drawable.octo_base),
                contentDescription = null,
                modifier = Modifier.size(dimensionResource(id = R.dimen.launcher_icon_size))
            )
        }
        Box(
            contentAlignment = Alignment.BottomCenter,
            modifier = Modifier
                .fillMaxSize()
                .padding(OctoAppTheme.dimens.margin1)
        ) {
            Text(
                text = stringResource(id = R.string.loading),
                style = OctoAppTheme.typography.label,
            )
        }
    }

    @Composable
    @Preview(
        name = "Preview",
        widthDp = PREVIEW_SIZE_DP,
        heightDp = PREVIEW_SIZE_DP,
        showBackground = true,
        backgroundColor = 0xFF000000
    )
    fun Preview() {
        InitialFragmentContent()
    }
}