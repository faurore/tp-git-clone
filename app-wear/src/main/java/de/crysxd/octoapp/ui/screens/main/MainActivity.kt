package de.crysxd.octoapp.ui.screens.main

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Check
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.core.content.edit
import androidx.wear.ambient.AmbientModeSupport
import androidx.wear.compose.material.Text
import androidx.wear.compose.material.rememberScalingLazyListState
import de.crysxd.octoapp.BuildConfig
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_WEAR_OS_APP
import de.crysxd.octoapp.di.WearInjector
import de.crysxd.octoapp.ui.OctoAppTheme
import de.crysxd.octoapp.ui.framework.AmbientModeSupport
import de.crysxd.octoapp.ui.framework.MainPageScaffold
import de.crysxd.octoapp.ui.framework.MenuItemChip
import de.crysxd.octoapp.ui.framework.OctoScalingLazyColumn
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onEach
import timber.log.Timber
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), AmbientModeSupport.AmbientCallbackProvider {

    private lateinit var ambientController: AmbientModeSupport.AmbientController
    private val ambientModeStartTime = mutableStateOf(0L)
    private val keyAvailable = "available"
    private val keyBetaDisclaimerShown = "betaDisclaimer"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ambientController = AmbientModeSupport.attach(this)
        ambientController.setAmbientOffloadEnabled(false)
        window.setBackgroundDrawable(ColorDrawable(Color.BLACK))

        val cachedAvailability = getSharedPreferences().getBoolean(keyAvailable, false)
        val betaDisclaimerShown = mutableStateOf(getSharedPreferences().getBoolean(keyBetaDisclaimerShown, BuildConfig.BENCHMARK))
        val featureAvailableFlow = BillingManager.billingFlow()
            .filter { it.isInitialized }
            .map { BillingManager.isFeatureEnabled(FEATURE_WEAR_OS_APP) }
            .distinctUntilChanged()
            .onEach { getSharedPreferences().edit { putBoolean(keyAvailable, it) } }
        val billingAvailableFlow = BillingManager.billingFlow()
            .map { it.isBillingAvailable }
            .distinctUntilChanged()

        setContent {
            //region Main UI
            val featureAvailable by featureAvailableFlow.collectAsState(initial = cachedAvailability)
            val state = when {
                !betaDisclaimerShown.value -> State.BetaDisclaimer
                !featureAvailable -> State.Disabled
                else -> State.Enabled
            }


            Crossfade(targetState = state) {
                when (it) {
                    State.BetaDisclaimer -> {
                        Timber.i("Showing beta disclaimer")
                        BetaDisclaimerUi(betaDisclaimerShown)
                    }

                    State.Disabled -> {
                        Timber.i("BillingManager reports app disabled, moving to disabled state")
                        val billingAvailable by billingAvailableFlow.collectAsState(initial = false)
                        WearAppDisabledUi { billingAvailable }
                    }

                    State.Enabled -> {
                        Timber.i("BillingManager reports app enabled")
                        AmbientModeSupport(ambientStartTime = ambientModeStartTime.value) {
                            MainUi(ambientModeStartTime.value > 0)
                        }
                    }

                }
            }
            //endregion
        }
    }

    //region Beta Disclaimer
    @Composable
    private fun BetaDisclaimerUi(betaDisclaimerShown: MutableState<Boolean>) = OctoAppTheme.WithTheme {
        MainPageScaffold {
            OctoScalingLazyColumn(state = rememberScalingLazyListState()) {
                item(key = "icon") {
                    Image(
                        painter = painterResource(R.drawable.octo_base),
                        contentDescription = null,
                        contentScale = ContentScale.FillHeight,
                        modifier = Modifier
                            .padding(top = 50.dp)
                            .height(72.dp)
                            .fillMaxWidth(),
                        alignment = Alignment.Center,
                    )
                }
                item(key = "title") {
                    Text(
                        text = "WearOS Beta",
                        textAlign = TextAlign.Center,
                        style = OctoAppTheme.typography.title,
                        modifier = Modifier
                            .padding(bottom = OctoAppTheme.dimens.margin1)
                            .fillMaxSize()
                    )
                }
                item(key = "detail") {
                    Text(
                        text = "The WearOS app is still in beta. Please report any bugs or other issues via the app's bug report feature and be kind!",
                        textAlign = TextAlign.Center,
                        style = OctoAppTheme.typography.base,
                        modifier = Modifier
                            .padding(bottom = OctoAppTheme.dimens.margin2)
                            .fillMaxSize()
                    )
                }
                item(key = "action") {
                    OctoAppTheme.ForMenuStyle(OctoAppTheme.MenuStyle.Settings) {
                        MenuItemChip(
                            text = "Understood",
                            icon = Icons.Rounded.Check,
                        ) {
                            getSharedPreferences().edit { putBoolean(keyBetaDisclaimerShown, true) }
                            betaDisclaimerShown.value = true
                        }
                    }
                }
            }
        }
    }
    //endregion

    private fun getSharedPreferences() = getSharedPreferences("billing-cache", MODE_PRIVATE)

    override fun onResume() {
        super.onResume()
        BillingManager.onResume()
        WearInjector.get().wearDataLayerService().pullConfiguration()
    }

    override fun onPause() {
        super.onPause()
        BillingManager.onPause()
    }

    //region Ambient
    override fun getAmbientCallback(): AmbientModeSupport.AmbientCallback = MyAmbientCallback()

    private inner class MyAmbientCallback : AmbientModeSupport.AmbientCallback() {

        private val minAmbientInterval = TimeUnit.MINUTES.toMillis(5)
        private var lastAmbientUpdate = 0L

        init {
            Timber.i("Ambient prepared!")
        }

        override fun onEnterAmbient(ambientDetails: Bundle) {
            Timber.i("Enter ambient: ${ambientDetails.keySet().associateWith { ambientDetails.get(it) }}")
            setAmbientMode(true)
            lastAmbientUpdate = 0L
        }

        override fun onExitAmbient() {
            Timber.i("Exit ambient")
            setAmbientMode(false)
            lastAmbientUpdate = 0L
        }

        override fun onUpdateAmbient() {
            if (System.currentTimeMillis() - lastAmbientUpdate > minAmbientInterval) {
                Timber.i("Updated ambient")
                setAmbientMode(true)
                lastAmbientUpdate = System.currentTimeMillis()
            } else {
                Timber.i("Skipping ambient update, too soon")
            }
        }

        fun setAmbientMode(ambientMode: Boolean) {
            ambientModeStartTime.value = if (ambientMode) System.currentTimeMillis() else 0L
        }
    }
    //endregion

    private sealed class State {
        object Disabled : State()
        object Enabled : State()
        object BetaDisclaimer : State()
    }
}