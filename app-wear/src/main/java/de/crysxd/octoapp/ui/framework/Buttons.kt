package de.crysxd.octoapp.ui.framework

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.wear.compose.material.Button
import androidx.wear.compose.material.ButtonDefaults
import androidx.wear.compose.material.ChipDefaults
import androidx.wear.compose.material.CompactChip
import androidx.wear.compose.material.Icon
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.R
import de.crysxd.octoapp.ext.showFailure
import de.crysxd.octoapp.ui.OctoAppTheme
import kotlinx.coroutines.launch
import timber.log.Timber

@Composable
fun PrimaryButtonChip(
    text: String,
    onClick: suspend () -> Unit,
) {
    var buttonEnabled by remember { mutableStateOf(true) }
    val context = LocalContext.current
    val scope = rememberCoroutineScope()

    CompactChip(
        enabled = buttonEnabled,
        onClick = {
            scope.launch {
                buttonEnabled = false
                try {
                    onClick()
                } catch (e: Exception) {
                    Timber.e(e)
                    context.showFailure(e)
                }
                buttonEnabled = true
            }
        },
        label = {
            Text(
                text = text,
                maxLines = 1,
                textAlign = TextAlign.Center,
                overflow = TextOverflow.Ellipsis,
                style = OctoAppTheme.typography.button,
                modifier = Modifier.fillMaxWidth()
            )
        },
        colors = ChipDefaults.chipColors(
            backgroundColor = colorResource(id = R.color.primary_button_background),
            iconColor = Color.Transparent,
            contentColor = MaterialTheme.colors.onSecondary,
        ),
    )
}


@Composable
fun SecondaryButtonChip(
    text: String,
    onClick: suspend () -> Unit,
) {
    var buttonEnabled by remember { mutableStateOf(true) }
    val context = LocalContext.current
    val scope = rememberCoroutineScope()

    CompactChip(
        enabled = buttonEnabled,
        modifier = Modifier.border(width = 1.dp, shape = MaterialTheme.shapes.small, color = colorResource(id = R.color.primary_button_background)),
        onClick = {
            scope.launch {
                buttonEnabled = false
                try {
                    onClick()
                } catch (e: Exception) {
                    Timber.e(e)
                    context.showFailure(e)
                }
                buttonEnabled = true
            }
        },
        label = {
            Text(
                text = text,
                maxLines = 1,
                textAlign = TextAlign.Center,
                overflow = TextOverflow.Ellipsis,
                style = OctoAppTheme.typography.button,
                modifier = Modifier.fillMaxWidth()
            )
        },
        colors = ChipDefaults.chipColors(
            backgroundColor = colorResource(id = R.color.input_background),
            iconColor = Color.Transparent,
            contentColor = MaterialTheme.colors.secondary,
        ),
    )
}

@Composable
fun PrimaryIconButton(
    icon: ImageVector,
    color: Color = MaterialTheme.colors.secondary,
    onClick: suspend () -> Unit,
) {
    var buttonEnabled by remember { mutableStateOf(true) }
    val context = LocalContext.current
    val scope = rememberCoroutineScope()

    Button(
        enabled = buttonEnabled,
        modifier = Modifier.size(40.dp),
        onClick = {
            scope.launch {
                buttonEnabled = false
                try {
                    onClick()
                } catch (e: Exception) {
                    Timber.e(e)
                    context.showFailure(e)
                }
                buttonEnabled = true
            }
        },
        content = {
            Icon(imageVector = icon, contentDescription = null)
        },
        colors = ButtonDefaults.buttonColors(
            backgroundColor = color,
            contentColor = MaterialTheme.colors.onSecondary
        )
    )
}