package de.crysxd.octoapp.ui.framework

import androidx.annotation.StringRes
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.wear.compose.material.MaterialTheme
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.ui.OctoAppTheme

@Composable
fun TitleDetails(@StringRes title: Int? = null, @StringRes detail: Int? = null) {
    title?.let {
        Text(
            text = stringResource(id = it),
            textAlign = TextAlign.Center,
            style = OctoAppTheme.typography.title,
            modifier = Modifier.fillMaxWidth()
        )
    }

    detail?.let {
        Text(
            text = stringResource(id = it),
            textAlign = TextAlign.Center,
            style = OctoAppTheme.typography.labelSmall,
            color = MaterialTheme.colors.onBackground.copy(alpha = 0.7f),
            modifier = Modifier
                .padding(top = OctoAppTheme.dimens.margin0to1)
                .fillMaxWidth(),
        )
    }
}