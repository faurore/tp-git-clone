package de.crysxd.octoapp.ui.screens.main

import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.wear.compose.material.Text
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.FormatEtaUseCase
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.ui.OctoAppTheme
import timber.log.Timber

@Composable
fun PrintHeaderContent(currentMessageSource: () -> Message.Current?) {
    val current = currentMessageSource()
    val flags = current?.state?.flags
    val percent = current?.progress?.completion?.let { LocalContext.current.getString(R.string.x_percent, it) }
    Timber.i("current: $current")

    if (percent != null) {
        Text(
            text = percent,
            style = OctoAppTheme.typography.titleLarge,
        )
    }

    val text = when {
        flags?.paused == true -> stringResource(id = R.string.paused)
        flags?.pausing == true -> stringResource(id = R.string.pausing)
        flags?.cancelling == true -> stringResource(id = R.string.cancelling)
        percent == null -> stringResource(id = R.string.loading)
        else -> current.progress?.printTimeLeft?.takeIf { it > 0 }?.let {
            BaseInjector.get().formatEtaUseCase().executeBlocking(
                FormatEtaUseCase.Params(secsLeft = it.toLong(), allowRelative = true, useCompactDate = false)
            )
        } ?: "--"
    }

    Text(
        text = text,
        style = OctoAppTheme.typography.label,
    )
}