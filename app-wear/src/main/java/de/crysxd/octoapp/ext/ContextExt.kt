package de.crysxd.octoapp.ext

import android.content.Context
import android.content.Intent
import androidx.wear.activity.ConfirmationActivity
import androidx.wear.activity.ConfirmationActivity.EXTRA_ANIMATION_DURATION_MILLIS
import androidx.wear.activity.ConfirmationActivity.EXTRA_ANIMATION_TYPE
import androidx.wear.activity.ConfirmationActivity.EXTRA_MESSAGE
import androidx.wear.activity.ConfirmationActivity.FAILURE_ANIMATION
import androidx.wear.activity.ConfirmationActivity.OPEN_ON_PHONE_ANIMATION
import androidx.wear.activity.ConfirmationActivity.SUCCESS_ANIMATION
import androidx.wear.widget.ConfirmationOverlay.DEFAULT_ANIMATION_DURATION_MS
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException

fun Context.showSuccess(
    message: String? = null
) = Intent(this, ConfirmationActivity::class.java)
    .putExtra(EXTRA_ANIMATION_TYPE, SUCCESS_ANIMATION)
    .putExtra(EXTRA_MESSAGE, message)
    .also { startActivity(it) }

fun Context.showFailure(
    error: Throwable
) = showFailure(
    (error as? UserMessageException)?.userMessage as? String
)

fun Context.showFailure(
    message: String? = null
) = Intent(this, ConfirmationActivity::class.java)
    .putExtra(EXTRA_ANIMATION_TYPE, FAILURE_ANIMATION)
    .putExtra(EXTRA_MESSAGE, message)
    .also { startActivity(it) }

fun Context.showOpenPhoneSuccess(
    message: String? = null,
    animationDuration: Int = DEFAULT_ANIMATION_DURATION_MS
) = Intent(this, ConfirmationActivity::class.java)
    .putExtra(EXTRA_ANIMATION_TYPE, OPEN_ON_PHONE_ANIMATION)
    .putExtra(EXTRA_MESSAGE, message)
    .putExtra(EXTRA_ANIMATION_DURATION_MILLIS, animationDuration)
    .also { startActivity(it) }

