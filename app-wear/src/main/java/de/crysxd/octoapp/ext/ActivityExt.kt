package de.crysxd.octoapp.ext

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity

fun Activity.setScreenStaysOn(on: Boolean) {
    if (on) {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    } else {
        window.clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }
}

fun Context.requireActivity(): AppCompatActivity = when (this) {
    is AppCompatActivity -> this
    is ContextWrapper -> baseContext.requireActivity()
    else -> throw java.lang.IllegalStateException("Unknown Context class $this")
}

fun Context.requireActivityOrNull(): AppCompatActivity? = when (this) {
    is AppCompatActivity -> this
    is ContextWrapper -> baseContext.requireActivity()
    else -> null
}