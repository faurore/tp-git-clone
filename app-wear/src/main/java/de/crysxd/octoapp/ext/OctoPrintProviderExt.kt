package de.crysxd.octoapp.ext

import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.exceptions.MissingPluginException
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.TriggerInitialCancelObjectMessageUseCase
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.engine.EventSource
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.dto.message.CancelObjectPluginMessage
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.atomic.AtomicLong

private val eventFlows = mutableMapOf<String, Flow<Event>>()
private val cancelObjectFlows = mutableMapOf<String, Flow<List<CancelObjectPluginMessage.Object>>>()
private val interpolationFlows = mutableMapOf<String, MutableStateFlow<Message.Current?>>()
private const val MIN_CAPABILITY_INTERVAL = 1000 * 60 * 5L
private const val SHARE_TIMEOUT = 5_000L

fun OctoPrintProvider.wearEventFlow(
    tag: String,
    instanceId: String,
) = eventFlows.getOrPut(instanceId) {
    val lastConnect = AtomicLong(0)
    eventFlow(
        tag = tag,
        instanceId = instanceId,
        config = EventSource.Config(
            throttle = 10,
            requestTerminalLogs = emptyList(),
            requestPlugins = true,
        )
    ).onEach {
        handleEvent(it, instanceId, lastConnect)
    }.onStart {
        Timber.d("Starting shared wear event flow on $instanceId")
    }.onCompletion {
        Timber.d("Stopping shared wear event flow on $instanceId")
    }.shareIn(
        scope = AppScope,
        started = SharingStarted.WhileSubscribed(SHARE_TIMEOUT),
        replay = 1
    )
}

private fun interpolationFlow(instanceId: String) = interpolationFlows.getOrPut(instanceId) { MutableStateFlow(null) }

fun injectCurrentMessage(instanceId: String, currentMessage: Message.Current) {
    Timber.i("Setting interpolation!!")
    interpolationFlow(instanceId).value = currentMessage
}

fun OctoPrintProvider.wearPassiveCurrentMessageFlow(
    tag: String,
    instanceId: String,
) = flow {
    emit(null)
    emitAll(passiveCurrentMessageFlow(tag = tag, instanceId = instanceId))
}.combine(interpolationFlow(instanceId = instanceId)) { actual, interpolation ->
    when {
        interpolation?.serverTime == null || actual?.serverTime == null -> {
            actual ?: interpolation
        }
        interpolation.serverTime > actual.serverTime -> interpolation.also {
            Timber.i("Interpolation for current message is being used!")
        }
        else -> actual
    }
}.filterNotNull()

fun OctoPrintProvider.wearCancelObjectFlow(
    tag: String,
    instanceId: String,
) = flow {
    var cache = CancelObjectPluginMessage()
    val flow = cancelObjectFlows.getOrPut(instanceId) {
        passiveCachedMessageFlow(tag = tag, instanceId = instanceId, clazz = CancelObjectPluginMessage::class).map { message ->
            // If messages contains a non-null empty object list we need to reset the cache. This message indicates state reset
            if (message?.objects?.isEmpty() == true) {
                Timber.i("Empty object list, resetting")
                cache = cache.copy(activeId = null)
            }

            // Merge message with previous ones. We always get one field only, so we need to merge
            // Also map the object to ensure consistent active flag
            val activeId = message?.activeId ?: cache.activeId

            cache = cache.copy(
                activeId = activeId,
                objects = (message?.objects ?: cache.objects)?.map { it.copy(active = it.id == activeId) }?.filter { it.id != null && it.ignore != true },
            )


            cache.objects ?: emptyList()
        }.onStart {
            emit(emptyList())
            Timber.d("Starting shared wear cancel object flow on $instanceId")
            try {
                BaseInjector.get().triggerInitialCancelObjectMessageUseCase().execute(TriggerInitialCancelObjectMessageUseCase.Params(instanceId))
                Timber.d("Requested initial cancel object message for $instanceId")
            } catch (e: MissingPluginException) {
                Timber.d("Cancel object plugin not installed on $instanceId")
            } catch (e: Exception) {
                Timber.e(e, "Failed to request initial cancel object message for $instanceId")
            }
        }.onCompletion {
            Timber.d("Stopping shared wear cancel object flow on $instanceId")
        }.shareIn(
            scope = AppScope,
            started = SharingStarted.WhileSubscribed(stopTimeoutMillis = SHARE_TIMEOUT),
            replay = 1
        )
    }

    emitAll(flow)
}

private fun handleEvent(event: Event, instanceId: String, lastConnect: AtomicLong) = AppScope.launch {
    try {
        when ((event as? Event.MessageReceived)?.message) {
            // Something changed, refresh capabilities
            is Message.Event.SettingsUpdated -> refreshCapabilities(instanceId, false)

            // Refresh capabilities when connected after a while
            is Message.Connected -> {
                Timber.d("Handling connected on $instanceId")

                val timeSinceLast = (System.currentTimeMillis() - lastConnect.get())
                lastConnect.set(System.currentTimeMillis())
                if (timeSinceLast > MIN_CAPABILITY_INTERVAL) {
                    refreshCapabilities(instanceId, false)
                } else {
                    Timber.d("Skipping capability refresh on connect")
                }
            }

            // Default
            else -> Unit
        }
    } catch (e: Exception) {
        Timber.e(e, "Failed to handle event: $event")
    }
}

private suspend fun refreshCapabilities(instanceId: String, loadSettings: Boolean) {
    Timber.i("Refreshing instance capabilities: $instanceId")
    val params = UpdateInstanceCapabilitiesUseCase.Params(
        instanceId = instanceId,
        updateCommands = false,
        updatePlugins = false,
        updateProfile = false,
        updateSettings = loadSettings,
        updateSystemInfo = false,
        updateVersion = false,
    )
    BaseInjector.get().updateInstanceCapabilitiesUseCase().execute(params)
}
