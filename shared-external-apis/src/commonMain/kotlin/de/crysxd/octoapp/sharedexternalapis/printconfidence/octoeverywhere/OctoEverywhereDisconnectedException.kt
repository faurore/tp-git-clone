package de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere

internal class OctoEverywhereDisconnectedException : Exception()