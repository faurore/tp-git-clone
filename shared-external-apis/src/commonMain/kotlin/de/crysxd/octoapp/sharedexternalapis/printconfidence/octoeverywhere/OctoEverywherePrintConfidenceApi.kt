package de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere

import de.crysxd.octoapp.sharedexternalapis.http.GenericHttpClientFactory
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidence
import de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere.dto.OctoEverywherePrintConfidenceResponse
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.ResponseException
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.request.url

internal class OctoEverywherePrintConfidenceApi(
    private val appToken: String,
    private val httpClient: HttpClient = GenericHttpClientFactory()
) {

    private val origin = PrintConfidence.Origin.OctoEverywhere

    suspend fun checkConfidence() = try {
        httpClient.post {
            url("https://octoeverywhere.com/api/gadget/GetStatusFromAppConnection")
            header("AppToken", appToken)
            setBody("{}")
            header("Content-Type", "application/json")
        }.body<OctoEverywherePrintConfidenceResponse>().result.let {
            PrintConfidence(
                description = when (it.state) {
                    0, 1 -> throw return@let null
                    else -> it.status ?: "Unknown"
                },
                confidence = it.rating?.let { r -> r / 10f },
                origin = origin,
                level = when {
                    // Rely on color
                    it.statusColor == "g" -> PrintConfidence.Level.High
                    it.statusColor == "y" -> PrintConfidence.Level.Medium
                    it.statusColor == "r" -> PrintConfidence.Level.Low

                    // Fallback with confidence
                    (it.rating ?: 0) > 7 -> PrintConfidence.Level.High
                    (it.rating ?: 0) > 4 -> PrintConfidence.Level.Medium
                    else -> PrintConfidence.Level.High
                }
            )
        }
    } catch (e: ResponseException) {
        when (e.response.status.value) {
            // Temporary Error - PrinterOffline
            // Temporary Error - PrinterConnectionTimeOut
            601,
            602 -> PrintConfidence(description = "Can't see your print", confidence = null, origin = origin, level = PrintConfidence.Level.Low)
            // App Connection Not Found
            // App Connection Expired or Revoked
            603,
            604 -> throw OctoEverywhereDisconnectedException()
            // PluginUpdateRequired
            610 -> PrintConfidence(description = "OctoEverywhere plugin outdated", confidence = null, origin = origin, level = PrintConfidence.Level.Low)
            // Not part of Gadget beta
            611 -> throw OctoEverywhereDisconnectedException()
            else -> null
        }
    }
}