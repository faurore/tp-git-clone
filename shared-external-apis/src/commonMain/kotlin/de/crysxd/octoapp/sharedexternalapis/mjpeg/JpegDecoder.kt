package de.crysxd.octoapp.sharedexternalapis.mjpeg

expect class JpegDecoder(
    usePool: Boolean,
    logTag: String,
    maxImageSize: Int?,
) {

    suspend fun decode(byteArray: ByteArray, length: Int): Image
    fun reset()

}