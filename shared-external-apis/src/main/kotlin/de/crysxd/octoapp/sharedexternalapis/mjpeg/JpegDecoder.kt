package de.crysxd.octoapp.sharedexternalapis.mjpeg

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

actual class JpegDecoder actual constructor(
    private val usePool: Boolean,
    private val logTag: String,
    private val maxImageSize: Int?,
) {
    private var bitmaps = emptyList<Bitmap>()
    private var sampleSize = 1
    private var lastBitmapUsed = 0

    actual fun reset() {
        bitmaps = emptyList()
        sampleSize = 1
    }

    actual suspend fun decode(byteArray: ByteArray, length: Int): Image {
        if (!usePool) {
            return requireNotNull(decodeImage(byteArray, length, null)) { "Decoded bitmap was null!" }
        }

        val ops = BitmapFactory.Options()
        ops.inBitmap = getNextBitmap(byteArray, length)
        ops.inSampleSize = sampleSize
        return requireNotNull(decodeImage(byteArray, length, ops)) { "Decoded bitmap was null!" }
    }

    private suspend fun decodeImage(byteArray: ByteArray, length: Int, options: BitmapFactory.Options?): Bitmap? = withContext(Dispatchers.IO) {
        val b = BitmapFactory.decodeByteArray(byteArray, 0, length, options)
        b
    }

    private suspend fun getNextBitmap(byteArray: ByteArray, length: Int): Bitmap {
        if (bitmaps.isEmpty()) {
            // Decode image bounds
            Napier.i(tag = logTag, message = "Creating bitmap pool")
            val ops = BitmapFactory.Options()
            ops.inJustDecodeBounds = true
            decodeImage(byteArray, length, ops)

            // Determine settings
            var width = ops.outWidth
            var height = ops.outHeight
            if (maxImageSize != null) {
                while (width > maxImageSize || height > maxImageSize) {
                    sampleSize += 1
                    width = ops.outWidth / sampleSize
                    height = ops.outHeight / sampleSize
                }
            }

            if (width != ops.outWidth || height != ops.outHeight) {
                Napier.i(tag = logTag, message = "Native resolution of ${ops.outWidth}x${ops.outHeight}px exceeds maximum edge length of $maxImageSize")
                Napier.i(tag = logTag, message = "Using sampleSize=$sampleSize, resulting in ${width}x${height}px")
            } else {
                Napier.i(tag = logTag, message = "Resolution is ${ops.outWidth}x${ops.outHeight}px")
            }

            // Create pool
            val bitmapPoolSize = if (usePool) 3 else 1
            bitmaps = (0 until bitmapPoolSize).map {
                Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            }
        }

        lastBitmapUsed = (lastBitmapUsed + 1) % bitmaps.size
        return bitmaps[lastBitmapUsed]
    }
}