package de.crysxd.octoapp.sharedexternalapis.mjpeg

import io.github.aakira.napier.Napier


internal class MjpegImageCache(maxImageSize: Int?, private val logTag: String) {
    private val maxSize = 1024 * 1024 * 10L
    private val array = ByteArrayOutputStream2()
    private val buffer = ByteArrayOutputStream2()
    private val decoder = JpegDecoder(usePool = true, logTag = logTag, maxImageSize = maxImageSize)

    fun reset() {
        array.reset()
    }

    fun push(bytes: ByteArray, length: Int) {
        require((array.size() + length) < maxSize) { "Byte cached overflow: $maxSize bytes" }
        array.write(bytes, 0, length)
    }

    suspend fun readImage(length: Int, boundaryLength: Int): Image? {
        val bitmap = if (length > 10) {
            try {
                decoder.decode(array.toByteArray(), length)
            } catch (e: Exception) {
                Napier.w(tag = logTag, message = "Failed to decode frame: $e")
                null
            }
        } else {
            null
        }
        dropUntil(boundaryLength)
        return bitmap
    }

    private fun dropUntil(until: Int) {
        val length = array.size() - until
        if (length >= 0) {
            buffer.reset()
            buffer.write(array.toByteArray(), until, array.size() - until)
            array.reset()
            array.write(buffer.toByteArray(), 0, buffer.size())
        } else {
            // This shouldn't happen, length is negative. This indicates an issue in index search...resetting the array
            // will cause next frame to fail to decode but after that we should be back on track
            array.reset()
        }
    }

    fun indexOf(offset: Int, boundaryStart: String): IndexResult {
        // Search start
        var startIndex = 0
        var startFound = false
        val length = array.size()
        val array = array.toByteArray()
        for (i in offset until length) {
            if (array[i].toInt().toChar() == boundaryStart[startIndex]) {
                startIndex++
            } else {
                startIndex = 0
            }

            if (startIndex == boundaryStart.length) {
                startFound = true
                startIndex = i - boundaryStart.length + 1
                break
            }
        }

        if (!startFound) return IndexResult(nextOffset = length - boundaryStart.length)

        var endIndex = -1
        // Search end
        for (i in startIndex until length) {
            val c1 = array.getOrNull(i + 0)?.toInt()?.toChar()
            val c2 = array.getOrNull(i + 1)?.toInt()?.toChar()
            val c3 = array.getOrNull(i + 2)?.toInt()?.toChar()
            val c4 = array.getOrNull(i + 3)?.toInt()?.toChar()
            if (c1 == '\n' && c2 == '\n' && i < length - 2) {
                endIndex = i + 2
                break
            }
            if (c1 == '\r' && c2 == '\n' && c3 == '\r' && c4 == '\n' && i < length - 4) {
                endIndex = i + 4
                break
            }
        }

        return if (endIndex < 0) {
            IndexResult(nextOffset = startIndex)
        } else {
            IndexResult(start = startIndex, end = endIndex, nextOffset = startIndex)
        }
    }

    data class IndexResult(
        val start: Int? = null,
        val end: Int? = null,
        val nextOffset: Int,
    )
}