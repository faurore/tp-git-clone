import SwiftUI
import OctoAppBase
import FirebaseCore
import FirebaseRemoteConfig
import FirebaseCrashlytics

#if DEBUG
let debug = true
#else
let debug = false
#endif

class AppDelegate: NSObject, UIApplicationDelegate {
    
    func application(
        _ app: UIApplication,
        open url: URL,
        options: [UIApplication.OpenURLOptionsKey : Any] = [:]
    ) -> Bool {
        Napier.i(tag: "AppDelegate", message: "Opening URL: \(url)")
        return true
    }
    
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil
    ) -> Bool {
        
        // Firebase
        FirebaseApp.configure()
        let remoteConfig = RemoteConfig.remoteConfig()
        let settings = RemoteConfigSettings()
        settings.minimumFetchInterval = debug ? 0 : 86400
        remoteConfig.configSettings = settings
        OctoConfig.shared.link(rc: DarwinRemoteConfigImpl(remoteConfig: remoteConfig))
        remoteConfig.fetchAndActivateWithLog()
        Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(!debug)

        // Koin
        SharedCommonInjector.shared.setComponent(
            t: SharedCommonComponent(
                platformModule: PlatformModule(),
                serializationModule: SerializationModule()
            )
        )
        
        SharedBaseInjector.shared.setComponent(
            t: SharedBaseComponent(
                sharedCommonComponent: SharedCommonInjector().get(),
                sharedBaseModule: SharedBaseModule(),
                repositoryModule: RepositoryModule(),
                loggingModule: LoggingModule(),
                networkModule: NetworkModule(networkServiceDiscovery: DarwinNetworkDiscovery(), dns: DarwinDns()),
                useCaseModule: UseCaseModule()
            )
        )
        
        return true
    }
}

@main
struct iOSApp: App {
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate

    init() {
        NapierProxyKt.doInitLogging(adapter: DarwinFirebaseAdapter(), isDebug: debug)
    }
    
    var body: some Scene {
        WindowGroup {
            OrchestratorView()
                .background(OctoTheme.colors.windowBackground)
                .onOpenURL { url in
                    Task {
                        let core = WindowGroupViewModelCore()
                        if (try await core.consumeDeeplink(link: url.description)).boolValue == false {
                            Napier.i(tag: "WindowGroup", message: "Passing link to the view")
                        }
                    }
                }
        }
    }
}
