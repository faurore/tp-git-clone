//
//  File.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct FileObjectStruct: Identifiable, Equatable {
    var id: String { return path }
    
    var origin: FileOriginStruct
    var gcodeDimension: FileGcodeDimensionsStruct?
    var filamentUseMm: [FileGcodeFilamentUse]?
    var estimatedPrintTime: TimeInterval?
    var display: String
    var name: String
    var type: String
    var typePath: [String]
    var thumbnail: String?
    var date: Date?
    var hash: String?
    var prints: PrintHistoryStruct?
    var path: String
    var size: Int64?
    var childFolders: [FileObjectStruct]?
    var children: [FileObjectStruct]?
    var isFolder: Bool
}

struct FileGcodeDimensionsStruct: Equatable {
    var depth: Double
    var height: Double
    var width: Double
}

struct FileGcodeFilamentUse: Equatable {
    var name: String
    var length: Double
    var volume: Double
}

enum FileOriginStruct: Identifiable {
    var id: FileOriginStruct { return self }
    var label: String {
        switch(self) {
        case .local: return "file_manager___file_details___file_location_local"~
        case .sdCard: return "file_manager___file_details___file_location_sd_card"~
        case .unknonwn: return "file_manager___file_details___file_location_unknown"~
        }
    }
    case local, sdCard, unknonwn
}

struct PrintHistoryStruct : Equatable {
    var failure: Int32
    var success: Int32
    var lastPrintSuccess: Bool?
    var lastPrintDate: Date?
}


extension FileObject {
    func toStruct() -> FileObjectStruct {
        let file = self as? FileObject.File
        
        return FileObjectStruct(
            origin: self.origin.toStruct(),
            gcodeDimension: file?.gcodeAnalysis?.dimensions?.toStruct(),
            filamentUseMm: file?.gcodeAnalysis?.toFilmentUseStruct(),
            estimatedPrintTime: file?.gcodeAnalysis?.estimatedPrintTime?.toSecondsInterval(),
            display: self.display,
            name: self.name,
            type: self.type,
            typePath: self.typePath,
            thumbnail: file?.thumbnail,
            date: file?.date.asDateFromMillisecondsSince1970(),
            hash: file?.hash,
            prints: file?.prints?.toStruct(),
            path: self.path,
            size: self.size?.int64Value,
            childFolders: (self as? FileObject.Folder)?.children.filter{ $0 is FileObject.Folder }.map { $0.toStruct() },
            children: (self as? FileObject.Folder)?.children.filter{ $0 is FileObject.File }.map { $0.toStruct() },
            isFolder: self is FileObject.Folder
        )
    }
}

private extension KotlinFloat {
    func toSecondsInterval() -> TimeInterval {
        return TimeInterval(self.doubleValue)
    }
}

extension FileObject.PrintHistory {
    func toStruct() -> PrintHistoryStruct {
        return PrintHistoryStruct(
            failure: self.failure,
            success: self.success,
            lastPrintSuccess: self.last?.success,
            lastPrintDate: self.last?.date != nil ? Date(milliseconds: self.last!.date) : nil
        )
    }
}

extension FileOrigin {
    func toStruct() -> FileOriginStruct {
        switch(self) {
        case .local: return .local
        case .sdcard: return .sdCard
        default: return .unknonwn
        }
    }
}

extension FileObject.GcodeAnalysisDimensions {
    func toStruct() -> FileGcodeDimensionsStruct {
        return FileGcodeDimensionsStruct(
            depth: self.depth,
            height: self.height,
            width: self.width
        )
    }
}


extension FileObject.GcodeAnalysis {
    func toFilmentUseStruct() -> [FileGcodeFilamentUse] {
        return self.filament.map { x in
            FileGcodeFilamentUse(
                name: x.key,
                length: x.value.length,
                volume: x.value.volume
            )
        }
    }
}
