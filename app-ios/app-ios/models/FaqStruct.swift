//
//  Faq.swift
//  OctoApp
//
//  Created by Christian on 26/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct FaqStruct: Identifiable, Equatable {
    let id: String
    let hidden: Bool?
    let title: String?
    let content: String?
    let youtubeUrl: String?
    let youtubeThumbnailUrl: String?
}

extension Faq {
    func toStruct()-> FaqStruct {
        return FaqStruct(
            id: id ?? UUID().uuidString,
            hidden: hidden?.boolValue,
            title: title,
            content: content,
            youtubeUrl: youtubeUrl,
            youtubeThumbnailUrl: youtubeThumbnailUrl
        )
    }
}
