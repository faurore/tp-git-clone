//
//  TutorialsStruct.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct TutorialStruct: Identifiable, Equatable {
    var id: String
    let url: String
    let title: String?
    let description: String?
    let thumbnailUrl: String?
    let publishedAt: Date?
}

extension Tutorial {
    func toStruct()-> TutorialStruct {
        return TutorialStruct(
            id: url,
            url: url,
            title: title,
            description: description_,
            thumbnailUrl: thumbnails.isEmpty ? nil : (thumbnails.first { $0.width > 250 && $0.width < 350} ?? thumbnails[0])?.url,
            publishedAt: publishedAt.toDate()
        )
    }
}
