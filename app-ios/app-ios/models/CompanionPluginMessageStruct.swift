//
//  CompanionPluginMessageStruct.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct CompanionPluginMessageStruct : Equatable {
    let m117: String?
    let mmuSelectionActive: Bool?
}

extension CompanionPluginMessage {
    func toStruct() -> CompanionPluginMessageStruct {
        return CompanionPluginMessageStruct(
            m117: m117,
            mmuSelectionActive: mmuSelectionActive?.boolValue
        )
    }
}
