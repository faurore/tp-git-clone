//
//  KnownBug.swift
//  OctoApp
//
//  Created by Christian on 26/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct KnownBugStruct: Identifiable, Equatable {
    var id: String
    let title: String?
    let status: String?
    let content: String?
}

extension KnownBug {
    func toStruct()-> KnownBugStruct {
        return KnownBugStruct(
            id: id ?? UUID().uuidString,
            title: title,
            status: status,
            content: content
        )
    }
}
