//
//  ProgressWidgetSettingsStruct.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

struct ProgressWidgetSettingsStruct : Equatable {
    let showUsedTime: Bool
    let showLeftTime: Bool
    let showThumbnail: Bool
    let showPrinterMessage: Bool
    let showLayer: Bool
    let showZHeight: Bool
    let etaStyle: ProgressWidgetSettings.EtaStyle
    let printNameStyle:  ProgressWidgetSettings.PrintNameStyle
    let fontSize:  ProgressWidgetSettings.FontSize
}

extension ProgressWidgetSettings {
    func toStruct() -> ProgressWidgetSettingsStruct {
        return ProgressWidgetSettingsStruct(
            showUsedTime: showUsedTime,
            showLeftTime: showLeftTime,
            showThumbnail: showThumbnail,
            showPrinterMessage: showPrinterMessage,
            showLayer: showLayer,
            showZHeight: showZHeight,
            etaStyle: etaStyle,
            printNameStyle: printNameStyle,
            fontSize: fontSize
        )
    }
}
