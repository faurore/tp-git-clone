//
//  MessageStruct.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import SwiftUI

struct MessageCurrentStruct: Equatable {
    let logs: [String]
    let temps: [HistoricTemperatureDataStruct]
    let state: PrinterStateStruct
    let progress: ProgressInformationStruct?
    let job: JobInformationStruct?
    let offsets: [String: Float]?
    let serverTime: Date
    let isHistoryMessage: Bool
    let originHash: Int32
}

struct HistoricTemperatureDataStruct : Equatable {
    let time: Date
    let components: [String: ComponentTemperatureStruct]
}

struct ComponentTemperatureStruct : Equatable {
    let actual: Float?
    let target: Float?
}

struct PrinterStateStruct : Equatable {
    let text: String?
    let flags: PrinterStateFlagsStruct
}

struct PrinterStateFlagsStruct: Equatable {
    let isPrinting: Bool
    let isOperational: Bool
    let isError: Bool
    let cancelling: Bool
    let pausing: Bool
    let paused: Bool
}

struct ProgressInformationStruct : Equatable {
    let completion: Float?
    let filepos: Int64?
    let printTime: TimeInterval?
    let printTimeLeft: TimeInterval?
    let printTimeLeftOrigin: String?
    let printTimeLeftColor: Color
    let printTImeLeftSystemIconName: String
}

struct JobInformationStruct : Equatable {
    let file: JobFileStruct?
}

struct JobFileStruct: Equatable {
    let name: String
    let path: String
    let origin: FileOriginStruct
    let display: String
    let size: Int64
    let date: Date
}


extension MessageCurrent {
    func toStruct() -> MessageCurrentStruct {
        return MessageCurrentStruct(
            logs: logs,
            temps: temps.map { $0.toStruct() },
            state: state.toStruct(),
            progress: progress?.toStruct(),
            job: job?.toStruct(),
            offsets: offsets?.mapValues{ $0.floatValue },
            serverTime: serverTime.toDate(),
            isHistoryMessage: isHistoryMessage,
            originHash: originHash
        )
    }
}

extension HistoricTemperatureData {
    func toStruct() -> HistoricTemperatureDataStruct {
        return HistoricTemperatureDataStruct(
            time: time.toDate(),
            components: components.mapValues { $0.toStruct() }
        )
    }
}

extension ComponentTemperature {
    func toStruct() -> ComponentTemperatureStruct {
        return ComponentTemperatureStruct(
            actual: actual?.floatValue,
            target: target?.floatValue
        )
    }
}


extension PrinterState.State {
    func toStruct() -> PrinterStateStruct {
        return PrinterStateStruct(text: text, flags: flags.toStruct())
    }
}

extension ProgressInformation {
    func toStruct() -> ProgressInformationStruct {
        var color: Color = .clear
        var iconName: String = ""
                
        switch printTimeLeftOrigin {
        case ProgressInformation.companion.ORIGIN_LINEAR: color = OctoTheme.colors.red
        case ProgressInformation.companion.ORIGIN_ANALYSIS: color = OctoTheme.colors.orange
        case ProgressInformation.companion.ORIGIN_MIXED_ANALYSIS: color = OctoTheme.colors.orange
        case ProgressInformation.companion.ORIGIN_AVERAGE: color = OctoTheme.colors.green
        case ProgressInformation.companion.ORIGIN_MIXED_AVERAGE: color = OctoTheme.colors.green
        case ProgressInformation.companion.ORIGIN_ESTIMATE: color = OctoTheme.colors.green
        case ProgressInformation.companion.ORIGIN_GENIUS: color = OctoTheme.colors.yellow
        default: color = .clear
        }
        
        switch printTimeLeftOrigin {
        case ProgressInformation.companion.ORIGIN_GENIUS: iconName = "star.fill"
        default: iconName = "circle.fill"
        }
        
        return ProgressInformationStruct(
            completion: completion?.floatValue,
            filepos: filepos?.int64Value,
            printTime: TimeInterval(printTime?.intValue ?? 0),
            printTimeLeft: TimeInterval(printTimeLeft?.intValue ?? 0),
            printTimeLeftOrigin: printTimeLeftOrigin,
            printTimeLeftColor: color,
            printTImeLeftSystemIconName: iconName
        )
    }
}

extension PrinterState.Flags {
    func toStruct() -> PrinterStateFlagsStruct {
        return PrinterStateFlagsStruct(
            isPrinting: isPrinting(),
            isOperational: isOperational(),
            isError: isError(),
            cancelling: cancelling,
            pausing: pausing,
            paused: paused
        )
    }
}

extension JobInformation {
    func toStruct() -> JobInformationStruct {
        return JobInformationStruct(
            file: file?.toStruct()
        )
    }
}

extension JobInformation.JobFile {
    func toStruct() -> JobFileStruct {
        return JobFileStruct(
            name: name,
            path: path,
            origin: origin.toStruct(),
            display: display,
            size: size,
            date: date.toDate()
        )
    }
}

