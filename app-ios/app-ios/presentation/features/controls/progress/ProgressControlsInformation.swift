//
//  ProgressControlsInformation.swift
//  OctoApp
//
//  Created by Christian on 28/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Flow
import OctoAppBase

struct ProgressControlsInformation: View {
    var current: MessageCurrentStruct?
    var companion: CompanionPluginMessageStruct?
    var settings: ProgressWidgetSettingsStruct?
    var role: ProgressControlsInformationRole = .normal
    var timeIntervalFormatter: DateComponentsFormatter {
        return DateComponentsFormatter()
    }
    var body: some View {
        let smallText = settings?.fontSize == .small
        let rowSpacing =  smallText ? OctoTheme.dimens.margin01 : OctoTheme.dimens.margin1
        
        VStack(spacing: rowSpacing) {
            HFlow(
                alignment: .top,
                itemSpacing: OctoTheme.dimens.margin12,
                rowSpacing: rowSpacing
            ) {
                if role == .webcamFullscreen {
                    InfoItem(
                        title: "progress_widget___title"~,
                        text: String(format: "x_percent_int"~, Int(current?.progress?.completion ?? 0.0).description),
                        smallText: smallText
                    )
                }
                
                if settings?.showUsedTime == true {
                    InfoItem(
                        title: "progress_widget___time_spent"~,
                        text: current?.progress?.printTime?.formatDefault() ?? "",
                        smallText: smallText
                    )
                }
                 
                if settings?.showLeftTime == true {
                    InfoItem(
                        title: "progress_widget___time_left"~,
                        text: current?.progress?.printTimeLeft?.formatDefault() ?? "",
                        smallText: smallText
                    )
                }
                
                if settings?.etaStyle != ProgressWidgetSettings.EtaStyle.none {
                    InfoItem(
                        title: "progress_widget___eta"~,
                        text: current?.progress?.printTimeLeft?.formatEta(compact: settings?.etaStyle == .compact) ?? "",
                        systemIconName: current?.progress?.printTImeLeftSystemIconName,
                        iconColor:  current?.progress?.printTimeLeftColor ?? .clear,
                        smallText: smallText
                    )
                }
                
                if settings?.showLayer == true {
                    InfoItem(
                        title: "progress_widget___layer"~,
                        text: "Coming soon",
                        smallText: smallText
                    )
                }
                
                if settings?.showZHeight == true {
                    InfoItem(
                        title: "progress_widget___z_height"~,
                        text: "Coming soon",
                        smallText: smallText
                    )
                }
                
            }
            .frame(maxWidth: .infinity, alignment: .leading)
            
            if settings?.printNameStyle != ProgressWidgetSettings.PrintNameStyle.none {
                InfoItem(
                    title: "progress_widget___print_name"~,
                    text: current?.job?.file?.display ?? "???",
                    smallText: smallText,
                    largeField: true,
                    maxLines: settings?.printNameStyle == .compact ? 1 : 5
                )
            }
            
            if settings?.showPrinterMessage == true {
                InfoItem(
                    title: "progress_widget___printer_message"~,
                    text: companion?.m117 ?? "progress_widget___printer_message_no_message"~,
                    smallText: smallText,
                    largeField: true
                )
            }
        
        }
    }
}

private extension TimeInterval {
    func formatDefault() -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute]
        formatter.unitsStyle = .abbreviated
        return formatter.string(from: self) ?? ""
    }
    
    func formatEta(compact: Bool) -> String {
        let df = DateFormatter()
        let date = Date() + self
        let daysUntil = Calendar.current.numberOfDaysBetween(Date(), and: date)

        if compact {
            df.timeStyle = .short
            df.dateStyle = .none
            let suffix = daysUntil > 1 ? " + \(daysUntil)" : ""
            return df.string(from: date) + suffix
        } else {
            df.dateStyle = Calendar.current.isDateInToday(date) ? .none : .short
            df.timeStyle = .short
            return df.string(from: date)
        }
    }
}

private extension Calendar {
    func numberOfDaysBetween(_ from: Date, and to: Date) -> Int {
        let fromDate = startOfDay(for: from)
        let toDate = startOfDay(for: to)
        let numberOfDays = dateComponents([.day], from: fromDate, to: toDate)
        
        return numberOfDays.day!
    }
}

private struct InfoItem : View {
    var title: String
    var text: String
    var systemIconName: String? = nil
    var iconColor: Color = .clear
    var smallText: Bool
    var largeField: Bool = false
    var maxLines = 1
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack(spacing: OctoTheme.dimens.margin0) {
                Text(title)
                    .foregroundColor(OctoTheme.colors.lightText)
                
                if let i = systemIconName {
                    Image(systemName: i)
                        .foregroundColor(iconColor)
                        .scaleEffect(x: 0.5, y: 0.5)
                }
            }
            .if(smallText) { $0.typographyLabelSmall() }
            .if(!smallText) { $0.typographyLabel() }
            
            Text(text)
                .foregroundColor(OctoTheme.colors.darkText)
                .if(smallText) { $0.typographyLabel() }
                .if(!smallText) { $0.typographyBase() }
                .lineLimit(maxLines)
                .multilineTextAlignment(.leading)
                .animation(nil, value: text)
          
        }
        .if (largeField) { $0.frame(maxWidth: .infinity, alignment: .leading)}
    }
}

enum ProgressControlsInformationRole {
    case normal, webcamFullscreen
}

struct ProgressControlsInformation_Previews: PreviewProvider {
    static var previews: some View {
        let current = MessageCurrentStruct(
            logs: [],
            temps: [],
            state: PrinterStateStruct(
                text: nil,
                flags: PrinterStateFlagsStruct(
                    isPrinting: true,
                    isOperational: true,
                    isError: false,
                    cancelling: false,
                    pausing: false,
                    paused: false
                )
            ),
            progress: ProgressInformationStruct(
                completion: 42,
                filepos: nil,
                printTime: TimeInterval(3244),
                printTimeLeft: TimeInterval(1113333),
                printTimeLeftOrigin: "genius",
                printTimeLeftColor: OctoTheme.colors.yellow,
                printTImeLeftSystemIconName: "star.fill"
            ),
            job: JobInformationStruct(
                file: JobFileStruct(
                    name: "Some print.gcode",
                    path: "",
                    origin: .local,
                    display: "Some print.gcode",
                    size: 0,
                    date: Date()
                )
            ),
            offsets: [:],
            serverTime: Date(),
            isHistoryMessage: false,
            originHash: 0
        )
        
        ProgressControlsInformation(
            current: current,
            companion: nil,
            settings: ProgressWidgetSettingsStruct(
                showUsedTime: true,
                showLeftTime: true,
                showThumbnail: true,
                showPrinterMessage: true,
                showLayer: true,
                showZHeight: true,
                etaStyle: .full,
                printNameStyle: .compact,
                fontSize: .normal
            )
        )
        .previewDisplayName("Normal")
        
        ProgressControlsInformation(
            current: current,
            companion: nil,
            settings: ProgressWidgetSettingsStruct(
                showUsedTime: true,
                showLeftTime: true,
                showThumbnail: true,
                showPrinterMessage: true,
                showLayer: true,
                showZHeight: true,
                etaStyle: .full,
                printNameStyle: .compact,
                fontSize: .small
            )
        )
        .previewDisplayName("Small font")
        
        ProgressControlsInformation(
            current: current,
            companion: nil,
            settings: ProgressWidgetSettingsStruct(
                showUsedTime: true,
                showLeftTime: true,
                showThumbnail: true,
                showPrinterMessage: true,
                showLayer: true,
                showZHeight: true,
                etaStyle: .compact,
                printNameStyle: .compact,
                fontSize: .normal
            ),
            role:. webcamFullscreen
        )
        .previewDisplayName("Webcam")
    }
}
