//
//  TemperatureControls.swift
//  OctoApp
//
//  Created by Christian on 22/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct TemperatureControls: View {
    @StateObject private var viewModel = TemperatureControlsViewModel()
    
    var body: some View {
        ControlsScaffold(title: "widget_temperature"~) {
            VStack(spacing: OctoTheme.dimens.margin1) {
                HStack(spacing: OctoTheme.dimens.margin1) {
                    if let x = viewModel.state[safe: 0] {
                        TemperatureControlsComponent(snapshot: x)
                    }
                    
                    if let x = viewModel.state[safe: 1] {
                        TemperatureControlsComponent(snapshot: x)
                    } else {
                        Spacer().frame(maxWidth: .infinity)
                    }
                }
                
                if viewModel.state.count > 2 {
                    HStack {
                        if let x = viewModel.state[safe: 2] {
                            TemperatureControlsComponent(snapshot: x)
                        }
                        
                        if let x = viewModel.state[safe: 3] {
                            TemperatureControlsComponent(snapshot: x)
                        } else {
                            Spacer().frame(maxWidth: .infinity)
                        }
                    }
                }
                
                if viewModel.state.count > 4 {
                    HStack {
                        if let x = viewModel.state[safe: 4] {
                            TemperatureControlsComponent(snapshot: x)
                        }
                        
                        if let x = viewModel.state[safe: 5] {
                            TemperatureControlsComponent(snapshot: x)
                        } else {
                            Spacer().frame(maxWidth: .infinity)
                        }
                    }
                }
                
                if viewModel.state.count > 6 {
                    HStack {
                        if let x = viewModel.state[safe: 6] {
                            TemperatureControlsComponent(snapshot: x)
                        }
                        
                        if let x = viewModel.state[safe: 7] {
                            TemperatureControlsComponent(snapshot: x)
                        } else {
                            Spacer().frame(maxWidth: .infinity)
                        }
                    }
                }
            }
        }
        .withInstanceId { viewModel.initWithInstance(instanceId: $0) }
    }
}

private extension Collection {
    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

private class TemperatureControlsViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    private var currentCore: TemperatureControlsViewModelCore? = nil
    @Published var state: [TemperatureSnapshotStruct?] = [TemperatureSnapshotStruct?](repeating: nil, count: 2)
    
    func initWithInstance(instanceId: String) {
        let core = TemperatureControlsViewModelCore(instanceId: instanceId)
        currentCore = core
        state = [TemperatureSnapshotStruct?](repeating: nil, count: Int(core.initialTemperatureComponentCount))
        core.temperatures.asPublisher()
            .sink { (state: TemperatureControlsViewModelCore.State) in
                self.state = state.components.map { $0.toStruct() }
            }
            .store(in: &bag)
    }
}

struct TemperatureControls_Previews: PreviewProvider {
    static var previews: some View {
        TemperatureControls()
    }
}
