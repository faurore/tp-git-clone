//
//  HelpContact.swift
//  OctoApp
//
//  Created by Christian on 26/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import UIKit
import MessageUI
import OctoAppBase

struct HelpContactView: View {
    var bugReport: Bool
    
    @StateObject private var viewModel = HelpContactViewModel()
    @Environment(\.dismiss) var dismiss
    
    @State private var input: String = ""
    @State private var phoneInfo = true
    @State private var octoPrintInfo = true
    @State private var appLogs = true
    
    @State private var result: Result<MFMailComposeResult, Error>? = nil
    @State private var showMailView = false
    @State private var showEmptyMessage = false
    @State private var showNoEmail = false
    @State private var loading = false
    @State private var mail: MailStruct? = nil
    
    var body: some View {
        ScrollView {
            VStack {
                Text("thank_you_for_sending_feedback_please_select_what_should_be_included_in_your_message"~)
                    .typographyBase()
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                OctoTextField(
                    placeholder: "message"~,
                    labelActiv: LocalizedStringKey("message"),
                    input: $input
                )
                .padding(.top, OctoTheme.dimens.margin1)
                .submitLabel(.continue)
                
                VStack {
                    Toggle("information_about_your_phone"~, isOn: $phoneInfo)
                        .disabled(bugReport)
                    Toggle("information_about_your_octoprint"~, isOn: $octoPrintInfo)
                        .disabled(bugReport)
                    Toggle("app_logs"~, isOn: $appLogs)
                }
                .toggleStyle(SwitchToggleStyle(tint: OctoTheme.colors.accent))
                .typographyBase()
                .padding([.top], OctoTheme.dimens.margin2)
                
                Text(String(format: "help___contact_detail_information"~, viewModel.contactTime, viewModel.contactTimeZone))
                    .multilineTextAlignment(.center)
                    .typographyLabelSmall()
                    .frame(maxWidth: .infinity, alignment: .center)
                    .padding(.top, OctoTheme.dimens.margin3)
                    .padding(.bottom, OctoTheme.dimens.margin1)
                
                OctoButton(
                    text: LocalizedStringKey("open_email"),
                    loading: loading,
                    clickListener: {
                        if !MFMailComposeViewController.canSendMail() {
                            showNoEmail = true
                        } else if input.isEmpty {
                            showEmptyMessage = true
                        } else {
                            loading = true
                            Task {
                                mail = await viewModel.prepareFeedbackMail(
                                    sendPhoneInfo: phoneInfo,
                                    sendOctoPrintInfo: octoPrintInfo,
                                    sendLogs: appLogs
                                )
                                loading = false
                                showMailView = true
                            }
                        }
                    }
                )
            }
            .padding(OctoTheme.dimens.margin12)
            .alert("error_please_enter_a_message"~, isPresented: $showEmptyMessage) {
                Button("OK"~, role: .cancel) { showEmptyMessage = false }
            }
            .alert("Email not set up on this device"~, isPresented: $showNoEmail) {
                Button("OK"~, role: .cancel) { showNoEmail = false }
            }
        }
        .navigationTitle("send_feedback"~)
        .background(OctoTheme.colors.windowBackground)
        .sheet(isPresented: $showMailView) {
            MailView(result: $result) { composer in
                composer.setSubject(mail?.subject ?? fallbackSubject)
                composer.setToRecipients([mail?.recepientAddress ?? fallbackEmail])
                composer.setMessageBody(input, isHTML: false)
                if let a = mail?.attachment {
                    composer.addAttachmentData(a.data, mimeType: a.mimeType, fileName: a.name)
                }
            }
        }
    }
}

private let fallbackEmail = "hello@octoprint.eu"
private let fallbackSubject = "Feedback OctoApp for iOS"

private class HelpContactViewModel : ObservableObject {
    private let tag = "HelpContactViewModel"
    private var core = HelpContactViewModelCore()
    var contactTimeZone: String {
        return core.contactTimeZone
    }
    var contactTime: String {
        guard let tz = (TimeZone(abbreviation: contactTimeZone) ?? TimeZone(identifier: contactTimeZone)) else {
            return "???"
        }
        
        let df = DateFormatter()
        df.timeZone = tz
        df.dateStyle = .none
        df.timeStyle = .short
        
        return df.string(from: Date())
    }
    
    func prepareFeedbackMail(
        sendPhoneInfo: Bool,
        sendOctoPrintInfo: Bool,
        sendLogs: Bool
    ) async  -> MailStruct {
        guard let mail = try? await core.prepareFeedbackMail(
            sendLogs: sendLogs,
            sendOctoPrintInfo: sendOctoPrintInfo,
            sendPhoneInfo: sendPhoneInfo
        ) else {
            return MailStruct(
                recepientAddress: fallbackEmail,
                subject: fallbackSubject,
                attachment: nil
            )
        }
        
        var attachment: AttachmentStruct?
        if let a = mail.attachments {
            do {
                attachment = try createAttachment(attachments: a)
            } catch {
                Napier.e(tag: tag, message: "Failed to create ZIP file: \(error)")
            }
        } else {
            attachment = nil
        }
        
        return MailStruct(
            recepientAddress: mail.receipientAddress,
            subject: mail.subject,
            attachment: attachment
        )
    }
    
    private func createAttachment(attachments: [HelpContactViewModelCore.Attachment]) throws -> AttachmentStruct {
        let zipName = "data.zip"
        let fileManager = Foundation.FileManager.default
        let cachesDirectory = fileManager.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let directory = cachesDirectory.appendingPathComponent("data")
        let zipFile = cachesDirectory.appendingPathComponent(zipName)
        
        try? fileManager.removeItem(at: directory)
        try? fileManager.removeItem(at: zipFile)
        try fileManager.createDirectory(at: directory, withIntermediateDirectories: true, attributes: nil)
        
        try attachments.forEach { a in
            let path = directory.appendingPathComponent(a.name)
            Napier.i(tag: tag, message: "Writing \(a.name) to \(path)")
            try a.bytes.toNsData().write(to: path)
        }
        
        let coord = NSFileCoordinator()
        var readError: NSError?
        var copyError: NSError?
        coord.coordinate(
            readingItemAt: directory,
            options: NSFileCoordinator.ReadingOptions.forUploading,
            error: &readError
        ) { (zippedURL: URL) -> Void in
            do {
                try fileManager.copyItem(at: zippedURL, to: zipFile)
            } catch let caughtCopyError {
                copyError = caughtCopyError as NSError
            }
        }
        
        if let anyError = readError ?? copyError  {
            Napier.i(tag: tag, message: "Failed to create ZIP file: \(anyError)")
            throw anyError
        } else {
            let data = try Data(contentsOf: zipFile)
            try? fileManager.removeItem(at: directory)
            try? fileManager.removeItem(at: zipFile)
            return AttachmentStruct(
                name: zipName,
                mimeType: "application/zip",
                data: data
            )
        }
    }
}

private struct MailStruct {
    let recepientAddress: String
    let subject: String
    let attachment: AttachmentStruct?
}

private struct AttachmentStruct {
    let name: String
    let mimeType: String
    let data: Data
}

private struct MailView: UIViewControllerRepresentable {
    
    @Environment(\.presentationMode) var presentation
    @Binding var result: Result<MFMailComposeResult, Error>?
    public var configure: ((MFMailComposeViewController) -> Void)?
    
    public class Coordinator: NSObject, MFMailComposeViewControllerDelegate {
        
        @Binding var presentation: PresentationMode
        @Binding var result: Result<MFMailComposeResult, Error>?
        
        init(presentation: Binding<PresentationMode>,
             result: Binding<Result<MFMailComposeResult, Error>?>) {
            _presentation = presentation
            _result = result
        }
        
        public func mailComposeController(_ controller: MFMailComposeViewController,
                                          didFinishWith result: MFMailComposeResult,
                                          error: Error?) {
            defer {
                $presentation.wrappedValue.dismiss()
            }
            guard error == nil else {
                self.result = .failure(error!)
                return
            }
            self.result = .success(result)
        }
    }
    
    public func makeCoordinator() -> Coordinator {
        return Coordinator(presentation: presentation,
                           result: $result)
    }
    
    public func makeUIViewController(context: UIViewControllerRepresentableContext<MailView>) -> MFMailComposeViewController {
        let vc = MFMailComposeViewController()
        vc.mailComposeDelegate = context.coordinator
        configure?(vc)
        return vc
    }
    
    public func updateUIViewController(
        _ uiViewController: MFMailComposeViewController,
        context: UIViewControllerRepresentableContext<MailView>) {
            
        }
}

struct HelpContact_Previews: PreviewProvider {
    static var previews: some View {
        NavigationStack {
            HelpContactView(bugReport: true)
        }
    }
}
