//
//  File.swift
//  OctoApp
//
//  Created by Christian on 30/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct NgrokSupport : View {
    @StateObject private var viewModel = NgrokSupportViewModel()
    
    var body: some View {
        Color.clear
            .frame(height: 0)
            .withInstanceId { viewModel.initWithInstance($0) }
    }
}

private class NgrokSupportViewModel : ObservableObject {
    private var currentCore: NgrokSupportViewModelCore? = nil
    var bag:Set<AnyCancellable> = []
    
    func initWithInstance(_ instanceId: String) {
        let core = NgrokSupportViewModelCore(instanceId: instanceId)
        currentCore = core
        Napier.i(tag: "NgrokSupportViewModel", message: "Ngrok support started")
        
        core.events.asPublisher()
            .sink { (_ : KotlinUnit) in }
            .store(in: &bag)
    }
}
