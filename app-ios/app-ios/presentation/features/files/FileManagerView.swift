//
//  FileList.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import Combine

struct FileManagerView: View {
    var path: String
    var title: String = ""
    
    @Environment(\.dismissToRoot) var dismiss
    @StateObject private var viewModel = FileManagerViewModel()

    var body: some View {
        ZStack{
            FileStartPrintConfirmSettingsView(
                confirmSettings: viewModel.confirmSettingsState,
                startPrinting: { viewModel.startPrint(confirmedMaterial: $0, confirmedTimelapse: $1) }
            )
            
            switch(viewModel.state) {
            case .loading: ProgressView()
            case .error(let e): do {
                ScreenError(
                    title: "file_manager___file_list___error_title"~,
                    description: "file_manager___file_list___error_description"~,
                    error: e,
                    onRetry: {  viewModel.reload() }
                )
            }
            case .loaded(let f): do {
                if (f.isFolder) {
                    FileListView(
                        file: f,
                        informAboutThumbnails: viewModel.informAboutThumbnails,
                        onDismissThumbnailInfo: {
                            withAnimation {
                                viewModel.dismissThumbnailInfo()
                            }
                        }
                    )
                } else {
                    FileDetailsView(
                        file: f,
                        loading: viewModel.prinStartLoading,
                        canStartPrint: viewModel.canStartPrint,
                        onStartPrint: { viewModel.startPrint() }
                    )
                }
            }
            }
        }
        .withInstanceId{ viewModel.initWith(instanceId: $0, path: path, title: title)}
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .navigationTitle(viewModel.title ?? title)
        .refreshable { await viewModel.reloadAndWait() }
        .onChange(of: viewModel.printStarted) { if $0 { dismiss() } }
        .animation(.default, value: viewModel.state)
    }
}


struct FileList_Previews: PreviewProvider {
    static var previews: some View {
        FileManagerView(path: "/", title: "Title")
    }
}
