//
//  FileSearchView.swift
//  OctoApp
//
//  Created by Christian on 23/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import Combine
import OctoAppBase

struct FileSearchView: View {
    @StateObject private var viewModel = FileSearchViewModel()
    var searchTerm: String
    
    var body: some View {
        ZStack {
            switch(viewModel.searchState) {
            case .loading: ProgressView()
            case .error(let e): ScreenError(error: e) { viewModel.search(searchTerm: searchTerm, skipCache: true)}
            case .loaded(let folders, let files): SearchResults(folders: folders, files: files)
            }
        }
        .withInstanceId { viewModel.initWith(instanceId: $0) }
        .onAppear { viewModel.search(searchTerm: searchTerm) }
        .onChange(of: searchTerm){ viewModel.search(searchTerm: $0) }
        .refreshable { /* Disable refresh :)*/ }
    }
}

private struct SearchResults : View {
    var folders: [FileObjectStruct]
    var files: [FileObjectStruct]
    
    var body : some View {
        List {
            FileListSection(files: folders)
            FileListSection(files: files)
        }
        .animation(.default, value: files)
        .animation(.default, value: folders)
    }
}

private class FileSearchViewModel : ObservableObject {
    
    var bag:Set<AnyCancellable> = []
    private var currentCore: FileSearchViewModelCore? = nil
    @Published var searchState: FileSearchState = .loading
    
    func initWith(instanceId: String) {
        let core = FileSearchViewModelCore(instanceId: instanceId)
        currentCore = core
        core.searchState
            .asPublisher()
            .sink(receiveValue: { (state: FlowState<FileSearchViewModelCore.SearchResult>) in
                if state is FlowStateLoading {
                    self.searchState = .loading
                } else if let s = state as? FlowStateError {
                    self.searchState = .error(s.throwable)
                } else if let s = (state as? FlowStateReady)?.data {
                    self.searchState = .loaded(
                        s.folders.map { $0.toStruct() },
                        s.files.map { $0.toStruct() }
                    )
                }
            })
            .store(in: &bag)
    }
    
    func search(searchTerm: String, skipCache: Bool = false) {
        currentCore?.search(term: searchTerm, skipCache: skipCache)
    }
}


private enum FileSearchState : Equatable {
    case loading, error(KotlinThrowable), loaded([FileObjectStruct], [FileObjectStruct])
}

struct FileSearchView_Previews: PreviewProvider {
    static var previews: some View {
        FileSearchView(searchTerm: "Something")
    }
}
