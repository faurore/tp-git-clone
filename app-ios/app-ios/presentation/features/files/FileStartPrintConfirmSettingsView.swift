//
//  FileStartPrintConfirmSettingsView.swift
//  OctoApp
//
//  Created by Christian on 23/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct FileStartPrintConfirmSettingsView: View {
    @State private var showTimelapseConfirmation = false
    @State private var showMaterialConfirmation = false
    @State private var timelapseConfirmed = false
    @State private var materialConfirmed = false
    var confirmSettings: FileStartPrintState
    var startPrinting: (Bool, Bool) -> Void
    
    var body: some View {
        Color.clear
            .frame(width: 0, height: 0)
            .alert(
                "Timelapse configuration is not yet supported",
                isPresented:  $showTimelapseConfirmation
            ) {
                Button("Continue with last config") {
                    startPrinting(materialConfirmed, true)
                    showTimelapseConfirmation = false
                }
                Button("cancel", role: .cancel) {
                    showTimelapseConfirmation = false
                }
            }
            .alert(
                "Material configuration is not yet supported",
                isPresented:  $showMaterialConfirmation
            ) {
                Button("Continue with last material") {
                    showMaterialConfirmation = false
                    startPrinting(true, timelapseConfirmed)
                }
                Button("cancel", role: .cancel) {
                    showMaterialConfirmation = false
                }
            }
            .onChange(of: confirmSettings) { c in
                switch (c) {
                case .materialConfirmation(let timelapse): do {
                    showTimelapseConfirmation = false
                    showMaterialConfirmation = true
                    timelapseConfirmed = timelapse
                    materialConfirmed = false
                }
                case .timelapseConfirmation(let material): do {
                    showMaterialConfirmation = false
                    showTimelapseConfirmation = true
                    timelapseConfirmed = false
                    materialConfirmed = material
                }
                default: return
                }
            }
    }
}

struct FileStartPrintConfirmSettingsView_Previews: PreviewProvider {
    static var previews: some View {
        FileStartPrintConfirmSettingsView(confirmSettings: .idle) { _, _ in}
    }
}
