//
//  SignInViewDiscover.swift
//  app-ios
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase
import Shimmer

struct SignInViewDiscover: View {
    
    var state: SignInViewModelCore.StateDiscover
    var onContinueWithUrl: (String, Bool) -> Void
    var onContinuePrevious: (OctoPrintInstanceInformationV3) -> Void
    var onDeletePrevious: (String) -> Void
    
    var body: some View {
        VStack(spacing: 0) {
            SignInViewDiscoverNetwork(services: state.services) { url in
                onContinueWithUrl(url, true)
            }
            
            SignInViewDiscoverReuse(
                configs: state.configs,
                hasQuickSwitch: state.hasQuickSwitch,
                onContinueWithConfig: onContinuePrevious,
                onDeletePrevious: onDeletePrevious
            )
            
            SignInViewDiscoverOthers {url in
                onContinueWithUrl(url, false)
            }
        }
    }
}

private struct SignInViewDiscoverNetwork: View {
    
    var services: [NetworkService]
    var onContinueWithUrl: (String) -> Void
    
    var body: some View {
        SignInOptionSection(text: "sign_in___discovery___discovered_devices") {
            //            ProgressView()
        }
        
        if (services.isEmpty) {
            ComponentSignInOption(
                title: "xx",
                subtitle: "xx",
                type: .placeholder
            ) { }.shimmering(duration: 3)
        } else {
            VStack {
                ForEach(services, id: \.id) { service in
                    ComponentSignInOption(
                        title: LocalizedStringKey(service.label),
                        subtitle: LocalizedStringKey(service.detailLabel),
                        type: .discovered
                    ) {
                        onContinueWithUrl(service.webUrl)
                    }
                }
            }
        }
    }
}

private struct SignInViewDiscoverReuse: View {
    
    var configs: [OctoPrintInstanceInformationV3]
    var hasQuickSwitch: Bool
    var onContinueWithConfig: (OctoPrintInstanceInformationV3) -> Void
    var onDeletePrevious: (String) -> Void
    
    @State private var showingSupporter = false
    @State private var showDelete = false
    
    var body: some View {
//        if !configs.isEmpty {
            SignInOptionSection(text: "sign_in___discovery___previously_connected_devices") {
                OctoIconButton(icon: showDelete ? "xmark" : "trash", color: OctoTheme.colors.normalText, iconScale: 0.9) {
                    withAnimation {
                        showDelete.toggle()
                    }
                }
            }
            
            VStack {
                if !hasQuickSwitch {
                    ComponentSignInOption(
                        title: LocalizedStringKey("sign_in___discovery___quick_switch_disabled_title"),
                        subtitle: LocalizedStringKey("sign_in___discovery___quick_switch_disabled_subtitle"),
                        type: .support
                    ) {
                        showingSupporter = true
                    }
                }
                
                ForEach(configs, id: \.id) { config in
                    HStack {
                        ComponentSignInOption(
                            title: LocalizedStringKey(config.label),
                            subtitle: "\(config.webUrl.host):\(config.webUrl.port)",
                            type: .reuse
                        ) {
                            if hasQuickSwitch {
                                onContinueWithConfig(config)
                            } else {
                                showingSupporter = true
                            }
                        }
                        .opacity(hasQuickSwitch ? 1 : 0.4)
                        
                        if(showDelete) {
                            OctoIconButton(icon: "trash") {
                                onDeletePrevious(config.id)
                                withAnimation {
                                    showDelete = false
                                }
                            }
                            .foregroundColor(OctoTheme.colors.destructive)
                        }
                    }
                }
            }.sheet(isPresented: $showingSupporter) {
                BecomeSupporterView()
            }
//        }
    }
}


private struct SignInViewDiscoverOthers: View {
    
    @State private var showingManual = false
    var onContinueWithUrl: (String) -> Void
    
    var body: some View {
        SignInOptionSection(text: "sign_in___discovery___other_options") {}
        
        ComponentSignInOption(
            title: "sign_in___discovery___connect_manually_title",
            subtitle: "sign_in___discovery___connect_manually_subtitle",
            type: .manual
        ) {
            showingManual.toggle()
        }
        .sheet(isPresented: $showingManual) {
            SignInViewManual(){ webUrl in
                onContinueWithUrl(webUrl)
            }
        }
    }
}


struct SignInViewDiscover_Previews: PreviewProvider {
    static var previews: some View {
        SignInViewDiscover(
            state: SignInViewModelCore.StateDiscover(services: [], configs: [], hasQuickSwitch: true),
            onContinueWithUrl: { _, _ in },
            onContinuePrevious: { _ in },
            onDeletePrevious: { _ in }
        )
    }
}
