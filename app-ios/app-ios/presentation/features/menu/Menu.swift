//
//  Menu.swift
//  OctoApp
//
//  Created by Christian on 19/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI
import OctoAppBase

struct Menu: View {
    
    var body: some View {
        NavigationStack {
            MenuList()
        }
        .padding(OctoTheme.dimens.margin12)
        .errorDialogDisplay()
        .deeplinkNavigation()
    }
}

private struct MenuList: View {

    @Environment(\.openURL) private var openUrl

    var body: some View {
        VStack {
            Text("Main menu")
                .typographyTitle()
                .padding(.top)
            
            Button(action: { openUrl(UriLibrary.shared.getHelpUri()) }) {
                MenuItem(
                    title: "Get help",
                    iconSystemName: "questionmark.circle",
                    foregroundColor: OctoTheme.colors.menuStyleSettingsForeground,
                    backgroundColor: OctoTheme.colors.menuStyleSettingsBackground,
                    showAsSubMenu: true
                )
            }

            Button(action: { openUrl(UriLibrary.shared.getConfigureRemoteAccessUri()) }) {
                MenuItem(
                    title: "Configure remote access",
                    iconSystemName: "cloud.fill",
                    foregroundColor: OctoTheme.colors.menuStyleSettingsForeground,
                    backgroundColor: OctoTheme.colors.menuStyleSettingsBackground,
                    showAsSubMenu: true
                )
            }
            
            Spacer()
        }
    }
}

struct Menu_Previews: PreviewProvider {
    static var previews: some View {
        Menu()
    }
}
