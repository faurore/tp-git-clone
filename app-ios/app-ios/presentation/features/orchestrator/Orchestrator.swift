//
//  Orchestrator.swift
//  OctoApp
//
//  Created by Christian on 20/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import Combine
import OctoAppBase

struct OrchestratorView: View {
    
    @Environment(\.scenePhase) private var scenePhase
    @ObservedObject var viewModel = OrchestratorViewModel()
    
    var body: some View {
        ZStack {
            if let active = viewModel.state as? OrchestratorViewModelCore.NavigationStateActive {
                OrchestratorBanner {
                    Controls()
                }
                .pluginSupport()
                .deeplinkNavigation()
                .environment(\.instanceId, active.instanceId)
                .environment(\.instanceLabel, active.instanceLabel)
                .environment(\.instanceColor, PrinterColorScheme(colorName: active.colorName))
            } else if viewModel.state is OrchestratorViewModelCore.NavigationStateLogin {
                SignInView(repairForInstanceId: nil)
                    .transition(.opacity)
            } else if let repair = viewModel.state as? OrchestratorViewModelCore.NavigationStateRepair {
                SignInView(repairForInstanceId: repair.instanceId)
                    .transition(.opacity)
            }
        }
        .errorDialogDisplay()
        .environmentObject(viewModel)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .animation(.default, value: viewModel.state)
        .onChange(of: scenePhase) { newScenePhase in
            switch newScenePhase {
            case .active:
                viewModel.onAppear()
            case .background:
                viewModel.onDisappear()
            default:
                print("Unhandled scene state \(newScenePhase)")
            }
        }
    }
}

class OrchestratorViewModel : ObservableObject {
    var bag:Set<AnyCancellable> = []
    private let core = OrchestratorViewModelCore()
    
    @Published var state: OrchestratorViewModelCore.NavigationState? = nil
    @Published var banner: OrchestratorBannerData = OrchestratorBannerData(connectionType: nil, instanceLabel: nil, instanceColor: nil)
    @Published var errorDialog: OrchestratorError? = nil
    var errorAction: () -> Void = {}
    
    init() {
        core.navigationState
            .asPublisher()
            .sink(receiveValue: { (state: OrchestratorViewModelCore.NavigationState) in
                self.state = state
            })
            .store(in: &bag)
        
        core.connectionState
            .asPublisher()
            .sink(receiveValue: { (connection: OrchestratorViewModelCore.ConnectionState) in
                let showBanner = connection.showLabelInBanner
                self.banner = OrchestratorBannerData(
                    connectionType: connection.connectionType,
                    instanceLabel: showBanner ? connection.instanceLabel : nil,
                    instanceColor: showBanner ? colorFromAppearanceName(name: connection.instanceColor) : nil
                )
            })
            .store(in: &bag)
        
        core.errorEvents
            .asPublisher()
            .sink(receiveValue: { (error: OrchestratorViewModelCore.ErrorEvent) in
                self.errorAction = { error.action?() }
                self.errorDialog = OrchestratorError(
                    date: Date(),
                    message: error.messageString,
                    detailMessage: error.detailsMessageString,
                    hasAction: error.action != nil
                )
            })
            .store(in: &bag)
    }
    
    func onAppear() {
        Task {
            try? await core.onAppOpen()
        }
    }
    
    func onDisappear() {
        Task {
            try? await core.onAppClose()
        }
    }
}

private enum OrchestratorState {
    case active, inactive
}

private struct InstanceIdKey: EnvironmentKey {
    static let defaultValue = "???"
}

private struct InstanceColorSchemeKey: EnvironmentKey {
    static let defaultValue = PrinterColorScheme(colorName: nil)
}

private struct DismissToRoot: EnvironmentKey {
    static let defaultValue: () -> Void = {}
}


private struct InstanceLabel: EnvironmentKey {
    static let defaultValue = "???"
}

extension EnvironmentValues {
    var instanceId: String {
        get { self[InstanceIdKey.self] }
        set { self[InstanceIdKey.self] = newValue }
    }
    var instanceColor: PrinterColorScheme {
        get { self[InstanceColorSchemeKey.self] }
        set { self[InstanceColorSchemeKey.self] = newValue }
    }
    var instanceLabel: String {
        get { self[InstanceLabel.self] }
        set { self[InstanceLabel.self] = newValue }
    }
    var dismissToRoot: () -> Void {
        get { self[DismissToRoot.self] }
        set { self[DismissToRoot.self] = newValue }
    }
}
