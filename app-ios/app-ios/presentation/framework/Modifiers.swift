//
//  Modifiers.swift
//  app-ios
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import SwiftUI
import OctoAppBase

struct WithInstanceId: ViewModifier {
    
    @State var created = false
    @Environment(\.instanceId) private var instanceId
    let cb: (String) -> Void
    
    func body(content: Content) -> some View {
        content
            .onAppear {
                if !created {
                    created = true
                    cb(instanceId)
                }
            }
            .onChange(of: instanceId, perform: cb)
    }
}

struct ColoredFont: ViewModifier {
    
    let font: Font
    let color: Color
    
    init(font: Font) {
        self.font = font
        self.color =  OctoTheme.colors.normalText
    }
    
    init(font: Font, color: Color) {
        self.font = font
        self.color =  color
    }
    
    func body(content: Content) -> some View {
        content
            .font(font)
            .foregroundColor(color)
    }
}

struct Surface: ViewModifier {
    
    let color: SurfaceColor
    let shadow: Bool
    
    private var mappedColor: Color {
        switch(color) {
        case .normal: return  OctoTheme.colors.inputBackground
        case .alternative: return  OctoTheme.colors.inputBackgroundAlternative
        case .red: return OctoTheme.colors.redTranslucent
        case .special(let c): return c
        }
    }
    
    private var shape : RoundedRectangle {
        return RoundedRectangle(cornerSize: CGSize(width: OctoTheme.dimens.cornerRadius, height: OctoTheme.dimens.cornerRadius))
    }
    
    func body(content: Content) -> some View {
        content
            .cornerRadius(OctoTheme.dimens.cornerRadius)
            .background(shape.fill(mappedColor))
            .if(shadow) { $0.background(shape.shadow(radius: 1)) }
    }
}

extension View {
    @ViewBuilder func `if`<Content: View>(_ condition: Bool, transform: (Self) -> Content) -> some View {
        if condition {
            transform(self)
        } else {
            self
        }
    }
    
    @ViewBuilder func ifPad<Content: View>(transform: (Self) -> Content) -> some View {
        if UIDevice.current.userInterfaceIdiom == .pad {
            transform(self)
        } else {
            self
        }
    }
    
    @ViewBuilder func ifNotPad<Content: View>(transform: (Self) -> Content) -> some View {
        if UIDevice.current.userInterfaceIdiom != .pad {
            transform(self)
        } else {
            self
        }
    }

    func withInstanceId(cb: @escaping (String) -> Void) -> some View {
        modifier(WithInstanceId(cb: cb))
    }
    
    func surface(color: SurfaceColor = .normal, shadow: Bool = false) -> some View {
        modifier(Surface(color: color, shadow: shadow))
    }
    
    func typographyBase() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.base))
    }
    
    func typographyLabel() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.label))
    }
    
    func typographyLabelSmall() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.labelSmall))
    }
    
    func typographyFocus() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.focus))
    }
    
    func typographyInput() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.input))
    }
    
    func typographyData() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.data))
    }
    
    func typographyDataLarge() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.dataLarge))
    }
    
    func typographyTitle() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.title, color: OctoTheme.colors.darkText))
    }
    
    func typographyTitleBig() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.titleBig, color: OctoTheme.colors.darkText))
    }
    
    func typographyTitleLarge() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.titleLarge, color: OctoTheme.colors.darkText))
    }
    
    func typographySubtitle() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.subtitle, color: OctoTheme.colors.darkText))
    }
    
    func typographyButton(small: Bool = false) -> some View {
        modifier(small ? ColoredFont(font: OctoTheme.typography.buttonSmall) : ColoredFont(font: OctoTheme.typography.button))
    }
 
    func typographyButtonLink() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.sectionHeader, color: OctoTheme.colors.accent))
    }
    
    func typographySectionHeader() -> some View {
        modifier(ColoredFont(font: OctoTheme.typography.sectionHeader, color: OctoTheme.colors.darkText))
    }

}

enum SurfaceColor {
    case normal, alternative, red, special(Color)
}

postfix operator ~
postfix func ~ (string: String) -> String {
    return StringResourcesKt.getString(id: string, formatArgs: KotlinArray(size: 0) { _ in nil })
}

private func trimCdata(_ string: String) -> String {
    return string.deletePrefix().deleteSuffix()
}

private func replaceFromat(_ string: String) -> String {
    do {
        StringResourcesKt.getString(id: string, formatArgs: KotlinArray(size: 0) {  _ in nil })
        let r = try NSRegularExpression(pattern: "%((?:\\.|\\d|\\$)*)[abcdefs]")
        return r.stringByReplacingMatches(in: string, range: NSMakeRange(0, string.count), withTemplate: "%$1@")
    } catch {
        return string
    }
}

private extension String {
    func deletePrefix() -> String {
        let pre = "<![CDATA["
        guard self.hasPrefix(pre) else { return self }
        return String(self.dropFirst(pre.count))
    }
    
    func deleteSuffix() -> String {
        let suf = "]]>"
        guard self.hasSuffix(suf) else { return self }
        return String(self.dropLast(suf.count))
    }
}
