//
//  TextField.swift
//  app-ios
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct OctoTextField: View {
    // placeolder should not be LocalizedStringKey as this would parse "Markdown", e.g. links
    var placeholder: String
    var labelActiv: LocalizedStringKey
    var alternativeBackgroung = false
    
    var keyboardType: UIKeyboardType = .default
    var input: Binding<String>
    var autocorrectionDisabled = false
    var multiline = false
    var autoFocus = false
    
    @State private var focused = false
    @State private  var placeholderShown = true
    @Namespace private var animation
    @FocusState private var focusedField: FocusField?
    @State var text: String = ""
    
    var body: some View {
        ZStack(alignment: .leading) {
            
            if(!focused && input.wrappedValue.isEmpty) {
                Text(placeholder)
                    .typographyInput()
                    .opacity(focused ? 0 : 0.66)
                    .lineLimit(1)
                    .matchedGeometryEffect(id: "Placeholder", in: animation)
                    .frame(maxWidth: .infinity, alignment: .leading)
            }
            VStack(alignment: .leading) {
                Text(labelActiv)
                    .foregroundColor(focused ? OctoTheme.colors.accent : OctoTheme.colors.lightText)
                    .typographyLabel()
                    .lineLimit(1)
                    .padding([.bottom], -OctoTheme.dimens.margin0)
                    .opacity(focused || !input.wrappedValue.isEmpty ? 1 : 0)
                
                ZStack(alignment: .leading) {
                    if (placeholderShown && input.wrappedValue.isEmpty) {
                        Text(placeholder)
                            .typographyInput()
                            .lineLimit(1)
                            .opacity(focused ? 0.66 : 0)
                            .matchedGeometryEffect(id: "Placeholder", in: animation)
                    }
                    
                    TextField(
                        "",
                        text: input,
                        onEditingChanged: { (editingChanged) in
                            withAnimation(.easeOut(duration: 0.2)) {
                                focused = editingChanged
                            }
                        }
                    )
                    .keyboardType(keyboardType)
                    .focused($focusedField, equals: .field)
                }
            }
        }
        .padding(OctoTheme.dimens.margin12)
        .surface(color: alternativeBackgroung ? .alternative : .normal)
        .onTapGesture {
            self.focusedField = .field
        }
        .onAppear {
            if autoFocus {
                self.focusedField = .field
            }
        }
    }
    
    private enum FocusField: Hashable {
        case field
    }
}

struct TextField_Previews: PreviewProvider {
    @State static var input = ""
    
    static var previews: some View {
        OctoTextField(placeholder: "Web URL", labelActiv: "Web URL – Copy this from your browser!", alternativeBackgroung: true,  input: $input)
            .previewDisplayName("Alternative")
        OctoTextField(placeholder: "Web URL", labelActiv: "Web URL – Copy this from your browser!", input: $input)
            .previewDisplayName("Normal")
    }
}
