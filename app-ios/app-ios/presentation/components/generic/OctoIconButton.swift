//
//  OctoIconButton.swift
//  app-ios
//
//  Created by Christian on 14/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct OctoIconButton: View {
    
    var icon: String
    var color: Color = OctoTheme.colors.accent
    var iconScale: CGFloat = 1
    var clickListener: () -> Void
    @State var pressed = false
    
    var body: some View {
        Button(action: clickListener) {
            Image(systemName: icon)
                .scaleEffect(iconScale)
                .font(.system(size: 24))
                .foregroundColor(color)
        }
        .buttonStyle(IconButtonStyle())
    }
}

private struct IconButtonStyle: ButtonStyle {

  func makeBody(configuration: Self.Configuration) -> some View {
    configuration.label
      .padding(OctoTheme.dimens.margin1)
      .scaleEffect(0.75)
      .background(configuration.isPressed ? OctoTheme.colors.clickIndicatorOverlay : .clear)
      .clipShape(Circle())
      .scaleEffect(1.25)
  }
}

struct OctoIconButton_Previews: PreviewProvider {
    static var previews: some View {
        OctoIconButton(icon: "xmark") {
            
        }
    }
}
