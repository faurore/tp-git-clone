//
//  LargeScreenStage.swift
//  OctoApp
//
//  Created by Christian on 15/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import SwiftUI

struct LargeScreenStage<Content: View> : View {
    
    let content : () -> Content
    private let maxWidth: CGFloat = 500
    private let maxHeight: CGFloat = 1000
    private let padding = OctoTheme.dimens.margin2
    
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    
    var body: some View {
        switch horizontalSizeClass {
            
        case .regular:
            GeometryReader { geo in
                let size = calcSize(inSize: geo.size)
                
                ZStack {
                    ZStack {
                        content()
                    }
                    .frame(width: size.width, height: size.height, alignment: .center)
                    
                    .background(OctoTheme.colors.windowBackground)
                    .clipShape(RoundedRectangle(
                        cornerSize: CGSize(width: OctoTheme.dimens.cornerRadius, height: OctoTheme.dimens.cornerRadius))
                    )
                    .shadow(color: .black.opacity(0.15), radius: 30)
                }
                .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .center)
                .padding(padding)
                .background(OctoTheme.colors.inputBackground)
            }
        default:
            content()
        }
    }
    
    private func calcSize(inSize: CGSize) -> CGSize {
        var width: CGFloat =  inSize.width
        var height: CGFloat = inSize.height
        
        if width > maxWidth {
            width = maxWidth
            height = min(height, maxHeight) - (padding * 2)
        } else if height >  maxHeight {
            height = maxHeight
            width = min(width, maxWidth) - (padding * 2)
        }
        
        return CGSize(width: width, height: height)
    }
}

struct LargeScreenStage_Previews: PreviewProvider {
    static var previews: some View {
        LargeScreenStage {
            Text("hello")
        }
    }
}
