//
//  Int64Ext.swift
//  OctoApp
//
//  Created by Christian on 20/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation

extension Int64 {
    func asStyleFileSize(intOnly: Bool = false) -> String {
        let kb = Double(self) / 1024.0
        let mb = kb / 1024.0
        let gb = mb / 1024.0
        
        if (intOnly) {
            if (gb >= 1) {
                return String(format: "%.0f GiB", gb)
            } else if (mb >= 1) {
                return String(format: "%.0f MiB", mb)
            } else {
                return String(format: "%.0f kiB", kb)
            }
        } else {
            if (gb >= 1) {
                return String(format: "%.2f GiB", gb)
            } else if (mb >= 1) {
                return String(format: "%.1f MiB", mb)
            } else {
                return String(format: "%.0f kiB", kb)
            }
        }
    }
    
    func asDateFromMillisecondsSince1970() -> Date {
        return Date(milliseconds: self)
    }
}

extension Int {
    func asStyleFileSize(intOnly: Bool = false) -> String {
        return Int64(self).asStyleFileSize(intOnly: intOnly)
    }
}
