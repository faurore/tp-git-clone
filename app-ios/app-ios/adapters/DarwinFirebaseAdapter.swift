//
//  DarwinFirebaseAdapter.swift
//  OctoApp
//
//  Created by Christian on 27/11/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase
import FirebaseCrashlytics

class DarwinFirebaseAdapter : FirebaseAntiLogAdapter {
    
    func log(line: String) {
        Crashlytics.crashlytics().log(line)
    }
    
    func log(t: KotlinThrowable) {
        Crashlytics.crashlytics().record(error: t.asError())
    }
}
