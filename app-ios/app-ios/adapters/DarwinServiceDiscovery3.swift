//
//  DarwinServiceDiscovery3.swift
//  app-ios
//
//  Created by Christian on 13/10/2022.
//  Copyright © 2022 orgName. All rights reserved.
//

import Foundation
import OctoAppBase

class DarwinServiceDiscovery3 : NetworkServiceDiscovery {
    
    func discover(callback: @escaping (NetworkService) -> Void) async throws {
        print("NETSERVICE: BEFORE")
        let browser = NetServiceBrowser()
        let agent = DarwinServiceDiscovery3Delegate(callback: callback)
        
        browser.delegate = agent
        browser.searchForServices(ofType: "_octoprint._tcp", inDomain: "local." )
        browser.schedule(in: RunLoop.main, forMode: .common)
        RunLoop.main.run()
        
        while true {
            try await Task.sleep(nanoseconds: 150_000_000)
        }
    }
    
}

class DarwinServiceDiscovery3Delegate : NSObject, NetServiceBrowserDelegate {
    
    var agent: ServiceAgent
    
    init(callback: @escaping (NetworkService) -> Void) {
        self.agent = ServiceAgent(callback: callback)
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didFind service: NetService, moreComing: Bool) {
        print("NETSERVICE: FIND \(service)")
        service.delegate = self.agent
        service.resolve(withTimeout: 5)
    }
    
    
    func netServiceBrowserWillSearch(_ browser: NetServiceBrowser) {
        print("NETSERVICE: START")
    }
    
    func netServiceBrowserDidStopSearch(_ browser: NetServiceBrowser) {
        print("NETSERVICE: STOP")
    }
    
    func netServiceBrowser(_ browser: NetServiceBrowser, didNotSearch errorDict: [String : NSNumber]) {
        print("NETSERVICE: ERROR \(errorDict)")
    }
   

}

class ServiceAgent: NSObject, NetServiceDelegate {
    var callback: (NetworkService) -> Void
    
    init(callback: @escaping (NetworkService) -> Void) {
        self.callback = callback
    }
    
    func netServiceDidStop(_ sender: NetService) {
        print("NETSERVICE: DID STOP \(sender)")
    }
    
    func netServiceDidPublish(_ sender: NetService) {
        print("NETSERVICE: DID PUBLISH \(sender)")
    }
    
    func netServiceWillResolve(_ sender: NetService) {
        print("NETSERVICE: WILL ADDRESS \(sender)")
    }
    
    func netServiceWillPublish(_ sender: NetService) {
        print("NETSERVICE: WILL PUBLISH \(sender)")
    }
    
    func netServiceDidResolveAddress(_ sender: NetService) {
        print("NETSERVICE: ADDRESS \(sender)")
        callback(NetworkService(label: sender.name, detailLabel: sender.hostName ?? "No host", webUrl: "http://\(String(describing: sender.hostName)):80/", originQuality: 100, origin: NetworkService.Origin.dnssd))
    }
}
