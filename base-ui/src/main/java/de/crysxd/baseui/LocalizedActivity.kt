package de.crysxd.baseui

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.coroutines.runBlocking
import java.util.Locale

abstract class LocalizedActivity : AppCompatActivity() {

    override fun attachBaseContext(newBase: Context) {
        val language = runBlocking {
            BaseInjector.get().getAppLanguageUseCase().execute(Unit)
        }.appLanguage

        super.attachBaseContext(language.let {
            val config = newBase.resources.configuration
            config.setLocale(Locale.forLanguageTag(language))
            newBase.createConfigurationContext(config)
        } ?: newBase)
    }
}