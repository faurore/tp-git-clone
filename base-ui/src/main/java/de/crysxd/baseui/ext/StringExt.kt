package de.crysxd.baseui.ext

import android.content.Context
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.toBitmap
import de.crysxd.baseui.R
import de.crysxd.baseui.utils.ThemePlugin
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.engine.models.job.ProgressInformation
import io.noties.markwon.Markwon

fun String?.asPrintTimeLeftImageResource() = when (this) {
    ProgressInformation.ORIGIN_GENIUS -> R.drawable.ic_round_star_18
    else -> R.drawable.eta_ball
}

fun String?.asPrintTimeLeftImagePainter(context: Context) = asPrintTimeLeftImageResource().let { res ->
    ContextCompat.getDrawable(context, res)?.toBitmap()?.let { BitmapPainter(it.asImageBitmap()) }
}

private val markdown by lazy {
    Markwon.builder(BaseInjector.get().context())
        .usePlugin(ThemePlugin(BaseInjector.get().context()))
        .build()
}

fun String.toMarkdown(): CharSequence = markdown.toMarkdown(this)