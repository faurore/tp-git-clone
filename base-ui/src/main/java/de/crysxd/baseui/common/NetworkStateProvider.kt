package de.crysxd.baseui.common

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import timber.log.Timber

class NetworkStateProvider(coroutineScope: CoroutineScope, context: Context) {

    private val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    @RequiresApi(Build.VERSION_CODES.N)
    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            updateNetworkState()
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            updateNetworkState()
        }
    }

    private val mutableNetworkState = MutableStateFlow<NetworkState>(NetworkState.Unknown)
    val networkState = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N) {
        mutableNetworkState
            .onStart {
                connectivityManager.registerDefaultNetworkCallback(networkCallback)
                updateNetworkState()
            }
            .onCompletion {
                connectivityManager.unregisterNetworkCallback(networkCallback)
            }
            .shareIn(coroutineScope, SharingStarted.WhileSubscribed())
    } else {
        flowOf(NetworkState.Unknown)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun updateNetworkState() {
        val network = connectivityManager.activeNetwork
        val wifiConnected = if (network != null) {
            val capabilities = connectivityManager.getNetworkCapabilities(network)
            capabilities != null && capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
        } else {
            false
        }
        Timber.i("Update network state: $wifiConnected")
        mutableNetworkState.value = if (wifiConnected) NetworkState.WifiConnected else NetworkState.WifiNotConnected
    }

    sealed class NetworkState {
        object Unknown : NetworkState()
        object WifiConnected : NetworkState()
        object WifiNotConnected : NetworkState()
    }
}