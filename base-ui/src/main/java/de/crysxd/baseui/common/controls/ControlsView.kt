package de.crysxd.baseui.common.controls

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.zIndex
import androidx.navigation.NavController
import de.crysxd.baseui.R
import de.crysxd.baseui.common.OctoToolbar
import de.crysxd.baseui.compose.controls.announcement.AnnouncementControls
import de.crysxd.baseui.compose.controls.cancelobject.CancelObjectControls
import de.crysxd.baseui.compose.controls.cancelobject.rememberCancelObjectState
import de.crysxd.baseui.compose.controls.executegcode.ExecuteGcodeControls
import de.crysxd.baseui.compose.controls.executegcode.rememberExecuteGcodeControlsState
import de.crysxd.baseui.compose.controls.extrude.ExtrudeControls
import de.crysxd.baseui.compose.controls.extrude.rememberExtrudeControlsState
import de.crysxd.baseui.compose.controls.gcodepreview.GcodeControls
import de.crysxd.baseui.compose.controls.gcodepreview.rememberGcodeControlsState
import de.crysxd.baseui.compose.controls.movetool.MoveToolControls
import de.crysxd.baseui.compose.controls.movetool.rememberMoveToolControlState
import de.crysxd.baseui.compose.controls.printconfidence.rememberPrintConfidenceState
import de.crysxd.baseui.compose.controls.progress.ProgressControls
import de.crysxd.baseui.compose.controls.progress.rememberProgressControlsState
import de.crysxd.baseui.compose.controls.quickaccess.QuickAccessControls
import de.crysxd.baseui.compose.controls.quickaccess.rememberQuickAccessState
import de.crysxd.baseui.compose.controls.quickprint.QuickPrintControls
import de.crysxd.baseui.compose.controls.quickprint.rememberQuickPrintState
import de.crysxd.baseui.compose.controls.temperature.TemperatureControls
import de.crysxd.baseui.compose.controls.temperature.rememberTemperatureControlsState
import de.crysxd.baseui.compose.controls.tune.TuneControls
import de.crysxd.baseui.compose.controls.tune.rememberTuneControlsState
import de.crysxd.baseui.compose.controls.webcam.WebcamActions
import de.crysxd.baseui.compose.controls.webcam.rememberWebcamControlsState
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.base.data.models.WidgetPreferences
import de.crysxd.octoapp.base.data.models.WidgetType
import de.crysxd.octoapp.engine.models.printer.PrinterState
import timber.log.Timber
import java.util.Collections

@Composable
fun ControlsView(
    editing: Boolean,
    active: Boolean,
    flags: PrinterState.Flags,
    preferences: WidgetPreferences,
    onStopEditing: () -> Unit,
    onTypesChanged: (List<Pair<WidgetType, Boolean>>) -> Unit,
    navController: NavController,
    controlsFragment: ControlsFragment,
) = ControlsBottomBarColumn(active) {
    val types = remember(flags.asState(), preferences) {
        preferences.prepare(controlTypes[flags.asState()] ?: emptyList()).toList()
    }

    EditToolbar(
        editing = editing,
        onStopEditing = onStopEditing
    )

    Controls(
        editing = editing,
        active = active,
        onTypesChanged = onTypesChanged,
        types = types,
        navController = navController,
        controlsFragment = controlsFragment,
        flags = flags,
    )
}

//region Internals
private sealed class State {
    object Connect : State()
    object Prepare : State()
    object Print : State()
}

private fun PrinterState.Flags.asState() = when {
    isPrinting() -> State.Print
    isOperational() -> State.Prepare
    else -> State.Connect
}

private val controlTypes = mapOf(
    State.Connect to listOf(
        WidgetType.AnnouncementWidget,
        WidgetType.WebcamWidget,
        WidgetType.QuickAccessWidget,
    ),
    State.Prepare to listOf(
        WidgetType.AnnouncementWidget,
        WidgetType.ControlTemperatureWidget,
        WidgetType.WebcamWidget,
        WidgetType.MoveToolWidget,
        WidgetType.QuickPrint,
        WidgetType.ExtrudeWidget,
        WidgetType.SendGcodeWidget,
        WidgetType.QuickAccessWidget,
    ),
    State.Print to listOf(
        WidgetType.AnnouncementWidget,
        WidgetType.ProgressWidget,
        WidgetType.ControlTemperatureWidget,
        WidgetType.WebcamWidget,
        WidgetType.GcodePreviewWidget,
        WidgetType.CancelObject,
        WidgetType.TuneWidget,
        WidgetType.ExtrudeWidget,
        WidgetType.SendGcodeWidget,
        WidgetType.QuickAccessWidget
    ),
)

@Composable
private fun ColumnScope.EditToolbar(
    editing: Boolean,
    onStopEditing: () -> Unit,
) = AnimatedVisibility(
    visible = editing,
    modifier = Modifier
        .zIndex(100f)
        .fillMaxWidth(),
) {
    Row(
        horizontalArrangement = Arrangement.End,
        modifier = Modifier
            .shadow(OctoAppTheme.dimens.elementShadow)
            .background(OctoAppTheme.colors.windowBackground)
    ) {
        IconButton(onClick = onStopEditing) {
            Icon(
                painter = painterResource(id = R.drawable.ic_round_check_24),
                contentDescription = stringResource(id = R.string.cd_done),
                tint = OctoAppTheme.colors.primaryButtonBackground
            )
        }
    }
}

@Composable
private fun Controls(
    editing: Boolean,
    active: Boolean,
    types: List<Pair<WidgetType, Boolean>>,
    navController: NavController,
    onTypesChanged: (List<Pair<WidgetType, Boolean>>) -> Unit,
    controlsFragment: ControlsFragment,
    flags: PrinterState.Flags,
) {
    var localTypes by remember { mutableStateOf(types) }
    LaunchedEffect(types, editing) { localTypes = types.filter { !it.second || editing } }

    val gcodePreviewState = rememberGcodeControlsState(active = active, navController = navController, isPrinting = flags.isPrinting())
    val progressState = rememberProgressControlsState(active = active)
    val temperatureState = rememberTemperatureControlsState(active = active)
    val webcamState = rememberWebcamControlsState(active = active, isFullscreen = false, navController = navController)
    val tuneState = rememberTuneControlsState(active = active, navController = navController)
    val sendGcodeState = rememberExecuteGcodeControlsState(active = active)
    val extrudeState = rememberExtrudeControlsState(active = active)
    val moveToolState = rememberMoveToolControlState(navController = navController)
    val quickAccessState = rememberQuickAccessState(active = active, fragment = controlsFragment, flags = flags)
    val quickPrintState = rememberQuickPrintState(active = active, navController = navController, childFragmentManager = controlsFragment.childFragmentManager)
    val cancelObjectState = rememberCancelObjectState(active = active, octoActivity = controlsFragment.requireOctoActivity())
    val printConfidenceState = rememberPrintConfidenceState(active = active)
    Timber.i("outer $localTypes")
    ControlsLayout(
        toolbarState = when (flags.asState()) {
            State.Connect -> OctoToolbar.State.Connect
            State.Prepare -> OctoToolbar.State.Prepare
            State.Print -> OctoToolbar.State.Print
        },
        editing = editing,
        onMove = { from, to ->
            Timber.i("inner $localTypes")
            // Find index in our list as AnnouncementControls throw the from.index and to.index off
            val fromIndex = localTypes.indexOfFirst { it.first == from.key }.takeIf { it >= 0 } ?: return@ControlsLayout
            val toIndex = localTypes.indexOfFirst { it.first == to.key }.takeIf { it >= 0 } ?: return@ControlsLayout
            localTypes = localTypes.toMutableList().also { Collections.swap(it, fromIndex, toIndex) }
            onTypesChanged(localTypes)
        },
        items = localTypes,
        toggleHidden = { type ->
            localTypes = localTypes.map { it.first to (if (it.first == type) !it.second else it.second) }
            onTypesChanged(localTypes)
        },
    ) { type, editState ->
        when (type) {
            WidgetType.ProgressWidget -> ProgressControls(state = { progressState }, gcodeState = { gcodePreviewState }, editState = editState)
            WidgetType.GcodePreviewWidget -> GcodeControls(state = gcodePreviewState, editState = editState)
            WidgetType.WebcamWidget -> WebcamActions(state = webcamState, editState = editState, confidenceState = printConfidenceState)
            WidgetType.ControlTemperatureWidget -> TemperatureControls(state = temperatureState, editState = editState)
            WidgetType.TuneWidget -> TuneControls(state = tuneState, editState = editState)
            WidgetType.SendGcodeWidget -> ExecuteGcodeControls(state = sendGcodeState, editState = editState)
            WidgetType.ExtrudeWidget -> ExtrudeControls(state = extrudeState, editState = editState)
            WidgetType.MoveToolWidget -> MoveToolControls(state = moveToolState, editState = editState)
            WidgetType.QuickAccessWidget -> QuickAccessControls(state = quickAccessState, editState = editState)
            WidgetType.AnnouncementWidget -> AnnouncementControls(editState = editState)
            WidgetType.QuickPrint -> QuickPrintControls(state = quickPrintState, editState = editState)
            WidgetType.CancelObject -> CancelObjectControls(state = cancelObjectState, editState = editState)
            else -> Unit
        }
    }
}
//endregion

