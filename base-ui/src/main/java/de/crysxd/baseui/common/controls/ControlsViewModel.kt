package de.crysxd.baseui.common.controls

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class ControlsViewModel(
    octoPrintProvider: OctoPrintProvider,
    private val togglePausePrintJobUseCase: TogglePausePrintJobUseCase,
    private val cancelPrintJobUseCase: CancelPrintJobUseCase,
    private val instanceId: String,
    private val octoPreferences: OctoPreferences,
) : ViewModel() {

    val currentMessage = octoPrintProvider
        .passiveCurrentMessageFlow(tag = "controls-current-message", instanceId = instanceId)
//        .rateLimit(rateMs = 500)

    val flags = octoPrintProvider
        .passiveCurrentMessageFlow(tag = "controls-flags", instanceId = instanceId)
        .map { it.state.flags }
        .distinctUntilChanged()

    val leftyMode = octoPreferences.updatedFlow2.map {
        it.leftHandMode
    }.distinctUntilChanged()

    fun togglePausePrint() = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        togglePausePrintJobUseCase.execute(TogglePausePrintJobUseCase.Params(instanceId = instanceId))
    }

    fun cancelPrint() = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        cancelPrintJobUseCase.execute(CancelPrintJobUseCase.Params(restoreTemperatures = false, instanceId = instanceId))
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "ControlsViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = ControlsViewModel(
            instanceId = instanceId,
            octoPrintProvider = BaseInjector.get().octoPrintProvider(),
            togglePausePrintJobUseCase = BaseInjector.get().togglePausePrintJobUseCase(),
            cancelPrintJobUseCase = BaseInjector.get().cancelPrintJobUseCase(),
            octoPreferences = BaseInjector.get().octoPreferences(),
        ) as T
    }
}