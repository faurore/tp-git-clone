package de.crysxd.baseui.common.configureremote

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.PorterDuff
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.widget.TextViewCompat
import de.crysxd.baseui.R
import de.crysxd.baseui.databinding.ConfigureRemoteAccessUspsViewBinding

class ConfigureRemoteUspView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val binding = ConfigureRemoteAccessUspsViewBinding.inflate(LayoutInflater.from(context), this)
    private var iconColor: Int = ContextCompat.getColor(context, R.color.light_text)
    private var textColor: Int = ContextCompat.getColor(context, R.color.dark_text)

    init {
        setBackgroundResource(R.drawable.bg_input)
    }

    fun configure(
        @ColorInt iconColorInt: Int,
        @ColorInt textColorInt: Int,
        @ColorInt backgroundColorInt: Int,
    ) {
        iconColor = iconColorInt
        textColor = textColorInt
        backgroundTintList = ColorStateList.valueOf(backgroundColorInt)
        binding.title.setTextColor(textColor)
    }

    fun addUsp(@DrawableRes iconRes: Int, description: CharSequence) {
        val icon = AppCompatImageView(context).apply {
            id = ViewCompat.generateViewId()
            setImageResource(iconRes)
            setColorFilter(iconColor, PorterDuff.Mode.SRC_IN)
        }
        val text = AppCompatTextView(context).apply {
            id = ViewCompat.generateViewId()
            text = description
            TextViewCompat.setTextAppearance(this, R.style.OctoTheme_TextAppearance)
            setTextColor(textColor)
        }
        binding.flow.referencedIds = binding.flow.referencedIds.toMutableList().apply {
            add(icon.id)
            add(text.id)
        }.toIntArray()
        addView(icon, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        addView(text, 0, LayoutParams.WRAP_CONTENT)
    }
}