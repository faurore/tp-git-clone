package de.crysxd.baseui.compose.controls.printconfidence

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.framework.isSpaghettiDetectiveUrl
import de.crysxd.octoapp.sharedexternalapis.printconfidence.PrintConfidence
import de.crysxd.octoapp.sharedexternalapis.printconfidence.obico.ObicoPrintConfidenceProvider
import de.crysxd.octoapp.sharedexternalapis.printconfidence.octoeverywhere.OctoEverywherePrintConfidenceProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import timber.log.Timber

class PrintConfidenceControlsViewModel(
    private val instanceId: String,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintProvider: OctoPrintProvider,
    private val octoEverywherePrintConfidenceProvider: OctoEverywherePrintConfidenceProvider,
    private val obicoPrintConfidenceProvider: ObicoPrintConfidenceProvider,
) : ViewModel() {

    val state: Flow<PrintConfidence?> = octoPrintRepository.instanceInformationFlow(instanceId)
        .filterNotNull()
        .map { determinePrintConfidenceService(it) }
        .distinctUntilChanged()
        .emitEvents()
        .retry(3)
        .catch { Timber.e("Stopping print quality", it) }
        .onStart { Timber.i("Observing print confidence for $instanceId") }
        .onCompletion { Timber.i("Stopping print confidence for $instanceId") }
        .shareIn(viewModelScope, started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 5000), replay = 1)

    private fun determinePrintConfidenceService(instance: OctoPrintInstanceInformationV3): Service? {
        val oeToken = instance.octoEverywhereConnection?.apiToken

        return when {
            oeToken != null -> Service.OctoEverywhere(oeToken)
            instance.webUrl.isSpaghettiDetectiveUrl() -> Service.Obico(baseUrl = instance.webUrl.toString(), apiKey = instance.apiKey)
            instance.alternativeWebUrl?.isSpaghettiDetectiveUrl() == true -> Service.Obico(baseUrl = instance.alternativeWebUrl.toString(), apiKey = instance.apiKey)
            else -> null
        }
    }

    private fun isPrinting() = octoPrintProvider.passiveCurrentMessageFlow(tag = "print-confidence", instanceId = instanceId)
        .map { it.state.flags.isPrinting() }
        .distinctUntilChanged()

    @OptIn(ExperimentalCoroutinesApi::class)
    private fun Flow<Service?>.emitEvents(): Flow<PrintConfidence?> = combine(isPrinting()) { service, isPrinting ->
        service to isPrinting
    }.flatMapLatest { (service, isPrinting) ->
        // Do not use any service if we are not printing
        if (!isPrinting) return@flatMapLatest flowOf(null)

        when (service) {
            is Service.OctoEverywhere -> {
                Timber.i("Print confidence from OctoEverywhere's Gadget")
                octoEverywherePrintConfidenceProvider.forAppKey(service.appKey).checkPrintQuality()
            }

            is Service.Obico -> {
                Timber.i("Print confidence from Obico")
                obicoPrintConfidenceProvider.forBaseUrlAndApiKey(baseUrl = service.baseUrl, apiKey = service.apiKey).checkPrintQuality()
            }

            null -> {
                Timber.i("Print confidence not available")
                flowOf(null)
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "PrintConfidenceControlsViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = PrintConfidenceControlsViewModel(
            instanceId = instanceId,
            octoPrintProvider = BaseInjector.get().octoPrintProvider(),
            octoPrintRepository = BaseInjector.get().octorPrintRepository(),
            octoEverywherePrintConfidenceProvider = OctoEverywherePrintConfidenceProvider(),
            obicoPrintConfidenceProvider = ObicoPrintConfidenceProvider()
        ) as T
    }

    private sealed class Service {
        data class OctoEverywhere(val appKey: String) : Service()
        data class Obico(val baseUrl: String, val apiKey: String) : Service()
    }
}