package de.crysxd.baseui.compose.controls.cancelobject

import android.graphics.RectF
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.exceptions.MissingPluginException
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.CancelObjectUseCase
import de.crysxd.octoapp.base.usecase.TriggerInitialCancelObjectMessageUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.dto.message.CancelObjectPluginMessage
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class CancelObjectViewModel(
    private val instanceId: String,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPreferences: OctoPreferences,
    private val octoPrintProvider: OctoPrintProvider,
    private val triggerInitialCancelObjectMessageUseCase: TriggerInitialCancelObjectMessageUseCase,
    private val cancelObjectUseCase: CancelObjectUseCase,
) : GenericLoadingControlsViewModel<List<CancelObjectViewModel.ObjectList>>() {

    private val loadingObjects = MutableStateFlow(emptySet<Int>())
    private val retryTrigger = MutableStateFlow(0)
    private var requestJob: Job? = null
    private var cache = CancelObjectPluginMessage()
    var stateCache: ViewState<ObjectList> = ViewState.Loading
        private set
    val state = retryTrigger.flatMapLatest {
        octoPrintProvider.passiveCachedMessageFlow(
            tag = "cancel-object-vm",
            clazz = CancelObjectPluginMessage::class,
            instanceId = instanceId
        )
    }.combine(loadingObjects) { message, loading ->
        // If messages contains a non-null empty object list we need to reset the cache. This message indicates state reset
        if (message?.objects?.isEmpty() == true) {
            Timber.i("Empty object list, resetting")
            cache = cache.copy(activeId = null)
        }

        // Merge message with previous ones. We always get one field only, so we need to merge
        // Also map the object to ensure consistent active flag
        val activeId = message?.activeId ?: cache.activeId
        cache = cache.copy(
            activeId = activeId,
            objects = (message?.objects ?: cache.objects)?.map { it.copy(active = it.id == activeId) },
        )

        // Map objects
        val objects = cache.objects?.filter { it.id != null && it.ignore != true }?.map {
            PrintObject(
                objectId = it.id!!,
                cancelled = it.cancelled == true,
                active = it.active == true,
                label = it.label ?: it.id.toString(),
                loading = loading.contains(it.id),
                boundingBox = it.boundingBox
            )
        }

        // If we have too many objects, take 2 before and 2 after the active
        val maxElements = 5
        val elementsBefore = (maxElements - 1) / 2
        val subSet = if (objects != null && objects.size > maxElements) {
            val activeIndex = objects.firstOrNull { it.active }?.let { objects.indexOf(it) } ?: 0
            var startIndex = (activeIndex - elementsBefore).coerceAtLeast(0)
            val endIndex = (startIndex + maxElements).coerceAtMost(objects.size)
            startIndex = (endIndex - maxElements).coerceAtLeast(0)

            objects.subList(startIndex, endIndex)
        } else {
            objects ?: emptyList()
        }

        when {
            objects == null -> ViewState.Loading
            objects.isEmpty() -> ViewState.Hidden
            else -> ViewState.Data(
                data = ObjectList(all = objects, subset = subSet),
                contentKey = objects.joinToString { it.label + it.objectId }
            )
        }
    }.onEach {
        stateCache = it
    }.onStart {
        emit(stateCache)
        cache = CancelObjectPluginMessage()
        triggerInitialMessage()
    }.catch {
        if (it is MissingPluginException) {
            emit(ViewState.Hidden)
        } else {
            emit(ViewState.Error(it))
            Timber.e(it)
        }
    }.shareIn(
        scope = viewModelScope,
        started = SharingStarted.WhileSubscribed(stopTimeoutMillis = 5000),
        replay = 1
    ).onStart {
        triggerInitialMessage()

        // Trigger again after 5s as the first sometimes does not seem to work...
        requestJob?.cancel()
        requestJob = viewModelScope.launch {
            delay(5000)
            triggerInitialMessage()
        }
    }.onEach {
        if (it is ViewState.Data) requestJob?.cancel()
    }

    private suspend fun triggerInitialMessage() = try {
        when {
            !octoPreferences.activePluginIntegrations -> Timber.i("Skipping CancelObject initial message, no active plugin integrations allowed")
            octoPrintRepository.get(instanceId)?.hasPlugin(OctoPlugins.CancelObject) != true -> Timber.i("CancelObject not installed")
            else -> {
                // Delay slightly so other more important requests go first
                val delay = 2.seconds
                Timber.i("Requesting objects after $delay")
                delay(delay)
                triggerInitialCancelObjectMessageUseCase.execute(param = TriggerInitialCancelObjectMessageUseCase.Params(instanceId))
            }
        }
    } catch (e: Exception) {
        Timber.e(e)
    }

    private val CancelObjectPluginMessage.Object.boundingBox: RectF?
        get() {
            return RectF(
                minX ?: return null,
                minY ?: return null,
                maxX ?: return null,
                maxY ?: return null,
            )
        }

    fun cancelObject(objectId: Int) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        loadingObjects.update { it + objectId }
        cancelObjectUseCase.execute(CancelObjectUseCase.Params(instanceId = instanceId, objectId = objectId))
        delay(timeMillis = 500)
        triggerInitialMessage()
    }.invokeOnCompletion {
        loadingObjects.update { it - objectId }
    }

    override fun retry() {
        stateCache = ViewState.Loading
        retryTrigger.update { it + 1 }
    }

    data class PrintObject(
        val objectId: Int,
        val cancelled: Boolean,
        val loading: Boolean,
        val active: Boolean,
        val label: String,
        val boundingBox: RectF?,
    )

    data class ObjectList(
        val all: List<PrintObject>,
        val subset: List<PrintObject>,
    )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "CancelObjectViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = CancelObjectViewModel(
            instanceId = instanceId,
            octoPrintProvider = BaseInjector.get().octoPrintProvider(),
            triggerInitialCancelObjectMessageUseCase = BaseInjector.get().triggerInitialCancelObjectMessageUseCase(),
            cancelObjectUseCase = BaseInjector.get().cancelObjectUseCase(),
            octoPreferences = BaseInjector.get().octoPreferences(),
            octoPrintRepository = BaseInjector.get().octorPrintRepository(),
        ) as T
    }
}