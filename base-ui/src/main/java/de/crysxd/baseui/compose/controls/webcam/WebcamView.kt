package de.crysxd.baseui.compose.controls.webcam

import android.app.ActionBar
import android.graphics.Bitmap
import android.graphics.Matrix
import android.view.TextureView
import android.view.ViewGroup
import android.widget.ImageView
import androidx.annotation.VisibleForTesting
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.AnimatedVisibilityScope
import androidx.compose.animation.expandHorizontally
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkHorizontally
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.viewinterop.AndroidView
import com.google.android.exoplayer2.PlaybackException
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.analytics.AnalyticsListener
import com.google.android.exoplayer2.video.VideoSize
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.framework.Badge
import de.crysxd.baseui.compose.framework.IconAction
import de.crysxd.baseui.compose.framework.LiveBadge
import de.crysxd.baseui.compose.framework.SmallPrimaryButton
import de.crysxd.baseui.compose.framework.SmallSecondaryButton
import de.crysxd.baseui.compose.framework.rememberValue
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.concurrent.atomic.AtomicInteger

@VisibleForTesting
var frame1Callback: () -> Unit = {}

@VisibleForTesting
var frame200Callback: () -> Unit = {}

@VisibleForTesting
val frameCounter = AtomicInteger(0)

@Composable
fun BoxScope.WebcamView(
    state: WebcamControlsState,
    boxState: AspectRatioBoxScope,
    allowTouch: Boolean,
    insets: PaddingValues = PaddingValues(),
) {
    val previous = remember { StateHolder() }
    val current = state.current

    Timber.i("Webcam is now $state")
    WebcamState(
        state = state,
        currentState = current,
        previousState = previous.state,
        boxState = boxState,
        allowTouch = allowTouch,
        insets = insets,
    )

    // Update last state if not
    if (state.current !is WebcamControlsViewModel.UiState.Error) {
        previous.state = current
    }
}

val WebcamOverlayBrush = Brush.verticalGradient(
    0.0f to Color.Black.copy(alpha = 0f),
    0.8f to Color.Black.copy(alpha = 0.4f)
)

//region Controls
@Composable
fun WebcamActions(
    webcamState: WebcamControlsState,
    horizontal: Boolean = true,
    additionalActions: @Composable () -> Unit,
) {
    val canSwitchWebcam = webcamState.current.canSwitchWebcam
    val canGrabImage = webcamState.grabImage != null
    val exception = (webcamState.current as? WebcamControlsViewModel.UiState.Error)?.exception
    val supportsFullscreen = webcamState.isFullscreen || webcamState.current.let {
        it is WebcamControlsViewModel.UiState.FrameReady || it is WebcamControlsViewModel.UiState.RichStreamReady
    }

    //region Switch
    AnimatedActionVisibility(horizontal = horizontal, visible = canSwitchWebcam) {
        IconAction(
            onClick = webcamState::onSwitchWebcam,
            tint = Color.White,
            painter = painterResource(id = R.drawable.ic_round_camera_front_24),
            contentDescription = stringResource(id = R.string.cd_switch_camera),
        )
    }
    //endregion
    //region Share
    AnimatedActionVisibility(horizontal = horizontal, visible = canGrabImage) {
        IconAction(
            onClick = { webcamState.grabImage?.let(webcamState::onShareImage) },
            tint = Color.White,
            painter = painterResource(id = R.drawable.ic_round_share_24),
            contentDescription = stringResource(id = R.string.cd_share),
        )
    }
    //endregion
    additionalActions()
    //region Fullscreen
    AnimatedActionVisibility(horizontal = horizontal, visible = supportsFullscreen) {
        IconAction(
            onClick = webcamState::onFullscreenClicked,
            tint = Color.White,
            painter = painterResource(id = if (webcamState.isFullscreen) R.drawable.ic_round_fullscreen_exit_24 else R.drawable.ic_round_fullscreen_24),
            contentDescription = stringResource(id = R.string.cd_fullscreen),
        )
    }
    //endregion
    //region Error info
    val localException = exception
    val octoActivity = OctoAppTheme.octoActivity
    AnimatedActionVisibility(horizontal = horizontal, visible = localException != null) {
        IconAction(
            onClick = { localException?.let { octoActivity?.showDialog(it) } },
            tint = Color.White,
            painter = painterResource(id = R.drawable.ic_round_info_24),
            contentDescription = stringResource(id = R.string.cd_details),
        )
    }
    //endregion
}

@Composable
private fun AnimatedActionVisibility(visible: Boolean, horizontal: Boolean, content: @Composable AnimatedVisibilityScope.() -> Unit) = AnimatedVisibility(
    visible = visible,
    content = content,
    enter = fadeIn() + if (horizontal) expandHorizontally() else expandVertically(),
    exit = fadeOut() + if (horizontal) shrinkHorizontally() else shrinkVertically(),
)

//endregion
//region State machine
@Composable
private fun BoxScope.WebcamState(
    state: WebcamControlsState,
    currentState: WebcamControlsViewModel.UiState,
    previousState: WebcamControlsViewModel.UiState?,
    boxState: AspectRatioBoxScope,
    insets: PaddingValues,
    allowTouch: Boolean,
): Unit = when (currentState) {
    is WebcamControlsViewModel.UiState.Loading -> {
        SideEffect { state.grabImage = null }
        WebcamLoading()
    }

    is WebcamControlsViewModel.UiState.RichStreamDisabled -> RichStreamDisabled()

    is WebcamControlsViewModel.UiState.WebcamNotAvailable -> WebcamNotAvailable(currentState)

    is WebcamControlsViewModel.UiState.FrameReady -> WebcamMjpeg(
        state = currentState,
        webcamState = state,
        webcamBoxState = boxState,
        getInitialScaleToFillType = state::getInitialScaleType,
        updateScaleToFillType = state::updateScaleToFillType,
        insets = insets,
        allowTouch = allowTouch,
    )

    is WebcamControlsViewModel.UiState.RichStreamReady -> WebcamRich(
        state = currentState,
        webcamState = state,
        webcamBoxState = boxState,
        getInitialScaleToFillType = state::getInitialScaleType,
        updateScaleToFillType = state::updateScaleToFillType,
        onRetry = state::onRetry,
        insets = insets,
        onTroubleShoot = state::onTroubleshoot,
    )

    is WebcamControlsViewModel.UiState.Error -> if (currentState.isManualReconnect) {
        SideEffect { state.grabImage = null }
        WebcamError(error = currentState, onRetry = state::onRetry, onTroubleShoot = state::onTroubleshoot)
    } else {
        previousState?.let {
            WebcamState(
                state = state,
                currentState = previousState,
                previousState = null,
                boxState = boxState,
                allowTouch = allowTouch,
                insets = insets,
            )
        }
        WebcamLoading()
    }
}

//endregion
//region Error
@Composable
private fun WebcamError(
    error: WebcamControlsViewModel.UiState.Error,
    onTroubleShoot: () -> Unit,
    onRetry: () -> Unit,
) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    verticalArrangement = Arrangement.Center,
    modifier = Modifier
        .fillMaxSize()
        .background(OctoAppTheme.colors.blackTranslucent)
        .padding(OctoAppTheme.dimens.margin2)
        .padding(bottom = OctoAppTheme.dimens.margin2)
) {
    Title(text = stringResource(id = R.string.connection_failed))
    error.streamUrl?.let { Detail(text = error.streamUrl) }
    Row(
        horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1)
    ) {
        SmallSecondaryButton(
            text = stringResource(id = R.string.trouble_shooting),
            modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
            onClick = onTroubleShoot
        )
        Action(
            text = stringResource(id = R.string.retry_general),
            onClick = onRetry
        )
    }
}

//endregion
//region Not available
@Composable
private fun WebcamNotAvailable(state: WebcamControlsViewModel.UiState.WebcamNotAvailable) = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
) {
    Title(text = stringResource(id = state.text))
}

//endregion
//region Rich stream disabled
@Composable
private fun RichStreamDisabled() = Column(
    horizontalAlignment = Alignment.CenterHorizontally,
    modifier = Modifier.padding(OctoAppTheme.dimens.margin2)
) {
    Title(text = stringResource(id = R.string.supporter_perk___title))
    Detail(text = stringResource(id = R.string.supporter_perk___description, stringResource(id = R.string.rich_stream_disabled_title)))
    Action(text = stringResource(id = R.string.support_octoapp), onClick = {
        UriLibrary.getPurchaseUri().open()
    })
}

//endregion
//region Mjpeg stream
@Composable
private fun BoxScope.WebcamMjpeg(
    webcamState: WebcamControlsState,
    state: WebcamControlsViewModel.UiState.FrameReady,
    allowTouch: Boolean,
    insets: PaddingValues,
    updateScaleToFillType: (ImageView.ScaleType) -> Unit,
    getInitialScaleToFillType: () -> ImageView.ScaleType,
    webcamBoxState: AspectRatioBoxScope,
) {
    //region Testing
    var dimension by remember { mutableStateOf(IntSize(0, 0)) }
    var isLive by remember { mutableStateOf(true) }
    val liveJob = rememberValue<Job?>(null)
    val scope = rememberCoroutineScope()
    //endregion
    //region Update size of outer box
    updateVideoSize(
        state = webcamBoxState,
        nativeHeight = dimension.height,
        nativeWidth = dimension.width,
        rotate90 = state.rotate90,
        enforcedAspectRatio = state.enforcedAspectRatio,
    )
    //endregion
    //region Webcam image
    AndroidView(
        factory = {
            val mv = MatrixView(it)
            val iv = ImageView(it)
            mv.addView(iv, ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
            mv.allowTouch = allowTouch
            mv.onScaleToFillChanged = { fill ->
                updateScaleToFillType(if (fill) ImageView.ScaleType.CENTER_CROP else ImageView.ScaleType.FIT_CENTER)
            }
            mv.scaleToFill = getInitialScaleToFillType() == ImageView.ScaleType.CENTER_CROP
            mv
        },
        update = {
            // We delay the state read as long as possible. This update block is the only thing executed for each frame, no recomposition is triggered
            // unless the dimension changes or the live badge is hidden

            val frame = state.frame.value
            val iv = it.getChildAt(0) as ImageView
            it.matrixInput = MatrixView.MatrixInput(
                flipH = state.flipH,
                flipV = state.flipV,
                rotate90 = state.rotate90,
                contentHeight = frame.height,
                contentWidth = frame.width,
            )
            iv.setImageBitmap(frame)

            //region State updates
            dimension = IntSize(frame.width, frame.height)
            isLive = true
            liveJob.value?.cancel()
            liveJob.value = scope.launch {
                delay(state.nextFrameDelayMs ?: 1000)
                isLive = false
            }
            //endregion
            //region Testing
            val fc = frameCounter.getAndIncrement()
            if (fc == 0) {
                frame1Callback()
            }
            if (fc == 200) {
                frame200Callback()
            }
            //endregion
        },
        modifier = Modifier.fillMaxWidth()
    )
    //endregion
    //region Indicators
    LiveBadge(
        visible = isLive,
        animateCountDownTo = state.nextFrameDelayMs?.let { System.currentTimeMillis() + it },
        modifier = Modifier
            .padding(insets)
            .padding(OctoAppTheme.dimens.margin1)
            .align(Alignment.TopEnd)
    )
    Badge(
        text = "${dimension.height}p",
        visible = state.showResolution,
        modifier = Modifier
            .padding(insets)
            .padding(OctoAppTheme.dimens.margin1)
            .align(Alignment.TopStart)
    )
    //endregion
    //region Actions
    webcamState.grabImage = {
        val matrix = Matrix()
        if (state.flipV) matrix.postScale(1f, -1f)
        if (state.flipH) matrix.postScale(-1f, 1f)
        if (state.rotate90) matrix.postRotate(-90f)
        Bitmap.createBitmap(state.frame.value, 0, 0, state.frame.value.width, state.frame.value.height, matrix, true)
    }
    //endregion
}

//endregion
//region Rich stream
@Composable
private fun WebcamRich(
    webcamState: WebcamControlsState,
    state: WebcamControlsViewModel.UiState.RichStreamReady,
    insets: PaddingValues,
    updateScaleToFillType: (ImageView.ScaleType) -> Unit,
    getInitialScaleToFillType: () -> ImageView.ScaleType,
    onRetry: () -> Unit,
    onTroubleShoot: () -> Unit,
    webcamBoxState: AspectRatioBoxScope,
) {
    //region Update size of outer box
    var videoSize by remember { mutableStateOf<IntSize?>(null) }
    DisposableEffect(Unit) {
        val listener = object : Player.Listener {
            override fun onVideoSizeChanged(vs: VideoSize) {
                super.onVideoSizeChanged(vs)
                videoSize = IntSize(vs.width, vs.height)

            }
        }

        state.player.addListener(listener)
        onDispose { state.player.removeListener(listener) }
    }
    videoSize?.let {
        updateVideoSize(
            state = webcamBoxState,
            nativeHeight = it.height,
            nativeWidth = it.width,
            rotate90 = state.rotate90,
            enforcedAspectRatio = state.enforcedAspectRatio,
        )
    }
    //endregion
    //region Analytics
    var loading by remember { mutableStateOf(!state.player.isPlaying) }
    var exception by remember { mutableStateOf<Exception?>(null) }
    DisposableEffect(Unit) {
        val listener = object : AnalyticsListener {
            override fun onIsPlayingChanged(eventTime: AnalyticsListener.EventTime, isPlaying: Boolean) {
                super.onIsPlayingChanged(eventTime, isPlaying)
                Timber.v("onIsPlayingChanged: $isPlaying")
                loading = !isPlaying

                if (isPlaying) {
                    exception = null
                }
            }

            override fun onPlayerError(eventTime: AnalyticsListener.EventTime, error: PlaybackException) {
                super.onPlayerError(eventTime, error)
                Timber.v("onPlayerError")
                loading = false
                exception = error

            }
        }

        state.player.addAnalyticsListener(listener)
        state.player.play()
        onDispose { state.player.removeAnalyticsListener(listener) }
    }
    //endregion
    //region View
    AndroidView(
        factory = {
            val mv = MatrixView(it)
            val tv = TextureView(it)
            state.player.setVideoTextureView(tv)
            webcamState.grabImage = { requireNotNull(tv.bitmap) { "Texture bitmap was null" } }

            mv.addView(tv, ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))
            mv.allowTouch = false
            mv.onScaleToFillChanged = { fill ->
                updateScaleToFillType(if (fill) ImageView.ScaleType.CENTER_CROP else ImageView.ScaleType.FIT_CENTER)
            }
            mv.scaleToFill = getInitialScaleToFillType() == ImageView.ScaleType.CENTER_CROP
            mv
        },
        update = {
            videoSize?.let { vs ->
                it.matrixInput = MatrixView.MatrixInput(
                    flipH = state.flipH,
                    flipV = state.flipV,
                    rotate90 = state.rotate90,
                    contentHeight = vs.height,
                    contentWidth = vs.width,
                )
            }
        },
        modifier = Modifier.fillMaxWidth()
    )
    //endregion
    //region Indicator
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(insets)
    ) {
        var live by remember { mutableStateOf(false) }
        LaunchedEffect(Unit) {
            while (isActive) {
                val isLive = state.player.isCurrentMediaItemLive
                val delay = state.player.currentLiveOffset
                live = isLive && delay < LiveDelayThresholdMs
                delay(1000)
            }
        }
        LiveBadge(
            visible = live && !loading && exception == null,
            modifier = Modifier
                .align(Alignment.TopEnd)
                .padding(OctoAppTheme.dimens.margin1)
        )
        Badge(
            text = videoSize?.let { "${it.height}p" } ?: "",
            visible = !loading && exception == null && videoSize != null && state.showResolution,
            modifier = Modifier
                .align(Alignment.TopStart)
                .padding(OctoAppTheme.dimens.margin1)
        )
    }
    //endregion
    //region Nested states
    if (loading && exception == null) {
        WebcamLoading()
    }

    exception?.let {
        WebcamError(
            onRetry = onRetry,
            error = WebcamControlsViewModel.UiState.Error(
                isManualReconnect = true,
                webcamCount = state.webcamCount,
                exception = it,
                streamUrl = state.uri.toString(),
            ),
            onTroubleShoot = onTroubleShoot,
        )
    }
    //endregion
}

//endregion
//region Loading
@Composable
private fun WebcamLoading() = Box(
    modifier = Modifier
        .background(OctoAppTheme.colors.blackTranslucent)
        .fillMaxSize(),
    contentAlignment = Alignment.Center,
) {
    CircularProgressIndicator(
        color = Color.White
    )
}

//endregion
//region Internals
@Composable
private fun Title(
    text: String,
    maxLines: Int = 3,
) = Text(
    text = text,
    style = OctoAppTheme.typography.subtitle,
    color = OctoAppTheme.colors.white,
    textAlign = TextAlign.Center,
    maxLines = maxLines,
    overflow = TextOverflow.Ellipsis,
)

@Composable
private fun Detail(text: String) = Text(
    text = text,
    style = OctoAppTheme.typography.label,
    color = OctoAppTheme.colors.whiteTranslucent,
    textAlign = TextAlign.Center,
    maxLines = 2,
    modifier = Modifier.padding(top = OctoAppTheme.dimens.margin01),
    overflow = TextOverflow.Ellipsis,
)

@Composable
private fun Action(
    text: String,
    onClick: () -> Unit
) = SmallPrimaryButton(
    text = text,
    modifier = Modifier.padding(top = OctoAppTheme.dimens.margin2),
    onClick = onClick
)

private fun updateVideoSize(
    state: AspectRatioBoxScope,
    enforcedAspectRatio: String?,
    nativeWidth: Int,
    nativeHeight: Int,
    rotate90: Boolean,
) {
    // Skip for invalid sizes
    if (nativeHeight == 0 || nativeWidth == 0) return

    val nativeAspectRatio = if (rotate90) {
        nativeHeight / nativeWidth.toFloat()
    } else {
        nativeWidth / nativeHeight.toFloat()
    }
    state.aspectRatio = enforcedAspectRatio?.let {
        try {
            val (w, h) = it.split(":")
            w.toFloat() / h.toFloat()
        } catch (e: Exception) {
            Timber.e(e, "Failed to parse aspect ratio: $it")
            null
        }
    } ?: nativeAspectRatio
}
//endregion