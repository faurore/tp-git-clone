package de.crysxd.baseui.compose.controls.cancelobject

import android.graphics.Paint
import android.graphics.PointF
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.viewinterop.AndroidView
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.fragment.navArgs
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.GenericLoadingControlsViewModel
import de.crysxd.baseui.compose.controls.gcodepreview.GcodePreviewControlsViewModel
import de.crysxd.baseui.compose.framework.ComposeBottomSheetFragment
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.framework.PrimaryButton
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.gcode.render.GcodeRenderView
import de.crysxd.octoapp.base.gcode.render.models.GcodeRenderContext
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CancelObjectBottomSheet : ComposeBottomSheetFragment() {

    override val instanceId
        get() = navArgs<CancelObjectBottomSheetArgs>().value.instanceId

    @Composable
    override fun Content() {
        //region State
        val padding = with(LocalDensity.current) { OctoAppTheme.dimens.margin2.roundToPx() }
        val gcodeState by rememberGcodePreviewControlsViewModel().state.collectAsState(initial = GcodePreviewControlsViewModel.ViewState.Loading())
        val cancelObjectViewModel = rememberCancelObjectViewModel()
        val renderState by rememberRenderViewModel().state.collectAsState(initial = CancelObjectRenderViewModel.State.Loading)
        val cancelObjectState by cancelObjectViewModel.state.collectAsState(initial = GenericLoadingControlsViewModel.ViewState.Loading)
        var ready by remember { mutableStateOf(false) }
        val alpha by animateFloatAsState(targetValue = if (ready) 1f else 0f)
        var selectedObject by remember { mutableStateOf<CancelObjectViewModel.PrintObject?>(null) }

        LaunchedEffect(cancelObjectState::class) {
            val cos = cancelObjectState
            if (cos is GenericLoadingControlsViewModel.ViewState.Error) {
                requireOctoActivity().showDialog(cos.exception)
                dismissAllowingStateLoss()
            }
        }
        //endregion

        Column {
            //region Title
            Text(
                text = stringResource(id = R.string.widget_cancel_object),
                style = OctoAppTheme.typography.title,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = OctoAppTheme.dimens.margin1),
            )
            //endregion
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .padding(OctoAppTheme.dimens.margin2)
                    .clip(MaterialTheme.shapes.large)
                    .background(OctoAppTheme.colors.inputBackground)
            ) {
                //region Loading
                CircularProgressIndicator(
                    modifier = Modifier.graphicsLayer { this.alpha = (1f - alpha) },
                    color = OctoAppTheme.colors.accent
                )
                //endregion
                //region Render
                AndroidView(
                    modifier = Modifier
                        .fillMaxWidth()
                        .aspectRatio(ratio = 1f),
                    factory = {
                        GcodeRenderView(it).apply {
                            setPadding(padding, padding, padding, padding)
                        }
                    },
                    update = { view ->
                        val readyState = (renderState as? CancelObjectRenderViewModel.State.Ready) ?: return@AndroidView
                        val rs = readyState.renderStyle
                        val pp = readyState.printerProfile
                        val s = readyState.settings
                        ready = true

                        view.renderParams = GcodeRenderView.RenderParams(
                            renderContext = (gcodeState as? GcodePreviewControlsViewModel.ViewState.DataReady)?.state?.value?.renderContext ?: GcodeRenderContext.Empty,
                            renderStyle = rs,
                            originInCenter = pp.volume.origin == PrinterProfile.Origin.Center,
                            printBedSizeMm = PointF(pp.volume.width, pp.volume.depth),
                            extrusionWidthMm = pp.extruder.nozzleDiameter,
                            quality = s.quality,
                        )

                        val objects = (cancelObjectState as? GenericLoadingControlsViewModel.ViewState.Data)?.data?.all ?: emptyList()
                        val selectedId = selectedObject?.objectId
                        val activePaint = Paint().apply {
                            color = ContextCompat.getColor(requireContext(), R.color.yellow)
                            style = Paint.Style.FILL
                            isAntiAlias = s.quality == GcodePreviewSettings.Quality.Ultra
                        }
                        val cancelledPaint = Paint().apply {
                            color = ContextCompat.getColor(requireContext(), R.color.light_grey)
                            style = Paint.Style.FILL
                            isAntiAlias = s.quality == GcodePreviewSettings.Quality.Ultra
                        }
                        val selectedPaint = Paint().apply {
                            color = ContextCompat.getColor(requireContext(), R.color.accent)
                            style = Paint.Style.FILL
                            isAntiAlias = s.quality == GcodePreviewSettings.Quality.Ultra
                        }

                        view.overlay = GcodeRenderView.Overlay(
                            onPaint = {
                                objects.forEach {
                                    val paint = when {
                                        it.cancelled -> cancelledPaint
                                        selectedId == it.objectId -> selectedPaint
                                        else -> activePaint
                                    }
                                    val cx = it.boundingBox?.centerX() ?: return@forEach
                                    val cy = it.boundingBox.centerY()
                                    val r = 8f
                                    paint.alpha = 128
                                    drawCircle(cx, cy, r, paint)

                                    if (!it.cancelled) {
                                        paint.alpha = 255
                                        drawCircle(cx, cy, r / 2, paint)
                                    }
                                }
                            },
                            onClick = { x, y ->
                                // Search clicked offset async
                                viewLifecycleOwner.lifecycleScope.launchWhenCreated {
                                    withContext(Dispatchers.Default) {
                                        selectedObject = objects.filter { !it.cancelled }.firstOrNull { it.boundingBox?.contains(x, y) == true }
                                    }
                                }

                                true
                            }
                        )
                    }
                )
                //endregion
            }
            //region Options
            AnimatedVisibility(
                visible = selectedObject != null,
                enter = fadeIn() + expandVertically(expandFrom = Alignment.Top),
                exit = fadeOut() + shrinkVertically(shrinkTowards = Alignment.Top),
            ) {
                PrimaryButton(
                    text = stringResource(id = R.string.widget_cancel_object),
                    modifier = Modifier
                        .padding(horizontal = OctoAppTheme.dimens.margin2)
                        .padding(top = OctoAppTheme.dimens.margin1, bottom = OctoAppTheme.dimens.margin2),
                    onClick = {
                        selectedObject?.let {
                            cancelAfterConfirmation(it, cancelObjectViewModel) {
                                selectedObject = null
                            }
                        }
                    }
                )
            }
            //endregion
        }
    }

    private fun cancelAfterConfirmation(
        obj: CancelObjectViewModel.PrintObject,
        viewModel: CancelObjectViewModel,
        onConfirmed: () -> Unit,
    ) = requireOctoActivity().showDialog(
        message = OctoActivity.Message.DialogMessage(
            text = { getString(R.string.cancel_object___confirmation_message, obj.label) },
            positiveButton = { getString(R.string.cancel_object___confirmation_positive) },
            negativeButton = { getString(R.string.cancel_object___confirmation_negative) },
            positiveAction = {
                onConfirmed()
                viewModel.cancelObject(obj.objectId)
            }
        )
    )

    @Composable
    private fun rememberCancelObjectViewModel(): CancelObjectViewModel {
        val vmFactory = CancelObjectViewModel.Factory(LocalOctoPrint.current.id)
        return viewModel(
            modelClass = CancelObjectViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory,
            viewModelStoreOwner = requireNotNull(OctoAppTheme.octoActivity)
        )
    }

    @Composable
    private fun rememberRenderViewModel(): CancelObjectRenderViewModel {
        val vmFactory = CancelObjectRenderViewModel.Factory(LocalOctoPrint.current.id)
        return viewModel(
            modelClass = CancelObjectRenderViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory,
        )
    }

    @Composable
    private fun rememberGcodePreviewControlsViewModel(): GcodePreviewControlsViewModel {
        val vmFactory = GcodePreviewControlsViewModel.Factory(LocalOctoPrint.current.id)
        return viewModel(
            modelClass = GcodePreviewControlsViewModel::class.java,
            key = vmFactory.id,
            factory = vmFactory,
        ).also {
            it.useLive()
        }
    }
}