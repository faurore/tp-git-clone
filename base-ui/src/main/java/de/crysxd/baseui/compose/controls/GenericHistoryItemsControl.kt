package de.crysxd.baseui.compose.controls

import android.content.Context
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import de.crysxd.baseui.compose.framework.AnimatedFlowRow
import de.crysxd.baseui.compose.framework.SmallPrimaryButton
import de.crysxd.baseui.compose.framework.SmallSecondaryButton
import de.crysxd.baseui.compose.theme.OctoAppTheme

@Composable
fun <T> GenericHistoryItemsControl(
    editState: EditState,
    state: GenericHistoryItemsControlState<T>,
    title: String?,
    otherOptionText: String,
    onOtherOptionClicked: () -> Unit,
    isPinned: (T) -> Boolean,
    text: Context.(T) -> String,
    announcement: (@Composable () -> Unit)? = null,
) = AnimatedVisibility(
    visible = state.visible || editState.editing,
    enter = fadeIn() + expandVertically(),
    exit = fadeOut() + shrinkVertically()
) {
    ControlsScaffold(
        title = title,
        editState = editState,
    ) {
        announcement?.invoke()

        val context = LocalContext.current
        AnimatedFlowRow(
            mainAxisSpacing = OctoAppTheme.dimens.margin01,
            crossAxisSpacing = OctoAppTheme.dimens.margin01,
        ) {
            state.items.forEach {
                item(key = context.text(it)) {
                    SmallPrimaryButton(
                        pinned = isPinned(it),
                        text = context.text(it),
                        onClick = { state.onClick(it) },
                        onLongClick = { state.onLongClick(it) }
                    )
                }
            }

            item(key = otherOptionText) {
                SmallSecondaryButton(
                    text = otherOptionText,
                    onClick = onOtherOptionClicked,
                )
            }
        }
    }
}

interface GenericHistoryItemsControlState<T> {
    val visible: Boolean
    val items: List<T>
    fun onClick(item: T)
    fun onLongClick(item: T)
}
