package de.crysxd.baseui.compose.controls.tune

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.octoapp.base.data.models.SerialCommunication
import de.crysxd.octoapp.base.data.repository.SerialCommunicationLogsRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.base.usecase.TunePrintUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.flow.shareIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.regex.Matcher
import java.util.regex.Pattern

class TuneControlsViewModel(
    serialCommunicationLogsRepository: SerialCommunicationLogsRepository,
    private val instanceId: String,
    private val executeGcodeCommandUseCase: ExecuteGcodeCommandUseCase,
    private val tunePrintUseCase: TunePrintUseCase,
) : ViewModel() {

    companion object {
        const val SETTINGS_POLLING_INTERVAL_MS = 30000L
        const val SETTINGS_POLLING_INITIAL_DELAY = 500L
    }

    private var backgroundPollJob: Job? = null
    private val mutableUiState = MutableStateFlow(UiState())
    val uiState = mutableUiState.asStateFlow()
        .onStart {
            backgroundPollJob?.cancel()
            backgroundPollJob = startBackgroundPoll()
        }
        .onCompletion { backgroundPollJob?.cancel() }
        .shareIn(viewModelScope, started = SharingStarted.WhileSubscribed(3000))
    private val mutableEditState = MutableStateFlow(EditState())
    val editState = mutableEditState.asStateFlow()

    // Matches either
    // "N1907 M221 S78*96" -> Flow rate command send during print
    // "Recv: echo:E0 Flow: 100%" -> Response to M221 command
    private val flowRatePattern = Pattern.compile("(M221 S|Recv.*Flow: )(\\d+)")

    // Matches all of:
    // "N1907 M220 S78*96" -> Feed rate command send during print
    // "Recv: FR:78%" -> Response to M220 command
    private val feedRatePattern = Pattern.compile("(M220 S|Recv.*FR:)(\\d+)")

    // Matches all of
    // "M106 S255" -> M106 command to set fan
    // "M107" -> M107 command to turn fan off
    private val fanSpeedPattern = Pattern.compile("M106( S(\\d+))?")
    private val fanOffPattern = Pattern.compile("M107")

    // Matches all of
    // "Recv: echo:Probe OffsetZ: 0.01" -> Response to offset change
    // "Recv: echo:Probe Offset Z0.01" -> Response to offset change
    private val zOffsetPattern = Pattern.compile("Probe Offset\\s*:?\\s*Z:?\\s*(-?\\d+\\.\\d+)")

    init {
        Timber.i("Init")

        viewModelScope.launch(Dispatchers.Default) {
            serialCommunicationLogsRepository.passiveFlow(includeOld = true, instanceId = instanceId)
                .onEach { extractValues(it) }
                .retry { Timber.e(it); delay(1000); true }
                .collect()
        }
    }

    private fun startBackgroundPoll() = viewModelScope.launch(Dispatchers.Default) {
        Timber.i("Starting background poll")
        delay(SETTINGS_POLLING_INITIAL_DELAY)
        while (isActive) {
            try {
                delay(SETTINGS_POLLING_INTERVAL_MS)
                doPollSettings()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
        Timber.i("Stopping background poll")
    }

    private fun extractValues(comm: SerialCommunication) {
        extractValue(flowRatePattern.matcher(comm.content), 2) { state, value -> state.copy(flowRate = value.toInt()) }
        extractValue(feedRatePattern.matcher(comm.content), 2) { state, value -> state.copy(feedRate = value.toInt()) }
        extractValue(fanSpeedPattern.matcher(comm.content), 2) { state, value -> state.copy(fanSpeed = ((value.toInt() / 255f) * 100f).toInt()) }
        extractValue(fanOffPattern.matcher(comm.content), 0) { state, _ -> state.copy(fanSpeed = 0) }
        extractValue(zOffsetPattern.matcher(comm.content), 1) { state, value -> state.copy(zOffsetMm = value.toFloat()) }
    }

    fun pollSettingsNow() = viewModelScope.launch {
        try {
            doPollSettings()
        } catch (e: Exception) {
            Timber.e(e)
            // We do not report this error to the user
        }
    }

    private suspend fun doPollSettings() {
        executeGcodeCommandUseCase.execute(
            ExecuteGcodeCommandUseCase.Param(
                command = GcodeCommand.Batch(listOf("M220", "M221")),
                fromUser = false
            )
        )
    }

    private fun extractValue(matcher: Matcher, groupIndex: Int, upgrade: (UiState, String) -> UiState) {
        if (matcher.find() && matcher.groupCount() < groupIndex + 1) {
            val new = matcher.group(groupIndex) ?: return Timber.w("Regex matched but no value: $matcher")
            mutableUiState.update { old -> upgrade(old, new) }
        }
    }

    fun babyStepUp() = doBabyStep(0.025f)

    fun babyStepDown() = doBabyStep(-0.025f)

    private fun doBabyStep(distanceMm: Float) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        Timber.i("Performing baby step: $distanceMm")
        val params = ExecuteGcodeCommandUseCase.Param(
            command = GcodeCommand.Single("M290 Z$distanceMm"),
            recordResponse = false,
            fromUser = false
        )
        executeGcodeCommandUseCase.execute(params)
    }

    fun applyChanges(
        feedRate: Int? = null,
        flowRate: Int? = null,
        fanSpeed: Int? = null
    ) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        mutableEditState.update { it.copy(loading = true, operationCompleted = false) }

        try {
            tunePrintUseCase.execute(
                TunePrintUseCase.Param(
                    feedRate = feedRate,
                    flowRate = flowRate,
                    fanSpeed = fanSpeed
                )
            )
        } finally {
            mutableEditState.update { it.copy(loading = false) }
        }

        mutableEditState.update { it.copy(operationCompleted = true) }
        mutableUiState.update {
            it.copy(
                feedRate = feedRate ?: it.feedRate,
                flowRate = flowRate ?: it.flowRate,
                fanSpeed = fanSpeed ?: it.fanSpeed
            )
        }
    }

    data class EditState(
        val initialValue: Boolean = false,
        val loading: Boolean = false,
        val operationCompleted: Boolean = false
    )

    data class UiState(
        val flowRate: Int? = null,
        val feedRate: Int? = null,
        val fanSpeed: Int? = null,
        val zOffsetMm: Float? = null,
    )

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "TuneControlsViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = TuneControlsViewModel(
            instanceId = instanceId,
            serialCommunicationLogsRepository = BaseInjector.get().serialCommunicationLogsRepository(),
            executeGcodeCommandUseCase = BaseInjector.get().executeGcodeCommandUseCase(),
            tunePrintUseCase = BaseInjector.get().tunePrintUseCase()
        ) as T
    }
}