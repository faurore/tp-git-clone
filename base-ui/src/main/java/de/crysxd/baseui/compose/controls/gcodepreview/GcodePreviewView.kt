package de.crysxd.baseui.compose.controls.gcodepreview

import android.content.res.Configuration.ORIENTATION_LANDSCAPE
import android.graphics.Paint
import android.graphics.PointF
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.Crossfade
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.animation.core.updateTransition
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.animation.with
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Slider
import androidx.compose.material.SliderDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.gcodepreview.GcodePreviewControlsViewModel.ViewState
import de.crysxd.baseui.compose.framework.Badge
import de.crysxd.baseui.compose.framework.IconAction
import de.crysxd.baseui.compose.framework.LiveBadge
import de.crysxd.baseui.compose.framework.PrimaryButton
import de.crysxd.baseui.compose.framework.ScreenPreview
import de.crysxd.baseui.compose.framework.ScreenPreviewTablet
import de.crysxd.baseui.compose.framework.rememberValue
import de.crysxd.baseui.compose.framework.staticStateOf
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.baseui.ext.open
import de.crysxd.octoapp.base.UriLibrary
import de.crysxd.octoapp.base.data.models.GcodePreviewSettings
import de.crysxd.octoapp.base.gcode.render.GcodeRenderView
import de.crysxd.octoapp.base.gcode.render.models.GcodeRenderContext
import de.crysxd.octoapp.base.gcode.render.models.RenderStyle
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.models.printer.PrinterProfile
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.NumberFormat
import kotlin.math.roundToInt

@Composable
@OptIn(ExperimentalAnimationApi::class)
fun GcodePreviewScope.GcodePreviewView(
    state: ViewState,
    canSyncWithLive: Boolean = true,
) {
    val transition = updateTransition(targetState = state, label = "AnimatedContent")
    transition.AnimatedContent(
        transitionSpec = { fadeIn(animationSpec = tween(220, delayMillis = 90)) with fadeOut(animationSpec = tween(90)) },
        contentKey = { it::class },
        contentAlignment = Alignment.Center,
    ) { target ->
        when (target) {
            is ViewState.DataReady -> DataReady(state = { target }, canSyncWithLive = canSyncWithLive)
            is ViewState.Error -> Error(target)
            is ViewState.FeatureDisabled -> FeatureDisabled()
            is ViewState.LargeFileDownloadRequired -> LargeFileDownloadRequired(target)
            is ViewState.Loading -> Loading(target)
            is ViewState.PrintingFromSdCard -> PrintingFromSdCard(state = target)
        }
    }
}

interface GcodePreviewScope {
    fun setManualProgress(layerIndex: Int, layerProgress: Float)
    fun onRetry()
    fun onDownload()
    fun syncWithLive()
    fun showSettings()
}

//region Internals
@Composable
private fun FeatureDisabled() = Column(
    modifier = Modifier
        .fillMaxSize()
        .padding(OctoAppTheme.dimens.margin2),
    verticalArrangement = Arrangement.SpaceBetween,
    horizontalAlignment = Alignment.CenterHorizontally,
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .padding(top = OctoAppTheme.dimens.margin2)
            .fillMaxWidth(),
    ) {
        Image(
            painter = painterResource(id = R.drawable.print_bed_ender),
            contentDescription = null
        )
        Image(
            painter = painterResource(id = R.drawable.gcode_preview),
            contentDescription = null
        )
    }

    Column {
        Text(
            text = stringResource(id = R.string.supporter_perk___title),
            style = OctoAppTheme.typography.subtitle,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth(),
        )
        Text(
            text = stringResource(id = R.string.gcode_preview___supporter_perk_description),
            style = OctoAppTheme.typography.base,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = OctoAppTheme.dimens.margin01)
        )
    }

    PrimaryButton(
        text = stringResource(id = R.string.support_octoapp),
        onClick = { UriLibrary.getPurchaseUri().open() }
    )
}

@Composable
private fun GcodePreviewScope.Error(
    state: ViewState.Error
) = BaseLayoutWithControls {
    GcodeControlsError(
        state = state,
        onRetry = ::onRetry,
    )
}

@Composable
private fun GcodePreviewScope.PrintingFromSdCard(
    state: ViewState.PrintingFromSdCard
) = BaseLayoutWithControls {
    GcodeControlsPrintingFromSdCard(
        state = state,
    )
}

@Composable
private fun GcodePreviewScope.LargeFileDownloadRequired(
    state: ViewState.LargeFileDownloadRequired
) = BaseLayoutWithControls {
    GcodeControlsReadyLargeFileDownloadRequired(
        state = state,
        onDownload = ::onDownload
    )
}

@Composable
private fun GcodePreviewScope.Loading(
    state: ViewState.Loading
) = BaseLayoutWithControls {
    GcodeControlsLoading(state = state)
}

@Composable
private fun GcodePreviewScope.DataReady(
    state: () -> ViewState.DataReady,
    canSyncWithLive: Boolean,
) = BaseLayoutWithControls(
    renderContext = { state().state.value.renderContext },
    isTrackingPrintProgress = { state().state.value.isTrackingPrintProgress }
) {
    //region Render view
    val padding = with(LocalDensity.current) { OctoAppTheme.dimens.margin2.roundToPx() }
    val liveJob = rememberValue<Job?>(null)
    var isLive by remember { mutableStateOf(state().state.value.isTrackingPrintProgress) }
    val scope = rememberCoroutineScope()
    AndroidView(
        modifier = Modifier.fillMaxSize(),
        factory = {
            GcodeRenderView(it).apply {
                setPadding(padding, padding, padding, padding)
            }
        },
        update = {
            isLive = state().state.value.isTrackingPrintProgress
            liveJob.value?.cancel()
            liveJob.value = scope.launch {
                delay(GcodeNotLiveIfNoUpdateForMs)
                isLive = false
            }

            it.renderParams = state().printerProfile?.let { printerProfile ->
                state().state.value.renderContext?.let { renderContext ->
                    state().renderStyle?.let { renderStyle ->
                        GcodeRenderView.RenderParams(
                            renderContext = renderContext,
                            renderStyle = renderStyle,
                            originInCenter = printerProfile.volume.origin == PrinterProfile.Origin.Center,
                            printBedSizeMm = PointF(printerProfile.volume.width, printerProfile.volume.depth),
                            extrusionWidthMm = printerProfile.extruder.nozzleDiameter,
                            quality = state().settings.quality,
                        )
                    }
                }
            }
        }
    )
    //endregion
    //region Live badge
    LiveBadge(
        visible = isLive,
        modifier = Modifier
            .align(Alignment.TopEnd)
            .padding(OctoAppTheme.dimens.margin1)
    )
    //endregion
    Row(
        verticalAlignment = Alignment.Bottom,
        horizontalArrangement = Arrangement.End,
        modifier = Modifier
            .align(Alignment.BottomCenter)
            .fillMaxWidth()
    ) {
        //region Exceed warning
        val exceedsPrintArea by remember(state) {
            derivedStateOf {
                state().state.value.exceedsPrintArea == true
            }
        }
        AnimatedVisibility(
            visible = exceedsPrintArea,
            enter = expandVertically(expandFrom = Alignment.Top) + fadeIn(),
            exit = shrinkVertically(shrinkTowards = Alignment.Top) + fadeOut(),
            modifier = Modifier.weight(1f)
        ) {
            ExceedsPrintAreaWarning(state().printerProfile)
        }
        //endregion
        //region Buttons
        ControlButtons(state = state, canSyncWithLive = canSyncWithLive)
        //endregion
    }
    //region Unsupported Gcode warning
    val unsupportedGcode by remember(state) {
        derivedStateOf {
            state().state.value.unsupportedGcode == true
        }
    }
    Badge(
        text = stringResource(R.string.gcode_preview___unsupported_gcode),
        visible = unsupportedGcode,
        modifier = Modifier
            .align(Alignment.TopStart)
            .padding(OctoAppTheme.dimens.margin1),
        background = OctoAppTheme.colors.yellow
    )
    //endregion
}

@Composable
private fun GcodePreviewScope.ControlButtons(
    state: () -> ViewState.DataReady,
    canSyncWithLive: Boolean,
) = Column(
    modifier = Modifier
        .padding(OctoAppTheme.dimens.margin2)
        .shadow(elevation = 3.dp, RoundedCornerShape(percent = 50))
        .clip(RoundedCornerShape(percent = 50))
        .background(OctoAppTheme.colors.primaryButtonBackground)
        .width(IntrinsicSize.Min)
) {
    val spacer = @Composable {
        Box(
            modifier = Modifier
                .height(1.dp)
                .padding(horizontal = OctoAppTheme.dimens.margin1)
                .background(OctoAppTheme.colors.whiteTranslucent2)
                .fillMaxWidth()
        )
    }

    //region Layer Up
    IconAction(
        painter = painterResource(id = R.drawable.ic_round_keyboard_arrow_up_24),
        contentDescription = stringResource(id = R.string.cd_next_layer),
        tint = OctoAppTheme.colors.white,
        onClick = {
            setManualProgress(
                layerIndex = state().state.value.renderContext?.layerNumber?.let { it + 1 } ?: 0,
                layerProgress = 1f,
                context = state().state.value.renderContext
            )
        }
    )
    //endregion
    //region Sync live
    if (canSyncWithLive) {
        spacer()
        val isTracking by remember(state) {
            derivedStateOf {
                state().state.value.isTrackingPrintProgress
            }
        }
        Crossfade(targetState = isTracking) {
            IconAction(
                painter = painterResource(id = if (it) R.drawable.ic_round_sync_disabled_24 else R.drawable.ic_round_sync_24),
                contentDescription = stringResource(id = R.string.cd_sync_with_print_progress),
                tint = OctoAppTheme.colors.white,
                onClick = {
                    if (it) {
                        setManualProgress(
                            layerIndex = null,
                            layerProgress = null,
                            context = state().state.value.renderContext
                        )
                    } else {
                        syncWithLive()
                    }
                }
            )
        }
    }
    //endregion
    //region Settings
    spacer()
    IconAction(
        painter = painterResource(id = R.drawable.ic_round_layers_24),
        contentDescription = stringResource(id = R.string.cd_settings),
        tint = OctoAppTheme.colors.white,
        onClick = { showSettings() }
    )
    //endregion
    //region Layer down
    spacer()
    IconAction(
        painter = painterResource(id = R.drawable.ic_round_keyboard_arrow_down_24),
        contentDescription = stringResource(id = R.string.cd_previous_layer),
        tint = OctoAppTheme.colors.white,
        onClick = {
            setManualProgress(
                layerIndex = state().state.value.renderContext?.layerNumber?.let { it - 1 } ?: 0,
                layerProgress = 1f,
                context = state().state.value.renderContext
            )
        }
    )
    //endregion
}

@Composable
private fun ExceedsPrintAreaWarning(printerProfile: PrinterProfile?) = Box {
    Row(
        modifier = Modifier
            .widthIn(max = 450.dp)
            .padding(start = OctoAppTheme.dimens.margin2, bottom = OctoAppTheme.dimens.margin2)
            .shadow(elevation = 3.dp, MaterialTheme.shapes.medium)
            .clip(MaterialTheme.shapes.medium)
            .background(OctoAppTheme.colors.yellow)
            .padding(OctoAppTheme.dimens.margin1),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_round_warning_amber_24),
            contentDescription = null,
            tint = OctoAppTheme.colors.textColoredBackground,
            modifier = Modifier.padding(end = OctoAppTheme.dimens.margin1)
        )
        Text(
            text = stringResource(
                id = R.string.gcode_preview___warning_print_area,
                printerProfile?.volume?.width?.toInt() ?: 0,
                printerProfile?.volume?.depth?.toInt() ?: 0,
                printerProfile?.name ?: "???"
            ),
            style = OctoAppTheme.typography.base,
            color = OctoAppTheme.colors.textColoredBackground,
            modifier = Modifier.weight(1f)
        )
    }
}

@Composable
private fun GcodePreviewScope.BaseLayoutWithControls(
    renderContext: () -> GcodeRenderContext? = { null },
    isTrackingPrintProgress: () -> Boolean = { false },
    content: @Composable BoxScope.() -> Unit,
) {
    if (LocalConfiguration.current.orientation == ORIENTATION_LANDSCAPE) {
        Row(Modifier.fillMaxSize()) {
            Box(
                modifier = Modifier
                    .fillMaxHeight()
                    .aspectRatio(1f),
                contentAlignment = Alignment.Center,
                content = content,
            )
            Controls(
                renderContextFactory = renderContext,
                modifier = Modifier.fillMaxSize(),
                isTrackingPrintProgressFactory = isTrackingPrintProgress,
            )
        }
    } else {
        Column(Modifier.fillMaxSize()) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                contentAlignment = Alignment.Center,
                content = content,
            )
            Controls(
                renderContextFactory = renderContext,
                isTrackingPrintProgressFactory = isTrackingPrintProgress,
            )
        }
    }
}

@Composable
private fun GcodePreviewScope.Controls(
    renderContextFactory: () -> GcodeRenderContext?,
    isTrackingPrintProgressFactory: () -> Boolean,
    modifier: Modifier = Modifier
) = Column(
    modifier = modifier
        .background(OctoAppTheme.colors.inputBackground)
        .padding(horizontal = OctoAppTheme.dimens.margin2)
        .padding(top = OctoAppTheme.dimens.margin2),
    verticalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin1, alignment = Alignment.CenterVertically)
) {
    val context = LocalContext.current
    var progressSliderValue by remember { mutableStateOf<Float?>(null) }
    var layerSliderValue by remember { mutableStateOf<Float?>(null) }
    val animatedProgressSliderValue by animateFloatAsState(
        targetValue = progressSliderValue ?: 0f,
        animationSpec = spring(stiffness = Spring.StiffnessLow)
    )
    val animatedLayerSliderValue by animateFloatAsState(
        targetValue = layerSliderValue ?: 0f,
        animationSpec = spring(stiffness = Spring.StiffnessLow)
    )
    val format = remember {
        NumberFormat.getInstance().also {
            it.minimumFractionDigits = 1
            it.maximumFractionDigits = 1
        }
    }
    val renderContext = renderContextFactory()
    val isTrackingPrintProgress = isTrackingPrintProgressFactory()

    LaunchedEffect(renderContext?.layerNumber, isTrackingPrintProgress) {
        layerSliderValue = renderContext?.layerNumber?.toFloat()
    }

    LaunchedEffect(renderContext?.layerProgress, isTrackingPrintProgress) {
        if (isTrackingPrintProgress) {
            progressSliderValue = renderContext?.layerProgress
        }
    }

    ControlSlider(
        value = { if (isTrackingPrintProgress) animatedLayerSliderValue.takeIf { layerSliderValue != null } else layerSliderValue },
        onValueChange = {
            layerSliderValue = it
            progressSliderValue = 1f
            setManualProgress(
                layerIndex = layerSliderValue?.roundToInt(),
                context = renderContext,
                layerProgress = progressSliderValue
            )
        },
        max = { renderContext?.layerCount?.let { it - 1f } ?: 1f },
        label = stringResource(id = R.string.gcode_preview___layer),
        valueText = { current ->
            renderContext?.layerCount?.let { count ->
                context.getString(
                    R.string.x_of_y,
                    renderContext.layerNumberDisplay(current.roundToInt()),
                    renderContext.layerCountDisplay(count)
                )
            }
        },
        stepSize = 1f,
        extraLabel = {
            renderContext?.layerZHeight?.let {
                context.getString(R.string.x_mm, format.format(it))
            }
        },
    )

    ControlSlider(
        value = {
            if (isTrackingPrintProgress) {
                animatedProgressSliderValue.takeIf { progressSliderValue != null }
            } else {
                progressSliderValue
            }
        },
        onValueChange = {
            progressSliderValue = it
            setManualProgress(layerProgress = it, context = renderContext)
        },
        max = { 1f },
        label = stringResource(id = R.string.gcode_preview___layer_progress),
        valueText = { context.getString(R.string.x_percent, it * 100f) },
        stepSize = 0.01f,
    )
}

@Composable
private fun ControlSlider(
    value: () -> Float?,
    max: () -> Float,
    stepSize: Float,
    onValueChange: (Float) -> Unit,
    label: String,
    valueText: (Float) -> String?,
    extraLabel: () -> String? = { null },
) = Column {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.fillMaxWidth()
    ) {
        Row(horizontalArrangement = Arrangement.spacedBy(OctoAppTheme.dimens.margin01)) {
            Text(
                text = label,
                style = OctoAppTheme.typography.subtitle,
                color = OctoAppTheme.colors.darkText
            )
            Text(
                text = value()?.let(valueText) ?: "",
                style = OctoAppTheme.typography.subtitle,
                color = OctoAppTheme.colors.lightText
            )
        }

        Text(
            text = extraLabel() ?: "",
            style = OctoAppTheme.typography.subtitle,
            color = OctoAppTheme.colors.lightText
        )
    }

    Slider(
        value = value() ?: 0f,
        onValueChange = { onValueChange(it) },
        valueRange = 0f..max(),
        steps = (max() / stepSize).toInt(),
        enabled = value() != null,
        colors = SliderDefaults.colors(
            disabledInactiveTickColor = Color.Transparent,
            activeTickColor = Color.Transparent,
            disabledActiveTickColor = Color.Transparent,
            inactiveTickColor = Color.Transparent,
        ),
        modifier = Modifier
            .height(60.dp)
            .offset(y = -(10.dp))
    )
}

private fun GcodePreviewScope.setManualProgress(
    layerIndex: Int? = null,
    layerProgress: Float? = null,
    context: GcodeRenderContext?,
) {
    setManualProgress(
        layerProgress = layerProgress?.coerceIn(0f, 1f) ?: context?.layerProgress ?: 1f,
        layerIndex = layerIndex?.coerceIn(0, (context?.layerCount ?: 1) - 1) ?: context?.layerNumber ?: 0
    )
}

//endregion

//region Preview
private object PreviewScope : GcodePreviewScope {
    override fun setManualProgress(layerIndex: Int, layerProgress: Float) = Unit
    override fun onDownload() = Unit
    override fun onRetry() = Unit
    override fun syncWithLive() = Unit
    override fun showSettings() = Unit
}

@Composable
@ScreenPreview
private fun PreviewLoadingIndeterminate() = OctoAppThemeForPreview {
    PreviewScope.GcodePreviewView(ViewState.Loading())
}

@Composable
@ScreenPreview
private fun PreviewLoadingDeterminate() = OctoAppThemeForPreview {
    PreviewScope.GcodePreviewView(ViewState.Loading(0.66f))
}

@Composable
@ScreenPreview
private fun PreviewDisabled() = OctoAppThemeForPreview {
    PreviewScope.GcodePreviewView(
        state = ViewState.FeatureDisabled(background = R.drawable.print_bed_ender),
    )
}

@Composable
@Preview
private fun PreviewPrintingFromSdCard() = OctoAppThemeForPreview {
    PreviewScope.GcodePreviewView(
        ViewState.PrintingFromSdCard(
            FileObject.File(
                display = "Somefile.gcode",
                name = "Somefile.gcode",
                date = 0,
                type = "type",
                size = 0,
                path = "path",
                gcodeAnalysis = null,
                hash = "hash",
                origin = FileOrigin.SdCard,
                prints = null,
                ref = null,
                thumbnail = null,
                typePath = emptyList(),
            )
        )
    )
}

@Composable
@ScreenPreview
private fun PreviewError() = OctoAppThemeForPreview {
    PreviewScope.GcodePreviewView(
        state = ViewState.Error(
            exception = object : Exception(), UserMessageException {
                override val userMessage = "This is a <b>crucial<i> error</i></b><br><a href=\"https://google.com\">Test</a>"

            }
        )
    )
}

@Composable
@ScreenPreview
private fun PreviewLargeFile() = OctoAppThemeForPreview {
    PreviewScope.GcodePreviewView(
        state = ViewState.LargeFileDownloadRequired(384354742L),
    )
}

@Composable
@ScreenPreview
private fun PreviewReady() = OctoAppThemeForPreview {
    var currentLayer by remember { mutableStateOf(0) }
    var currentLayerProgress by remember { mutableStateOf(0.8436f) }

    object : GcodePreviewScope {
        override fun onDownload() = Unit
        override fun onRetry() = Unit
        override fun syncWithLive() = Unit
        override fun showSettings() = Unit
        override fun setManualProgress(layerIndex: Int, layerProgress: Float) {
            currentLayer = layerIndex
            currentLayerProgress = layerProgress
        }
    }.GcodePreviewView(
        state = ViewState.DataReady(
            settings = GcodePreviewSettings(),
            printerProfile = PrinterProfile(volume = PrinterProfile.Volume(220f, 220f, 220f, PrinterProfile.Origin.LowerLeft)),
            renderStyle = RenderStyle(
                printHeadPaint = Paint(),
                background = R.drawable.print_bed_ender,
                paintPalette = { Paint() },
                previousLayerPaint = Paint(),
                remainingLayerPaint = Paint(),
            ),
            state = staticStateOf(
                GcodePreviewControlsViewModel.EnrichedRenderContext(
                    renderContext = GcodeRenderContext(
                        previousLayerPaths = emptyList(),
                        completedLayerPaths = emptyList(),
                        remainingLayerPaths = emptyList(),
                        layerCount = 42,
                        layerNumber = currentLayer,
                        layerProgress = currentLayerProgress,
                        layerZHeight = 0.4f,
                        gcodeBounds = null,
                        printHeadPosition = null,
                        layerCountDisplay = { 41 },
                        layerNumberDisplay = { 1 },
                    ),
                    exceedsPrintArea = currentLayer == 0,
                    unsupportedGcode = currentLayer == 4,
                    isTrackingPrintProgress = currentLayer == 0,
                )
            )
        )
    )
}

@Composable
@ScreenPreviewTablet
private fun PreviewReadyTablet() = OctoAppThemeForPreview {
    PreviewScope.GcodePreviewView(
        state = ViewState.DataReady(
            settings = GcodePreviewSettings(),
            printerProfile = PrinterProfile(volume = PrinterProfile.Volume(220f, 220f, 220f, PrinterProfile.Origin.LowerLeft)),
            renderStyle = RenderStyle(
                printHeadPaint = Paint(),
                background = R.drawable.print_bed_ender,
                paintPalette = { Paint() },
                previousLayerPaint = Paint(),
                remainingLayerPaint = Paint(),
            ),
            state = staticStateOf(
                GcodePreviewControlsViewModel.EnrichedRenderContext(
                    renderContext = GcodeRenderContext(
                        previousLayerPaths = emptyList(),
                        completedLayerPaths = emptyList(),
                        remainingLayerPaths = emptyList(),
                        layerCount = 42,
                        layerNumber = 0,
                        layerProgress = 13.37f,
                        layerZHeight = 0.4f,
                        gcodeBounds = null,
                        printHeadPosition = null,
                        layerCountDisplay = { 41 },
                        layerNumberDisplay = { 1 },
                    ),
                    exceedsPrintArea = true,
                    unsupportedGcode = true,
                    isTrackingPrintProgress = true,
                )
            )
        )
    )
}
//endregion