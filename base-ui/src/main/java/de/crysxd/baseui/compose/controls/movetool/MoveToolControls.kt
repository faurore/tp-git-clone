package de.crysxd.baseui.compose.controls.movetool

import android.graphics.Matrix
import android.graphics.Paint
import android.graphics.Rect
import androidx.compose.animation.AnimatedContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.animateIntOffsetAsState
import androidx.compose.animation.core.spring
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.animation.with
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.geometry.center
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.drawscope.withTransform
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.layout.SubcomposeLayout
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.ControlsScaffold
import de.crysxd.baseui.compose.controls.EditState
import de.crysxd.baseui.compose.controls.movetool.MoveToolControlsViewModel.Direction
import de.crysxd.baseui.compose.framework.IconAction
import de.crysxd.baseui.compose.framework.LocalOctoPrint
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.text.NumberFormat
import kotlin.math.roundToInt

@Composable
fun MoveToolControls(
    state: MoveToolControlsState,
    editState: EditState,
) = ControlsScaffold(
    title = stringResource(id = R.string.widget_move),
    actionIcon = painterResource(id = R.drawable.ic_round_settings_24),
    onAction = { _, _ -> state.showSettings() },
    editState = editState,
) {
    ControlsRow {
        ControlsXY(
            modifier = Modifier.box(),
            state = state
        )
        ControlsZ(
            modifier = Modifier.box(),
            state = state
        )
        ResolutionSelector(
            state = state,
            modifier = Modifier.box()
        )
    }
}

interface MoveToolControlsState {
    fun jog(x: Direction = Direction.None, y: Direction = Direction.None, z: Direction = Direction.None)
    fun setJogResolution(mm: Float)
    fun homeXYAxis()
    fun homeZAxis()
    fun showSettings()
}

private interface ControlsRowScope {
    val arrowIcon: Painter
    val homeIcon: Painter
}

@Composable
fun rememberMoveToolControlState(navController: NavController): MoveToolControlsState {
    val vmFactory = MoveToolControlsViewModel.Factory(LocalOctoPrint.current.id)
    val context = LocalContext.current
    val vm = viewModel(
        modelClass = MoveToolControlsViewModel::class.java,
        key = vmFactory.id,
        factory = vmFactory
    )

    return remember(context, vm) {
        object : MoveToolControlsState {
            override fun jog(x: Direction, y: Direction, z: Direction) {
                vm.jog(x = x, y = y, z = z)
            }

            override fun setJogResolution(mm: Float) {
                vm.jogResolution = mm
            }

            override fun homeXYAxis() {
                vm.homeXYAxis()
            }

            override fun homeZAxis() {
                vm.homeZAxis()
            }

            override fun showSettings() {
                vm.showSettings(context, navController)
            }
        }
    }
}

//region Internals
@Composable
private fun ControlsRow(
    content: @Composable ControlsRowScope.() -> Unit,
) {
    val scope = object : ControlsRowScope {
        override val arrowIcon = painterResource(id = R.drawable.ic_round_keyboard_arrow_up_24)
        override val homeIcon = painterResource(id = R.drawable.ic_round_home_24)
    }

    SubcomposeLayout { constraints ->
        val width = constraints.maxWidth
        val height = subcompose(slotId = 0) { scope.content() }.map { it.measure(constraints) }.maxOf { it.height }
        val placeables = subcompose(slotId = 1) { scope.content() }.map { it.measure(constraints.copy(minHeight = height, maxHeight = height)) }
        val gap = (width - placeables.sumOf { it.width }) / (placeables.size - 1).toFloat()

        layout(width, height) {
            var x = 0f
            placeables.forEach {
                it.place(x.roundToInt(), 0)
                x += it.width + gap
            }
        }
    }
}

fun Modifier.box() = composed {
    clip(MaterialTheme.shapes.large)
        .background(OctoAppTheme.colors.inputBackground)
}

@Composable
private fun ControlsRowScope.ControlsXY(
    modifier: Modifier,
    state: MoveToolControlsState
) = Column(
    modifier
        .drawAxis(verticalAxisLabel = "Y", horizonAxisLabel = "X")
        .padding(OctoAppTheme.dimens.margin1)
        .height(IntrinsicSize.Min)
        .width(IntrinsicSize.Min)
) {
    Row(modifier = Modifier.weight(1f)) {
        RowSpacer()
        Button(
            painter = arrowIcon,
            contentDescription = stringResource(id = R.string.cd_move_plus_y),
            onClick = { state.jog(y = Direction.Positive) },
            animationDirectionY = -1,
            painterRotation = 0,
        )
        RowSpacer()
    }

    Row(modifier = Modifier.weight(1f)) {
        Button(
            painter = arrowIcon,
            painterRotation = 270,
            contentDescription = stringResource(id = R.string.cd_move_minus_x),
            onClick = { state.jog(x = Direction.Negative) },
            animationDirectionX = -1,
        )
        Button(
            painter = homeIcon,
            painterRotation = 0,
            contentDescription = stringResource(id = R.string.cd_home_xy),
            onClick = state::homeXYAxis,
        )
        Button(
            painter = arrowIcon,
            painterRotation = 90,
            contentDescription = stringResource(id = R.string.cd_move_plus_x),
            onClick = { state.jog(x = Direction.Positive) },
            animationDirectionX = 1,
        )
    }

    Row(modifier = Modifier.weight(1f)) {
        RowSpacer()
        Button(
            painter = arrowIcon,
            painterRotation = 180,
            contentDescription = stringResource(id = R.string.cd_move_minus_y),
            onClick = { state.jog(y = Direction.Negative) },
            animationDirectionY = 1,
        )
        RowSpacer()
    }
}

@Composable
private fun ControlsRowScope.ControlsZ(
    modifier: Modifier,
    state: MoveToolControlsState
) = Column(
    modifier
        .drawAxis(verticalAxisLabel = "Z")
        .padding(OctoAppTheme.dimens.margin1)
        .height(IntrinsicSize.Min)
        .width(IntrinsicSize.Min)
) {
    Button(
        painter = arrowIcon,
        painterRotation = 0,
        contentDescription = stringResource(id = R.string.cd_move_z_up),
        onClick = { state.jog(z = Direction.Positive) },
        animationDirectionY = -1,
        modifier = Modifier.weight(1f)
    )

    Button(
        painter = homeIcon,
        painterRotation = 0,
        contentDescription = stringResource(id = R.string.cd_home_z),
        onClick = state::homeZAxis,
        modifier = Modifier.weight(1f)
    )

    Button(
        painter = arrowIcon,
        painterRotation = 180,
        contentDescription = stringResource(id = R.string.cd_move_z_down),
        onClick = { state.jog(z = Direction.Negative) },
        animationDirectionY = 1,
        modifier = Modifier.weight(1f)
    )
}

@Composable
private fun RowScope.RowSpacer() = Spacer(modifier = Modifier.weight(1f))

@Composable
private fun ResolutionSelector(
    state: MoveToolControlsState,
    modifier: Modifier
) = Column(
    modifier = modifier.height(IntrinsicSize.Min),
    verticalArrangement = Arrangement.Center
) {
    val options = listOf(100f, 10f, 1f, 0.1f, 0.025f)
    var selected by rememberSaveable { mutableStateOf(options[1]) }
    val diameterRange = 8.dp..22.dp
    val diameterStep = (diameterRange.endInclusive - diameterRange.start) / options.size

    LaunchedEffect(selected) { state.setJogResolution(selected) }

    options.forEachIndexed { index, mm ->
        ResolutionOption(
            selected = selected == mm,
            resolutionMm = mm,
            diameterDp = diameterRange.endInclusive - diameterStep * index,
            onClick = { selected = mm }
        )
    }
}

@Composable
@OptIn(ExperimentalAnimationApi::class)
private fun ColumnScope.ResolutionOption(
    selected: Boolean,
    resolutionMm: Float,
    diameterDp: Dp,
    onClick: () -> Unit,
) = AnimatedContent(
    targetState = selected,
    modifier = Modifier
        .weight(1f)
        .width(100.dp)
        .clickable(onClick = onClick),
    transitionSpec = {
        fadeIn() + scaleIn(spring(dampingRatio = 0.6f, stiffness = 400f)) with fadeOut() + scaleOut()
    }
) {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Center
    ) {
        if (it) {
            Text(
                text = stringResource(id = R.string.x_mm, NumberFormat.getInstance().format(resolutionMm)),
                style = OctoAppTheme.typography.label,
                color = OctoAppTheme.colors.darkText
            )
        } else {
            Box(
                modifier = Modifier
                    .clip(CircleShape)
                    .size(diameterDp)
                    .background(OctoAppTheme.colors.primaryButtonBackground)
            )
        }
    }
}

private fun Modifier.drawAxis(verticalAxisLabel: String, horizonAxisLabel: String? = null) = composed {
    val color = OctoAppTheme.colors.darkText.copy(alpha = 0.3f)
    val bounds = remember { Rect() }
    val matrix = remember { Matrix() }
    val textDimension = remember { FloatArray(2) }
    val textPaint = remember {
        Paint().apply {
            setColor(color.toArgb())
        }
    }

    drawBehind {
        drawIntoCanvas {
            val dashHeight = 14.dp.toPx()
            val dashWidth = 1.dp.toPx()
            val dashPadding = 2.dp.toPx()

            fun drawAxisInternal(label: String, labelRotation: Float = 0f) {
                textPaint.textSize = dashHeight * 0.6f
                textPaint.getTextBounds(label, 0, label.length, bounds)
                textDimension[0] = bounds.width().toFloat()
                textDimension[1] = bounds.height().toFloat()
                matrix.reset()
                matrix.postRotate(labelRotation)
                matrix.mapPoints(textDimension)
                val textWidth = textDimension[0]
                val textHeight = textDimension[1]
                val textXStart = size.center.x - (textWidth / 2)
                val dashX = size.center.x - dashWidth / 2
                val textYBottom = (dashHeight / 2) + (textHeight / 2)
                val textYTop = textYBottom - textHeight
                val textRect = androidx.compose.ui.geometry.Rect(
                    offset = Offset(x = textXStart, y = textYTop),
                    size = Size(textWidth, textHeight)
                )

                drawRect(
                    color = color,
                    size = Size(dashWidth, textYTop - dashPadding),
                    topLeft = Offset(x = dashX, y = 0f)
                )
                drawRect(
                    color = color,
                    size = Size(dashWidth, dashHeight - textYBottom - dashPadding),
                    topLeft = Offset(x = dashX, y = textYBottom + dashPadding)
                )
                drawRect(
                    color = color,
                    size = Size(dashWidth, dashHeight),
                    topLeft = Offset(x = dashX, y = size.height - dashHeight)
                )

                rotate(degrees = labelRotation, pivot = textRect.center) {
                    with(it.nativeCanvas) {
                        drawText(
                            label,
                            textRect.center.x - bounds.width() / 2,
                            textRect.center.y + bounds.height() / 2,
                            textPaint
                        )
                    }
                }
            }

            drawAxisInternal(label = verticalAxisLabel)


            if (horizonAxisLabel != null) {

                withTransform(transformBlock = { rotate(-90f) }) {
                    drawAxisInternal(label = horizonAxisLabel, labelRotation = 90f)
                }
            }
        }
    }
}

@Composable
private fun Button(
    painter: Painter,
    painterRotation: Int,
    contentDescription: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    animationDirectionX: Int = 0,
    animationDirectionY: Int = 0,
) {
    var targetOffset by remember { mutableStateOf(IntOffset.Zero) }
    val offset by animateIntOffsetAsState(targetValue = targetOffset, spring(dampingRatio = 0.5f, stiffness = 350f))
    val scope = rememberCoroutineScope()
    val animationRange = with(LocalDensity.current) { 15.dp.roundToPx() }
    val feedback = LocalHapticFeedback.current

    IconAction(
        modifier = modifier
            .offset { offset }
            .rotate(painterRotation.toFloat()),
        onClick = {
            onClick()

            if (animationDirectionX != 0 || animationDirectionY != 0) {
                scope.launch {
                    feedback.performHapticFeedback(HapticFeedbackType.TextHandleMove)
                    targetOffset = IntOffset(x = animationRange * animationDirectionX, y = animationRange * animationDirectionY)
                    delay(300)
                    feedback.performHapticFeedback(HapticFeedbackType.TextHandleMove)
                    targetOffset = IntOffset.Zero
                }
            }
        },
        painter = painter,
        contentDescription = contentDescription,
    )
}
//endregion

//region Preview
@Composable
@Preview
private fun Preview() = OctoAppThemeForPreview {
    MoveToolControls(
        editState = EditState.ForPreview,
        state = object : MoveToolControlsState {
            override fun jog(x: Direction, y: Direction, z: Direction) = Unit
            override fun setJogResolution(mm: Float) = Unit
            override fun homeXYAxis() = Unit
            override fun homeZAxis() = Unit
            override fun showSettings() = Unit
        },
    )
}
//endregion