package de.crysxd.baseui.compose.controls.temperature

import android.content.Context
import android.text.InputType
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asFlow
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import de.crysxd.baseui.R
import de.crysxd.baseui.common.enter_value.EnterValueFragmentArgs
import de.crysxd.baseui.utils.NavigationResultMediator
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.rateLimit
import de.crysxd.octoapp.base.usecase.BaseChangeTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.SetTargetTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.SetTemperatureOffsetUseCase
import de.crysxd.octoapp.base.utils.AnimationTestUtils
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber

class TemperatureControlsViewModel(
    private val instanceId: String,
    temperatureDataRepository: TemperatureDataRepository,
    private val octoPrintRepository: OctoPrintRepository,
    private val setTemperatureOffsetUseCase: SetTemperatureOffsetUseCase,
    private val setTargetTemperaturesUseCase: SetTargetTemperaturesUseCase,
) : ViewModel() {

    val temperature = temperatureDataRepository.flow(instanceId = instanceId).let {
        // Slow down update rate for test
        if (AnimationTestUtils.animationsDisabled) {
            it.rateLimit(2000)
        } else {
            it.rateLimit(1000)
        }
    }.map {
        it.filter { !it.isHidden }
    }.retry {
        Timber.e(it)
        delay(1000)
        true
    }

    fun getInitialComponentCount() = octoPrintRepository.getActiveInstanceSnapshot()?.activeProfile?.let {
        var counter = if (it.extruder.sharedNozzle) 1 else it.extruder.count
        if (it.heatedBed) counter++
        if (it.heatedChamber) counter++
        Timber.i("Using $counter temperature controls for initial setup ($it)")
        counter
    } ?: 2

    private suspend fun setTemperature(temp: Int, component: String) {
        setTargetTemperaturesUseCase.execute(
            BaseChangeTemperaturesUseCase.Params(
                instanceId = instanceId,
                temp = BaseChangeTemperaturesUseCase.Temperature(temperature = temp.toFloat(), component = component)
            )
        )
    }

    private suspend fun setOffset(offset: Int, component: String) {
        setTemperatureOffsetUseCase.execute(
            BaseChangeTemperaturesUseCase.Params(
                instanceId = instanceId,
                temp = BaseChangeTemperaturesUseCase.Temperature(temperature = offset.toFloat(), component = component)
            )
        )
    }

    fun changeTemperature(
        context: Context,
        navController: NavController,
        component: TemperatureDataRepository.TemperatureSnapshot
    ) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        val targetResult = NavigationResultMediator.registerResultCallback<String?>()
        val offsetResult = NavigationResultMediator.registerResultCallback<String?>()
        val target = component.current.target?.toInt()
        val offset = component.offset?.toInt()

        navController.navigate(
            R.id.action_enter_value,
            EnterValueFragmentArgs(
                title = context.getString(R.string.x_temperature, component.componentLabel),
                action = context.getString(R.string.set_temperature),
                resultId = targetResult.first,
                hint = context.getString(R.string.target_temperature),
                value = null,
                inputType = InputType.TYPE_CLASS_NUMBER,
                resultId2 = offsetResult.first,
                hint2 = context.getString(R.string.temperature_widget___change_offset),
                value2 = offset?.toString(),
                inputType2 = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED,
                selectAll = true
            ).toBundle()
        )

        withContext(Dispatchers.Default) {
            targetResult.second.asFlow().first()
        }?.let { temp ->
            val tempInt = temp.toIntOrNull()
            if (tempInt != null && tempInt != target) {
                setTemperature(tempInt, component.component)
            } else {
                Timber.i("No change, dropping target change to $temp")
            }
        }

        withContext(Dispatchers.Default) {
            offsetResult.second.asFlow().first()
        }?.let { temp ->
            val tempInt = temp.toIntOrNull()
            if (tempInt != null && tempInt != target) {
                setOffset(tempInt, component.component)
            } else {
                Timber.i("No change, dropping offset change to $temp")
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "TemperatureControlsViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = TemperatureControlsViewModel(
            instanceId = instanceId,
            octoPrintRepository = BaseInjector.get().octorPrintRepository(),
            setTargetTemperaturesUseCase = BaseInjector.get().setTargetTemperatureUseCase(),
            setTemperatureOffsetUseCase = BaseInjector.get().setTemperatureOffsetUseCase(),
            temperatureDataRepository = BaseInjector.get().temperatureDataRepository(),
        ) as T
    }
}
