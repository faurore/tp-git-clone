package de.crysxd.baseui.compose.controls.executegcode

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.controls.GenericHistoryItemsControlsViewModel
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.GcodeHistoryItem
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase
import de.crysxd.octoapp.base.usecase.ExecuteGcodeCommandUseCase.Response.RecordedResponse
import de.crysxd.octoapp.base.usecase.GetGcodeShortcutsUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class ExecuteGcodeControlsViewModel(
    private val instanceId: String,
    private val octoPreferences: OctoPreferences,
    private val octoPrintProvider: OctoPrintProvider,
    private val getGcodeShortcutsUseCase: GetGcodeShortcutsUseCase,
    private val executeGcodeCommandUseCase: ExecuteGcodeCommandUseCase,
    octoPrintRepository: OctoPrintRepository,
) : GenericHistoryItemsControlsViewModel<GcodeHistoryItem>(
    instanceId = instanceId,
    octoPreferences = octoPreferences,
    octoPrintProvider = octoPrintProvider,
    octoPrintRepository = octoPrintRepository,
) {

    @OptIn(ExperimentalCoroutinesApi::class)
    override val items = flow { emit(getGcodeShortcutsUseCase.execute(Unit)) }
        .flatMapLatest { it }
        .distinctUntilChanged()

    override val OctoPreferences.isAlwaysAllowedToShow get() = allowTerminalDuringPrint
    override val OctoPrintInstanceInformationV3.isNeverAllowedToShow get() = false

    fun executeGcodeCommand(item: GcodeHistoryItem) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        if (needsConfirmation()) {
            postMessage(
                OctoActivity.Message.DialogMessage(
                    text = { getString(R.string.widget_gcode_send___confirmation_message, item.name) },
                    positiveAction = { doExecuteGcodeCommand(command = item.command) },
                    negativeButton = { getString(R.string.cancel) },
                    positiveButton = { getString(R.string.widget_gcode_send___confirmation_action) }
                )
            )
        } else {
            doExecuteGcodeCommand(command = item.command)
        }
    }

    private fun doExecuteGcodeCommand(command: String) = viewModelScope.launch(ExceptionReceivers.coroutineExceptionHandler) {
        val gcodeCommand = GcodeCommand.Batch(command.split("\n"))

        postMessage(
            OctoActivity.Message.SnackbarMessage(
                text = { con ->
                    con.getString(
                        if (gcodeCommand.commands.size == 1) {
                            R.string.sent_x
                        } else {
                            R.string.sent_x_and_y_others
                        }, gcodeCommand.commands.first(), gcodeCommand.commands.size - 1
                    )
                },
                debounce = true
            )
        )

        val responses = executeGcodeCommandUseCase.execute(
            ExecuteGcodeCommandUseCase.Param(
                command = gcodeCommand,
                fromUser = true,
                recordResponse = true,
                instanceId = instanceId,
            )
        )

        postMessage(
            OctoActivity.Message.SnackbarMessage(
                type = OctoActivity.Message.SnackbarMessage.Type.Positive,
                text = { con ->
                    con.getString(
                        if (gcodeCommand.commands.size == 1) {
                            R.string.received_response_for_x
                        } else {
                            R.string.received_response_for_x_and_y_others
                        }, gcodeCommand.commands.first(), gcodeCommand.commands.size - 1
                    )
                },
                action = {
                    postMessage(
                        OctoActivity.Message.DialogMessage(
                            text = {
                                responses.joinToString("<br><br>") {
                                    listOf(
                                        listOf((it as? RecordedResponse)?.sendLine ?: ""),
                                        (it as? RecordedResponse)?.responseLines ?: emptyList()
                                    ).flatten().joinToString("<br>")
                                }
                            }
                        )
                    )
                },
                actionText = { it.getString(R.string.show_logs) }
            )
        )
    }

    @Suppress("UNCHECKED_CAST")
    class Factory(private val instanceId: String) : ViewModelProvider.Factory {
        val id = "ExecuteGcodeControlsViewModel@$instanceId"

        override fun <T : ViewModel> create(modelClass: Class<T>) = with(BaseInjector.get()) {
            ExecuteGcodeControlsViewModel(
                instanceId = instanceId,
                octoPreferences = octoPreferences(),
                getGcodeShortcutsUseCase = getGcodeShortcutsUseCase(),
                octoPrintProvider = octoPrintProvider(),
                executeGcodeCommandUseCase = executeGcodeCommandUseCase(),
                octoPrintRepository = octorPrintRepository()
            ) as T
        }
    }
}