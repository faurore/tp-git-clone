package de.crysxd.baseui.compose.framework

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.compositionLocalOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import de.crysxd.baseui.compose.theme.LocalOctoAppColors
import de.crysxd.baseui.compose.theme.OctoAppColors
import de.crysxd.baseui.utils.colorTheme
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector

val LocalOctoPrint = compositionLocalOf<OctoPrintInstanceInformationV3> { throw IllegalStateException("No OctoPrint active") }
private val repo by lazy { BaseInjector.get().octorPrintRepository() }

@Composable
fun WithActiveOctoPrint(content: @Composable () -> Unit) {
    val active by BaseInjector.get().octorPrintRepository().instanceInformationFlow().collectAsState(initial = null)
    active?.let {
        CompositionLocalProvider(
            LocalOctoPrint provides it,
            LocalOctoAppColors provides LocalOctoAppColors.current.forOctoPrint(active)
        ) {
            content()
        }
    }
}

@Composable
fun WithOctoPrint(instanceId: String?, content: @Composable () -> Unit) {
    val octoPrintFlow = remember(instanceId) { repo.instanceInformationFlow(instanceId) }
    val octoPrint by octoPrintFlow.collectAsState(repo.get(instanceId))

    octoPrint?.let {
        CompositionLocalProvider(
            LocalOctoPrint provides it,
            LocalOctoAppColors provides LocalOctoAppColors.current.forOctoPrint(octoPrint)
        ) {
            content()
        }
    }
}

private fun OctoAppColors.forOctoPrint(instance: OctoPrintInstanceInformationV3?) = object : OctoAppColors by this {

    override val activeColorScheme: Color
        @Composable get() = Color(instance.colorTheme.dark)

    override val activeColorSchemeLight: Color
        @Composable get() = Color(instance.colorTheme.light)

}