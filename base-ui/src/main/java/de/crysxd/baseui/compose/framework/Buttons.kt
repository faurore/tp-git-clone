package de.crysxd.baseui.compose.framework

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.scale
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import de.crysxd.baseui.R
import de.crysxd.baseui.compose.theme.OctoAppTheme
import de.crysxd.baseui.compose.theme.OctoAppThemeForPreview
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import timber.log.Timber

@Composable
fun IconAction(
    painter: Painter,
    contentDescription: String?,
    modifier: Modifier = Modifier,
    tint: Color = OctoAppTheme.colors.primaryButtonBackground,
    onClick: () -> Unit,
) = Icon(
    painter = painter,
    contentDescription = contentDescription,
    tint = tint,
    modifier = modifier
        .clickable(
            interactionSource = MutableInteractionSource(),
            indication = if (tint == Color.White) {
                rememberRipple(bounded = false, color = Color.White, radius = 30.dp)
            } else {
                rememberRipple(bounded = false, radius = 30.dp)
            },
            onClick = onClick,
        )
        .padding(OctoAppTheme.dimens.margin1)
)

@Composable
fun PrimaryButton(
    text: String,
    modifier: Modifier = Modifier,
    loading: Boolean = false,
    shape: Shape = MaterialTheme.shapes.small,
    enabled: Boolean = true,
    onLongClick: () -> Unit = {},
    onClick: () -> Unit
) = BaseButton(
    text = text,
    onClick = onClick,
    isSecondary = false,
    isSmall = false,
    shape = shape,
    loading = loading,
    enabled = enabled,
    modifier = modifier,
    onLongClick = onLongClick,
    isTransparent = false,
)

@Composable
fun SmallPrimaryButton(
    text: String,
    modifier: Modifier = Modifier,
    shape: Shape = MaterialTheme.shapes.small,
    enabled: Boolean = true,
    pinned: Boolean = false,
    onLongClick: () -> Unit = {},
    onClick: () -> Unit,
) {
    val base = @Composable { m: Modifier ->
        BaseButton(
            text = text,
            onClick = onClick,
            isSecondary = false,
            isSmall = true,
            shape = shape,
            enabled = enabled,
            modifier = m,
            onLongClick = onLongClick,
            isTransparent = false,
            loading = false,
        )
    }

    if (pinned) {
        Row(
            modifier = modifier
                .clip(shape)
                .background(OctoAppTheme.colors.inputBackground),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_round_push_pin_10),
                contentDescription = null,
                tint = OctoAppTheme.colors.primaryButtonBackground,
                modifier = Modifier.padding(end = OctoAppTheme.dimens.margin0, start = OctoAppTheme.dimens.margin01)
            )

            base(
                Modifier.thenIf(!enabled) {
                    border(
                        border = BorderStroke(width = 1.dp, color = OctoAppTheme.colors.lightText),
                        shape = shape
                    )
                }
            )
        }
    } else {
        base(modifier)
    }
}

@Composable
fun SecondaryButton(
    text: String,
    modifier: Modifier = Modifier,
    shape: Shape = MaterialTheme.shapes.small,
    enabled: Boolean = true,
    loading: Boolean = false,
    onLongClick: () -> Unit = {},
    onClick: () -> Unit
) = BaseButton(
    text = text,
    onClick = onClick,
    isSecondary = true,
    shape = shape,
    isSmall = false,
    enabled = enabled,
    modifier = modifier,
    loading = loading,
    onLongClick = onLongClick,
    isTransparent = false,
)

@Composable
fun SmallSecondaryButton(
    text: String,
    modifier: Modifier = Modifier,
    shape: Shape = MaterialTheme.shapes.small,
    enabled: Boolean = true,
    onLongClick: () -> Unit = {},
    onClick: () -> Unit
) = BaseButton(
    text = text,
    onClick = onClick,
    isSecondary = true,
    shape = shape,
    isSmall = true,
    loading = false,
    enabled = enabled,
    onLongClick = onLongClick,
    modifier = modifier,
    isTransparent = false,
)

@Composable
fun LinkButton(
    text: String,
    modifier: Modifier = Modifier,
    shape: Shape = MaterialTheme.shapes.small,
    enabled: Boolean = true,
    color: Color = LocalContentColor.current,
    onLongClick: () -> Unit = {},
    onClick: () -> Unit,
) = CompositionLocalProvider(LocalContentColor provides color) {
    BaseButton(
        text = text,
        onClick = onClick,
        isSecondary = false,
        shape = shape,
        isSmall = true,
        loading = false,
        onLongClick = onLongClick,
        isTransparent = true,
        enabled = enabled,
        modifier = modifier,
    )
}

@Composable
fun LinkButtonRow(
    texts: List<String>,
    onClicks: List<() -> Unit>,
    modifier: Modifier = Modifier,
    alignment: Alignment.Horizontal = Alignment.End,
    color: Color = OctoAppTheme.colors.primaryButtonBackground,
) = CompositionLocalProvider(LocalContentColor provides color) {
    Row(
        modifier = modifier,
        horizontalArrangement = Arrangement.spacedBy(-OctoAppTheme.dimens.margin12, alignment)
    ) {
        require(texts.size == onClicks.size) { "Need equal amount of texts and click listeners, is ${texts.size} <> ${onClicks.size}" }
        texts.indices.forEach {
            LinkButton(
                text = texts[it],
                onClick = onClicks[it]
            )
        }
    }
}

@Composable
@OptIn(ExperimentalFoundationApi::class)
private fun BaseButton(
    text: String,
    onClick: () -> Unit,
    onLongClick: () -> Unit,
    modifier: Modifier,
    isSmall: Boolean,
    loading: Boolean,
    shape: Shape,
    isTransparent: Boolean,
    enabled: Boolean,
    isSecondary: Boolean,
) {
    //region Colors
    val rippleColor = when {
        isSecondary -> OctoAppTheme.colors.secondaryButtonText
        isTransparent -> LocalContentColor.current
        else -> OctoAppTheme.colors.textColoredBackground
    }

    val targetBackground = when {
        !enabled || loading -> OctoAppTheme.colors.veryLightGrey
        isTransparent -> Color.Transparent
        isSecondary -> OctoAppTheme.colors.inputBackground
        else -> OctoAppTheme.colors.primaryButtonBackground

    }
    val targetForeground = when {
        !enabled || loading -> OctoAppTheme.colors.lightText
        isTransparent -> LocalContentColor.current
        isSecondary -> OctoAppTheme.colors.secondaryButtonText
        else -> OctoAppTheme.colors.textColoredBackground
    }
    val targetElevation = when {
        !enabled || loading -> 0.dp
        isSmall -> 0.dp
        else -> 3.dp
    }

    val background by animateColorAsState(targetValue = targetBackground)
    val foreground by animateColorAsState(targetValue = targetForeground)
    val elevation by animateDpAsState(targetValue = targetElevation)
    //endregion

    Surface(
        modifier = modifier
            .thenIf(!isSmall) { fillMaxWidth() },
        border = if (isSecondary) {
            BorderStroke(1.dp, if (enabled) OctoAppTheme.colors.secondaryButtonText else OctoAppTheme.colors.lightText)
        } else {
            null
        },
        elevation = elevation,
        contentColor = foreground,
        color = background,
        shape = shape,
    ) {
        fun saveCall(b: () -> Unit) = try {
            b()
        } catch (e: Exception) {
            Timber.e(e)
            ExceptionReceivers.dispatchException(e)
        }

        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .combinedClickable(
                    role = Role.Button,
                    onClick = { saveCall(onClick) },
                    onLongClick = { saveCall(onLongClick) },
                    enabled = enabled,
                    interactionSource = MutableInteractionSource(),
                    indication = rememberRipple(color = rippleColor)
                )
        ) {
            //region Loading
            val textAlpha by animateFloatAsState(targetValue = if (loading) 0f else 1f)
            AnimatedVisibility(visible = loading) {
                CircularProgressIndicator(
                    color = LocalContentColor.current,
                    modifier = Modifier.scale(0.66f)
                )
            }
            //endregion
            //region Text
            Text(
                text = text,
                style = when {
                    isTransparent -> OctoAppTheme.typography.buttonLink
                    isSmall -> OctoAppTheme.typography.buttonSmall
                    else -> OctoAppTheme.typography.button
                },
                color = LocalContentColor.current,
                textAlign = TextAlign.Center,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .graphicsLayer { alpha = textAlpha }
                    .padding(
                        if (isSmall) {
                            PaddingValues(horizontal = OctoAppTheme.dimens.margin2, vertical = OctoAppTheme.dimens.margin1)
                        } else {
                            PaddingValues(horizontal = OctoAppTheme.dimens.margin2, vertical = OctoAppTheme.dimens.margin2)
                        }
                    ),
            )
            //endregion
        }
    }
}

//region Preview
@Composable
@Preview
private fun PreviewNormal() = OctoAppThemeForPreview {
    Column(Modifier.padding(20.dp)) {
        PrimaryButton(text = "Button") {}
        Spacer(modifier = Modifier.height(20.dp))
        PrimaryButton(text = "Button", enabled = false) {}
        PrimaryButton(text = "Button", loading = true) {}
        Spacer(modifier = Modifier.height(20.dp))
        SecondaryButton(text = "Button") {}
        Spacer(modifier = Modifier.height(20.dp))
        SecondaryButton(text = "Button", enabled = false) {}
        SecondaryButton(text = "Button", loading = true) {}
    }
}

@Composable
@Preview
private fun PreviewSmall() = OctoAppThemeForPreview {
    var background by remember { mutableStateOf(Color.Transparent) }
    val background2 by animateColorAsState(targetValue = background)

    Column(
        Modifier
            .padding(20.dp)
            .background(background2)
    ) {
        SmallPrimaryButton(
            text = "Color",
            onClick = { background = Color.Red },
            onLongClick = { background = Color.Green }
        )
        Spacer(modifier = Modifier.height(20.dp))
        SmallPrimaryButton(
            text = "Color",
            enabled = false,
            onClick = { background = Color.Red },
            onLongClick = { background = Color.Green }
        )
        Spacer(modifier = Modifier.height(20.dp))
        SmallSecondaryButton(
            text = "Reset",
            onClick = { background = Color.Transparent },
            onLongClick = { background = Color.Transparent }
        )
        Spacer(modifier = Modifier.height(20.dp))
        SmallSecondaryButton(
            text = "Reset",
            onClick = { background = Color.Transparent },
            onLongClick = { background = Color.Transparent },
            enabled = false
        )
        Spacer(modifier = Modifier.height(20.dp))
        SmallPrimaryButton(
            text = "Reset",
            pinned = true,
            onClick = { background = Color.Transparent },
            onLongClick = { background = Color.Transparent }
        )
        Spacer(modifier = Modifier.height(20.dp))
        SmallPrimaryButton(
            text = "Reset",
            pinned = true,
            onClick = { background = Color.Transparent },
            onLongClick = { background = Color.Transparent },
            enabled = false
        )
    }
}


@Composable
@Preview
private fun PreviewLink() = OctoAppThemeForPreview {
    Column(Modifier.padding(20.dp)) {
        LinkButton(text = "Button") {}
        Spacer(modifier = Modifier.height(20.dp))
        LinkButton(text = "Button", enabled = false) {}
    }
}

//endregion