package de.crysxd.baseui.menu.settings

import android.content.Context
import de.crysxd.baseui.R
import de.crysxd.baseui.common.LinkClickMovementMethod
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.baseui.menu.base.SubMenuItem
import de.crysxd.octoapp.base.billing.FEATURE_AUTOMATIC_LIGHTS
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_AUTOMATIC_LIGHTS
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_CONFIRM_POWER_OFF
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_RESET_DEFAULT_POWER_DEVICES
import de.crysxd.octoapp.base.di.BaseInjector
import kotlinx.parcelize.Parcelize

@Parcelize
class PowerSettingsMenu : Menu {
    override suspend fun getMenuItem() = listOf(
        AutomaticLightsSettingsMenuItem(),
        ConfirmPowerOffSettingsMenuItem(),
        ResetDefaultPowerDevicesMenuItem(),
    )

    override suspend fun getTitle(context: Context) = context.getString(R.string.main_menu___menu_settings_title)
    override suspend fun getSubtitle(context: Context) = context.getString(R.string.main_menu___submenu_subtitle)

    override fun getBottomMovementMethod(host: MenuHost) =
        LinkClickMovementMethod(object : LinkClickMovementMethod.OpenWithIntentLinkClickedListener(host.getMenuActivity()) {
            override fun onLinkClicked(context: Context, url: String?): Boolean {
                return if (url == "privacy") {
                    host.pushMenu(PrivacyMenu())
                    true
                } else {
                    super.onLinkClicked(context, url)
                }
            }
        })
}

class ResetDefaultPowerDevicesMenuItem : MenuItem {
    override val itemId = MENU_ITEM_RESET_DEFAULT_POWER_DEVICES
    override var groupId = ""
    override val order = 104
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_power_24

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_reset_default_power_devices)
    override suspend fun onClicked(host: MenuHost?) {
        BaseInjector.get().octorPrintRepository().updateActive {
            it.copy(appSettings = it.appSettings?.copy(defaultPowerDevices = null))
        }
    }
}

class AutomaticLightsSettingsMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_AUTOMATIC_LIGHTS
    override var groupId = ""
    override val order = 118
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_wb_incandescent_24
    override val billingManagerFeature = FEATURE_AUTOMATIC_LIGHTS
    override val subMenu: Menu get() = AutomaticLightsSettingsMenu()

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_automatic_lights)
}

class ConfirmPowerOffSettingsMenuItem : SubMenuItem() {
    override val itemId = MENU_ITEM_CONFIRM_POWER_OFF
    override var groupId = ""
    override val order = 119
    override val style = MenuItemStyle.Settings
    override val enforceSingleLine = false
    override val icon = R.drawable.ic_round_power_24
    override val subMenu: Menu get() = ConfirmPowerOffSettingsMenu()

    override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_confirm_power_off)
}
