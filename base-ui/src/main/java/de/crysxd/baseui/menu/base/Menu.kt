package de.crysxd.baseui.menu.base

import android.content.Context
import android.os.Parcelable
import android.view.View
import de.crysxd.baseui.common.LinkClickMovementMethod

interface Menu : Parcelable {
    fun shouldLoadBlocking() = false
    suspend fun getMenuItem(): List<MenuItem>
    suspend fun shouldShowMenu(host: MenuHost) = true
    suspend fun getTitle(context: Context): CharSequence? = null
    suspend fun getSubtitle(context: Context): CharSequence? = null
    fun getCustomHeaderView(host: MenuHost): View? = null
    fun getEmptyStateIcon(): Int = 0
    fun getNormalStateIcon(): Int = 0
    fun getEmptyStateActionText(context: Context): String? = null
    fun getEmptyStateAction(context: Context): (() -> Unit)? = null
    fun getEmptyStateSubtitle(context: Context): CharSequence? = null
    fun getCheckBoxText(context: Context): CharSequence? = null
    fun getBottomText(context: Context): CharSequence? = null
    fun getBottomMovementMethod(host: MenuHost): LinkClickMovementMethod? = null
    fun onDestroy() = Unit
}