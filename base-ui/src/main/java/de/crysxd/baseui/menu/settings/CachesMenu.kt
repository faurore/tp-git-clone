package de.crysxd.baseui.menu.settings

import android.content.Context
import de.crysxd.baseui.R
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.baseui.menu.base.RevolvingOptionsMenuItem
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.ext.asStyleFileSize
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.parcelize.Parcelize

@Parcelize
class CachesMenu : Menu {

    override suspend fun getMenuItem() = listOf(
        GcodeCacheMenuItem(),
        MediaCacheMenuItem(),
        HttpCacheMenuItem(),
        ClearCachesMenuItem(),
    )

    override suspend fun getTitle(context: Context) = context.getString(R.string.caches_menu___title)
    override suspend fun getSubtitle(context: Context) = context.getString(R.string.caches_menu___subtitle)

    private class GcodeCacheMenuItem : CacheMenuItem(
        get = BaseInjector.get().octoPreferences()::gcodeCacheSize::get,
        set = BaseInjector.get().octoPreferences()::gcodeCacheSize::set,
        currentSize = { BaseInjector.get().localGcodeFileDataSource().totalSize() },
        checkCache = BaseInjector.get().localGcodeFileDataSource()::checkCacheSize
    ) {
        override val itemId = "gcodeCache"
        override var groupId = "cache"
        override val order = 0

        override fun getTitle(context: Context) = context.getString(R.string.caches_menu___gcode___title)
        override fun getDescription(context: Context) = context.getString(R.string.caches_menu___gcode___description)
    }

    private class MediaCacheMenuItem : CacheMenuItem(
        get = BaseInjector.get().octoPreferences()::mediaCacheSize::get,
        set = BaseInjector.get().octoPreferences()::mediaCacheSize::set,
        currentSize = { BaseInjector.get().localMediaFileDataSource().totalSize() },
        checkCache = BaseInjector.get().localMediaFileDataSource()::checkCacheSize
    ) {
        override val itemId = "mediaCache"
        override var groupId = "cache"
        override val order = 1

        override fun getTitle(context: Context) = context.getString(R.string.caches_menu___media___title)
        override fun getDescription(context: Context) = context.getString(R.string.caches_menu___media___description)
    }

    private class HttpCacheMenuItem : CacheMenuItem(
        get = BaseInjector.get().octoPreferences()::httpCacheSize::get,
        set = BaseInjector.get().octoPreferences()::httpCacheSize::set,
        currentSize = { BaseInjector.get().httpCache().size() },
        checkCache = { }
    ) {
        override val itemId = "httpCache"
        override var groupId = "cache"
        override val order = 2

        override fun getTitle(context: Context) = context.getString(R.string.caches_menu___http___title)
        override fun getDescription(context: Context) = context.getString(R.string.caches_menu___http___description)
    }

    private class ClearCachesMenuItem : MenuItem {
        override val itemId = "clearCache"
        override var groupId = "clear"
        override val order = 10
        override val style = MenuItemStyle.Settings
        override val icon = R.drawable.ic_round_delete_forever_24

        override suspend fun onClicked(host: MenuHost?) {
            withContext(Dispatchers.IO) {
                BaseInjector.get().localGcodeFileDataSource().clear()
                BaseInjector.get().localMediaFileDataSource().clear()
                BaseInjector.get().httpCache().evictAll()
            }
            host?.reloadMenu()
        }

        override fun getTitle(context: Context) = context.getString(R.string.caches_menu___clear_caches)
    }

    private abstract class CacheMenuItem(
        private val get: () -> Long,
        private val set: (Long) -> Unit,
        private val currentSize: () -> Long,
        private val checkCache: () -> Unit,
    ) : RevolvingOptionsMenuItem() {
        override val activeValue get() = get().toString()
        override val options
            get() = listOf(32.MiB, 64.MiB, 128.MiB, 256.MiB, 512.MiB, 1024.MiB, 2048.MiB, 4096.MiB, 8192.MiB)
                .map { Option(label = "${getCachedSize().asStyleFileSize()} / ${it.asStyleFileSize()}", value = it.toString()) }

        override val style = MenuItemStyle.Settings
        override val icon = R.drawable.ic_round_sd_storage_24

        @Suppress("PrivatePropertyName")
        private val Int.MiB
            get() = this * 1024 * 1024L

        private var lastSize = 0L
        private var lastSizeTime = 0L

        fun getCachedSize(): Long {
            // We cache the size here to not access files over and over while inflating the item
            if (System.currentTimeMillis() - lastSizeTime > 500) {
                lastSize = currentSize()
                lastSizeTime = System.currentTimeMillis()
            }
            return lastSize
        }

        override suspend fun handleOptionActivated(host: MenuHost?, option: Option) {
            set(option.value.toLong())
            lastSizeTime = 0L
            withContext(Dispatchers.IO) { checkCache() }
        }
    }
}