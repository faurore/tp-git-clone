package de.crysxd.baseui.menu.switchprinter

import android.content.Context
import android.text.InputType
import androidx.annotation.IdRes
import androidx.lifecycle.asFlow
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.baseui.common.enter_value.EnterValueFragment
import de.crysxd.baseui.common.enter_value.EnterValueFragmentArgs
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.menu.base.Menu
import de.crysxd.baseui.menu.base.MenuHost
import de.crysxd.baseui.menu.base.MenuItem
import de.crysxd.baseui.menu.base.MenuItemStyle
import de.crysxd.baseui.utils.NavigationResultMediator
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.MenuItems.MENU_ITEM_ADD_INSTANCE
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlinx.parcelize.Parcelize
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull

private val isQuickSwitchEnabled get() = BillingManager.isFeatureEnabled(FEATURE_QUICK_SWITCH)
private val isAnyActive get() = BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()?.webUrl != null

@Parcelize
class ManageOctoPrintInstancesMenu : Menu {

    override fun shouldLoadBlocking() = true
    override suspend fun getTitle(context: Context) = context.getString(R.string.control_center___manage_menu___title)

    override suspend fun getMenuItem(): List<MenuItem> {
        val items = BaseInjector.get().octorPrintRepository().getAll().map {
            SwitchInstanceMenuItem(instanceId = it.id)
        }

        val static = listOf(
            AddInstanceMenuItem()
        )

        return listOf(items, static).flatten()
    }

    private class SwitchInstanceMenuItem(private val instanceId: String) : MenuItem {

        private val instanceInfo
            get() = BaseInjector.get().octorPrintRepository().get(instanceId)

        override val itemId = "edit_instance_$instanceId"
        override var groupId = "edit"
        override val order = 151
        override val canBePinned = false
        override val showAsSubMenu = false
        override val style = MenuItemStyle.Settings
        override val secondaryButtonIcon = R.drawable.ic_round_delete_24
        override val icon = R.drawable.ic_round_edit_24

        override fun getTitle(context: Context) = context.getString(R.string.control_center___manage_menu___edit, instanceInfo?.label ?: "(deleted)")

        override suspend fun onClicked(host: MenuHost?) {
            host ?: return
            val result = NavigationResultMediator.registerResultCallback<String?>()
            val context = host.requireContext()
            val navController = requireNotNull(host.getNavController()) { "No nav controller" }

            navController.navigate(
                R.id.action_enter_value,
                EnterValueFragmentArgs(
                    title = context.getString(R.string.sign_in___discover___web_url_hint),
                    hint = context.getString(R.string.sign_in___discover___web_url_hint_active),
                    action = context.getString(R.string.sign_in___continue),
                    resultId = result.first,
                    value = instanceInfo?.webUrl.toString(),
                    inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_URI,
                    validator = WebUrlValidator()
                ).toBundle()
            )

            val cc = (host.getMenuActivity() as OctoActivity).controlCenter
            cc.dismiss()

            AppScope.launch {
                result.second.asFlow().first()?.let { webUrl ->
                    BaseInjector.get().octorPrintRepository().update(id = instanceId) {
                        it.copy(webUrl = webUrl.toUrl())
                    }
                }

                cc.open()
            }
        }

        override suspend fun onSecondaryClicked(host: MenuHost?) {
            (host?.getMenuActivity() as? OctoActivity)?.showDialog(
                OctoActivity.Message.DialogMessage(
                    text = { getString(R.string.main_menu___delete_octoprint_dialog_message, instanceInfo?.label ?: "(deleted)") },
                    positiveAction = {
                        BaseInjector.get().octorPrintRepository().remove(instanceId)
                        host.reloadMenu()
                    },
                    negativeAction = {},
                    negativeButton = { getString(R.string.cancel) },
                )
            )
        }
    }

    @Parcelize
    private class WebUrlValidator : EnterValueFragment.ValueValidator {
        override fun validate(context: Context, value: String) =
            context.getString(R.string.sign_in___discovery___error_invalid_url).takeIf { value.toHttpUrlOrNull() == null }
    }

    private class AddInstanceMenuItem : MenuItem {
        override val itemId = MENU_ITEM_ADD_INSTANCE
        override var groupId = ""
        override val order = 150
        override val style = MenuItemStyle.Settings
        override val icon = R.drawable.ic_round_add_24

        override fun isEnabled(@IdRes destinationId: Int): Boolean = isQuickSwitchEnabled
        override fun isVisible(destinationId: Int) = isAnyActive
        override fun getTitle(context: Context) = context.getString(R.string.main_menu___item_add_instance)
        override suspend fun onClicked(host: MenuHost?) {
            host?.getHostFragment()?.requireOctoActivity()?.let {
                it.enforceAllowAutomaticNavigationFromCurrentDestination()
                it.controlCenter.dismiss()
            }

            BaseInjector.get().octorPrintRepository().clearActive()
            host?.closeMenu()
            host?.getHostFragment()?.requireOctoActivity()?.controlCenter?.dismiss()
        }
    }
}
