package de.crysxd.baseui.menu.base

interface ToggleMenuItem : MenuItem {
    val isChecked: Boolean
    override val canRunWithAppInBackground get() = false

    suspend fun handleToggleFlipped(host: MenuHost?, enabled: Boolean)
    override suspend fun onClicked(host: MenuHost?) = handleToggleFlipped(host, !isChecked)
}