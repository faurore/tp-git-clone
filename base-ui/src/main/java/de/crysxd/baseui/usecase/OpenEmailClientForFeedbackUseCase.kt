package de.crysxd.baseui.usecase

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.ktx.remoteConfig
import de.crysxd.baseui.OctoActivity
import de.crysxd.baseui.R
import de.crysxd.octoapp.base.data.repository.NotificationIdRepository
import de.crysxd.octoapp.base.di.modules.FileModule
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.base.usecase.UseCase
import de.crysxd.octoapp.base.utils.PendingIntentCompat
import timber.log.Timber
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import javax.inject.Inject


class OpenEmailClientForFeedbackUseCase @Inject constructor(
    private val publicFileFactory: FileModule.PublicFileFactory,
    private val notificationIdRepository: NotificationIdRepository,
) : UseCase<OpenEmailClientForFeedbackUseCase.Params, Unit>() {

    @Suppress("BlockingMethodInNonBlockingContext")
    override suspend fun doExecute(param: Params, timber: Timber.Tree) = Intent(Intent.ACTION_SEND).also {
        val version = param.appVersion ?: param.context.packageManager.getPackageInfo(param.context.packageName, 0).versionName
        val email = Firebase.remoteConfig.getString("contact_email")
        val subject = "Feedback OctoApp for Android $version"
        it.type = "message/rfc822"
        it.putExtra(Intent.EXTRA_SUBJECT, subject)
        it.putExtra(Intent.EXTRA_TEXT, param.message)
        it.putExtra(Intent.EXTRA_EMAIL, arrayOf(email))
        it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)

        if (param.bugReport != null) {
            val (zipFile, zipFileUri) = publicFileFactory.createPublicFile("data.zip")
            ZipOutputStream(zipFile.outputStream()).use { zip ->
                param.bugReport.files.forEach {
                    zip.putNextEntry(ZipEntry(it.name))
                    zip.write(it.bytes)
                    zip.flush()
                }
            }

            it.putExtra(Intent.EXTRA_STREAM, zipFileUri)
        }
    }.let { intent ->
        // Activity running? Launch directly
        if (OctoActivity.instance?.takeIf { it.lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED) }?.startActivity(intent) != null) {
            return@let
        }

        // Activity not running, post notification
        val notificationId = notificationIdRepository.getWearOsBugReportNotificationId()
        val manager = param.context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val channelId = param.context.getString(R.string.updates_notification_channel)
        val notification = NotificationCompat.Builder(param.context, channelId)
            .setContentText(param.context.getString(R.string.wear_bug_report___notification_description))
            .setContentTitle(param.context.getString(R.string.wear_bug_report___notification_title))
            .setSmallIcon(R.drawable.ic_notification_default)
            .setAutoCancel(true)
            .setColorized(true)
            .setLocalOnly(true)
            .setColor(ContextCompat.getColor(param.context, R.color.primary_dark))
            .setContentIntent(PendingIntent.getActivity(param.context, notificationId, intent, PendingIntentCompat.FLAG_IMMUTABLE))
            .build()

        manager.notify(notificationId, notification)
    }

    data class Params(
        val context: Context,
        val bugReport: CreateBugReportUseCase.BugReport?,
        val message: String,
        val appVersion: String? = null,
    )
}
