package de.crysxd.octoapp.base.data.models

import android.content.Context
import androidx.annotation.ColorRes
import de.crysxd.octoapp.base.R
import io.ktor.http.Url

data class Announcement(
    val text: Context.() -> CharSequence,
    val refreshInterval: Long = 0,
    val actionText: Context.() -> CharSequence?,
    val actionUri: Context.() -> Url?,
    val id: String,
    val canHide: Boolean = true,
    @ColorRes val backgroundColor: Int = R.color.input_background,
    @ColorRes val foregroundColor: Int = R.color.accent,
)
