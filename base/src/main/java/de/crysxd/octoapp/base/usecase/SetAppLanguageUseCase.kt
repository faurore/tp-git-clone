package de.crysxd.octoapp.base.usecase

import android.app.Activity
import android.content.Context
import com.jakewharton.processphoenix.ProcessPhoenix
import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoPreferences
import timber.log.Timber
import java.util.Locale
import javax.inject.Inject


class SetAppLanguageUseCase @Inject constructor(
    private val octoPreferences: OctoPreferences,
    private val context: Context,
) : UseCase<SetAppLanguageUseCase.Param, Unit>() {

    override suspend fun doExecute(param: Param, timber: Timber.Tree) {
        timber.i("Setting language to ${param.locale?.language}")
        octoPreferences.appLanguage = param.locale?.language

        OctoAnalytics.setUserProperty(OctoAnalytics.UserProperty.AppLanguage, param.locale?.language)
        OctoAnalytics.logEvent(
            OctoAnalytics.Event.AppLanguageChanged,
            mapOf(
                "language" to (param.locale?.language ?: "reset")
            )
        )

        ProcessPhoenix.triggerRebirth(context)
    }

    data class Param(
        val locale: Locale?,
        val activity: Activity
    )
}