package de.crysxd.octoapp.base.gcode.parse.models

import android.graphics.RectF
import java.io.Serializable

data class Gcode(
    val layers: List<LayerInfo>,
    val cacheKey: String,
    val minX: Float,
    val maxX: Float,
    val minY: Float,
    val maxY: Float,
) : Serializable {
    val bounds get() = RectF(minX, maxY, maxX, minY)
}