package de.crysxd.octoapp.base.ext

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigFetchThrottledException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber

suspend fun FirebaseRemoteConfig.fetchAndActivateSafe() = withContext(Dispatchers.IO) {
    try {
        fetchAndActivate().blockingAwait()
        true
    } catch (e: FirebaseRemoteConfigFetchThrottledException) {
        Timber.d("Remote config is throttled: ${e.message}")
        false
    } catch (e: java.lang.Exception) {
        // Continue with old values
        false
    }
}
