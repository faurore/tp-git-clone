package de.crysxd.octoapp.base.data

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.ByteArrayOutputStream
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

class OctoPrintInstanceInformationSerializer(
    private val json: Json
) {

    fun serialize(instances: List<OctoPrintInstanceInformationV3>): ByteArray {
        return ByteArrayOutputStream().use { bos ->
            GZIPOutputStream(bos).use { zip ->
                zip.bufferedWriter().use { writer ->
                    val text = json.encodeToString(instances)
                    writer.write(text)
                    writer.flush()
                }
            }
            bos.toByteArray()
        }
    }

    fun deserialize(serialized: ByteArray): List<OctoPrintInstanceInformationV3> {
        return GZIPInputStream(serialized.inputStream()).use { zip ->
            zip.bufferedReader().use { reader ->
                val text = reader.readText()
                json.decodeFromString(text)
            }
        }
    }
}