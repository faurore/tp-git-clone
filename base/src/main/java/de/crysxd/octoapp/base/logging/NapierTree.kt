package de.crysxd.octoapp.base.logging

import android.util.Log
import io.github.aakira.napier.LogLevel
import io.github.aakira.napier.Napier
import timber.log.Timber

class NapierTree : Timber.DebugTree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        val level = when (priority) {
            Log.ASSERT -> LogLevel.ASSERT
            Log.ERROR -> LogLevel.ERROR
            Log.WARN -> LogLevel.WARNING
            Log.INFO -> LogLevel.INFO
            Log.DEBUG -> LogLevel.DEBUG
            Log.VERBOSE -> LogLevel.VERBOSE
            else -> LogLevel.ASSERT
        }

        Napier.log(priority = level, tag = tag, throwable = t, message = message)
    }
}