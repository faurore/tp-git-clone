package de.crysxd.octoapp.base.di.modules

import android.content.Context
import dagger.Module
import dagger.Provides
import de.crysxd.octoapp.base.data.repository.GcodeFileRepository
import de.crysxd.octoapp.base.data.repository.MediaFileRepository
import de.crysxd.octoapp.base.data.repository.NotificationIdRepository
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.data.repository.TimelapseRepository
import de.crysxd.octoapp.base.data.repository.WidgetPreferencesRepository
import de.crysxd.octoapp.base.data.source.LocalGcodeFileDataSource
import de.crysxd.octoapp.base.data.source.LocalMediaFileDataSource
import de.crysxd.octoapp.base.data.source.RemoteGcodeFileDataSource
import de.crysxd.octoapp.base.data.source.RemoteMediaFileDataSource
import de.crysxd.octoapp.base.di.BaseScope
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.network.SslKeyStoreHandler

@Module
open class OctoPrintModule {

    @Provides
    open fun provideOctoPrintRepository(): OctoPrintRepository = SharedBaseInjector.get().printerConfigRepository

    @Provides
    open fun provideWidgetPreferencesRepository(): WidgetPreferencesRepository = SharedBaseInjector.get().widgetPreferencesRepository

    @Provides
    open fun provideOctoPrintProvider() = SharedBaseInjector.get().octoPrintProvider

    @Provides
    open fun provideGcodeHistoryRepository() = SharedBaseInjector.get().gcodeHistoryRepository

    @Provides
    open fun provideExtrusionHistoryRepository() = SharedBaseInjector.get().extrusionHistoryRepository

    @Provides
    open fun providePinnedMenuItemRepository() = SharedBaseInjector.get().pinnedMenuItemRepository

    @BaseScope
    @Provides
    open fun provideGcodeFileRepository(
        localDataSource: LocalGcodeFileDataSource,
        remoteDataSource: RemoteGcodeFileDataSource,
    ) = GcodeFileRepository(
        localDataSource = localDataSource,
        remoteDataSource = remoteDataSource
    )

    @Provides
    open fun provideFileListRepository() = SharedBaseInjector.get().fileListRepository

    @Provides
    open fun provideSerialCommunicationLogsRepository() = SharedBaseInjector.get().serialCommunicationLogsRepository

    @Provides
    open fun provideTemperatureDataRepository() = SharedBaseInjector.get().temperatureDataRepository

    @BaseScope
    @Provides
    open fun provideNotificationIdRepository(
        octoPrintRepository: OctoPrintRepository,
        context: Context,
    ) = NotificationIdRepository(
        octoPrintRepository = octoPrintRepository,
        sharedPreferences = context.getSharedPreferences("notification_id_cache", Context.MODE_PRIVATE)
    )

    @Provides
    open fun provideTutorialsRepository() = SharedBaseInjector.get().tutorialsRepository

    @Provides
    open fun provideLocalDnsResolver() = SharedBaseInjector.get().dnsResolver

    @Provides
    fun provideSslKeyStoreHandler() = SharedBaseInjector.get().keyStoreProvider as SslKeyStoreHandler

    @BaseScope
    @Provides
    open fun provideMediaFileRepository(
        localMediaFileDataSource: LocalMediaFileDataSource,
        remoteMediaFileDataSource: RemoteMediaFileDataSource,
    ) = MediaFileRepository(
        localDataSource = localMediaFileDataSource,
        remoteDataSource = remoteMediaFileDataSource
    )

    @BaseScope
    @Provides
    open fun provideTimelapseRepository(
        context: Context,
        octoPrintProvider: OctoPrintProvider,
        localMediaFileDataSource: LocalMediaFileDataSource,
        remoteMediaFileDataSource: RemoteMediaFileDataSource,
    ) = TimelapseRepository(
        context = context,
        octoPrintProvider = octoPrintProvider,
        localMediaFileDataSource = localMediaFileDataSource,
        remoteMediaFileDataSource = remoteMediaFileDataSource,
    )
}