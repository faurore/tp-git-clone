package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand
import timber.log.Timber
import javax.inject.Inject

class HomePrintHeadUseCase @Inject constructor(
    private val octoPrintProvider: OctoPrintProvider
) : UseCase<HomePrintHeadUseCase.Params, Unit>() {

    sealed class Axis(val printHeadCommand: PrintHeadCommand) {
        object All : Axis(PrintHeadCommand.HomeAllAxisPrintHeadCommand)
        object XY : Axis(PrintHeadCommand.HomeXYAxisPrintHeadCommand)
        object Z : Axis(PrintHeadCommand.HomeZAxisPrintHeadCommand)
    }

    data class Params(
        val axis: Axis,
        val instanceId: String?,
    )

    override suspend fun doExecute(param: Params, timber: Timber.Tree) {
        octoPrintProvider.octoPrint(param.instanceId).printerApi.executePrintHeadCommand(param.axis.printHeadCommand)
    }
}