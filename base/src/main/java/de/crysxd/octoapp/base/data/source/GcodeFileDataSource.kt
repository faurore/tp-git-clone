package de.crysxd.octoapp.base.data.source

import de.crysxd.octoapp.base.gcode.parse.models.Gcode
import de.crysxd.octoapp.engine.models.files.FileObject
import kotlinx.coroutines.flow.Flow

interface GcodeFileDataSource {

    fun canLoadFile(file: FileObject.File): Boolean

    fun loadFile(file: FileObject.File, allowLargeFileDownloads: Boolean): Flow<LoadState>

    suspend fun cacheGcode(file: FileObject.File, gcode: Gcode)

    sealed class LoadState {
        data class Loading(val progress: Float) : LoadState()
        data class FailedLargeFileDownloadRequired(val downloadSize: Long) : LoadState()
        data class Ready(val gcode: Gcode) : LoadState()
        data class Failed(val exception: Throwable) : LoadState()
    }
}