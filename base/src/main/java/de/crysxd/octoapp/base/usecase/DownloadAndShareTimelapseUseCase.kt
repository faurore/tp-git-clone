package de.crysxd.octoapp.base.usecase

import android.content.Context
import android.os.Build
import android.webkit.MimeTypeMap
import androidx.core.app.ShareCompat
import de.crysxd.octoapp.base.data.repository.TimelapseRepository
import de.crysxd.octoapp.base.di.modules.FileModule
import de.crysxd.octoapp.engine.models.timelapse.TimelapseFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class DownloadAndShareTimelapseUseCase @Inject constructor(
    private val timelapseRepository: TimelapseRepository,
    private val publicFileFactory: FileModule.PublicFileFactory,
) : UseCase<DownloadAndShareTimelapseUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, timber: Timber.Tree) = withContext(Dispatchers.IO) {
        // Create cache dir and make sure we delete all old files
        val (shareFile, uri) = publicFileFactory.createPublicFile(param.file.name)

        try {
            // Download
            timber.i("Downloading ${param.file.downloadPath}")
            val remoteFile = requireNotNull(timelapseRepository.download(param.file)) { "Unable to obtain remote file" }
            val mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(remoteFile.name)
            remoteFile.inputStream().use { input ->
                shareFile.outputStream().use { output ->
                    input.copyTo(output)
                }
            }

            // Add to gallery
            if (param.addToGallery) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    timelapseRepository.pushToGallery(param.file, remoteFile)
                } else {
                    throw UnsupportedOperationException("Only possible on Android 10 and newer")
                }
            }

            // Share
            else {
                timber.i("Sharing ${param.file.downloadPath} from $uri with mime type $mimeType")
                ShareCompat.IntentBuilder(param.context)
                    .setStream(uri)
                    .setChooserTitle(param.file.name)
                    .setType(mimeType ?: "file/*")
                    .startChooser()
            }

            Unit
        } catch (e: Exception) {
            shareFile.delete()
            throw e
        }
    }

    data class Params(
        val context: Context,
        val file: TimelapseFile,
        val addToGallery: Boolean = false,
    )
}