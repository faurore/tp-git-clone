package de.crysxd.octoapp.base.data.source

import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.await
import de.crysxd.octoapp.base.logging.TimberLogger
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.utils.LoggingInterceptorLogger
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.sharedcommon.http.framework.resolve
import de.crysxd.octoapp.engine.framework.urlFromEncodedPath
import io.ktor.client.request.get
import io.ktor.client.statement.bodyAsChannel
import io.ktor.utils.io.jvm.javaio.toInputStream
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import timber.log.Timber
import java.io.InputStream
import java.util.logging.Logger

class RemoteMediaFileDataSource(
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintProvider: OctoPrintProvider,
) {

    suspend fun loadPublicMedia(url: String) = withContext(Dispatchers.IO) {
        Timber.i("Loading $url from server")
        val logger = TimberLogger(Logger.getLogger("RemoteMediaDataSource")).logger
        val okHttp = OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor(LoggingInterceptorLogger(logger)).setLevel(HttpLoggingInterceptor.Level.BASIC))
            .build()
        return@withContext doLoad(url, okHttp)
    }

    fun ensureAbsoluteUrl(url: String?): String {
        val resolvedUrl = requireNotNull(url) { "URL is null" }.toHttpUrlOrNull() ?: octoPrintRepository.getActiveInstanceSnapshot()?.webUrl?.resolve(url)
        return requireNotNull(resolvedUrl) { "URL is null after resolving" }.toString()
    }

    suspend fun loadFromOctoPrint(encodedPath: String): InputStream = withContext(Dispatchers.IO) {
        Timber.i("Loading $encodedPath from server")

        return@withContext octoPrintProvider.octoPrint().genericRequest { url, httpClient ->
            httpClient.get {
                urlFromEncodedPath(baseUrl = url, *encodedPath.split("/").toTypedArray())
            }
        }.bodyAsChannel().toInputStream()
    }

    private suspend fun doLoad(url: String, okHttpClient: OkHttpClient) =
        okHttpClient.newCall(Request.Builder().get().url(url).build()).await().body?.byteStream() ?: throw IllegalStateException("No body")

}