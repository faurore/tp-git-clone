package de.crysxd.octoapp.filemanager.ui.select

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import de.crysxd.octoapp.engine.models.files.FileObject

class MoveAndCopyFilesViewModel : ViewModel() {
    val selectedFile = MutableLiveData<FileObject?>(null)
    var copyFile = false
}