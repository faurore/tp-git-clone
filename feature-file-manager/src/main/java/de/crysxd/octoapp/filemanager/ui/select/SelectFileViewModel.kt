package de.crysxd.octoapp.filemanager.ui.select

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.squareup.picasso.Picasso
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.FileListState
import de.crysxd.octoapp.base.ext.sorted
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.LoadFileUseCase
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase.Params
import de.crysxd.octoapp.base.usecase.MoveFileUseCase
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.filemanager.R
import de.crysxd.octoapp.filemanager.ui.details.FileDetailsFragmentArgs
import de.crysxd.octoapp.filemanager.upload.Upload
import de.crysxd.octoapp.filemanager.upload.UploadMediator
import de.crysxd.octoapp.sharedcommon.utils.isInFuture
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import timber.log.Timber
import kotlin.time.Duration.Companion.days

@OptIn(ExperimentalCoroutinesApi::class)
class SelectFileViewModel(
    private val loadFilesUseCase: LoadFilesUseCase,
    private val loadFileUseCase: LoadFileUseCase,
    private val moveFileUseCase: MoveFileUseCase,
    private val octoPreferences: OctoPreferences,
    private val octoPrintProvider: OctoPrintProvider,
    private val uploadMediator: UploadMediator,
    val picasso: LiveData<Picasso?>,
) : BaseViewModel() {

    companion object {
        private val HIDE_THUMBNAIL_HINT_FOR = 21.days
    }

    val fileOrigin = FileOrigin.Local
    private val showThumbnailFlow = MutableStateFlow(true)
    private var lastFolder = MutableStateFlow<FileObject.Folder?>(null)
    private var lastFileList: List<FileObject>? = null
    private var lastSelectedFile: FileObject.File? = null
    private val trigger = MutableStateFlow<Int?>(null)
    private var skipCacheOnNext = false
    private val uploadsFlow = lastFolder.flatMapLatest {
        uploadMediator.getActiveUploads(fileOrigin, it)
    }

    val uiState = trigger.filterNotNull().combine(lastFolder) { _, folder ->
        folder
    }.flatMapLatest {
        Timber.i("Loading ${it?.path ?: "/"} ")
        loadFilesUseCase.execute(Params.ForFolder(fileOrigin = fileOrigin, folder = it, skipCache = skipCacheOnNext)).flatMapLatest { state ->
            skipCacheOnNext = false
            when (state) {
                is FileListState.Error -> flowOf(UiState.Error(state.exception))
                FileListState.Loading -> flowOf(UiState.Loading())
                is FileListState.Loaded -> {
                    val allFiles = (state.file as FileObject.Folder).children

                    if (isAnyThumbnailPresent(allFiles)) {
                        showThumbnailFlow.value = false
                    }

                    flowOf(allFiles).combineFiles()
                }
            }
        }
    }.retry(1).catch {
        Timber.e(it)
    }.catch {
        Timber.e(it)
        emit(UiState.Error(it))
    }

    private fun Flow<List<FileObject>>.combineFiles() = combine(getSelectFile()) { allFiles, selectedFile ->
        Timber.i("Selected file received: ${selectedFile?.path}")
        allFiles to selectedFile
    }.combine(octoPreferences.updatedFlow) { allFilesAndSelected, _ ->
        allFilesAndSelected
    }.combine(uploadsFlow) { allFilesAndSelected, uploads ->
        val (allFiles, selectedFile) = allFilesAndSelected
        listOf(
            listOfNotNull(selectedFile).map { f -> FileWrapper.SelectedFileObjectWrapper(f) },
            uploads.sortedBy { it.name }.map { f -> FileWrapper.UploadWrapper(f) },
            allFiles.sorted().map { f -> FileWrapper.FileObjectWrapper(f) },
        ).flatten()
    }.combine(showThumbnailFlow) { files, showThumbnail ->
        UiState.DataReady(files, showThumbnail)
    }

    fun loadFiles(folder: FileObject.Folder?) = viewModelScope.launch(coroutineExceptionHandler) {
        lastFolder.value = folder
        showThumbnailFlow.value = folder == null && !isHideThumbnailHint()
        trigger.value = (trigger.value ?: 0) + 1
    }

    fun reload() = viewModelScope.launch(coroutineExceptionHandler) {
        lastFileList = null
        skipCacheOnNext = true
        lastSelectedFile = null
        trigger.value = (trigger.value ?: 0) + 1
    }

    private fun getSelectFile() = octoPrintProvider.passiveCurrentMessageFlow("list-files").map {
        it.job?.file
    }.combine(lastFolder) { selectedFile, folder ->
        folder to selectedFile
    }.distinctUntilChangedBy {
        it.second?.path
    }.map {
        // Only load selected file when the folder is null (root dir)
        it.second.takeIf { _ -> it.first == null }
    }.flatMapLatest {
        it?.let {
            Timber.i("Selected file updated: ${it.path}")
            flow<FileObject.File?> {
                val snap = lastSelectedFile
                if (snap?.path == it.path) {
                    emit(snap)
                } else {
                    lastSelectedFile = loadFileUseCase.execute(LoadFileUseCase.Params(fileOrigin, it.path))
                    Timber.i("Downloaded details for selected file: $lastSelectedFile")
                    emit(snap)
                }
            }
        } ?: flowOf(null)
    }.catch {
        Timber.e(it)
        emit(null)
    }

    private fun isHideThumbnailHint() = octoPreferences.hideThumbnailHintUntil.isInFuture()

    fun hideThumbnailHint() {
        octoPreferences.hideThumbnailHintUntil = Clock.System.now() + HIDE_THUMBNAIL_HINT_FOR
        showThumbnailFlow.value = false
    }

    private fun isAnyThumbnailPresent(files: List<FileObject>): Boolean = files.any {
        when (it) {
            is FileObject.Folder -> isAnyThumbnailPresent(it.children)
            is FileObject.File -> !it.thumbnail.isNullOrBlank()
        }
    }

    fun selectFile(file: FileObject) = viewModelScope.launch(coroutineExceptionHandler) {
        when (file) {
            is FileObject.File -> navContoller.navigate(R.id.action_show_file_details, FileDetailsFragmentArgs(file).toBundle())
            is FileObject.Folder -> navContoller.navigate(R.id.action_open_folder, SelectFileFragmentArgs(file).toBundle())
        }
    }

    fun search() = viewModelScope.launch(coroutineExceptionHandler) {
        navContoller.navigate(R.id.action_search_file)
    }

    fun moveFileHere(fileObject: FileObject, copyFile: Boolean) = viewModelScope.launch(coroutineExceptionHandler) {
        moveFileUseCase.execute(MoveFileUseCase.Params(file = fileObject, newPath = lastFolder.value?.path ?: "/", copyFile = copyFile))
    }

    sealed class UiState {
        open class Loading : UiState()
        data class Error(val exception: Throwable) : UiState()
        data class DataReady(val files: List<FileWrapper>, val showThumbnailHint: Boolean) : UiState()
    }

    sealed class FileWrapper {
        abstract val date: Long
        abstract val path: String?
        abstract val name: String?
        abstract val size: Long?
        abstract val lastPrintDate: Long
        abstract val wasPrinted: Boolean

        data class UploadWrapper(val upload: Upload) : FileWrapper() {
            override val date = upload.startTime.time
            override val path = upload.parent?.path + upload.name
            override val name = upload.name
            override val size = upload.size
            override val lastPrintDate = 0L
            override val wasPrinted = false
        }

        data class SelectedFileObjectWrapper(val fileObject: FileObject.File) : FileWrapper() {
            override val date = fileObject.date
            override val path = fileObject.path
            override val name = fileObject.name
            override val size = fileObject.size
            override val lastPrintDate = fileObject.prints?.last?.date ?: 0
            override val wasPrinted = false
        }

        data class FileObjectWrapper(val fileObject: FileObject) : FileWrapper() {
            override val path = fileObject.path
            override val name = fileObject.name
            override val size = fileObject.size
            override val lastPrintDate = (fileObject as? FileObject.File)?.prints?.last?.date ?: 0
            override val wasPrinted = (fileObject as? FileObject.File)?.prints?.last?.success == true
            override val date = when (fileObject) {
                is FileObject.File -> fileObject.date
                is FileObject.Folder -> Long.MAX_VALUE
            }
        }
    }
}
