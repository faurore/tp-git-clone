package de.crysxd.octoapp.framework.robots

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By.res
import androidx.test.uiautomator.Direction
import androidx.test.uiautomator.UiDevice
import de.crysxd.baseui.compose.framework.TestTags
import de.crysxd.octoapp.tests.condition.waitTime

object BottomToolbarRobot {

    fun confirmButtonWithSwipe(buttonId: String) = with(UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())) {
        findObject(res(buttonId)).click()
        waitTime(500)
        val track = findObject(res(TestTags.BottomBar.SwipeTrack))
        track.swipe(Direction.RIGHT, 1f, 2000)
    }
}