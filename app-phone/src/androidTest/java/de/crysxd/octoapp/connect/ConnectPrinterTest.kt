package de.crysxd.octoapp.connect

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.RootMatchers.isDialog
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.adevinta.android.barista.rule.BaristaRule
import com.adevinta.android.barista.rule.flaky.AllowFlaky
import com.google.common.truth.Truth.assertThat
import de.crysxd.octoapp.MainActivity
import de.crysxd.octoapp.R
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.framework.robots.MenuRobot
import de.crysxd.octoapp.framework.robots.SignInRobot
import de.crysxd.octoapp.framework.robots.WorkspaceRobot
import de.crysxd.octoapp.framework.rules.AcceptAllAccessRequestRule
import de.crysxd.octoapp.framework.rules.AutoConnectPrinterRule
import de.crysxd.octoapp.framework.rules.ResetDaggerRule
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.tests.TestEnvironmentLibrary
import de.crysxd.octoapp.tests.condition.waitFor
import de.crysxd.octoapp.tests.condition.waitForDialog
import de.crysxd.octoapp.tests.condition.waitTime
import de.crysxd.octoapp.tests.ext.setActive
import de.crysxd.octoapp.tests.rules.IdleTestEnvironmentRule
import de.crysxd.octoapp.tests.rules.TestDocumentationRule
import de.crysxd.octoapp.tests.utils.PsuUtils.turnAllOff
import de.crysxd.octoapp.tests.utils.VirtualPrinterUtils.setVirtualPrinterEnabled
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers.allOf
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain

class ConnectPrinterTest {

    private val testEnv = TestEnvironmentLibrary.Terrier
    private val powerControlsTestEnv = TestEnvironmentLibrary.Dachshund
    private val wrongEnv = OctoPrintInstanceInformationV3(
        id = "random",
        webUrl = "http://127.0.0.1:100".toUrl(),
        apiKey = "XXXXXXX"
    )

    private val baristaRule = BaristaRule.create(MainActivity::class.java)

    @get:Rule
    val chain = RuleChain.outerRule(baristaRule)
        .around(IdleTestEnvironmentRule(testEnv, powerControlsTestEnv))
        .around(TestDocumentationRule())
        .around(ResetDaggerRule())
        .around(AcceptAllAccessRequestRule(testEnv))
        .around(AutoConnectPrinterRule())

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_auto_connect_is_disabled_THEN_connect_button_can_be_used() {
        // GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
        BaseInjector.get().octoPreferences().isAutoConnectPrinter = false
        baristaRule.launchActivity()

        // Wait for ready to connect
        WorkspaceRobot.waitForConnectWorkspace()
        waitTime(4000) // Wait to see if we auto connect
        waitFor(allOf(withText(R.string.connect_printer___waiting_for_user_title)))
        onView(withText(R.string.connect_printer___begin_connection)).perform(click())
        waitForDialog(withText(R.string.connect_printer___begin_connection_cofirmation_positive))
        onView(withText(R.string.connect_printer___begin_connection_cofirmation_positive)).inRoot(isDialog()).perform(click())
        onView(withText(R.string.connect_printer___action_turn_psu_off)).check(matches(not(isDisplayed())))
        onView(withText(R.string.connect_printer___action_turn_psu_on)).check(matches(not(isDisplayed())))
        WorkspaceRobot.waitForPrepareWorkspace()
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_auto_connect_is_disabled_and_PSU_can_be_controlled_THEN_connect_button_can_be_used() {
        // GIVEN
        BaseInjector.get().octorPrintRepository().setActive(
            powerControlsTestEnv.copy(
                appSettings = AppSettings(
                    defaultPowerDevices = mapOf(AppSettings.DEFAULT_POWER_DEVICE_PSU to "psucontrol:psu")
                )
            )
        )
        BaseInjector.get().octoPreferences().isAutoConnectPrinter = false
        powerControlsTestEnv.turnAllOff()
        powerControlsTestEnv.setVirtualPrinterEnabled(false)
        baristaRule.launchActivity()

        // Wait for ready to connect
        WorkspaceRobot.waitForConnectWorkspace()
        waitTime(4000) // Wait to see if we auto connect
        waitFor(allOf(withText(R.string.connect_printer___waiting_for_user_title)))
        onView(withText(R.string.connect_printer___begin_connection)).perform(click())
        waitForDialog(withText(R.string.connect_printer___begin_connection_cofirmation_positive))
        onView(withText(R.string.connect_printer___begin_connection_cofirmation_positive)).inRoot(isDialog()).perform(click())

        // Turn on printer (simulate by turning on virtual printer)
        waitFor(allOf(withText(R.string.connect_printer___action_turn_psu_on), isDisplayed()))
        onView(withText(R.string.connect_printer___action_turn_psu_on)).perform(click())
        MenuRobot.waitForMenuToBeClosed()
        waitFor(allOf(withText(R.string.connect_printer___action_turn_psu_off), isDisplayed()), timeout = 8000)
        powerControlsTestEnv.setVirtualPrinterEnabled(true)
        WorkspaceRobot.waitForPrepareWorkspace()
    }


    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_OctoPrint_not_available_and_no_quick_switch_THEN_other_OctoPrint_can_be_connected() {
        // GIVEN
        BaseInjector.get().octorPrintRepository().setActive(wrongEnv)
        BillingManager.enabledForTest = false
        baristaRule.launchActivity()

        // Wait for ready to connect
        WorkspaceRobot.waitForConnectWorkspace()
        waitFor(allOf(withText(R.string.connect_printer___octoprint_not_available_title), isDisplayed()))
        onView(withText(R.string.connect_printer___action_change_octoprint)).perform(click())
        MenuRobot.assertMenuTitle(R.string.main_menu___title_quick_switch_disabled)
        MenuRobot.clickMenuButton(R.string.main_menu___item_sign_out)
        MenuRobot.waitForMenuToBeClosed()
        SignInRobot.waitForDiscoveryOptionsToBeShown()
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_OctoPrint_not_available_and_quick_switch_available_THEN_other_OctoPrint_can_be_connected() {
        // GIVEN
        BaseInjector.get().octorPrintRepository().setActive(testEnv)
        BaseInjector.get().octorPrintRepository().setActive(wrongEnv)
        BillingManager.enabledForTest = true
        baristaRule.launchActivity()

        // Wait for ready to connect
        WorkspaceRobot.waitForConnectWorkspace()
        waitFor(allOf(withText(R.string.connect_printer___octoprint_not_available_title), isDisplayed()))
        onView(withText(R.string.connect_printer___action_change_octoprint)).perform(click())

        val matchers = allOf(
            ViewMatchers.hasDescendant(withText(testEnv.label)),
            ViewMatchers.withId(R.id.content)
        )
        waitFor(matchers)
        onView(matchers).perform(click())

        // Wait for switch completed
        WorkspaceRobot.waitForConnectWorkspace()
        WorkspaceRobot.waitForPrepareWorkspace()
        assertThat(BaseInjector.get().octorPrintRepository().getActiveInstanceSnapshot()?.webUrl).isEqualTo(testEnv.webUrl)
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_power_controls_are_available_THEN_psu_can_be_turned_on() {
        // GIVEN
        // We need a bit of wait before/after changing virtual printer, OctoPrint otherwise gets overloaded...
        // Also make sure PSU is turned off
        BaseInjector.get().octorPrintRepository().setActive(
            powerControlsTestEnv.copy(
                appSettings = AppSettings(
                    defaultPowerDevices = mapOf(AppSettings.DEFAULT_POWER_DEVICE_PSU to "psucontrol:psu")
                )
            )
        )
        powerControlsTestEnv.turnAllOff()
        powerControlsTestEnv.setVirtualPrinterEnabled(false)
        baristaRule.launchActivity()

        // Wait for ready and turn on PSU
        WorkspaceRobot.waitForConnectWorkspace()

        // Turn on printer (simulate by turning on virtual printer)
        waitFor(allOf(withText(R.string.connect_printer___action_turn_psu_on), isDisplayed()), timeout = 10_000)
        onView(withText(R.string.connect_printer___action_turn_psu_on)).perform(click())
        MenuRobot.waitForMenuToBeClosed()
        waitFor(allOf(withText(R.string.connect_printer___action_turn_psu_off), isDisplayed()), timeout = 8000)
        powerControlsTestEnv.setVirtualPrinterEnabled(true)
        WorkspaceRobot.waitForPrepareWorkspace()
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_power_controls_are_not_set_up_THEN_psu_can_be_configured_and_turned_on() {
        // GIVEN
        // We need a bit of wait before/after changing virtual printer, OctoPrint otherwise gets overloaded...
        // Also make sure PSU is turned off
        BaseInjector.get().octorPrintRepository().setActive(powerControlsTestEnv)
        powerControlsTestEnv.turnAllOff()
        powerControlsTestEnv.setVirtualPrinterEnabled(false)
        baristaRule.launchActivity()

        // Wait for ready and turn on PSU
        WorkspaceRobot.waitForConnectWorkspace()

        // Configure power
        waitFor(allOf(withText(R.string.connect_printer___action_configure_psu), isDisplayed()), timeout = 10_000)
        onView(withText(R.string.connect_printer___action_configure_psu)).perform(click())
        waitTime(2000)
        MenuRobot.clickMenuButton("PSU")
        waitTime(2000)

        // Turn on printer (simulate by turning on virtual printer)
        waitFor(allOf(withText(R.string.connect_printer___action_turn_psu_on), isDisplayed()), timeout = 10_000)
        onView(withText(R.string.connect_printer___action_turn_psu_on)).perform(click())
        MenuRobot.waitForMenuToBeClosed()
        waitFor(allOf(withText(R.string.connect_printer___action_turn_psu_off), isDisplayed()), timeout = 8000)
        powerControlsTestEnv.setVirtualPrinterEnabled(true)
        WorkspaceRobot.waitForPrepareWorkspace()
    }

    @Test(timeout = 30_000)
    @AllowFlaky(attempts = 5)
    fun WHEN_power_controls_are_not_set_up_THEN_psu_can_be_disabled() {
        // GIVEN
        // We need a bit of wait before/after changing virtual printer, OctoPrint otherwise gets overloaded...
        // Also make sure PSU is turned off
        BaseInjector.get().octorPrintRepository().setActive(powerControlsTestEnv)
        powerControlsTestEnv.turnAllOff()
        powerControlsTestEnv.setVirtualPrinterEnabled(false)
        baristaRule.launchActivity()

        // Wait for ready and turn on PSU
        WorkspaceRobot.waitForConnectWorkspace()

        // Configure power
        waitFor(allOf(withText(R.string.connect_printer___action_configure_psu), isDisplayed()), timeout = 10_000)
        onView(withText(R.string.connect_printer___action_configure_psu)).perform(click())

        // Short delay to improve test stability, then select
        waitTime(2000)
        MenuRobot.clickMenuButton(R.string.power_menu___no_device)

        // Check button not shown
        waitFor(allOf(withText(R.string.show_actions), isDisplayed()), timeout = 10_000)
    }
}