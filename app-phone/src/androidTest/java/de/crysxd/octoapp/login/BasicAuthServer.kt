package de.crysxd.octoapp.login

import fi.iki.elonen.NanoHTTPD
import okhttp3.Credentials
import org.junit.rules.TestWatcher
import org.junit.runner.Description

class BasicAuthServer(
    val port: Int = 8000,
    val username: String = "test",
    val password: String = "test",
) : TestWatcher() {

    private val mockWebServer = object : NanoHTTPD(port) {
        override fun serve(session: IHTTPSession): Response {
            val credentials = session.headers["authorization"] ?: ""
            val credentialsCorrect = credentials == Credentials.basic(username, password)

            return when {
                !credentialsCorrect -> newFixedLengthResponse(
                    Response.Status.UNAUTHORIZED,
                    "application/json",
                    "{}".byteInputStream(),
                    2
                ).also {
                    it.addHeader("WWW-Authenticate", "Basic realm=\"OctoApp test server\"")
                }

                else -> newFixedLengthResponse(
                    Response.Status.NOT_FOUND,
                    "application/json",
                    "".byteInputStream(),
                    0
                )
            }
        }
    }

    override fun starting(description: Description?) = mockWebServer.start()

    override fun failed(e: Throwable?, description: Description?) = mockWebServer.stop()

    override fun succeeded(description: Description?) = mockWebServer.stop()

}