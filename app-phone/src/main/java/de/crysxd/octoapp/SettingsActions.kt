package de.crysxd.octoapp

import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.utils.AppScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch

class SettingsActions {

    init {
        AppScope.launch {
            SharedBaseInjector.get().preferences.updatedFlow2
                .map { it.isCrashReportingEnabled }
                .distinctUntilChanged()
                .collectLatest {
                    FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(it)
                }
        }

        AppScope.launch {
            SharedBaseInjector.get().preferences.updatedFlow2
                .map { it.isAnalyticsEnabled }
                .distinctUntilChanged()
                .collectLatest {
                    Firebase.analytics.setAnalyticsCollectionEnabled(it)
                }
        }

        AppScope.launch {
            SharedBaseInjector.get().preferences.updatedFlow2
                .map { it.appTheme }
                .distinctUntilChanged()
                .collectLatest {
                    BaseInjector.get().applyAppThemeUseCase().executeBlocking(Unit)
                }
        }
    }
}