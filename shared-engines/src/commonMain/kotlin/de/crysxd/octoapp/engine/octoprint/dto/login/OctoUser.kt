package de.crysxd.octoapp.engine.octoprint.dto.login

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoUser(
    val permissions: List<String> = emptyList(),
    val groups: List<String> = emptyList(),
    val name: String? = null
) {
    val isGuest get() = groups.contains("guests")
    val canAccessSystemCommands get() = permissions.contains("SYSTEM")
}