package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.login.LoginResponse
import de.crysxd.octoapp.engine.octoprint.dto.login.OctoLoginResponse

internal fun OctoLoginResponse.map() = LoginResponse(
    session = session,
    groups = groups,
    name = name,
    isAdmin = admin
)