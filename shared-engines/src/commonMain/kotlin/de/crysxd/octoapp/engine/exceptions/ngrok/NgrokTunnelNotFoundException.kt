package de.crysxd.octoapp.engine.exceptions.ngrok

import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException.Companion.REMOTE_SERVICE_NGROK
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class NgrokTunnelNotFoundException(webUrl: Url) : NetworkException(
    userFacingMessage = "The ngrok tunnel is gone and was removed. This can happen when you are using the free ngrok tier and restart your OctoPrint.\n\nWhen OctoApp can reach your OctoPrint again, the new tunnel URL will be fetched automatically.",
    webUrl = webUrl,
), RemoteServiceConnectionBrokenException {
    override val remoteServiceName = REMOTE_SERVICE_NGROK
}