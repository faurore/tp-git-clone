package de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective

import kotlinx.serialization.Serializable

@Serializable
data class SpaghettiCamFrame(
    val snapshot: String? = null
)
