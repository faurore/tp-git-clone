package de.crysxd.octoapp.engine.octoprint.dto.plugins.cancelobject

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class CancelObjectCommand(
    @SerialName("cancelled") val objectId: Int
) {
    val command: String = "cancel"
}