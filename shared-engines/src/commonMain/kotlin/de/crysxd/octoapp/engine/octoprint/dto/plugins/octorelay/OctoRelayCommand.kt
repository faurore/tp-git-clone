package de.crysxd.octoapp.engine.octoprint.dto.plugins.octorelay

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoRelayCommand(val command: String, val pin: String)