package de.crysxd.octoapp.engine.models.settings

import kotlinx.serialization.Serializable

@Serializable
data class Settings(
    val webcam: WebcamSettings = WebcamSettings(),
    val plugins: PluginSettingsGroup = PluginSettingsGroup(),
    val temperatureProfiles: List<TemperatureProfile> = emptyList(),
    val terminalFilters: List<TerminalFilter> = emptyList(),
    val appearance: Appearance = Appearance(),
) {

    @Serializable
    data class Appearance(
        val name: String? = null,
        val color: String = "default"
    )

    @Serializable
    data class TerminalFilter(
        val name: String,
        val regex: String
    )

    @Serializable
    data class TemperatureProfile(
        val name: String,
        val bed: Float? = null,
        val extruder: Float? = null,
        val chamber: Float? = null,
    )

    @Serializable
    data class PluginSettingsGroup(
        val gcodeViewer: GcodeViewer? = null,
        val octoEverywhere: OctoEverywhere? = null,
        val ngrok: Ngrok? = null,
        val spaghettiDetective: SpaghettiDetective? = null,
        val spoolManager: SpoolManager? = null,
        val filamentManager: FilamentManager? = null,
        val octoAppCompanion: OctoAppCompanion? = null,
        val multiCamSettings: MultiCam? = null,
        val discovery: Discovery? = null,
        val uploadAnything: UploadAnything? = null,
        val mmu2FilamentSelect: Mmu2FilamentSelect? = null,
        val cancelObject: CancelObject? = null,
        val psuControl: PsuControl? = null,
        val wled: Wled? = null,
        val octoCam: OctoCam? = null,
        val octoLight: OctoLight? = null,
        val ophom: Ophom? = null,
        val octoHue: OctoHue? = null,
        val myStrom: MyStrom? = null,
        val wS281x: WS281x? = null,
        val enclosure: Enclosure? = null,
        val gpioControl: GpioControl? = null,
        val octoRelay: OctoRelay? = null,
        val tasmota: Tasmota? = null,
        val tpLinkSmartPlug: TpLinkSmartPlug? = null,
        val tradfri: Tradfri? = null,
        val tuya: Tuya? = null,
        val usbRelayControl: UsbRelayControl? = null,
        val wemoSwitch: WemoSwitch? = null,
    )

    interface PluginSettings

    @Serializable
    data class GcodeViewer(
        val mobileSizeThreshold: Long = 0,
        val sizeThreshold: Long = 0
    ) : PluginSettings

    @Serializable
    data class Tradfri(
        val devices: List<Device> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Device(
            val id: String,
            val name: String,
        )
    }

    @Serializable
    data class Tuya(
        val devices: List<Device>?
    ) : PluginSettings {
        @Serializable
        data class Device(
            val label: String,
        )
    }

    @Serializable
    data class TpLinkSmartPlug(
        val devices: List<Device> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val label: String,
        )
    }

    @Serializable
    data class WemoSwitch(
        val devices: List<Device> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val label: String,
        )
    }

    @Serializable
    data class Tasmota(
        val devices: List<Device>?
    ) : PluginSettings {
        @Serializable
        data class Device(
            val ip: String,
            val idx: String,
            val label: String,
        )
    }

    @Serializable
    data class GpioControl(
        val devices: List<Device>?
    ) : PluginSettings {
        @Serializable
        data class Device(
            val name: String,
            val index: Int,
        )
    }

    @Serializable
    data class Enclosure(
        val outputs: List<Output>?
    ) : PluginSettings {
        @Serializable
        data class Output(
            val label: String,
            val type: String,
            val indexId: Int,
        )
    }

    @Serializable
    data class UsbRelayControl(
        val devices: List<Device> = emptyList()
    ) : PluginSettings {
        @Serializable
        data class Device(
            val name: String,
            val index: Int,
        )
    }

    @Serializable
    data class OctoRelay(
        val devices: List<Device>?
    ) : PluginSettings {
        @Serializable
        data class Device(
            val id: String,
            val displayName: String,
        )
    }

    @Serializable
    object WS281x : PluginSettings

    @Serializable
    object Wled : PluginSettings

    @Serializable
    object OctoCam : PluginSettings

    @Serializable
    object OctoLight : PluginSettings

    @Serializable
    object Ophom : PluginSettings

    @Serializable
    object OctoHue : PluginSettings

    @Serializable
    object MyStrom : PluginSettings

    @Serializable
    object PsuControl : PluginSettings

    @Serializable
    object CancelObject : PluginSettings

    @Serializable
    data class Ngrok(
        val authName: String? = null,
        val authPassword: String? = null,
    ) : PluginSettings

    @Serializable
    object SpaghettiDetective : PluginSettings

    @Serializable
    data class OctoAppCompanion(
        val encryptionKey: String?,
        val version: String?,
    ) : PluginSettings

    @Serializable
    data class MultiCam(
        val profiles: List<WebcamSettings> = emptyList()
    ) : PluginSettings

    @Serializable
    data class Discovery(
        val uuid: String?
    ) : PluginSettings

    @Serializable
    data class UploadAnything(
        val allowedExtensions: List<String> = emptyList()
    ) : PluginSettings

    @Serializable
    object SpoolManager : PluginSettings

    @Serializable
    object FilamentManager : PluginSettings

    @Serializable
    data class OctoEverywhere(
        val printerKey: String? = null
    ) : PluginSettings

    @Serializable
    data class Mmu2FilamentSelect(
        val filament1: String? = null,
        val filament2: String? = null,
        val filament3: String? = null,
        val filament4: String? = null,
        val filament5: String? = null,
        val labelSource: LabelSource = LabelSource.Manual,
    ) : PluginSettings {
        enum class LabelSource {
            Manual,
            FilamentManager,
            SpoolManager,
        }
    }
}