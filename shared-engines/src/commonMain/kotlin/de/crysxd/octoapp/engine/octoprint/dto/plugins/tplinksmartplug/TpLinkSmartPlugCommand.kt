package de.crysxd.octoapp.engine.octoprint.dto.plugins.tplinksmartplug

import kotlinx.serialization.Serializable

@Serializable
internal data class TpLinkSmartPlugCommand(val command: String, val ip: String)
