package de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolmanager

import de.crysxd.octoapp.engine.octoprint.serializer.OctoSafeFloatSerializer
import kotlinx.serialization.Serializable

@Serializable
internal data class OctoSpoolManagerSpool(
    val color: String? = null,
    val colorName: String? = null,
    val databaseId: String? = null,
    val vendor: String? = null,
    val material: String? = null,
    val displayName: String? = null,
    val isActive: Boolean? = null,
    @Serializable(with = OctoSafeFloatSerializer::class) val remainingWeight: Float? = null,
    val isTemplate: Boolean? = null,
)