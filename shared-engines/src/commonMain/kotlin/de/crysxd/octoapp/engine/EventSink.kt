package de.crysxd.octoapp.engine

import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject

internal interface EventSink {
    suspend fun injectInterpolatedEvent(event: (Message.Current?) -> Message.Event)
    suspend fun injectInterpolatedPrintStart(file: FileObject.File)
    suspend fun injectInterpolatedTemperatureTarget(targets: Map<String, Float>)
    suspend fun emitEvent(e: Event)
}