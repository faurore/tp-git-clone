package de.crysxd.octoapp.engine.models.printer

@Suppress("Unused")
data class PrinterState(
    val state: State = State(),
    val temperature: Map<String, ComponentTemperature> = emptyMap()
) {

    data class State(
        val text: String? = null,
        val flags: Flags = Flags()
    )

    data class Flags(
        val operational: Boolean = false,
        val paused: Boolean = false,
        val printing: Boolean = false,
        val cancelling: Boolean = false,
        val pausing: Boolean = false,
        val sdReady: Boolean = false,
        val error: Boolean = false,
        val ready: Boolean = false,
        val finishing: Boolean = false,
        val resuming: Boolean = false,
        val closedOrError: Boolean = false,
    ) {
        fun isPrinting() = listOf(printing, paused, pausing, cancelling).any { it }
        fun isOperational() = listOf(operational).any { it }
        fun isError() = listOf(error, closedOrError).any { it }
    }
}