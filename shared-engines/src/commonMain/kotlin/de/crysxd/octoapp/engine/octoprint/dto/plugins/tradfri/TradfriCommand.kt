package de.crysxd.octoapp.engine.octoprint.dto.plugins.tradfri

import kotlinx.serialization.Serializable

@Serializable
internal data class TradfriCommand(val command: String, val dev: Device) {
    @Serializable
    data class Device(
        val id: String,
        val name: String
    )
}

