package de.crysxd.octoapp.engine.octoprint.dto.commands

import kotlinx.serialization.Serializable

@Serializable
data class OctoGcodeCommand(val commands: List<String>)