package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileObject

internal fun OctoFileObject.map(): FileObject = when (this) {
    is OctoFileObject.File -> FileObject.File(
        name = name,
        path = path,
        display = display,
        ref = refs.map(),
        origin = origin.map(),
        size = size,
        type = type,
        date = date?.toEpochMilliseconds() ?: 0L,
        prints = prints?.let { ph ->
            FileObject.PrintHistory(
                last = ph.last?.let { lp ->
                    FileObject.PrintHistory.LastPrint(
                        date = lp.date.toEpochMilliseconds(),
                        success = lp.success,
                    )
                },
                success = ph.success,
                failure = ph.failure,
            )
        },
        gcodeAnalysis = gcodeAnalysis?.let { ga ->
            FileObject.GcodeAnalysis(
                dimensions = ga.dimensions?.let {
                    FileObject.GcodeAnalysis.Dimensions(
                        depth = it.depth,
                        height = it.height,
                        width = it.width
                    )
                },
                estimatedPrintTime = ga.estimatedPrintTime,
                filament = ga.filament.mapValues {
                    FileObject.GcodeAnalysis.FilamentUse(length = it.value.length, volume = it.value.volume)
                }
            )
        },
        hash = hash,
        thumbnail = thumbnail,
        typePath = typePath,
    )

    is OctoFileObject.Folder -> FileObject.Folder(
        name = name,
        typePath = typePath,
        type = type,
        path = path,
        display = display,
        size = size,
        origin = origin.map(),
        ref = refs.map(),
        children = children.map { it.map() }
    )
}

private fun OctoFileObject.Reference?.map() = this?.let {
    FileObject.Reference(
        download = it.download,
        resource = it.resource
    )
}