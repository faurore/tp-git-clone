package de.crysxd.octoapp.engine.models.job

data class Job(
    val progress: ProgressInformation = ProgressInformation(),
    val info: JobInformation = JobInformation()
)