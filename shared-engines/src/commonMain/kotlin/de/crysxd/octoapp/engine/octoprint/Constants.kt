package de.crysxd.octoapp.engine.octoprint

import io.ktor.util.AttributeKey

internal object Constants {
    const val OctoPrintTag = "KTOR/OctoPrint"
    const val OctoPrintApiKeyHeader = "X-Api-Key"

    val OctoPrintSuppressApiKey = AttributeKey<Boolean>("suppressApiKey")
}