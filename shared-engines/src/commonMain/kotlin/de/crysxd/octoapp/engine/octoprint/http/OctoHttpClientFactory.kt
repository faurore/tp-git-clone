package de.crysxd.octoapp.engine.octoprint.http

import de.crysxd.octoapp.engine.octoprint.Constants
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder
import de.crysxd.octoapp.engine.octoprint.api.OctoUserApi
import de.crysxd.octoapp.engine.octoprint.serializer.OctoInstantSerializer
import de.crysxd.octoapp.sharedcommon.http.DefaultHttpClient
import de.crysxd.octoapp.sharedcommon.http.framework.installBasicAuthInterceptorPlugin
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.HttpClientEngine
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.plugin
import io.ktor.client.plugins.websocket.WebSockets
import io.ktor.client.request.HttpRequestBuilder
import io.ktor.client.request.setBody
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.json
import kotlinx.datetime.Instant
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule

internal fun createOctoPrintHttpClient(
    engine: HttpClientEngine? = null,
    settings: OctoPrintEngineBuilder.OctoHttpClientSettings,
): HttpClient {
    val sharedConfig: HttpClientConfig<*>.() -> Unit = {
        expectSuccess = false
        followRedirects = true

        installJsonSerialization()
        installWebSockets()
    }

    return DefaultHttpClient(settings.general, Constants.OctoPrintTag, engine, sharedConfig).apply {
        installExceptionInspector(settings.exceptionInspector)
        installBasicAuthInterceptorPlugin()
        installApiKeyInterceptor(settings.apiKey)
        installOctoPrintGenerateExceptionInterceptorPlugin { OctoUserApi(baseUrlRotator = settings.baseUrlRotator, httpClient = it) }
    }
}

inline fun <reified T> HttpRequestBuilder.setJsonBody(body: T) {
    setBody(body)
    contentType(ContentType.Application.Json)
}

internal fun HttpClient.installExceptionInspector(inspector: ExceptionInspector) {
    plugin(HttpSend).intercept { request ->
        try {
            execute(request)
        } catch (e: Exception) {
            inspector.inspect(e)
            throw e
        }
    }
}

internal fun HttpClientConfig<*>.installWebSockets() {
    install(WebSockets) {
        pingInterval = 20_000
    }
}

internal fun HttpClientConfig<*>.installJsonSerialization() {
    install(io.ktor.client.plugins.contentnegotiation.ContentNegotiation) {
        json(
            OctoJson()
        )
    }
}

@OptIn(ExperimentalSerializationApi::class)
internal fun OctoJson() = Json {
    prettyPrint = false
    isLenient = true
    ignoreUnknownKeys = true
    encodeDefaults = true
    explicitNulls = false
    coerceInputValues = true
    allowSpecialFloatingPointValues = true

    serializersModule = SerializersModule {
        contextual(Instant::class, OctoInstantSerializer())
    }
}