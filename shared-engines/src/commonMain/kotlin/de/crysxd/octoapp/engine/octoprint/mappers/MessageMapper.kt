package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage

internal fun OctoMessage.map(): Message = when (this) {
    is OctoMessage.Current -> map()
    is OctoMessage.Connected -> map()
    is OctoMessage.Event -> map()
    is Message -> this
    is OctoMessage.ReAuthRequired,
    is OctoMessage.UnknownMessage -> Message.UnknownMessage
    else -> throw Error("Missing mapper for ${this::class.qualifiedName}")
}

internal fun OctoMessage.Event.map(): Message.Event = when (this) {
    is OctoMessage.Event.FileSelected -> Message.Event.FileSelected(
        origin = origin.map(),
        name = name,
        path = path,
    )

    is OctoMessage.Event.FirmwareData -> Message.Event.FirmwareData(
        firmwareName = firmwareName,
        machineType = machineType,
        extruderCount = extruderCount,
    )

    is OctoMessage.Event.PrinterConnected -> Message.Event.PrinterConnected(
        baudrate = baudrate,
        port = port
    )

    is OctoMessage.Event.Unknown -> Message.Event.Unknown(type)

    OctoMessage.Event.Disconnected -> Message.Event.Disconnected
    OctoMessage.Event.Connecting -> Message.Event.Connecting
    OctoMessage.Event.MovieDone -> Message.Event.MovieDone
    OctoMessage.Event.MovieFailed -> Message.Event.MovieFailed
    OctoMessage.Event.MovieRendering -> Message.Event.MovieRendering
    OctoMessage.Event.PrintCancelled -> Message.Event.PrintCancelled
    OctoMessage.Event.PrintCancelling -> Message.Event.PrintCancelling
    OctoMessage.Event.PrintDone -> Message.Event.PrintDone
    OctoMessage.Event.PrintFailed -> Message.Event.PrintFailed
    OctoMessage.Event.PrintPaused -> Message.Event.PrintPaused
    OctoMessage.Event.PrintPausing -> Message.Event.PrintPausing
    OctoMessage.Event.PrintResumed -> Message.Event.PrintResumed
    OctoMessage.Event.PrintStarted -> Message.Event.PrintStarted
    OctoMessage.Event.PrinterProfileModified -> Message.Event.PrinterProfileModified
    OctoMessage.Event.PrinterStateChanged -> Message.Event.PrinterStateChanged
    OctoMessage.Event.SettingsUpdated -> Message.Event.SettingsUpdated
    OctoMessage.Event.UpdatedFiles -> Message.Event.UpdatedFiles
}

internal fun OctoMessage.Connected.map() = Message.Connected(
    version = version,
    displayVersion = displayVersion,
)

internal fun OctoMessage.Current.map() = Message.Current(
    logs = logs,
    temps = temps.map { it.map() },
    state = state.map(),
    progress = progress.map(),
    job = job.map(),
    isHistoryMessage = isHistoryMessage,
    offsets = offsets,
    originHash = originHash,
    serverTime = serverTime.toEpochMilliseconds(),
)