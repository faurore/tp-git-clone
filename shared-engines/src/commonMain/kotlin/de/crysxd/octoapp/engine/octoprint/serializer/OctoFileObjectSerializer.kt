package de.crysxd.octoapp.engine.octoprint.serializer

import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileObject
import kotlinx.serialization.json.JsonContentPolymorphicSerializer
import kotlinx.serialization.json.JsonElement
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

internal class OctoFileObjectSerializer : JsonContentPolymorphicSerializer<OctoFileObject>(OctoFileObject::class) {
    override fun selectDeserializer(element: JsonElement) = when (element.jsonObject["type"]?.jsonPrimitive?.content) {
        OctoFileObject.FILE_TYPE_FOLDER -> OctoFileObject.Folder.serializer()
        else -> OctoFileObject.File.serializer()
    }
}