package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.MaterialsApi
import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.currentCoroutineContext

class OctoMaterialApi(
    private val delegates: List<Delegate>
) : MaterialsApi {

    override suspend fun isMaterialManagerAvailable(settings: Settings) = delegates
        .any { it.isMaterialManagerAvailable(settings) }

    override suspend fun getMaterials(settings: Settings) = with(CoroutineScope(currentCoroutineContext() + Dispatchers.SharedIO)) {
        delegates
            .filter { it.isMaterialManagerAvailable(settings) }
            .map { async { it.getMaterials(settings) } }
            .flatMap { it.await() }
    }

    override suspend fun activateMaterial(uniqueMaterialId: UniqueId) = delegates
        .firstOrNull { it.providerId == uniqueMaterialId.providerId }
        ?.activateMaterial(uniqueMaterialId)
        ?: throw IllegalStateException("No one is responsible for material: $uniqueMaterialId")


    interface Delegate : MaterialsApi {
        val providerId: String
    }
}