package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.api.JobApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.commands.JobCommand
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoJobCommand
import de.crysxd.octoapp.engine.octoprint.dto.job.OctoJob
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post

internal class OctoJobApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val eventSink: EventSink,
) : JobApi {

    override suspend fun executeJobCommand(command: JobCommand) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "job")
            when (command) {
                JobCommand.CancelJobCommand -> setJsonBody(OctoJobCommand.CancelJobCommand())
                JobCommand.PauseJobCommand -> setJsonBody(OctoJobCommand.PauseJobCommand())
                JobCommand.RestartJobCommand -> setJsonBody(OctoJobCommand.RestartJobCommand())
                JobCommand.ResumeJobCommand -> setJsonBody(OctoJobCommand.ResumeJobCommand())
                JobCommand.StartJobCommand -> setJsonBody(OctoJobCommand.StartJobCommand())
                JobCommand.TogglePauseCommand -> setJsonBody(OctoJobCommand.TogglePauseCommand())
            }
        }
    }.let {
        eventSink.injectInterpolatedEvent { cm ->
            when (command) {
                JobCommand.CancelJobCommand -> Message.Event.PrintCancelling
                JobCommand.PauseJobCommand -> Message.Event.PrintPausing
                JobCommand.RestartJobCommand -> Message.Event.PrintCancelling
                JobCommand.ResumeJobCommand -> Message.Event.PrintResumed
                JobCommand.StartJobCommand -> Message.Event.PrintStarted
                JobCommand.TogglePauseCommand -> if (cm?.state?.flags?.let { it.paused || it.pausing } == true) Message.Event.PrintResumed else Message.Event.PrintPausing
            }
        }
    }

    override suspend fun getJob() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "job")
        }.body<OctoJob>()
    }.map()
}