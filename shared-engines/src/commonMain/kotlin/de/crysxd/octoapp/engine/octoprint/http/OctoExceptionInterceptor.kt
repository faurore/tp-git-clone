package de.crysxd.octoapp.engine.octoprint.http

import de.crysxd.octoapp.engine.api.UserApi
import de.crysxd.octoapp.engine.exceptions.DownloadTooLargeException
import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.exceptions.MissingPermissionException
import de.crysxd.octoapp.engine.exceptions.PrintBootingException
import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.engine.exceptions.PrinterHttpsException
import de.crysxd.octoapp.engine.exceptions.PrinterNotFoundException
import de.crysxd.octoapp.engine.exceptions.PrinterNotOperationalException
import de.crysxd.octoapp.engine.exceptions.PrinterUnavailableException
import de.crysxd.octoapp.engine.exceptions.ngrok.NgrokTunnelNotFoundException
import de.crysxd.octoapp.engine.exceptions.octoeverywhere.OctoEverywhereCantReachPrinterException
import de.crysxd.octoapp.engine.exceptions.octoeverywhere.OctoEverywhereConnectionNotFoundException
import de.crysxd.octoapp.engine.exceptions.octoeverywhere.OctoEverywhereSubscriptionMissingException
import de.crysxd.octoapp.engine.exceptions.spaghettidetective.SpaghettiDetectiveCantReachPrinterException
import de.crysxd.octoapp.engine.exceptions.spaghettidetective.SpaghettiDetectiveTunnelNotFoundException
import de.crysxd.octoapp.engine.exceptions.spaghettidetective.SpaghettiDetectiveTunnelUsageLimitReachedException
import de.crysxd.octoapp.engine.framework.isNgrokUrl
import de.crysxd.octoapp.engine.framework.isOctoEverywhereUrl
import de.crysxd.octoapp.engine.framework.isSpaghettiDetectiveUrl
import de.crysxd.octoapp.engine.octoprint.Constants.OctoPrintTag
import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import de.crysxd.octoapp.sharedcommon.exceptions.UntrustedCertificateException
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.call.HttpClientCall
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.plugin
import io.ktor.client.statement.bodyAsText
import kotlinx.coroutines.CancellationException

private const val tag = "OctoExceptionInterceptor"

internal fun HttpClient.installOctoPrintGenerateExceptionInterceptorPlugin(
    userApiFactory: (HttpClient) -> UserApi
) = plugin(HttpSend).intercept { request ->

    try {
        val call = execute(request)
        when (call.response.status.value) {
            // OctoPrint / Generic
            101 -> call
            in 200..204 -> call
            409 -> throw PrinterNotOperationalException(call.request.url)
            401 -> throw generate401Exception(call)
            403 -> throw generate403Exception(call, userApiFactory)
            404 -> throw generate404Exception(call)
            413 -> throw DownloadTooLargeException(call.request.url)
            in 501..599 -> throw PrintBootingException(call.request.url)

            // OctoEverywhere
            601 -> throw OctoEverywhereCantReachPrinterException(call.request.url)
            603, 604, 606 -> throw OctoEverywhereConnectionNotFoundException(call.request.url, call.response.status.value)
            605 -> throw OctoEverywhereSubscriptionMissingException(call.request.url)
            607 -> throw DownloadTooLargeException(call.request.url)

            // Spaghetti Detective
            481 -> throw SpaghettiDetectiveTunnelUsageLimitReachedException(call.request.url)
            482 -> throw SpaghettiDetectiveCantReachPrinterException(call.request.url)

            else -> throw generateGenericException(call)
        }
    } catch (e: Exception) {
        val certException = e.findUntrustedCertificateException()
        Napier.d(tag = tag, message = "Handling ${e::class.qualifiedName}: ${e.message}")

        when {
            e is CancellationException -> throw e

            certException != null -> throw PrinterHttpsException(
                url = request.url.build(),
                cause = certException,
                certificate = certException.certificate,
                weakHostnameVerificationRequired = certException.weakHostnameVerificationRequired
            )

            e is NetworkException -> throw e

            e.isOrIsCausedByNamed(
                // DNS error
                "UnknownHostException",
                "NSURLErrorDomain Code=-1003",
                "UnresolvedAddressException",

                // Port closed
                "ConnectException",
                "NSURLErrorDomain Code=-1004",

                // Timeout
                "SocketTimeoutException",
                "ConnectTimeoutException",
            ) -> throw PrinterUnavailableException(
                webUrl = request.url.build(),
                e = e.unpack()
            )

            e.isOrIsCausedByNamed(
                "SSLHandshakeException",
                "SSLPeerUnverifiedException",
                "CertPathValidatorException",
                "NSURLErrorDomain Code=-1202",
            ) -> throw PrinterHttpsException(
                url = request.url.build(),
                cause = e.unpack(),
                certificate = null,
                weakHostnameVerificationRequired = false,
            )

            else -> throw NetworkException(
                originalCause = e.unpack(),
                userFacingMessage = e.unpack().message,
                webUrl = request.url.build()
            )
        }
    }
}

private fun Throwable.unpack(): Throwable = cause?.takeIf { this is CancellationException } ?: this

private fun Throwable.findUntrustedCertificateException(): UntrustedCertificateException? =
    (this as? UntrustedCertificateException) ?: cause?.findUntrustedCertificateException()

private fun Throwable.isOrIsCausedByNamed(vararg names: String): Boolean {
    fun nsErrorMatch() = this::class.simpleName == "DarwinHttpRequestException" && names.filter { it.startsWith("NS") }.any { message?.contains(it) == true }
    fun classMatch() = names.contains(this::class.simpleName)
    return (classMatch() || nsErrorMatch()) || cause?.isOrIsCausedByNamed(*names) == true
}

private suspend fun generate404Exception(
    call: HttpClientCall
): Exception {
    val body = call.response.bodyAsText()

    // Special handling for ngrok.com. If the body contains the error marker for tunnel gone, the setup is broken
    return if (call.request.url.isNgrokUrl() && body.contains("ERR_NGROK_3200")) {
        NgrokTunnelNotFoundException(call.request.url)
    } else {
        PrinterNotFoundException(httpUrl = call.request.url, body = body)
    }
}

private suspend fun generate403Exception(
    call: HttpClientCall,
    userApiFactory: (HttpClient) -> UserApi
): Exception {
    // Prevent a loop. We will below request the /currentuser endpoint to test the API key
    val invalidApiKeyException = InvalidApiKeyException(call.request.url)
    when {
        call.request.url.pathSegments.last() == "currentuser" -> {
            Napier.w(tag = OctoPrintTag, message = "Got 403 on currentuser endpoint -> assume API key no longer valid but this is weird, preventing loop")
            return invalidApiKeyException
        }

        else -> Unit
    }

    Napier.w(tag = OctoPrintTag, message = "Got 403, trying to get user")

    // We don't know what caused the 403. Requesting the currentuser will tell us whether we are a guest, meaning the API
    // key is not valid. If we are not a guest, 403 indicates a missing permission
    try {
        val isGuest = userApiFactory.invoke(call.client).getCurrentUser().isGuest
        return if (isGuest) {
            Napier.w(tag = OctoPrintTag, message = "Got 403, user is guest")
            invalidApiKeyException
        } else {
            Napier.w(tag = OctoPrintTag, message = "Got 403, permission is missing")
            MissingPermissionException(call.request.url)
        }
    } catch (e: SpaghettiDetectiveTunnelNotFoundException) {
        Napier.w(tag = OctoPrintTag, message = "Got 403, caused by TSD tunnel deleted")
        throw SpaghettiDetectiveTunnelNotFoundException(call.request.url)
    } catch (e: Exception) {
        Napier.w(tag = OctoPrintTag, message = "Got 403, failed to determine user status: $e")
        return invalidApiKeyException
    }
}

private suspend fun generate401Exception(
    call: HttpClientCall
): Exception {
    // Special case for TSD, here a 401 means the tunnel is gone (account deleted/printer removed/...)
    if (call.request.url.isSpaghettiDetectiveUrl()) {
        throw SpaghettiDetectiveTunnelNotFoundException(call.request.url)
    }

    // Special case for OctoEverywhere, here a 401 means the tunnel is broken
    if (call.request.url.isOctoEverywhereUrl()) {
        throw OctoEverywhereConnectionNotFoundException(call.request.url, call.response.status.value)
    }

    // Standard case: Basic Auth
    return call.response.headers["WWW-Authenticate"]?.let { authHeader ->
        BasicAuthRequiredException(header = authHeader, webUrl = call.request.url)
    } ?: generateGenericException(call)
}

private suspend fun generateGenericException(
    call: HttpClientCall
) = PrinterApiException(
    httpUrl = call.request.url,
    responseCode = call.response.status.value,
    body = call.response.bodyAsText()
)
