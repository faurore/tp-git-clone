package de.crysxd.octoapp.engine.models.connection

sealed class ConnectionQuality {
    object Normal : ConnectionQuality()
    data class Degraded(val reason: Throwable) : ConnectionQuality()
}