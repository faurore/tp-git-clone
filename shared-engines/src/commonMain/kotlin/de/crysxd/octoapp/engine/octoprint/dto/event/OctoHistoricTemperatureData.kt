@file:UseSerializers(OctoInstantSerializer::class)

package de.crysxd.octoapp.engine.octoprint.dto.event

import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoComponentTemperature
import de.crysxd.octoapp.engine.octoprint.serializer.OctoHistoricTemperatureDataSerializer
import de.crysxd.octoapp.engine.octoprint.serializer.OctoInstantSerializer
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable
import kotlinx.serialization.UseSerializers

@Serializable(with = OctoHistoricTemperatureDataSerializer::class)
data class OctoHistoricTemperatureData(
    val time: Instant,
    val components: Map<String, OctoComponentTemperature> = emptyMap()
)