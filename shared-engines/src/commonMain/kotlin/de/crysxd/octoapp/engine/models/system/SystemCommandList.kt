package de.crysxd.octoapp.engine.models.system

data class SystemCommandList(
    val core: List<SystemCommand> = emptyList(),
    val custom: List<SystemCommand> = emptyList(),
    val plugin: List<SystemCommand> = emptyList(),
) {
    val all get() = listOfNotNull(core, custom, plugin).flatten()
}
