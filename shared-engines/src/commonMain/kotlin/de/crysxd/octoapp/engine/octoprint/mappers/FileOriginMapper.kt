package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileOrigin

internal fun OctoFileOrigin.map() = when (this) {
    OctoFileOrigin.Local -> FileOrigin.Local
    OctoFileOrigin.SdCard -> FileOrigin.SdCard
}

internal fun FileOrigin.map() = when (this) {
    FileOrigin.Local -> OctoFileOrigin.Local
    FileOrigin.SdCard -> OctoFileOrigin.SdCard
}