package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.api.PrinterApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.commands.BedCommand
import de.crysxd.octoapp.engine.models.commands.ChamberCommand
import de.crysxd.octoapp.engine.models.commands.GcodeCommand
import de.crysxd.octoapp.engine.models.commands.PrintHeadCommand
import de.crysxd.octoapp.engine.models.commands.ToolCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoBedCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoChamberCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoGcodeCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoPrintHeadCommand
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoToolCommand
import de.crysxd.octoapp.engine.octoprint.dto.printer.OctoPrinterState
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.post

internal class OctoPrinterApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val eventSink: EventSink,
) : PrinterApi {

    override suspend fun getPrinterState() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "printer")
        }.body<OctoPrinterState>()
    }.map()

    override suspend fun executeChamberCommand(cmd: ChamberCommand) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "chamber")
            when (cmd) {
                is ChamberCommand.SetTargetTemperature -> setJsonBody(OctoChamberCommand.SetTargetTemperature(cmd.target))
                is ChamberCommand.SetTemperatureOffset -> setJsonBody(OctoChamberCommand.SetTemperatureOffset(cmd.offset))
            }
        }.let {
            if (cmd is ChamberCommand.SetTargetTemperature) {
                eventSink.injectInterpolatedTemperatureTarget(mapOf("chamber" to cmd.target))
            }
        }
    }

    override suspend fun executeToolCommand(cmd: ToolCommand) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "tool")

            when (cmd) {
                is ToolCommand.ExtrudeFilament -> setJsonBody(OctoToolCommand.ExtrudeFilament(amount = cmd.amount))
                is ToolCommand.SetTargetTemperature -> setJsonBody(OctoToolCommand.SetTargetTemperature(targets = cmd.targets))
                is ToolCommand.SetTemperatureOffset -> setJsonBody(OctoToolCommand.SetTemperatureOffset(offsets = cmd.offsets))
            }
        }.let {
            when (cmd) {
                is ToolCommand.ExtrudeFilament -> Unit
                is ToolCommand.SetTemperatureOffset -> Unit
                is ToolCommand.SetTargetTemperature -> eventSink.injectInterpolatedTemperatureTarget(cmd.targets)
            }
        }
    }

    override suspend fun executeBedCommand(cmd: BedCommand) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "bed")
            when (cmd) {
                is BedCommand.SetTargetTemperature -> setJsonBody(OctoBedCommand.SetTargetTemperature(target = cmd.target))
                is BedCommand.SetTemperatureOffset -> setJsonBody(OctoBedCommand.SetTemperatureOffset(offset = cmd.offset))
            }
        }.let {
            if (cmd is BedCommand.SetTargetTemperature) {
                eventSink.injectInterpolatedTemperatureTarget(mapOf("bed" to cmd.target))
            }
        }
    }

    override suspend fun executePrintHeadCommand(cmd: PrintHeadCommand) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "printhead")

            when (cmd) {
                is PrintHeadCommand.JogPrintHeadCommand -> setJsonBody(OctoPrintHeadCommand.JogPrintHeadCommand(x = cmd.x, y = cmd.y, z = cmd.z, speed = cmd.speed))
                is PrintHeadCommand.HomePrintHeadCommand -> setJsonBody(OctoPrintHeadCommand.HomePrintHeadCommand(axes = cmd.axes))
            }
        }
    }

    override suspend fun executeGcodeCommand(cmd: GcodeCommand) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "printer", "command")
            when (cmd) {
                is GcodeCommand.Batch -> setJsonBody(OctoGcodeCommand(commands = cmd.commands))
                is GcodeCommand.Single -> setJsonBody(OctoGcodeCommand(commands = listOf(cmd.command)))
            }
        }
    }
}