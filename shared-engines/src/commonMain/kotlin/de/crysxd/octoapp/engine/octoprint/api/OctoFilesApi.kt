package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.EventSink
import de.crysxd.octoapp.engine.api.FilesApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.commands.FileCommand
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import de.crysxd.octoapp.engine.octoprint.dto.commands.OctoFileCommand
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileList
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileObject
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.plugins.onDownload
import io.ktor.client.plugins.onUpload
import io.ktor.client.request.delete
import io.ktor.client.request.forms.InputProvider
import io.ktor.client.request.forms.formData
import io.ktor.client.request.forms.submitFormWithBinaryData
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.statement.bodyAsChannel
import io.ktor.http.ContentType
import io.ktor.http.Headers
import io.ktor.http.HttpHeaders

internal class OctoFilesApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val eventSink: EventSink,
) : FilesApi {


    override suspend fun getAllFiles(origin: FileOrigin) = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "files", origin.map().toSerialName()) {
                parameter("recursive", "true")
            }
        }.body<OctoFileList>()
    }.map()


    override suspend fun getRootFolder(origin: FileOrigin) = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "files", origin.map().toSerialName())
        }.body<OctoFileList>()
    }.map()


    override suspend fun getSubFolder(file: FileObject.Folder): FileObject.Folder = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "files", file.origin.map().toSerialName(), *file.path.splitIntoSegments())
        }.body<OctoFileObject.Folder>()
    }.map() as FileObject.Folder

    override suspend fun getFile(origin: FileOrigin, path: String): FileObject.File = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "files", origin.map().toSerialName(), *path.splitIntoSegments())
        }.body<OctoFileObject.File>()
    }.map() as FileObject.File


    override suspend fun executeFileCommand(file: FileObject, command: FileCommand) = baseUrlRotator.request<Unit> {
        httpClient.post {
            urlFromPath(baseUrl = it, "api", "files", file.origin.map().toSerialName(), *file.path.splitIntoSegments())
            when (command) {
                is FileCommand.CopyFile -> setJsonBody(OctoFileCommand.CopyFile(destination = command.destination))
                is FileCommand.MoveFile -> setJsonBody(OctoFileCommand.MoveFile(destination = command.destination))
                is FileCommand.SelectFile -> setJsonBody(OctoFileCommand.SelectFile(print = command.print))
            }
        }
    }.let {
        when (command) {
            is FileCommand.CopyFile -> Unit
            is FileCommand.MoveFile -> Unit
            is FileCommand.SelectFile -> if (command.print) {
                eventSink.injectInterpolatedEvent { Message.Event.FileSelected(origin = file.origin, path = file.path, name = file.name) }

                if (command.print && file is FileObject.File) {
                    eventSink.injectInterpolatedEvent { Message.Event.PrintStarted }
                    eventSink.injectInterpolatedPrintStart(file)
                }
            }
        }
    }

    override suspend fun deleteFile(fileObject: FileObject) = baseUrlRotator.request<Unit> {
        httpClient.delete {
            urlFromPath(baseUrl = it, "api", "files", fileObject.origin.map().toSerialName(), *fileObject.path.splitIntoSegments())
        }
    }


    override suspend fun createFolder(parent: FileObject.Folder, name: String) = baseUrlRotator.request<Unit> {
        httpClient.submitFormWithBinaryData(
            formData {
                append(key = "foldername", value = name)
                append(key = "path", value = parent.path)
            }
        ) {
            urlFromPath(baseUrl = it, "api", "files", parent.origin.map().toSerialName())
        }
    }

    override suspend fun uploadFile(parent: FileObject.Folder, input: InputProvider, name: String, progressUpdate: (Float) -> Unit) =
        baseUrlRotator.request<Unit> {
            httpClient.submitFormWithBinaryData(
                formData {
                    append(
                        key = "file",
                        value = input,
                        headers = Headers.build {
                            append(HttpHeaders.ContentDisposition, "filename=${name}")
                            append(HttpHeaders.ContentType, ContentType.Application.OctetStream.contentType)
                        }
                    )
                    append(key = "path", value = parent.path)
                }
            ) {
                urlFromPath(baseUrl = it, "api", "files", parent.origin.map().toSerialName())
                onUpload { bytesSentTotal, contentLength ->
                    progressUpdate(if (contentLength > 0) (bytesSentTotal / contentLength.toFloat()) else -1f)
                }
            }
        }

    override suspend fun downloadFile(file: FileObject.File, progressUpdate: (Float) -> Unit) = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "downloads", "files", file.origin.map().toSerialName(), *file.path.splitIntoSegments())
            onDownload { bytesSentTotal, contentLength ->
                progressUpdate(if (contentLength > 0) (bytesSentTotal / contentLength.toFloat()) else -1f)
            }
        }.bodyAsChannel()
    }

    private fun String.splitIntoSegments() = split("/").toTypedArray()

}