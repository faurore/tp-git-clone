package de.crysxd.octoapp.engine.octoprint.dto.plugins.tradfri

import kotlinx.serialization.Serializable

@Serializable
internal data class TradfriResponse(
    val state: Boolean = false
)