package de.crysxd.octoapp.engine.octoprint.dto.message

import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import kotlinx.serialization.Serializable

@Serializable
data class CompanionPluginMessage(
    val m117: String? = null,
    val mmuSelectionActive: Boolean? = null,
) : OctoMessage, Message
