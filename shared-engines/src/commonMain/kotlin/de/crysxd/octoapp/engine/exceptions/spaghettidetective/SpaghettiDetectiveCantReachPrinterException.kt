package de.crysxd.octoapp.engine.exceptions.spaghettidetective

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class SpaghettiDetectiveCantReachPrinterException(webUrl: Url) : NetworkException(
    userFacingMessage = "Spaghetti Detective can't reach your OctoPrint at the moment",
    technicalMessage = "Received error code 482 from Spaghetti Detective",
    webUrl = webUrl,
)