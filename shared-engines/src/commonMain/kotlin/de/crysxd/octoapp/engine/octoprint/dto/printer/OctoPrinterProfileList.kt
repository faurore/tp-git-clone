package de.crysxd.octoapp.engine.octoprint.dto.printer

import kotlinx.serialization.Serializable

@Serializable
data class OctoPrinterProfileList(
    val profiles: Map<String, OctoPrinterProfile>
)