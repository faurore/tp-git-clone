package de.crysxd.octoapp.engine.models.login

data class LoginResponse(
    val session: String? = null,
    val name: String? = null,
    val groups: List<String> = emptyList(),
    val isAdmin: Boolean = false
)