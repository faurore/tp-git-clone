package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.files.FileList
import de.crysxd.octoapp.engine.octoprint.dto.files.OctoFileList

internal fun OctoFileList.map() = FileList(
    total = total,
    free = free,
    files = files.map { it.map() }
)