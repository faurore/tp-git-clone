package de.crysxd.octoapp.engine.octoprint.dto.commands

import kotlinx.serialization.Serializable

@Serializable
sealed class OctoFileCommand(val command: String) {

    @Serializable
    data class SelectFile(val print: Boolean = false) : OctoFileCommand(command = "select")

    @Serializable
    data class MoveFile(val destination: String) : OctoFileCommand(command = "move")

    @Serializable
    data class CopyFile(val destination: String) : OctoFileCommand(command = "copy")

}