package de.crysxd.octoapp.engine.models.commands

@Suppress("Unused")
sealed class BedCommand {
    data class SetTargetTemperature(val target: Float) : BedCommand()
    data class SetTemperatureOffset(val offset: Float) : BedCommand()
}

