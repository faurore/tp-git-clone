package de.crysxd.octoapp.engine.octoprint.http

fun interface ExceptionInspector {
    fun inspect(e: Throwable)
}