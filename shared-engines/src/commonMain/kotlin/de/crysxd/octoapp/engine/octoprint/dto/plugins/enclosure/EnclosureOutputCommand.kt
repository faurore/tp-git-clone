package de.crysxd.octoapp.engine.octoprint.dto.plugins.enclosure

import kotlinx.serialization.Serializable

@Serializable
internal data class EnclosureOutputCommand(val status: Boolean)

