package de.crysxd.octoapp.engine.octoprint.serializer

import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.KSerializer
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder

class OctoSafeFloatSerializer : KSerializer<Float?> {

    override val descriptor: SerialDescriptor = PrimitiveSerialDescriptor("OctoSafeFloat", PrimitiveKind.FLOAT)

    override fun deserialize(decoder: Decoder): Float? = try {
        decoder.decodeFloat()
    } catch (e: Exception) {
        null
    }

    @OptIn(ExperimentalSerializationApi::class)
    override fun serialize(encoder: Encoder, value: Float?) = value?.let { encoder.encodeFloat(it) } ?: encoder.encodeNull()
}