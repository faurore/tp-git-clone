package de.crysxd.octoapp.engine.octoprint.event

import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.engine.api.LoginApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.getConnectionType
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.connection.ConnectionQuality
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoEventTransportAuthentication
import de.crysxd.octoapp.engine.octoprint.dto.event.OctoMessage
import de.crysxd.octoapp.engine.octoprint.http.OctoJson
import de.crysxd.octoapp.engine.octoprint.mappers.map
import de.crysxd.octoapp.engine.octoprint.serializer.OctoEventMessageListSerializer
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.request.parameter
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.bodyAsText
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.datetime.Clock
import kotlinx.serialization.encodeToString
import kotlin.time.Duration.Companion.seconds

internal class OctoHttpEventTransport(
    parentJob: Job,
    parenTag: String,
    loginApi: () -> LoginApi,
    private val degradationReason: () -> Throwable,
    private val eventSink: OctoEventSource,
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient,
    private val onCurrentReceived: (Pair<String, OctoMessage.Current>?) -> Unit,
    private val isBackupTransport: Boolean,
) : OctoEventTransport(
    loginApi = loginApi,
    parentJob = parentJob,
    parenTag = parenTag,
) {

    private val json = OctoJson()
    private var reconnectCounter = 0
    override val name = "XHR"
    private var loadBalancerId = (100..999).random()

    override fun connect() {
        recreateScope().apply {
            val sessionId = loadBalancerId to uuid4().toString().split("-")[0]
            onCurrentReceived(null)

            launch {
                val login = logIn()

                Napier.i(tag = tag, message = "Starting XHR transport session ${sessionId.first}/${sessionId.second}")
                receiveSingle(sessionId).second

                Napier.d(tag = tag, message = "Sending auth ${login.split(":")[0]}:****")
                send(sessionId, json.encodeToString(OctoEventTransportAuthentication(login)))

                receiveAll(sessionId)
                sendConfigurations(sessionId)

                reconnectCounter = 0
            }
        }
    }

    private fun CoroutineScope.receiveAll(sessionId: Pair<Int, String>) = launch {
        var lastBaseUrl: Url? = null
        while (isActive) {

            val (messages, baseUrl) = receiveSingle(sessionId)

            if (lastBaseUrl != baseUrl) {
                lastBaseUrl = baseUrl
                val quality = if (isBackupTransport) ConnectionQuality.Degraded(degradationReason()) else ConnectionQuality.Normal
                eventSink.emitEvent(Event.Connected(connectionType = baseUrl.getConnectionType(), connectionQuality = quality))
            }

            messages.forEach {
                if (it is OctoMessage.Current) {
                    onCurrentReceived("<unavailable>" to it)
                }

                Napier.v(tag = tag, message = "Received message ${it.toString().take(128)}")
                eventSink.emitEvent(Event.MessageReceived(it.map()))
            }


            // Simulate OctoPrint's throttle
            delay(1.seconds * config.value.throttle.throttle)
        }
    }

    private fun CoroutineScope.sendConfigurations(sessionId: Pair<Int, String>) = launch {
        launch {
            config.collectLatest {
                Napier.i(tag = tag, message = "Sending throttle: ${it.throttle}")
                send(sessionId, json.encodeToString(it.throttle))
                Napier.i(tag = tag, message = "Sending subscription: ${it.subscription}")
                send(sessionId, json.encodeToString(it.subscription))
            }
        }
    }

    private suspend fun send(sessionId: Pair<Int, String>, message: String) = baseUrlRotator.request {
        httpClient.post {
            urlFromPath(baseUrl = it, "sockjs", sessionId.first.toString(), sessionId.second, "xhr_send")
            parameter("t", Clock.System.now().toEpochMilliseconds())
            setBody("[\n\"${message.replace("\"", "\\\"")}\"\n]")
        }
    }

    private suspend fun receiveSingle(sessionId: Pair<Int, String>): Pair<List<OctoMessage>, Url> = baseUrlRotator.request { baseUrl ->
        httpClient.post {
            urlFromPath(baseUrl = baseUrl, "sockjs", sessionId.first.toString(), sessionId.second, "xhr")
            parameter("t", Clock.System.now().toEpochMilliseconds())
        }.let {
            val text = it.bodyAsText()
            when {
                text.isEmpty() || it.status == HttpStatusCode.NoContent -> emptyList()
                text[0] == 'o' -> {
                    Napier.i(tag = tag, message = "Session start confirmed")
                    emptyList()
                }
                text[0] == 'a' -> parseMessage(text.drop(1))
                else -> throw IllegalStateException("Recieved illegal message: $text")
            } to baseUrl
        }
    }

    private fun parseMessage(text: String): List<OctoMessage> =
        json.decodeFromString(deserializer = OctoEventMessageListSerializer(), string = text)

    override fun handleException(e: Throwable) {
        scope.launch {
            Napier.e(tag = tag, throwable = e, message = "Received error, reconnecting")

            if (reconnectCounter > 1) {
                eventSink.emitEvent(Event.Disconnected(e))
            }

            delay((1.seconds * reconnectCounter).coerceAtMost(3.seconds))
            Napier.w(tag = tag, message = "Reconnecting after exception")
            connect()
        }
    }
}