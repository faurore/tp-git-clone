package de.crysxd.octoapp.engine.octoprint.dto.plugins.companion

import kotlinx.serialization.Serializable

@Serializable
data class AppRegistration(
    val fcmToken: String,
    val instanceId: String,
    val displayName: String,
    val model: String,
    val appVersion: String,
    val appBuild: Long,
    val appLanguage: String,
) {
    val command = "registerForNotifications"
}