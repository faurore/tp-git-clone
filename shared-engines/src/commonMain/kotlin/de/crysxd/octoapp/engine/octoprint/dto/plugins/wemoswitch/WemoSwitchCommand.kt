package de.crysxd.octoapp.engine.octoprint.dto.plugins.wemoswitch

import kotlinx.serialization.Serializable

@Serializable
internal data class WemoSwitchCommand(val command: String, val ip: String)

