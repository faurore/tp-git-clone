package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.commands.FileCommand
import de.crysxd.octoapp.engine.models.files.FileList
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import io.ktor.client.request.forms.InputProvider
import io.ktor.utils.io.ByteReadChannel

interface FilesApi {

    suspend fun getAllFiles(origin: FileOrigin): FileList

    suspend fun getFile(origin: FileOrigin, path: String): FileObject.File

    suspend fun getRootFolder(origin: FileOrigin): FileList

    suspend fun getSubFolder(file: FileObject.Folder): FileObject.Folder

    suspend fun executeFileCommand(file: FileObject, command: FileCommand)

    suspend fun deleteFile(fileObject: FileObject)

    suspend fun createFolder(parent: FileObject.Folder, name: String)

    suspend fun uploadFile(parent: FileObject.Folder, input: InputProvider, name: String, progressUpdate: (Float) -> Unit)

    suspend fun downloadFile(file: FileObject.File, progressUpdate: (Float) -> Unit = {}): ByteReadChannel

}