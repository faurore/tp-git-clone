package de.crysxd.octoapp.engine.octoprint.dto.timelapse

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable


@Serializable
data class OctoTimelapseConfig(
    val type: Type = Type.Off,
    val fps: Int? = null,
    val postRoll: Int? = null,
    val save: Boolean = false,
    val minDelay: Float? = null,
    val interval: Int? = null,
    val retractionZHop: Float? = null,
) {

    @Serializable
    enum class Type {
        @SerialName("off")
        Off,

        @SerialName("timed")
        Timed,

        @SerialName("zchange")
        ZChange
    }
}