package de.crysxd.octoapp.engine.octoprint.dto.plugins.tuya

import kotlinx.serialization.Serializable

@Serializable
internal data class TuyaCommand(val command: String, val label: String)

