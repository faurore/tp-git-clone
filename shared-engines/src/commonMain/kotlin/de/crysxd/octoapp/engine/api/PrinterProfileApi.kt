package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.printer.PrinterProfileList

interface PrinterProfileApi {
    suspend fun getPrinterProfiles(): PrinterProfileList
}