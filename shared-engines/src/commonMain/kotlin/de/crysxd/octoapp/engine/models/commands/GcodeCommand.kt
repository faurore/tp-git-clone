package de.crysxd.octoapp.engine.models.commands

@Suppress("Unused")
sealed class GcodeCommand {
    data class Single(val command: String) : GcodeCommand()
    data class Batch(val commands: List<String>) : GcodeCommand()

}