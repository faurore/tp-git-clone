package de.crysxd.octoapp.engine.models.job

import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin

data class JobInformation(
    val file: JobFile? = null
) {
    data class JobFile(
        val name: String,
        val path: String,
        val origin: FileOrigin,
        val display: String,
        val size: Long,
        val date: Long,
    ) {
        fun asBasicFile() = FileObject.File(
            name = name,
            path = path,
            date = date,
            display = display,
            size = size,
            origin = origin,
            type = "machinecode",
            typePath = listOf("machinecode")
        )
    }
}