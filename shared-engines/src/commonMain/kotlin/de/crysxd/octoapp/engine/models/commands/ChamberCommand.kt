package de.crysxd.octoapp.engine.models.commands

@Suppress("Unused")
sealed class ChamberCommand {
    data class SetTargetTemperature(val target: Float) : ChamberCommand()
    data class SetTemperatureOffset(val offset: Float) : ChamberCommand()
}

