package de.crysxd.octoapp.engine.octoprint.dto.plugins.spoolmanager

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoSpoolManagerList(
    val allSpools: List<OctoSpoolManagerSpool> = emptyList(),
    val selectedSpools: List<OctoSpoolManagerSpool?> = emptyList()
)