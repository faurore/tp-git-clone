package de.crysxd.octoapp.engine.octoprint.dto.printer

import de.crysxd.octoapp.engine.octoprint.serializer.OctoSafeFloatSerializer
import kotlinx.serialization.Serializable

@Serializable
data class OctoComponentTemperature(
    // Enclosure plugin likes to send "" as null Float.
    @Serializable(with = OctoSafeFloatSerializer::class) val actual: Float? = null,
    @Serializable(with = OctoSafeFloatSerializer::class) val target: Float? = null,
    @Serializable(with = OctoSafeFloatSerializer::class) val offset: Float? = null
)