package de.crysxd.octoapp.engine.octoprint.mappers

import de.crysxd.octoapp.engine.models.system.SystemInfo
import de.crysxd.octoapp.engine.octoprint.dto.system.OctoSystemInfo

internal fun OctoSystemInfo.map() = SystemInfo(
    generatedAt = info.generatedAt,
    printerFirmware = info.printerFirmware,
    safeMode = info.safeMode,
)