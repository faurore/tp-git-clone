package de.crysxd.octoapp.engine.models

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import kotlinx.serialization.Serializable

@Serializable
@CommonParcelize
data class UniqueId(
    val providerId: String,
    val id: String
) : CommonParcelable {
    companion object {
        fun fromString(uniqueId: String) = uniqueId.split(":").let {
            require(it.size == 2) { "Expected 2 parts, got ${it.size} from '$uniqueId'" }
            UniqueId(it[0], it[1])
        }
    }

    override fun toString() = "$providerId:$id"
}