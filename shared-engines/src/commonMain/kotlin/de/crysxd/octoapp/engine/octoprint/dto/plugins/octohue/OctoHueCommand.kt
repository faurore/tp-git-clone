package de.crysxd.octoapp.engine.octoprint.dto.plugins.octohue

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoHueCommand(val command: String = "togglehue")
