package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.api.VersionApi
import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.octoprint.dto.version.OctoVersionInfo
import de.crysxd.octoapp.engine.octoprint.mappers.map
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get

internal class OctoVersionApi(
    val baseUrlRotator: BaseUrlRotator,
    val httpClient: HttpClient,
) : VersionApi {

    override suspend fun getVersion() = baseUrlRotator.request {
        httpClient.get {
            urlFromPath(baseUrl = it, "api", "version")
        }.body<OctoVersionInfo>()
    }.map()
}