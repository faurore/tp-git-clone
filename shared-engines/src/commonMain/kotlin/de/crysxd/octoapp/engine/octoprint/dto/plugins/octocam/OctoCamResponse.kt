package de.crysxd.octoapp.engine.octoprint.dto.plugins.octocam

import kotlinx.serialization.Serializable

@Serializable
data class OctoCamResponse(
    val torchOn: Boolean = false
)