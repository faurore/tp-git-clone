package de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective

import kotlinx.serialization.Serializable

@Serializable
sealed class SpaghettiDetectiveCommand(val command: String) {
    @Serializable
    class GetPluginStatus : SpaghettiDetectiveCommand("get_plugin_status")
}