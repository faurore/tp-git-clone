package de.crysxd.octoapp.engine.models.connection

data class Connection(
    val state: State = State.OTHER,
    val port: String? = null,
    val baudrate: Int? = null,
    val printerProfile: String
) {
    // This field is derived from a UI string and is not reliable!
    // This should only be used for UI states etc but not for important decisions
    enum class State {
        MAYBE_DETECTING_SERIAL_PORT,
        MAYBE_DETECTING_BAUDRATE,
        MAYBE_CONNECTING,
        MAYBE_OPERATIONAL,
        MAYBE_CLOSED,
        MAYBE_ERROR_FAILED_TO_AUTODETECT_SERIAL_PORT,
        MAYBE_CONNECTION_ERROR,
        MAYBE_DETECTING_SERIAL_CONNECTION,
        MAYBE_PRINTING,
        MAYBE_UNKNOWN_ERROR,
        OTHER,
    }
}