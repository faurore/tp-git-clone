package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

class WebSocketUnstableException(val webSocketUrl: Url, webUrl: Url, cause: Throwable) : NetworkException(
    webUrl = webUrl,
    technicalMessage = "Failed to establish a reliable connection to $webSocketUrl",
    userFacingMessage = "OctoApp was unable to establish a reliable WebSocket connection to receive live updates from OctoPrint. This can be caused by an improper " +
            "reverse proxy setup but is also a known issue with octo4a when used with OctoEverywhere.\n\n" +
            "OctoApp switched to an alternative transport mode, but this will cause the app to be slower and taking longer to connect.",
    originalCause = cause,
    learnMoreLink = "https://community.octoprint.org/t/reverse-proxy-configuration-examples/1107",
)