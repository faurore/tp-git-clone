package de.crysxd.octoapp.engine.models.login

data class User(
    val permissions: List<String> = emptyList(),
    val groups: List<String> = emptyList(),
    val name: String? = null
) {
    val isGuest get() = groups.contains("guests")
    val canAccessSystemCommands get() = permissions.contains("SYSTEM")
}