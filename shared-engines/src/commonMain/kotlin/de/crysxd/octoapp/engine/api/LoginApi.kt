package de.crysxd.octoapp.engine.api

import de.crysxd.octoapp.engine.models.login.LoginBody
import de.crysxd.octoapp.engine.models.login.LoginResponse


interface LoginApi {

    suspend fun passiveLogin(body: LoginBody = LoginBody()): LoginResponse

}