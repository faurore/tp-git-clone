package de.crysxd.octoapp.engine.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.ktor.http.Url

open class PrinterApiException(
    httpUrl: Url,
    val responseCode: Int,
    val body: String
) : NetworkException(
    webUrl = httpUrl,
    technicalMessage = "Received unexpected response code $responseCode from $httpUrl (body=$body)",
    userFacingMessage = "There was an error in the communication with OctoPrint because an unexpected response was received."
)