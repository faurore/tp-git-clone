package de.crysxd.octoapp.engine.models.version

import kotlinx.serialization.Serializable

@Serializable
data class VersionInfo(
    val apiVersion: String? = null,
    val severVersion: String? = null,
    val serverVersionText: String? = null,
)