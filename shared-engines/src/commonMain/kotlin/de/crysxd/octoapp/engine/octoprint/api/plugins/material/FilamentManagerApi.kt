package de.crysxd.octoapp.engine.octoprint.api.plugins.material

import de.crysxd.octoapp.engine.framework.BaseUrlRotator
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.UniqueId
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import de.crysxd.octoapp.engine.octoprint.api.OctoMaterialApi
import de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager.OctoFilamentManagerList
import de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager.OctoFilamentManagerSelection
import de.crysxd.octoapp.engine.octoprint.dto.plugins.filamentmanager.OctoFilamentManagerSelectionRequest
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.get
import io.ktor.client.request.patch
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.currentCoroutineContext
import de.crysxd.octoapp.engine.models.material.Material as IMaterial

class FilamentManagerApi(
    private val baseUrlRotator: BaseUrlRotator,
    private val httpClient: HttpClient
) : OctoMaterialApi.Delegate {

    override val providerId = OctoPlugins.FilamentManager

    private val colorLut = mapOf(
        "White" to "#ffffff",
        "Yellow" to "#ffd500",
        "Green" to "#07e328",
        "Blue" to "#07e328",
        "Gray" to "#8c8c8c",
        "Grey" to "#8c8c8c",
        "Magenta" to "#fc19d7",
        "Purple" to "#ab008f",
        "Teal" to "#04c9c6",
        "Pink" to "#ff00c3",
        "Black" to "#000000",
        "Natural" to "#e5e6d5",
        "Transparent" to "#bfbfbf",
        "Fusili" to "#00ff00",
    )

    override suspend fun isMaterialManagerAvailable(settings: Settings) = settings.plugins.filamentManager != null


    override suspend fun getMaterials(settings: Settings) = with(CoroutineScope(currentCoroutineContext() + Dispatchers.SharedIO)) {
        val selectionDeferred = async {
            baseUrlRotator.request {
                httpClient.get {
                    urlFromPath(baseUrl = it, "plugin", OctoPlugins.FilamentManager, "selections")
                }.body<OctoFilamentManagerSelection>()
            }
        }

        val material = baseUrlRotator.request {
            httpClient.get {
                urlFromPath(baseUrl = it, "plugin", OctoPlugins.FilamentManager, "spools")
            }.body<OctoFilamentManagerList>()
        }

        val selection = selectionDeferred.await()

        material.spools.map { spool ->
            val color = spool.name?.findColor()
            Material(
                id = UniqueId(providerId = providerId, id = spool.id),
                displayName = spool.name ?: spool.id,
                color = color?.value,
                colorName = color?.key,
                vendor = spool.profile.vendor ?: "Unknown",
                material = spool.profile.material ?: "Unknown",
                providerDisplayName = "FilamentManager",
                activeToolIndex = selection.selections.firstOrNull { it.spool.id == spool.id }?.tool,
                weightGrams = spool.weight?.let { w -> spool.used?.let { u -> w - u } },
            )
        }
    }

    override suspend fun activateMaterial(uniqueMaterialId: UniqueId) = baseUrlRotator.request<Unit> {
        httpClient.patch {
            urlFromPath(baseUrl = it, "plugin", "filamentmanager", "selections", "0")
            setJsonBody(
                OctoFilamentManagerSelectionRequest(
                    selection = OctoFilamentManagerSelectionRequest.Selection(
                        spool = OctoFilamentManagerSelectionRequest.SpoolSelection(
                            id = uniqueMaterialId.id
                        )
                    )
                )
            )
        }
    }

    private fun String.findColor() =
        colorLut.entries.firstOrNull { contains(it.key, ignoreCase = true) }

    @CommonParcelize
    data class Material(
        override val id: UniqueId,
        override val displayName: String,
        override val vendor: String,
        override val material: String,
        override val color: String?,
        override val colorName: String?,
        override val providerDisplayName: String,
        override val activeToolIndex: Int?,
        override val weightGrams: Float?
    ) : IMaterial {
        override fun copyWithName(displayName: String) = copy(displayName = displayName)
    }
}