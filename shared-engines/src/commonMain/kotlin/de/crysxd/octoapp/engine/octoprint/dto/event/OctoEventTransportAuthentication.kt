package de.crysxd.octoapp.engine.octoprint.dto.event

import kotlinx.serialization.Serializable

@Serializable
internal data class OctoEventTransportAuthentication(
    val auth: String
)