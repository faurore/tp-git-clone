package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.models.version.VersionInfo
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoVersionApiTest {

    @Test
    fun WHEN_server_version_is_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                        {
                            "api": "0.1",
                            "server": "1.8.3",
                            "text": "OctoPrint 1.8.3"
                        }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoVersionApi(rotator, http)
        //endregion
        //region WHEN
        val login = target.getVersion()
        //endregion
        //region THEN
        assertEquals(
            expected = VersionInfo(
                serverVersionText = "OctoPrint 1.8.3",
                severVersion = "1.8.3",
                apiVersion = "0.1"
            ),
            actual = login,
            message = "Expected response to match"
        )
        //endregion
    }
}