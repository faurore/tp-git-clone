package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.octoprint.Constants.OctoPrintApiKeyHeader
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class OctoPrintProbeApiTest {

    @Test
    fun WHEN_connection_is_probed_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertTrue(
                    actual = !request.headers.contains(OctoPrintApiKeyHeader),
                    message = "Expected API key to be absent"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = """
                      {
                        "groups": [
                            "guests"
                        ],
                        "name": null,
                        "permissions": []
                    }
                    """.trimIndent(),
                )
            }
        )
        val target = OctoProbeApi(rotator, http)
        //endregion
        //region WHEN
        val success = target.probe()
        //endregion
        //region THEN
        assertTrue(
            actual = success,
            message = "Expected success"
        )
        //endregion
    }

    @Test
    fun WHEN_connection_is_probed_but_invalid_response_is_returned_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertTrue(
                    actual = !request.headers.contains(OctoPrintApiKeyHeader),
                    message = "Expected API key to be absent"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    content = "{}",
                )
            }
        )
        val target = OctoProbeApi(rotator, http)
        //endregion
        //region WHEN
        val success = target.probe()
        //endregion
        //region THEN
        assertFalse(
            actual = success,
            message = "Expected failure"
        )
        //endregion
    }

    @Test
    fun WHEN_connection_is_probed_but_invalid_return_code_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                assertTrue(
                    actual = !request.headers.contains(OctoPrintApiKeyHeader),
                    message = "Expected API key to be absent"
                )
                respond(
                    status = HttpStatusCode.Forbidden,
                    content = ""
                )
            }
        )
        val target = OctoProbeApi(rotator, http)
        //endregion
        //region WHEN
        val success = target.probe()
        //endregion
        //region THEN
        assertFalse(
            actual = success,
            message = "Expected failure"
        )
        //endregion
    }

    @Test
    fun WHEN_connection_is_probed_but_fails_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://this-does-not-exist-too-bad.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
        )
        val target = OctoProbeApi(rotator, http)
        //endregion
        //region WHEN
        val success = target.probe()
        //endregion
        //region THEN
        assertFalse(
            actual = success,
            message = "Expected failure"
        )
        //endregion
    }
}