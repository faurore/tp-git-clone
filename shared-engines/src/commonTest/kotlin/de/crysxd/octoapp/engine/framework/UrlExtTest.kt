package de.crysxd.octoapp.engine.framework

import de.crysxd.octoapp.engine.models.connection.ConnectionType
import io.ktor.http.Url
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class UrlExtTest {


    @Test
    fun WHEN_a_octoeverywhere_url_is_probed_THEN_it_is_detected() {
        assertEquals(
            expected = false,
            actual = Url("https://octoeverywhere.com").isOctoEverywhereUrl(),
            message = "Expected base not to be detected"
        )
        assertEquals(
            expected = true,
            actual = Url("https://shared-something.octoeverywhere.com").isOctoEverywhereUrl(),
            message = "Expected shared to be detected"
        )
        assertEquals(
            expected = true,
            actual = Url("https://something.octoeverywhere.com").isOctoEverywhereUrl(),
            message = "Expected app link to be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.google.com").isOctoEverywhereUrl(),
            message = "Expected Google to not be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.obico.com").isOctoEverywhereUrl(),
            message = "Expected Obico to not be detected"
        )
    }

    @Test
    fun WHEN_a_shared_octoeverywhere_url_is_probed_THEN_it_is_detected() {
        assertEquals(
            expected = false,
            actual = Url("https://octoeverywhere.com").isSharedOctoEverywhereUrl(),
            message = "Expected base not to be detected"
        )
        assertEquals(
            expected = true,
            actual = Url("https://shared-something.octoeverywhere.com").isSharedOctoEverywhereUrl(),
            message = "Expected shared to be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.octoeverywhere.com").isSharedOctoEverywhereUrl(),
            message = "Expected app link to be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.ngrok.com").isSharedOctoEverywhereUrl(),
            message = "Expected Google to not be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.obico.com").isSharedOctoEverywhereUrl(),
            message = "Expected Obico to not be detected"
        )
    }

    @Test
    fun WHEN_a_ngrok_url_is_probed_THEN_it_is_detected() {
        assertEquals(
            expected = false,
            actual = Url("https://ngrok.io").isNgrokUrl(),
            message = "Expected base not to be detected"
        )
        assertEquals(
            expected = true,
            actual = Url("https://something.ngrok.io").isNgrokUrl(),
            message = "Expected shared to be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.google.com").isNgrokUrl(),
            message = "Expected Google to not be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.obico.com").isNgrokUrl(),
            message = "Expected Obico to not be detected"
        )
    }

    @Test
    fun WHEN_a_tsd_or_obico_url_is_probed_THEN_it_is_detected() {
        assertEquals(
            expected = true,
            actual = Url("https://thespaghettidetective.com").isSpaghettiDetectiveUrl(),
            message = "Expected base to be detected"
        )
        assertEquals(
            expected = true,
            actual = Url("https://obico.io").isSpaghettiDetectiveUrl(),
            message = "Expected base to be detected"
        )
        assertEquals(
            expected = true,
            actual = Url("https://something.tunnels.app.thespaghettidetective.com").isSpaghettiDetectiveUrl(),
            message = "Expected tunel to be detected"
        )
        assertEquals(
            expected = true,
            actual = Url("https://something.tunnels.app.obico.io").isSpaghettiDetectiveUrl(),
            message = "Expected tunel to be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.google.com").isSpaghettiDetectiveUrl(),
            message = "Expected Google to not be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.obico.com").isSpaghettiDetectiveUrl(),
            message = "Expected Obico to not be detected"
        )
    }

    @Test
    fun WHEN_a_tailscale_url_is_probed_THEN_it_is_detected() {
        assertEquals(
            expected = true,
            actual = Url("https://100.0.0.1").isTailscale(),
            message = "Expected Tailscale IP to be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://192.168.0.1").isTailscale(),
            message = "Expected local IP not to be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.google.com").isTailscale(),
            message = "Expected Google to not be detected"
        )
        assertEquals(
            expected = false,
            actual = Url("https://something.obico.com").isTailscale(),
            message = "Expected Obico to not be detected"
        )
    }

    @Test
    fun WHEN_a_url_is_probed_THEN_the_correct_connection_type_is_detected() {
        assertEquals(
            expected = ConnectionType.SpaghettiDetective,
            actual = Url("https://thespaghettidetective.com").getConnectionType(),
            message = "Expected base to be detected"
        )
        assertEquals(
            expected = ConnectionType.SpaghettiDetective,
            actual = Url("https://obico.io").getConnectionType(),
            message = "Expected base to be detected"
        )
        assertEquals(
            expected = ConnectionType.Ngrok,
            actual = Url("https://something.ngrok.io").getConnectionType(),
            message = "Expected tunel to be detected"
        )
        assertEquals(
            expected = ConnectionType.OctoEverywhere,
            actual = Url("https://shared-something.octoeverywhere.com").getConnectionType(),
            message = "Expected tunel to be detected"
        )
        assertEquals(
            expected = ConnectionType.OctoEverywhere,
            actual = Url("https://something.octoeverywhere.com").getConnectionType(),
            message = "Expected Google to not be detected"
        )
        assertEquals(
            expected = ConnectionType.Tailscale,
            actual = Url("https://100.1.0.0").getConnectionType(),
            message = "Expected Obico to not be detected"
        )
        assertEquals(
            expected = ConnectionType.DefaultCloud,
            actual = Url("https://google.com").getConnectionType(),
            message = "Expected default to not returned"
        )
    }

    @Test
    fun WHEN_checked_if_a_url_is_based_on_another_THEN_the_correct_result_is_returned() {
        assertFalse(
            actual = Url("https://thespaghettidetective.com/some/path/").isBasedOn(null),
            message = "Expected base to be detected"
        )
        assertTrue(
            actual = Url("https://thespaghettidetective.com/some/path/").isBasedOn(Url("https://thespaghettidetective.com/some/path/")),
            message = "Expected base to be detected"
        )
        assertTrue(
            actual = Url("https://thespaghettidetective.com/some/path/").isBasedOn(Url("https://thespaghettidetective.com/some/path")),
            message = "Expected base to be detected"
        )
        assertTrue(
            actual = Url("https://thespaghettidetective.com/some/path").isBasedOn(Url("https://thespaghettidetective.com/some/path/")),
            message = "Expected base to be detected"
        )
        assertTrue(
            actual = Url("https://thespaghettidetective.com/some/path").isBasedOn(Url("https://thespaghettidetective.com/")),
            message = "Expected base to be detected"
        )
        assertTrue(
            actual = Url("https://thespaghettidetective.com/some/path").isBasedOn(Url("https://thespaghettidetective.com")),
            message = "Expected base to be detected"
        )
        assertTrue(
            actual = Url("https://user@thespaghettidetective.com/some/path").isBasedOn(Url("https://user@thespaghettidetective.com/some/path/")),
            message = "Expected base to be detected"
        )
        assertTrue(
            actual = Url("https://thespaghettidetective.com/some/path").isBasedOn(Url("https://user@thespaghettidetective.com/some/path/")),
            message = "Expected base to be detected"
        )
        assertFalse(
            actual = Url("https://thespaghettidetective2.com/some/path").isBasedOn(Url("https://user@thespaghettidetective.com/some/path/")),
            message = "Expected base to be detected"
        )
        assertFalse(
            actual = Url("https://thespaghettidetective.com:500/some/path").isBasedOn(Url("https://user@thespaghettidetective.com/some/path/")),
            message = "Expected base to be detected"
        )
        assertFalse(
            actual = Url("http://thespaghettidetective.com/some/path").isBasedOn(Url("https://user@thespaghettidetective.com/some/path/")),
            message = "Expected base to be detected"
        )
    }
}