package de.crysxd.octoapp.engine.framework

import io.ktor.client.HttpClient
import io.ktor.client.plugins.HttpTimeout
import io.ktor.client.request.get
import io.ktor.http.Url
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class BaseUrlRotatorTest {

    @Test
    fun WHEN_a_host_is_unreachable_THEN_the_next_base_url_is_used() = runBlocking {
        //region GIVEN
        val baseUrls = listOf(
            Url("http://127.0.0.2"), // Wrong host
            Url("https://localhost:1111"), // Wrong port
            Url("https://sidof.local"), // Wrong local DNS
            Url("http://thissyudfysfsdf.com"), // Wrong DNS
            Url("http://gstatic.com"), // Correct
        )

        val http = HttpClient {
            install(HttpTimeout) {
                connectTimeoutMillis = 2_000
                socketTimeoutMillis = 2_000
            }
        }

        val target = ActiveBaseUrlRotator(baseUrls)
        //endregion
        //region WHEN
        val recordedBaseUrls = mutableListOf<Url>()
        val response = target.request { baseUrl ->
            recordedBaseUrls += baseUrl
            http.get {
                urlFromPath(baseUrl = baseUrl, "generate_204")
            }
        }

        println("Status is ${response.status}")
        //endregion
        //region THEN
        assertEquals(expected = recordedBaseUrls, actual = baseUrls, message = "Expected all baseUrls to be tried")
        assertEquals(expected = 204, actual = response.status.value, message = "Expected status to be 204")
        //endregion
    }
}