package de.crysxd.octoapp.engine.framework

import io.ktor.client.HttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respondOk
import io.ktor.client.request.get
import io.ktor.http.Url
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class HttpRequestBuilderTest {

    @Test
    fun WHEN_a_url_is_build_THEN_the_result_is_correct() = runBlocking {
        //region GIVEN
        val http = HttpClient(MockEngine { respondOk() })
        //endregion
        //region WHEN
        http.get {
            urlFromPath(baseUrl = Url("http://gstatic.com"), "generate_204")
            assertEquals(expected = "http://gstatic.com/generate_204", actual = url.buildString(), message = "Expected URL to match")

            urlFromPath(baseUrl = Url("http://gstatic.com"), "generate", "204")
            assertEquals(expected = "http://gstatic.com/generate/204", actual = url.buildString(), message = "Expected URL to match")

            urlFromPath(baseUrl = Url("http://gstatic.com"), "generate/204")
            assertEquals(expected = "http://gstatic.com/generate%2F204", actual = url.buildString(), message = "Expected URL to match")

            urlFromPath(baseUrl = Url("http://gstatic.com"), "generate 204")
            assertEquals(expected = "http://gstatic.com/generate%20204", actual = url.buildString(), message = "Expected URL to match")

            urlFromPath(baseUrl = Url("http://gstatic.com"), "generate+204")
            assertEquals(expected = "http://gstatic.com/generate+204", actual = url.buildString(), message = "Expected URL to match")

            urlFromPath(baseUrl = Url("http://gstatic.com"), "generate?204")
            assertEquals(expected = "http://gstatic.com/generate%3F204", actual = url.buildString(), message = "Expected URL to match")

            urlFromPath(baseUrl = Url("http://gstatic.com"), "generate&204")
            assertEquals(expected = "http://gstatic.com/generate%26204", actual = url.buildString(), message = "Expected URL to match")

            urlFromPath(baseUrl = Url("http://gstatic.com"), "generate🥹204")
            assertEquals(expected = "http://gstatic.com/generate%F0%9F%A5%B9204", actual = url.buildString(), message = "Expected URL to match")

            urlFromPath(baseUrl = Url("http://gstatic.com"), "generate%204")
            assertEquals(expected = "http://gstatic.com/generate%25204", actual = url.buildString(), message = "Expected URL to match")
        }
        //endregion
        //region THEN
        Unit
        //endregion
    }

    @Test
    fun WHEN_a_url_is_build_from_an_encoded_path_THEN_the_result_is_correct() = runBlocking {
        //region GIVEN
        val http = HttpClient(MockEngine { respondOk() })
        //endregion
        //region WHEN
        http.get {
            urlFromEncodedPath(baseUrl = Url("http://gstatic.com"), "/downloads/timelapse/test%20%5B%20182%20%5D_20221029142807.mp4")
            assertEquals(
                expected = "http://gstatic.com/downloads/timelapse/test%20%5B%20182%20%5D_20221029142807.mp4",
                actual = url.buildString(),
                message = "Expected URL to match"
            )
        }
        http.get {
            urlFromEncodedPath(baseUrl = Url("http://gstatic.com"), "/downloads/timelapse/test .mp4")
            assertEquals(expected = "http://gstatic.com/downloads/timelapse/test .mp4", actual = url.buildString(), message = "Expected URL to match")
        }
        //endregion
        //region THEN
        Unit
        //endregion
    }
}