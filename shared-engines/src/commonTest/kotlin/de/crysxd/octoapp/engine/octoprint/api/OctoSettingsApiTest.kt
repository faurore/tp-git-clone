package de.crysxd.octoapp.engine.octoprint.api

import de.crysxd.octoapp.engine.framework.ActiveBaseUrlRotator
import de.crysxd.octoapp.engine.mocks.assertGet
import de.crysxd.octoapp.engine.models.settings.Settings
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.engine.octoprint.OctoPrintEngineBuilder.OctoHttpClientSettings
import de.crysxd.octoapp.engine.octoprint.http.createOctoPrintHttpClient
import io.ktor.client.engine.mock.MockEngine
import io.ktor.client.engine.mock.respond
import io.ktor.http.HttpStatusCode
import io.ktor.http.Url
import io.ktor.http.headersOf
import kotlinx.coroutines.runBlocking
import kotlin.test.Test
import kotlin.test.assertEquals

class OctoSettingsApiTest {

    @Test
    fun WHEN_settings_are_loaded_THEN_the_response_is_returned() = runBlocking {
        //region GIVEN
        val rotator = ActiveBaseUrlRotator(listOf(Url("http://gstatic.com")))
        val http = createOctoPrintHttpClient(
            settings = OctoHttpClientSettings(
                apiKey = "key",
                baseUrlRotator = rotator,
            ),
            engine = MockEngine { request ->
                request.assertGet()
                assertEquals(
                    expected = Url("http://gstatic.com/api/settings"),
                    actual = request.url,
                    message = "Expected body to match"
                )
                respond(
                    status = HttpStatusCode.OK,
                    headers = headersOf("Content-Type", "application/json"),
                    //region content = ...
                    content = """
                      {
                            "api": {
                                "allowCrossOrigin": false,
                                "key": "522217826CF841A78537F56765092AA8"
                            },
                            "appearance": {
                                "closeModalsWithClick": true,
                                "color": "orange",
                                "colorIcon": true,
                                "colorTransparent": false,
                                "defaultLanguage": "_default",
                                "fuzzyTimes": true,
                                "name": "Beagle",
                                "showFahrenheitAlso": false,
                                "showInternalFilename": true
                            },
                            "devel": {
                                "pluginTimings": false
                            },
                            "feature": {
                                "autoUppercaseBlacklist": [
                                    "M117",
                                    "M118"
                                ],
                                "g90InfluencesExtruder": false,
                                "keyboardControl": true,
                                "modelSizeDetection": true,
                                "pollWatched": false,
                                "printCancelConfirmation": true,
                                "printStartConfirmation": false,
                                "rememberFileFolder": false,
                                "sdSupport": true,
                                "temperatureGraph": true,
                                "uploadOverwriteConfirmation": true
                            },
                            "folder": {
                                "timelapse": "/octoprint/octoprint/timelapse",
                                "uploads": "/octoprint/octoprint/uploads",
                                "watched": "/octoprint/octoprint/watched"
                            },
                            "gcodeAnalysis": {
                                "bedZ": 0.0,
                                "runAt": "idle"
                            },
                            "plugins": {
                                "action_command_notification": {
                                    "enable": true,
                                    "enable_popups": false
                                },
                                "action_command_prompt": {
                                    "command": "M876",
                                    "enable": "detected",
                                    "enable_emergency_sending": true,
                                    "enable_signal_support": true
                                },
                                "announcements": {
                                    "channel_order": [
                                        "_important",
                                        "_releases",
                                        "_blog",
                                        "_plugins",
                                        "_octopi"
                                    ],
                                    "channels": {
                                        "_blog": {
                                            "description": "Development news, community spotlights, OctoPrint On Air episodes and more from the official OctoBlog.",
                                            "name": "On the OctoBlog",
                                            "priority": 2,
                                            "read_until": 1661175900,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/octoblog.xml"
                                        },
                                        "_important": {
                                            "description": "Important announcements about OctoPrint.",
                                            "name": "Important Announcements",
                                            "priority": 1,
                                            "read_until": 1521111600,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/important.xml"
                                        },
                                        "_octopi": {
                                            "description": "News around OctoPi, the Raspberry Pi image including OctoPrint.",
                                            "name": "OctoPi News",
                                            "priority": 2,
                                            "read_until": 1659616200,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/octopi.xml"
                                        },
                                        "_plugins": {
                                            "description": "Announcements of new plugins released on the official Plugin Repository.",
                                            "name": "New Plugins in the Repository",
                                            "priority": 2,
                                            "read_until": 1662958800,
                                            "type": "rss",
                                            "url": "https://plugins.octoprint.org/feed.xml"
                                        },
                                        "_releases": {
                                            "description": "Announcements of new releases and release candidates of OctoPrint.",
                                            "name": "Release Announcements",
                                            "priority": 2,
                                            "read_until": 1663673400,
                                            "type": "rss",
                                            "url": "https://octoprint.org/feeds/releases.xml"
                                        }
                                    },
                                    "display_limit": 3,
                                    "enabled_channels": [
                                        "_important",
                                        "_releases",
                                        "_blog",
                                        "_plugins",
                                        "_octopi"
                                    ],
                                    "forced_channels": [
                                        "_important"
                                    ],
                                    "summary_limit": 300,
                                    "ttl": 360
                                },
                                "backup": {
                                    "restore_unsupported": false
                                },
                                "cancelobject": {
                                    "aftergcode": null,
                                    "allowed": "",
                                    "beforegcode": null,
                                    "ignored": "ENDGCODE,STARTGCODE",
                                    "markers": true,
                                    "object_regex": [
                                        {
                                            "objreg": "; process (.*)"
                                        },
                                        {
                                            "objreg": ";MESH:(.*)"
                                        },
                                        {
                                            "objreg": "; printing object (.*)"
                                        },
                                        {
                                            "objreg": ";PRINTING: (.*)"
                                        }
                                    ],
                                    "reptag": "Object",
                                    "shownav": true,
                                    "stoptags": false
                                },
                                "discovery": {
                                    "addresses": null,
                                    "httpPassword": null,
                                    "httpUsername": null,
                                    "ignoredAddresses": null,
                                    "ignoredInterfaces": null,
                                    "interfaces": null,
                                    "model": {
                                        "description": null,
                                        "name": null,
                                        "number": null,
                                        "serial": null,
                                        "url": null,
                                        "vendor": null,
                                        "vendorUrl": null
                                    },
                                    "pathPrefix": null,
                                    "publicHost": null,
                                    "publicPort": null,
                                    "upnpUuid": "bda5fb9e-3a29-4e2b-af2f-fbfe4dc97e99",
                                    "zeroConf": []
                                },
                                "errortracking": {
                                    "enabled": true,
                                    "enabled_unreleased": false,
                                    "unique_id": "4a1caac0-a9df-4b26-a72f-ef4ee91d24bc",
                                    "url_coreui": "https://e1d6b5d760f241408382d62a3e7fb416@o118517.ingest.sentry.io/1374096",
                                    "url_server": "https://af30d06358144fb8af076ffd8984136d@o118517.ingest.sentry.io/1373987"
                                },
                                "eventmanager": {
                                    "availableEvents": [
                                        "Startup",
                                        "Shutdown",
                                        "ConnectivityChanged",
                                        "Connecting",
                                        "Connected",
                                        "Disconnecting",
                                        "Disconnected",
                                        "PrinterStateChanged",
                                        "PrinterReset",
                                        "ClientOpened",
                                        "ClientClosed",
                                        "ClientAuthed",
                                        "ClientDeauthed",
                                        "UserLoggedIn",
                                        "UserLoggedOut",
                                        "Upload",
                                        "FileSelected",
                                        "FileDeselected",
                                        "UpdatedFiles",
                                        "MetadataAnalysisStarted",
                                        "MetadataAnalysisFinished",
                                        "MetadataStatisticsUpdated",
                                        "FileAdded",
                                        "FileRemoved",
                                        "FileMoved",
                                        "FolderAdded",
                                        "FolderRemoved",
                                        "FolderMoved",
                                        "TransferStarted",
                                        "TransferDone",
                                        "TransferFailed",
                                        "PrintStarted",
                                        "PrintDone",
                                        "PrintFailed",
                                        "PrintCancelling",
                                        "PrintCancelled",
                                        "PrintPaused",
                                        "PrintResumed",
                                        "Error",
                                        "PowerOn",
                                        "PowerOff",
                                        "Home",
                                        "ZChange",
                                        "Waiting",
                                        "Dwelling",
                                        "Cooling",
                                        "Alert",
                                        "Conveyor",
                                        "Eject",
                                        "EStop",
                                        "PositionUpdate",
                                        "FirmwareData",
                                        "ToolChange",
                                        "RegisteredMessageReceived",
                                        "CommandSuppressed",
                                        "InvalidToolReported",
                                        "FilamentChange",
                                        "CaptureStart",
                                        "CaptureDone",
                                        "CaptureFailed",
                                        "PostRollStart",
                                        "PostRollEnd",
                                        "MovieRendering",
                                        "MovieDone",
                                        "MovieFailed",
                                        "SlicingStarted",
                                        "SlicingDone",
                                        "SlicingFailed",
                                        "SlicingCancelled",
                                        "SlicingProfileAdded",
                                        "SlicingProfileModified",
                                        "SlicingProfileDeleted",
                                        "PrinterProfileAdded",
                                        "PrinterProfileModified",
                                        "PrinterProfileDeleted",
                                        "SettingsUpdated",
                                        "plugin_backup_backup_created",
                                        "plugin_firmware_check_warning",
                                        "plugin_ngrok_connected",
                                        "plugin_ngrok_closed",
                                        "plugin_pluginmanager_install_plugin",
                                        "plugin_pluginmanager_uninstall_plugin",
                                        "plugin_pluginmanager_enable_plugin",
                                        "plugin_pluginmanager_disable_plugin",
                                        "plugin_psucontrol_psu_state_changed",
                                        "plugin_softwareupdate_update_succeeded",
                                        "plugin_softwareupdate_update_failed"
                                    ],
                                    "subscriptions": []
                                },
                                "firmware_check": {
                                    "ignore_infos": false
                                },
                                "gcodeviewer": {
                                    "mobileSizeThreshold": 2097152,
                                    "sizeThreshold": 20971520,
                                    "skipUntilThis": null
                                },
                                "mmu2filamentselect": {
                                    "filament1": "",
                                    "filament2": "",
                                    "filament3": "",
                                    "filament4": "",
                                    "filament5": "",
                                    "labelSource": "manual",
                                    "timeout": 30,
                                    "timeoutAction": "printerDialog"
                                },
                                "multicam": {
                                    "multicam_profiles": [
                                        {
                                            "URL": "http://192.168.1.242/webcam/?action=stream",
                                            "flipH": false,
                                            "flipV": false,
                                            "isButtonEnabled": false,
                                            "name": "Default",
                                            "rotate90": false,
                                            "snapshot": "http:/192.168.1.242/webcam/?action=snapshot",
                                            "streamRatio": "16:9"
                                        },
                                        {
                                            "URL": "http://octopi.local/webcam/?action=stream",
                                            "flipH": false,
                                            "flipV": false,
                                            "isButtonEnabled": true,
                                            "name": "Webcam 1",
                                            "rotate90": false,
                                            "snapshot": "http://octopi.local/webcam/?action=stream",
                                            "streamRatio": "16:9"
                                        }
                                    ]
                                },
                                "ngrok": {
                                    "auth_name": "name",
                                    "auth_pass": "pass",
                                    "auto_connect": true,
                                    "disable_local_ip_check": false,
                                    "hostname": "",
                                    "port": 5000,
                                    "region": "ap",
                                    "show_qr_code": false,
                                    "subdomain": "",
                                    "token": "1x5BkMgxgzwjwfLQ7qQweO0cg7U_5GK47r7JPgLcEXNbAPmap",
                                    "trust_basic_authentication": false
                                },
                                "octoapp": {
                                    "encryptionKey": "4c7cf1e0-9720-4a18-ad66-4acaf3790917",
                                    "version": "1.0.12"
                                },
                                "octoeverywhere": {
                                    "AddPrinterUrl": "https://octoeverywhere.com/getstarted?isFromOctoPrint=true&printerid=347R9DNGCJZ5IS3D9VYK7JLL1J5REM7ZRKZZN9K2BANG455618ZZZY4QBYC0",
                                    "ConnectedAccounts": "",
                                    "HasConnectedAccounts": false,
                                    "HttpFrontendIsHttps": false,
                                    "HttpFrontendPort": 5001,
                                    "NoAccountConnectedLastInformDateTime": "Mon, 26 Sep 2022 08:40:02 GMT",
                                    "Pid": "pid",
                                    "PluginUpdateRequired": false,
                                    "PluginVersion": "1.10.5",
                                    "PrinterKey": "key"
                                },
                                "pluginmanager": {
                                    "confirm_disable": false,
                                    "dependency_links": false,
                                    "hidden": [],
                                    "ignore_throttled": false,
                                    "notices": "https://plugins.octoprint.org/notices.json",
                                    "notices_ttl": 360,
                                    "pip_args": null,
                                    "pip_force_user": false,
                                    "repository": "https://plugins.octoprint.org/plugins.json",
                                    "repository_ttl": 1440
                                },
                                "psucontrol": {
                                    "GPIODevice": "",
                                    "autoOn": false,
                                    "autoOnTriggerGCodeCommands": "G0,G1,G2,G3,G10,G11,G28,G29,G32,M104,M106,M109,M140,M190",
                                    "connectOnPowerOn": false,
                                    "disconnectOnPowerOff": false,
                                    "enablePowerOffWarningDialog": true,
                                    "enablePseudoOnOff": false,
                                    "idleIgnoreCommands": "M105",
                                    "idleTimeout": 30,
                                    "idleTimeoutWaitTemp": 50,
                                    "invertonoffGPIOPin": false,
                                    "invertsenseGPIOPin": false,
                                    "offGCodeCommand": "M81",
                                    "offSysCommand": "",
                                    "onGCodeCommand": "M80",
                                    "onSysCommand": "",
                                    "onoffGPIOPin": 0,
                                    "postOnDelay": 0.0,
                                    "powerOffWhenIdle": false,
                                    "pseudoOffGCodeCommand": "M81",
                                    "pseudoOnGCodeCommand": "M80",
                                    "senseGPIOPin": 0,
                                    "senseGPIOPinPUD": "",
                                    "sensePollingInterval": 5,
                                    "senseSystemCommand": "",
                                    "sensingMethod": "INTERNAL",
                                    "sensingPlugin": "",
                                    "switchingMethod": "GCODE",
                                    "switchingPlugin": "",
                                    "turnOffWhenError": false,
                                    "turnOnWhenApiUploadPrint": false
                                },
                                "softwareupdate": {
                                    "cache_ttl": 1440,
                                    "check_overlay_py2_url": "https://plugins.octoprint.org/update_check_overlay_py2.json",
                                    "check_overlay_ttl": 360,
                                    "check_overlay_url": "https://plugins.octoprint.org/update_check_overlay.json",
                                    "credentials": {},
                                    "ignore_throttled": false,
                                    "minimum_free_storage": 150,
                                    "notify_users": true,
                                    "octoprint_branch_mappings": [
                                        {
                                            "branch": "master",
                                            "commitish": [
                                                "master"
                                            ],
                                            "name": "Stable"
                                        },
                                        {
                                            "branch": "rc/maintenance",
                                            "commitish": [
                                                "rc/maintenance"
                                            ],
                                            "name": "Maintenance RCs"
                                        },
                                        {
                                            "branch": "rc/devel",
                                            "commitish": [
                                                "rc/maintenance",
                                                "rc/devel"
                                            ],
                                            "name": "Devel RCs"
                                        }
                                    ],
                                    "octoprint_checkout_folder": null,
                                    "octoprint_method": "pip",
                                    "octoprint_pip_target": "https://github.com/OctoPrint/OctoPrint/archive/{target_version}.zip",
                                    "octoprint_release_channel": "master",
                                    "octoprint_tracked_branch": "staging/bugfix",
                                    "octoprint_type": "github_release",
                                    "pip_command": null,
                                    "pip_enable_check": false,
                                    "queued_updates": [],
                                    "updatelog_cutoff": 43200
                                },
                                "tracking": {
                                    "enabled": false,
                                    "events": {
                                        "commerror": true,
                                        "plugin": true,
                                        "pong": true,
                                        "printer": true,
                                        "printer_safety_check": true,
                                        "printjob": true,
                                        "slicing": true,
                                        "startup": true,
                                        "throttled": true,
                                        "update": true,
                                        "webui_load": true
                                    },
                                    "ping": null,
                                    "pong": 86400,
                                    "server": null,
                                    "unique_id": "1499cbbc-a778-42ab-b32c-c557f1ed59bd"
                                },
                                "virtual_printer": {
                                    "ambientTemperature": 21.3,
                                    "brokenM29": true,
                                    "brokenResend": false,
                                    "busyInterval": 2.0,
                                    "capabilities": {
                                        "AUTOREPORT_POS": false,
                                        "AUTOREPORT_SD_STATUS": true,
                                        "AUTOREPORT_TEMP": true,
                                        "EMERGENCY_PARSER": true,
                                        "EXTENDED_M20": false
                                    },
                                    "commandBuffer": 4,
                                    "echoOnM117": true,
                                    "enable_eeprom": true,
                                    "enabled": true,
                                    "errors": {
                                        "checksum_mismatch": "Checksum mismatch",
                                        "checksum_missing": "Missing checksum",
                                        "command_unknown": "Unknown command {}",
                                        "lineno_mismatch": "expected line {} got {}",
                                        "lineno_missing": "No Line Number with checksum, Last Line: {}",
                                        "maxtemp": "MAXTEMP triggered!",
                                        "mintemp": "MINTEMP triggered!"
                                    },
                                    "firmwareName": "Virtual Marlin 1.0",
                                    "forceChecksum": false,
                                    "hasBed": true,
                                    "hasChamber": false,
                                    "includeCurrentToolInTemps": true,
                                    "includeFilenameInOpened": true,
                                    "klipperTemperatureReporting": false,
                                    "locked": false,
                                    "m105NoTargetFormatString": "{heater}:{actual:.2f}",
                                    "m105TargetFormatString": "{heater}:{actual:.2f}/ {target:.2f}",
                                    "m114FormatString": "X:{x} Y:{y} Z:{z} E:{e[current]} Count: A:{a} B:{b} C:{c}",
                                    "m115FormatString": "FIRMWARE_NAME:{firmware_name} PROTOCOL_VERSION:1.0",
                                    "m115ReportCapabilities": true,
                                    "numExtruders": 1,
                                    "okAfterResend": false,
                                    "okBeforeCommandOutput": false,
                                    "okFormatString": "ok",
                                    "passcode": "1234",
                                    "pinnedExtruders": null,
                                    "preparedOks": [],
                                    "repetierStyleTargetTemperature": false,
                                    "reprapfwM114": false,
                                    "resend_ratio": 0,
                                    "resetLines": [
                                        "start",
                                        "Marlin: Virtual Marlin!",
                                        "",
                                        "SD card ok"
                                    ],
                                    "rxBuffer": 64,
                                    "sdFiles": {
                                        "longname": false,
                                        "longname_quoted": true,
                                        "size": true
                                    },
                                    "sendBusy": false,
                                    "sendWait": true,
                                    "sharedNozzle": false,
                                    "simulateReset": true,
                                    "smoothieTemperatureReporting": false,
                                    "supportF": false,
                                    "supportM112": true,
                                    "support_M503": true,
                                    "throttle": 0.01,
                                    "waitInterval": 1.0
                                }
                            },
                            "scripts": {
                                "gcode": {
                                    "afterPrintCancelled": "; disable motors\nM84\n\n;disable all heaters\n{% snippet 'disable_hotends' %}\n{% snippet 'disable_bed' %}\n;disable fan\nM106 S0",
                                    "psucontrol_post_on": "",
                                    "psucontrol_pre_off": "",
                                    "snippets/disable_bed": "{% if printer_profile.heatedBed %}M140 S0\n{% endif %}",
                                    "snippets/disable_hotends": "{% if printer_profile.extruder.sharedNozzle %}M104 T0 S0\n{% else %}{% for tool in range(printer_profile.extruder.count) %}M104 T{{ tool }} S0\n{% endfor %}{% endif %}"
                                }
                            },
                            "serial": {
                                "abortHeatupOnCancel": true,
                                "ackMax": 1,
                                "additionalBaudrates": [],
                                "additionalPorts": [],
                                "alwaysSendChecksum": false,
                                "autoconnect": false,
                                "baudrate": null,
                                "baudrateOptions": [
                                    250000,
                                    230400,
                                    115200,
                                    57600,
                                    38400,
                                    19200,
                                    9600
                                ],
                                "blacklistedBaudrates": [],
                                "blacklistedPorts": [],
                                "blockWhileDwelling": false,
                                "blockedCommands": [
                                    "M0",
                                    "M1"
                                ],
                                "capAutoreportPos": true,
                                "capAutoreportSdStatus": true,
                                "capAutoreportTemp": true,
                                "capBusyProtocol": true,
                                "capEmergencyParser": true,
                                "capExtendedM20": true,
                                "checksumRequiringCommands": [
                                    "M110"
                                ],
                                "disableSdPrintingDetection": false,
                                "disconnectOnErrors": true,
                                "emergencyCommands": [
                                    "M112",
                                    "M108",
                                    "M410"
                                ],
                                "enableShutdownActionCommand": false,
                                "encoding": "ascii",
                                "exclusive": true,
                                "externalHeatupDetection": true,
                                "firmwareDetection": true,
                                "helloCommand": "M110 N0",
                                "ignoreEmptyPorts": false,
                                "ignoreErrorsFromFirmware": false,
                                "ignoreIdenticalResends": false,
                                "ignoredCommands": [],
                                "log": false,
                                "logPositionOnCancel": false,
                                "logPositionOnPause": true,
                                "longRunningCommands": [
                                    "G4",
                                    "G28",
                                    "G29",
                                    "G30",
                                    "G32",
                                    "M400",
                                    "M226",
                                    "M600"
                                ],
                                "lowLatency": false,
                                "maxTimeoutsIdle": 2,
                                "maxTimeoutsLong": 5,
                                "maxTimeoutsPrinting": 5,
                                "neverSendChecksum": false,
                                "notifySuppressedCommands": "warn",
                                "pausingCommands": [
                                    "M0",
                                    "M1",
                                    "M25"
                                ],
                                "port": null,
                                "portOptions": [
                                    "VIRTUAL"
                                ],
                                "repetierTargetTemp": false,
                                "resendRatioStart": 100,
                                "resendRatioThreshold": 10,
                                "sanityCheckTools": true,
                                "sdAlwaysAvailable": false,
                                "sdCancelCommand": "M25",
                                "sdLowerCase": false,
                                "sdRelativePath": false,
                                "sendChecksumWithUnknownCommands": false,
                                "sendM112OnError": true,
                                "supportResendsWithoutOk": "detect",
                                "swallowOkAfterResend": true,
                                "timeoutBaudrateDetectionPause": 1.0,
                                "timeoutCommunication": 30.0,
                                "timeoutCommunicationBusy": 3.0,
                                "timeoutConnection": 10.0,
                                "timeoutDetectionConsecutive": 2.0,
                                "timeoutDetectionFirst": 10.0,
                                "timeoutPosAutoreport": 5.0,
                                "timeoutPositionLogWait": 10.0,
                                "timeoutSdStatus": 1.0,
                                "timeoutSdStatusAutoreport": 1.0,
                                "timeoutTemperature": 5.0,
                                "timeoutTemperatureAutoreport": 2.0,
                                "timeoutTemperatureTargetSet": 2.0,
                                "triggerOkForM29": true,
                                "unknownCommandsNeedAck": false,
                                "useParityWorkaround": "detect",
                                "waitForStart": false
                            },
                            "server": {
                                "allowFraming": false,
                                "commands": {
                                    "serverRestartCommand": "s6-svc -r /var/run/s6/services/octoprint",
                                    "systemRestartCommand": null,
                                    "systemShutdownCommand": null
                                },
                                "diskspace": {
                                    "critical": 209715200,
                                    "warning": 524288000
                                },
                                "onlineCheck": {
                                    "enabled": true,
                                    "host": "1.1.1.1",
                                    "interval": 15,
                                    "name": "octoprint.org",
                                    "port": 53
                                },
                                "pluginBlacklist": {
                                    "enabled": true,
                                    "ttl": 15,
                                    "url": "https://plugins.octoprint.org/blacklist.json"
                                }
                            },
                            "slicing": {
                                "defaultSlicer": null
                            },
                            "system": {
                                "actions": [],
                                "events": null
                            },
                            "temperature": {
                                "cutoff": 30,
                                "profiles": [
                                    {
                                        "bed": 100,
                                        "chamber": null,
                                        "extruder": 210,
                                        "name": "ABS"
                                    },
                                    {
                                        "bed": 60,
                                        "chamber": null,
                                        "extruder": 180,
                                        "name": "PLA"
                                    }
                                ],
                                "sendAutomatically": false,
                                "sendAutomaticallyAfter": 1
                            },
                            "terminalFilters": [
                                {
                                    "name": "Suppress temperature messages",
                                    "regex": "regex"
                                },
                                {
                                    "name": "Suppress SD status messages",
                                    "regex": "regex"                                
                                },
                                {
                                    "name": "Suppress position messages",
                                    "regex": "regex" 
                                },
                                {
                                    "name": "Suppress wait responses",
                                    "regex": "regex"                               
                                },
                                {
                                    "name": "Suppress processing responses",
                                    "regex": "regex"
                                 }
                            ],
                            "webcam": {
                                "bitrate": "10000k",
                                "cacheBuster": false,
                                "ffmpegCommandline": "{ffmpeg} -framerate {fps} -i \"{input}\" -vcodec {videocodec} -threads {threads} -b:v {bitrate} -f {containerformat} -y {filters} \"{output}\"",
                                "ffmpegPath": "/usr/bin/ffmpeg",
                                "ffmpegThreads": 1,
                                "ffmpegVideoCodec": "libx264",
                                "flipH": false,
                                "flipV": false,
                                "rotate90": false,
                                "snapshotSslValidation": true,
                                "snapshotTimeout": 5,
                                "snapshotUrl": "http://192.168.1.242/webcam/?action=snapshot",
                                "streamRatio": "16:9",
                                "streamTimeout": 5,
                                "streamUrl": "http://192.168.1.242/webcam/?action=stream",
                                "streamWebrtcIceServers": [
                                    "stun:stun.l.google.com:19302"
                                ],
                                "timelapseEnabled": true,
                                "watermark": true,
                                "webcamEnabled": true
                            }
                        }
                    """.trimIndent(),
                    //endregion
                )
            }
        )
        val target = OctoSettingsApi(rotator, http)
        //endregion
        //region WHEN
        val response = target.getSettings()
        //endregion
        //region THEN
        assertEquals(
            expected = Settings(
                webcam = WebcamSettings(
                    streamUrl = "http://192.168.1.242/webcam/?action=stream",
                    flipH = false,
                    flipV = false,
                    rotate90 = false,
                    webcamEnabled = true,
                    streamRatio = "16:9",
                    snapshotUrl = "http://192.168.1.242/webcam/?action=snapshot",
                ),
                plugins = Settings.PluginSettingsGroup(
                    gcodeViewer = Settings.GcodeViewer(
                        sizeThreshold = 20971520,
                        mobileSizeThreshold = 2097152,
                    ),
                    octoEverywhere = Settings.OctoEverywhere(
                        printerKey = "key"
                    ),
                    ngrok = Settings.Ngrok(
                        authPassword = "pass",
                        authName = "name",
                    ),
                    octoAppCompanion = Settings.OctoAppCompanion(
                        encryptionKey = "4c7cf1e0-9720-4a18-ad66-4acaf3790917",
                        version = "1.0.12"
                    ),
                    multiCamSettings = Settings.MultiCam(
                        profiles = listOf(
                            WebcamSettings(
                                streamUrl = "http://192.168.1.242/webcam/?action=stream",
                                flipH = false,
                                flipV = false,
                                rotate90 = false,
                                webcamEnabled = true,
                                streamRatio = "16:9",
                                snapshotUrl = null,
                            ),
                            WebcamSettings(
                                streamUrl = "http://octopi.local/webcam/?action=stream",
                                flipH = false,
                                flipV = false,
                                rotate90 = false,
                                webcamEnabled = true,
                                streamRatio = "16:9",
                                snapshotUrl = null,
                            )
                        )
                    ),
                    discovery = Settings.Discovery(
                        uuid = "bda5fb9e-3a29-4e2b-af2f-fbfe4dc97e99"
                    ),
                    cancelObject = Settings.CancelObject,
                    psuControl = Settings.PsuControl,
                    mmu2FilamentSelect = Settings.Mmu2FilamentSelect(
                        filament1 = null,
                        filament2 = null,
                        filament3 = null,
                        filament4 = null,
                        filament5 = null,
                        labelSource = Settings.Mmu2FilamentSelect.LabelSource.Manual
                    )
                ),
                temperatureProfiles = listOf(
                    Settings.TemperatureProfile(
                        name = "ABS",
                        bed = 100f,
                        extruder = 210f,
                    ),
                    Settings.TemperatureProfile(
                        name = "PLA",
                        bed = 60f,
                        extruder = 180f,
                    ),
                ),
                terminalFilters = listOf(
                    Settings.TerminalFilter(name = "Suppress temperature messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress SD status messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress position messages", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress wait responses", regex = "regex"),
                    Settings.TerminalFilter(name = "Suppress processing responses", regex = "regex")
                ), appearance = Settings.Appearance(name = "Beagle", color = "orange")
            ),
            actual = response,
            message = "Expected response to match"
        )
        //endregion
    }
}