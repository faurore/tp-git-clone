plugins {
    kotlin("multiplatform")
    id("com.android.library")
    id("kotlin-parcelize")
    kotlin("plugin.serialization") version "1.7.0"
}

kotlin {
    android()

    listOf(
        iosX64(),
        iosArm64(),
        iosSimulatorArm64()
    ).forEach {
        it.binaries.framework {
            baseName = "shared-engines"
        }
    }

    sourceSets {
        val ktorVersion = project.rootProject.ext.get("ktor_version") as String

        val commonMain by getting {
            dependencies {
                implementation(project(":shared-common"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation("io.ktor:ktor-client-mock:$ktorVersion")
                implementation(kotlin("test"))
            }
        }
        val androidMain by getting {
        }
        val androidTest by getting
        val iosX64Main by getting
        val iosArm64Main by getting
        val iosSimulatorArm64Main by getting
        val iosMain by creating {
            dependsOn(commonMain)
            iosX64Main.dependsOn(this)
            iosArm64Main.dependsOn(this)
            iosSimulatorArm64Main.dependsOn(this)
        }
        val iosX64Test by getting
        val iosArm64Test by getting
        val iosSimulatorArm64Test by getting
        val iosTest by creating {
            dependsOn(commonTest)
            iosX64Test.dependsOn(this)
            iosArm64Test.dependsOn(this)
            iosSimulatorArm64Test.dependsOn(this)
        }
    }
}

android {
    namespace = "de.crysxd.octoapp.sharedengines"
    compileSdk = project.rootProject.ext.get("target_sdk") as Int
    defaultConfig {
        minSdk = project.rootProject.ext.get("min_sdk") as Int
        targetSdk = project.rootProject.ext.get("target_sdk") as Int
    }
}