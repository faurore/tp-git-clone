package de.crysxd.octoapp.base.utils.image

import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image

actual fun Image.applyTransformations(rotate90: Boolean, flipH: Boolean, flipV: Boolean): Image = this