package de.crysxd.octoapp.base

actual object OctoConfig {

    private var remoteConfig: DarwinRemoteConfig? = null
        get() = requireNotNull(field) { "Firebase Remote Config not yet set. Call link(DarwinRemoteConfig) first." }

    fun link(rc: DarwinRemoteConfig) {
        remoteConfig = rc
    }

    actual fun getLong(field: OctoConfigField<Long>): Long = remoteConfig?.getLong(field.key) ?: field.default
    actual fun get(field: OctoConfigField<String>): String = remoteConfig?.getString(field.key) ?: field.default
    actual fun getBoolean(field: OctoConfigField<Boolean>): Boolean = when (remoteConfig?.getString(field.key)) {
        "true" -> true
        "false" -> false
        else -> field.default
    }
}

interface DarwinRemoteConfig {
    fun getLong(key: String): Long?
    fun getString(key: String): String?
}