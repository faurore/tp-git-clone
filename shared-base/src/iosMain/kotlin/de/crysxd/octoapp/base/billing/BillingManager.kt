package de.crysxd.octoapp.base.billing

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf

actual object BillingManager {
    actual fun isFeatureEnabled(feature: String): Boolean = false
    actual fun isFeatureEnabledFlow(feature: String): Flow<Boolean> = flowOf(false)
    actual fun shouldAdvertisePremium(): Boolean = false
    actual fun onResume() = Unit
    actual fun onPause() = Unit
    actual fun getPurchases(): Set<String> = emptySet()
}