package de.crysxd.octoapp.base.logging

import io.github.aakira.napier.LogLevel
import kotlinx.datetime.TimeZone

object CachedAntiLog : BaseAntiLog() {

    private const val maxLength = 1000 * 1024
    private val cache = StringBuilder()
    override var minPriority = LogLevel.INFO
    override val writeTime = true
    override val writeExceptionAsText = true
    override val name = "CachedAntiLog"
    override val timeZone = TimeZone.UTC

    override fun writeLine(line: String) {
        cache.appendLine(line)

        // Exceeding limit? Drop first 10%
        if (cache.length > maxLength) {
            cache.removeRange(0..((maxLength * 0.1).toInt()))
            cache.trimToSize()
        }
    }

    override fun writeException(t: Throwable) {
        // Nothing to do
    }

    fun getCache() = cache.toString()

    fun clear() = cache.clear()
}