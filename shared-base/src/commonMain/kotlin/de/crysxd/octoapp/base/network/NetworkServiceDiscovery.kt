package de.crysxd.octoapp.base.network

import de.crysxd.octoapp.base.data.models.NetworkService

interface NetworkServiceDiscovery {

    companion object {
        val Noop = object  : NetworkServiceDiscovery {
            override suspend fun discover(callback: (NetworkService) -> Unit)  = Unit
        }
    }

    suspend fun discover(callback: (NetworkService) -> Unit)
}

