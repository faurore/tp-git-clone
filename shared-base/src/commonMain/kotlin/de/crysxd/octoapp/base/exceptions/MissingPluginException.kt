package de.crysxd.octoapp.base.exceptions

import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException


class MissingPluginException(val pluginId: String) : SuppressedIllegalStateException("Missing plugin '$pluginId'")