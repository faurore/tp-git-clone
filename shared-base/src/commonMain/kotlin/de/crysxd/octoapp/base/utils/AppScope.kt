package de.crysxd.octoapp.base.utils

import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedException
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob

private val appJob = SupervisorJob()
val AppScope = CoroutineScope(appJob + Dispatchers.Main.immediate + CoroutineExceptionHandler { _, throwable ->
    if (throwable !is CancellationException && throwable !is SuppressedException && throwable.cause !is CancellationException && throwable.cause !is SuppressedException) {
        Napier.e(tag = "AppScope", throwable = throwable, message = "Caught NON-CONTAINED exception in AppScope!")
        ExceptionReceivers.dispatchException(throwable)
    }
})