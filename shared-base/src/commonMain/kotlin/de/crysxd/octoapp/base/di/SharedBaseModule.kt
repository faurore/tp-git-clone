package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.io.FileManager
import de.crysxd.octoapp.base.io.SettingsStore
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.di.BaseModule
import org.koin.dsl.module


class SharedBaseModule : BaseModule {

    private fun provideSettings(platform: Platform): SettingsStore = SettingsStore(platform)
    private fun provideFileManager(platform: Platform): FileManager = FileManager(platform)

    override val koinModule = module {
        single { provideSettings(get()) }
        single { provideFileManager(get()) }
    }
}