package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.FileListState
import de.crysxd.octoapp.base.data.repository.FileListRepository
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.sorted
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class LoadFilesUseCase(
    private val fileRepository: FileListRepository,
    private val octoPrintRepository: OctoPrintRepository,
) : UseCase2<LoadFilesUseCase.Params, Flow<FileListState>>() {

    override suspend fun doExecute(param: Params, logger: Logger): Flow<FileListState> {
        require(param.fileOrigin == FileOrigin.Local) { "Can only load local files" }
        val instanceId = param.instanceId ?: octoPrintRepository.getActiveInstanceSnapshot()?.id ?: throw IllegalStateException("No OctoPrint is active")

        return when (param) {
            is Params.All -> fileRepository.getAllFiles(instanceId = instanceId, skipCache = param.skipCache)
            is Params.ForFolder -> fileRepository.getFiles(instanceId = instanceId, path = param.folder?.path ?: "/", skipCache = param.skipCache)
            is Params.ForPath -> fileRepository.getFiles(instanceId = instanceId, path = param.path, skipCache = param.skipCache)
        }.map {
            when (it) {
                is FileListState.Error -> it
                is FileListState.Loaded -> it.copy(
                    file = when (val f = it.file) {
                        is FileObject.File -> f
                        is FileObject.Folder -> f.copy(children = f.children.sorted())
                    }
                )
                FileListState.Loading -> it
            }
        }
    }

    sealed class Params {
        abstract val fileOrigin: FileOrigin
        abstract val instanceId: String?
        abstract val skipCache: Boolean

        data class ForFolder(
            override val fileOrigin: FileOrigin,
            val folder: FileObject.Folder?,
            override val instanceId: String? = null,
            override val skipCache: Boolean = false
        ) : Params()

        data class ForPath(
            override val fileOrigin: FileOrigin,
            val path: String,
            override val instanceId: String? = null,
            override val skipCache: Boolean = false
        ) : Params()

        data class All(
            override val fileOrigin: FileOrigin,
            override val instanceId: String? = null,
            override val skipCache: Boolean = false
        ) : Params()
    }
}
