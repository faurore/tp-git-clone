package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.engine.framework.isSpaghettiDetectiveUrl
import de.crysxd.octoapp.engine.octoprint.dto.plugins.spaghettidetective.SpaghettiDetectiveDataUsage
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.update

@OptIn(ExperimentalCoroutinesApi::class)
class RemoteAccessObicoViewModelCore(instanceId: String) : RemoteAccessBaseViewModelCore(instanceId) {

    override val tag = "RemoteAccessObicoViewModelCore"
    override val remoteServiceName = RemoteServiceConnectionBrokenException.REMOTE_SERVICE_TSD
    private val printerConfig = SharedBaseInjector.get().printerConfigRepository
    private val printerProvider = SharedBaseInjector.get().octoPrintProvider

    private val reloadTrigger = MutableStateFlow(0)
    val dataUsage = reloadTrigger.combine(printerConfig.instanceInformationFlow(instanceId = instanceId)) { _, instance ->
        flow {
            emit(FlowState.Loading())
            if (instance?.alternativeWebUrl?.isSpaghettiDetectiveUrl() == true) {
                val obicoUrl = requireNotNull(instance.alternativeWebUrl)
                val obicoOnlyInstance = instance.copy(webUrl = obicoUrl, alternativeWebUrl = null)
                val state = printerProvider.createAdHocOctoPrint(obicoOnlyInstance)
                    .asOctoPrint()
                    .spaghettiDetectiveApi
                    .getDataUsage()

                // Ensure user sees loading :)
                delay(350)

                emit(FlowState.Ready(state))
            } else {
                emit(FlowState.Loading())
            }
        }
    }.flatMapLatest {
        it
    }.catch {
        Napier.e(tag = tag, message = "Failed to load Obico data usage", throwable = it)
        FlowState.Error<SpaghettiDetectiveDataUsage>(it)
    }

    override fun Url.isMyUrl() = isSpaghettiDetectiveUrl()

    suspend fun getLoginUrl(baseUrl: String?): String? = getLoginUrl(
        GetRemoteServiceConnectUrlUseCase.RemoteService.SpaghettiDetective(baseUrl = baseUrl?.toUrl())
    )

    suspend fun reloadDataUsage() {
        reloadTrigger.update { it + 1 }
        dataUsage.first { it !is FlowState.Loading }
    }
}