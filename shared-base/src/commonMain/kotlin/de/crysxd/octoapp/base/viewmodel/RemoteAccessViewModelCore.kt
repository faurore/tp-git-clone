package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.viewmodel.RemoteAccessViewModelCore.Service.Manual
import de.crysxd.octoapp.base.viewmodel.RemoteAccessViewModelCore.Service.Ngrok
import de.crysxd.octoapp.base.viewmodel.RemoteAccessViewModelCore.Service.Obico
import de.crysxd.octoapp.base.viewmodel.RemoteAccessViewModelCore.Service.OctoEverywhere
import de.crysxd.octoapp.engine.framework.isNgrokUrl
import de.crysxd.octoapp.engine.framework.isOctoEverywhereUrl
import de.crysxd.octoapp.engine.framework.isSpaghettiDetectiveUrl
import de.crysxd.octoapp.engine.octoprint.OctoPlugins
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map

class RemoteAccessViewModelCore(instanceId: String) {

    private val tag = "RemoteAccessViewModelCore"
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository

    val services = printerConfigRepository.instanceInformationFlow(instanceId = instanceId).map { config ->
        val serivces: MutableMap<Service, Int> = mutableMapOf(
            OctoEverywhere to 10,
            Obico to 10,
            Ngrok to 5,
            Manual to 0,
        )

        // Installed service has medium importance
        if (config?.hasPlugin(OctoPlugins.Ngrok) == true) serivces[Ngrok] = 50
        if (config?.hasPlugin(OctoPlugins.OctoEverywhere) == true) serivces[OctoEverywhere] = 100
        if (config?.hasPlugin(OctoPlugins.Obico) == true) serivces[Obico] = 100

        // Connected service has highest importance
        when {
            config?.alternativeWebUrl?.isNgrokUrl() == true -> serivces[Ngrok] = 1000
            config?.alternativeWebUrl?.isOctoEverywhereUrl() == true -> serivces[OctoEverywhere] = 1000
            config?.alternativeWebUrl?.isSpaghettiDetectiveUrl() == true -> serivces[Obico] = 1000
        }

        Napier.d(tag = tag, message = "Remote services: ${serivces.toList()}")
        serivces.toList().map {
            // Add bias to have a random order on values with same importance
            it.first to (it.second + it.first.bias)
        }.sortedByDescending {
            it.second
        }.map {
            it.first
        }
    }.map {
        Napier.i(tag = tag, message = "Providing remote services in order: $it")
        State(it)
    }.catch {
        Napier.i(tag = tag, message = "Failed to create services", throwable = it)
        emit(State(Service.values().toList()))
    }

    data class State(
        val services: List<Service>

    )

    enum class Service {
        OctoEverywhere, Obico, Ngrok, Manual;

        val bias get() = biases[this]!!

        companion object {
            private val biases = values().associateWith { (0..10).random() }
        }
    }
}