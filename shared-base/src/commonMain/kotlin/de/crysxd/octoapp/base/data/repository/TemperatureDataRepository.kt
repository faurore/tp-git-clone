package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.texts.getString
import de.crysxd.octoapp.engine.models.event.HistoricTemperatureData
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.printer.ComponentTemperature
import io.github.aakira.napier.Napier
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.merge
import kotlinx.coroutines.flow.retry
import kotlinx.coroutines.launch


@OptIn(ExperimentalCoroutinesApi::class)
class TemperatureDataRepository(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
) {

    companion object {
        const val MAX_ENTRIES = 350
    }

    private val tag = "TemperatureDataRepository"
    private val defaultComponents = listOf("tool0", "tool1", "tool2", "tool3", "bed", "chamber")
    private val caches = mutableMapOf<String?, MutableList<HistoricTemperatureData>>()
    private val flows = mutableMapOf<String?, MutableSharedFlow<List<TemperatureSnapshot>>>()

    init {
        AppScope.launch(Dispatchers.Default) {
            Napier.i(tag = tag, message = "Collecting temperatures")

            octoPrintRepository.allInstanceInformationFlow()
                .distinctUntilChangedBy { m ->
                    m.values.map {
                        it.appSettings?.hiddenTemperatureComponents.hashCode() + it.activeProfile.hashCode()
                    }
                }
                .flatMapLatest { instances ->
                    listOf(instances.values, listOf(null)).flatten().map {
                        octoPrintProvider.passiveCurrentMessageFlow(
                            tag = "temperature-repository",
                            instanceId = it?.id
                        ).extractTemperatures(it)
                    }.merge()
                }.retry {
                    Napier.e(tag = tag, message = "Exception in temperature flow", throwable = it)
                    delay(1000)
                    true
                }.collect()
        }
    }

    private fun getFlow(instanceId: String?) = flows.getOrPut(instanceId) { MutableSharedFlow(replay = 1) }

    private fun getCache(instanceId: String?) = caches.getOrPut(instanceId) { mutableListOf() }

    private fun Flow<Message.Current>.extractTemperatures(instance: OctoPrintInstanceInformationV3?) = map { message ->
        val settings = octoPrintRepository.get(instance?.id)?.appSettings
        val profile = octoPrintRepository.get(instance?.id)?.activeProfile
        val flow = getFlow(instance?.id)
        val data = getCache(instance?.id)

        // Clear data if we received a history message
        if (message.isHistoryMessage) {
            Napier.i(tag = tag, message = "Received history message, clearing data")
            data.clear()
        }

        // Attach all
        data.addAll(message.temps)

        // Too many data points? Drop
        if (data.size > MAX_ENTRIES) {
            repeat(data.size - MAX_ENTRIES) {
                data.removeAt(0)
            }
        }

        // We have any data?
        if (data.isNotEmpty()) {
            val snapshot = data.flatMap { i -> i.components.keys }.distinct().mapNotNull { component ->
                val lastData = data.last().components.get(component) ?: return@mapNotNull null

                // Check if this component should be included
                val isChamber = component == "chamber" && profile?.heatedChamber != false
                val isBed = component == "bed" && profile?.heatedBed != false
                val isTool = component.startsWith("tool") && (profile?.extruder?.sharedNozzle == false || component == "tool0")
                // This is a fix for a "feature" in the Prusa firmware. Hotend temperatures sometimes come with random leading 'O', drop them.
                // https://github.com/prusa3d/Prusa-Firmware/issues/3507
                val isPrusaJunk = component.length >= 2 && component.dropLast(1).uppercase().all { it == 'O' } && component.last() == 'T'
                val isOther = component != "chamber" && component != "bed" && !component.startsWith("tool") && !isPrusaJunk
                val include = isOther || isTool || isChamber || isBed
                if (!include) {
                    return@mapNotNull null
                }

                val history = data.map {
                    TemperatureHistoryPoint(
                        time = it.time,
                        temperature = it.components.get(component)?.actual ?: 0f
                    )
                }

                TemperatureSnapshot(
                    history = history.sortedBy { it.time },
                    component = component,
                    current = lastData,
                    canControl = defaultComponents.contains(component),
                    offset = message.offsets?.get(component),
                    isHidden = settings?.hiddenTemperatureComponents?.contains(component) == true,
                    componentLabel = getComponentLabel(component),
                    maxTemp = getMaxTemp(component),
                )
            }.sortedWith(compareByDescending<TemperatureSnapshot> { it.canControl }.thenBy { it.component })

            flow.emit(snapshot)
        }
    }.retry {
        Napier.e(tag = tag, message = "Exception in temperature flow (2)", throwable = it)
        delay(1000)
        true
    }

    fun flow(instanceId: String? = null) = getFlow(instanceId).asSharedFlow()

    private fun getComponentLabel(component: String) = when (component) {
        "tool0" -> getString("general___hotend")
        "tool1" -> getString("general___hotend_2")
        "tool2" -> getString("general___hotend_3")
        "tool3" -> getString("general___hotend_4")
        "bed" -> getString("general___bed")
        "chamber" -> getString("general___chamber")
        else -> component
    }.toString()

    private fun getMaxTemp(component: String) = when (component) {
        "tool0" -> 250
        "tool1" -> 250
        "tool3" -> 250
        "tool4" -> 250
        "bed" -> 100
        "chamber" -> 100
        else -> 100
    }

    data class TemperatureSnapshot(
        val component: String,
        val componentLabel: String,
        val current: ComponentTemperature,
        val history: List<TemperatureHistoryPoint>,
        val canControl: Boolean,
        val offset: Float?,
        val isHidden: Boolean,
        val maxTemp: Int,
    )

    data class TemperatureHistoryPoint(
        val temperature: Float,
        val time: Long,
    )
}