package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.models.AppSettings
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.io.SettingsStore
import de.crysxd.octoapp.base.io.getSerializableOrNull
import de.crysxd.octoapp.base.io.putSerializable
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.engine.framework.isBasedOn
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock

class OctoPrintRepository(
    settingsStore: SettingsStore,
    private val sensitiveDataMask: SensitiveDataMask,
    private val octoPreferences: OctoPreferences,
) {

    private val Tag = "OctoPrintRepository"
    private val settings = settingsStore.forNameSpace("printer-configs")
    private val instanceInformationFlow = MutableStateFlow<Map<String, OctoPrintInstanceInformationV3>>(emptyMap())
    private val lock = Mutex()

    init {
        postInstances()
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    fun instanceInformationFlow(instanceId: String? = null) = instanceInformationFlow.flatMapLatest { map ->
        if (instanceId == null) {
            octoPreferences.updatedFlow.map { octoPreferences.activeInstanceId }.distinctUntilChanged()
        } else {
            flowOf(instanceId)
        }.map { id ->
            id?.let { map[it] }
        }
    }

    fun allInstanceInformationFlow() = instanceInformationFlow

    fun getActiveInstanceSnapshot() = octoPreferences.activeInstanceId?.let { instanceInformationFlow.value[it] }

    private fun postInstances() = runBlocking {
        instanceInformationFlow.value = load()
            .onEach { sensitiveDataMask.registerInstance(it) }
            .associateBy { it.id }

        Napier.i(tag = Tag, message = "Posting ${instanceInformationFlow.value.size} instances")
    }

    private fun storeOctoprintInstanceInformation(id: String, instance: OctoPrintInstanceInformationV3?) {
        Napier.i(tag = Tag, message = "Updating $id to $instance")
        val updated = getAll().filter { it.id != id }.toMutableList()
        instance?.let { updated.add(it) }
        store(updated)
        postInstances()
    }

    fun setActive(instance: OctoPrintInstanceInformationV3, trigger: String) {
        Napier.i(tag = Tag, message = "Setting as active: ${instance.webUrl} (trigger=$trigger)")
        storeOctoprintInstanceInformation(instance.id, instance)
        octoPreferences.activeInstanceId = instance.id
    }

    suspend fun updateActive(block: suspend (OctoPrintInstanceInformationV3) -> OctoPrintInstanceInformationV3?) {
        getActiveInstanceSnapshot()?.let {
            update(it.id, block)
        }
    }

    suspend fun update(id: String, block: suspend (OctoPrintInstanceInformationV3) -> OctoPrintInstanceInformationV3?) {
        lock.withLock {
            get(id)?.let {
                val new = block(it)
                if (new != it) {
                    Napier.i(tag = Tag, message = "Updating instance with $id")
                    storeOctoprintInstanceInformation(it.id, new)
                } else {
                    Napier.v(tag = Tag, message = "Drop update, no changes")
                }
            }
        }
    }

    suspend fun updateAppSettingsForActive(block: suspend (AppSettings) -> AppSettings) {
        updateActive {
            it.copy(appSettings = block(it.appSettings ?: AppSettings()))
        }
    }

    suspend fun updateAppSettings(id: String, block: suspend (AppSettings) -> AppSettings) {
        update(id) {
            it.copy(appSettings = block(it.appSettings ?: AppSettings()))
        }
    }

    fun clearActive() {
        Napier.i(tag = Tag, message = "Clearing active")
        octoPreferences.activeInstanceId = null
    }

    fun clear() {
        clearActive()
        store(emptyList())
        postInstances()
    }

    fun remove(id: String) {
        Napier.i(tag = Tag, message = "Removing $id")
        val all = getAll().filter { it.id != id }
        store(all)
        postInstances()
    }

    fun get(id: String?) = if (id == null) {
        getActiveInstanceSnapshot()
    } else {
        getAll().firstOrNull { it.id == id }
    }

    suspend fun atomicChange(block: suspend OctoPrintRepository.() -> Unit) = lock.withLock {
        block(this)
    }

    fun getAll() = instanceInformationFlow.value.values.toList()

    fun setAll(all: List<OctoPrintInstanceInformationV3>) {
        store(all)
        postInstances()
    }

    @Deprecated("Do not use")
    fun findInstances(url: Url?) = getAll().mapNotNull {
        val u = url?.toString()?.toUrlOrNull() ?: return@mapNotNull null

        val webUrl = it.webUrl
        val alternativeWebUrl = it.alternativeWebUrl
        when {
            u.isBasedOn(webUrl) -> it to false
            u.isBasedOn(alternativeWebUrl) -> it to true
            else -> null
        }
    }

    fun import(list: List<OctoPrintInstanceInformationV3>) {
        store(list)
        postInstances()
    }

    private fun load(): List<OctoPrintInstanceInformationV3> = settings.getSerializableOrNull("configs") ?: emptyList()

    private fun store(value: List<OctoPrintInstanceInformationV3>) = settings.putSerializable("configs", value)
}