package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.sharedcommon.di.Injector

object SharedBaseInjector : Injector<SharedBaseComponent>()