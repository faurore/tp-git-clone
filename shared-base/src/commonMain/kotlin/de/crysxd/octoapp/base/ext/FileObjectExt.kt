package de.crysxd.octoapp.base.ext

import de.crysxd.octoapp.base.data.models.FileManagerSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.engine.models.files.FileObject

fun List<FileObject>.sorted() = SharedBaseInjector.get().preferences.fileManagerSettings.let { settings ->
    val fileComparator = when (settings.sortBy) {
        FileManagerSettings.SortBy.UploadTime -> compareBy<FileObject.File> { it.date }
        FileManagerSettings.SortBy.PrintTime -> compareBy { it.prints?.last?.date ?: 0L }
        FileManagerSettings.SortBy.FileSize -> compareBy { it.size }
        FileManagerSettings.SortBy.Name -> compareBy { it.name }
    }.thenBy { it.path }

    val files = filterIsInstance<FileObject.File>()
        .filter { f -> f.prints?.last == null || !settings.hidePrintedFiles }
        .sortedWith(fileComparator).let {
            when (settings.sortDirection) {
                FileManagerSettings.SortDirection.Ascending -> it
                FileManagerSettings.SortDirection.Descending -> it.reversed()
            }
        }

    val folders = filterIsInstance<FileObject.Folder>()
        .sortedBy { it.display }

    listOf(
        folders,
        files,
    ).flatten()
}

fun FileObject.searchInTree(term: String): List<FileObject> {
    fun FileObject.isMatch() = display.contains(term, ignoreCase = true) || name.contains(term, ignoreCase = true)
    fun FileObject.flatten(): List<FileObject> = when (this) {
        is FileObject.File -> listOf(this)
        is FileObject.Folder -> children.flatMap { it.flatten() }.let {
            if (isMatch() && path != "/") it + this else it
        }
    }

    return if (term.isEmpty()) {
        flatten()
    } else {
        flatten().filter { it.isMatch() }
    }
}
