package de.crysxd.octoapp.base.network

import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.ext.composeErrorMessage
import de.crysxd.octoapp.base.ext.composeMessageStack
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.base.utils.texts.getString
import de.crysxd.octoapp.engine.exceptions.InvalidApiKeyException
import de.crysxd.octoapp.engine.exceptions.PrinterHttpsException
import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.engine.framework.isBasedOn
import de.crysxd.octoapp.engine.octoprint.http.ExceptionInspector
import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.exceptions.NetworkException
import io.github.aakira.napier.Napier
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.seconds

class DetectBrokenSetupInterceptor(
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintId: String,
) : ExceptionInspector {

    companion object {
        var enabled = true
        private var disabledUntil = Instant.DISTANT_PAST
        private const val Tag = "DetectBrokenSetupInterceptor"
    }

    override fun inspect(e: Throwable) {
        if (enabled && e is NetworkException && isBrokenSetup(e)) {
            val isForActive = octoPrintRepository.getActiveInstanceSnapshot()?.id == octoPrintId
            val instance = octoPrintRepository.get(octoPrintId)
            Napier.w(tag = Tag, message = "Handling ${e::class.simpleName} on ${instance?.id}")

            // Only handle if the OctoPrint having the issue is active
            if (!isForActive) {
                return
            }

            // Remove remote connection if broken
            if (e is RemoteServiceConnectionBrokenException) {
                val correctUrl = e.webUrl.isBasedOn(instance?.alternativeWebUrl)
                if (!correctUrl) {
                    Napier.e(tag = Tag, message = "Received remote error for ${e.webUrl} but not based on current ${instance?.alternativeWebUrl} -> Ignoring")
                    return
                }

                runBlocking {
                    Napier.w(tag = Tag, message = "Caught OctoEverywhere/SpaghettiDetective/ngrok exception, removing connection")
                    octoPrintRepository.update(octoPrintId) {
                        it.copy(
                            alternativeWebUrl = null,
                            octoEverywhereConnection = null,
                            remoteConnectionFailure = RemoteConnectionFailure(
                                errorMessage = e.composeErrorMessage().toString().replace("<br>", "\n"),
                                errorMessageStack = e.composeMessageStack().toString().replace("<br>", "\n"),
                                stackTrace = e.stackTraceToString(),
                                remoteServiceName = e.remoteServiceName,
                                dateMillis = Clock.System.now().toEpochMilliseconds()
                            )
                        )
                    }
                }
            }

            // Dispatch to user
            if (Clock.System.now() > disabledUntil) {
                disabledUntil = Clock.System.now() + 15.seconds
                ExceptionReceivers.dispatchException(
                    BrokenSetupException(
                        original = e,
                        userMessage = when (e) {
                            is PrinterHttpsException -> getString("sign_in___broken_setup___https_issue", e.originalCause?.message ?: e.message ?: "")
                            is BasicAuthRequiredException -> getString("sign_in___broken_setup___basic_auth_required")
                            is InvalidApiKeyException -> getString("sign_in___broken_setup___api_key_revoked")
                            else -> e.composeErrorMessage()
                        }.toString(),
                        instance = requireNotNull(instance) { "Broken instance $octoPrintId not found" },
                        needsRepair = e !is RemoteServiceConnectionBrokenException
                    )
                )
            }
        }
    }

    private fun isBrokenSetup(e: Throwable) =
        e is BasicAuthRequiredException || e is PrinterHttpsException || e is InvalidApiKeyException || e is RemoteServiceConnectionBrokenException

    fun interface Factory {
        fun buildFor(octoPrintId: String): DetectBrokenSetupInterceptor
    }
}