package de.crysxd.octoapp.base.io

import com.russhwolf.settings.Settings
import de.crysxd.octoapp.sharedcommon.Platform
import de.crysxd.octoapp.sharedcommon.di.SharedCommonInjector
import io.github.aakira.napier.Napier
import kotlinx.serialization.DeserializationStrategy
import kotlinx.serialization.SerializationStrategy
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString


expect class SettingsStore(platform: Platform) {
    fun forNameSpace(nameSpace: String): Settings
}

val SettingsJson by lazy { SharedCommonInjector.get().json }

inline fun <reified T> Settings.putSerializable(key: String, value: T) = putString(key, SettingsJson.encodeToString(value))

inline fun <reified T> Settings.putSerializable(key: String, serializer: SerializationStrategy<T>, value: T) =
    putString(key, SettingsJson.encodeToString(serializer, value))


inline fun <reified T> Settings.getSerializableOrNull(key: String): T? = getStringOrNull(key)?.let {
    try {
        SettingsJson.decodeFromString<T>(it)
    } catch (e: Exception) {
        Napier.e(tag = "Settings", message = "Failed to deserialize ${T::class.qualifiedName}", throwable = e)
        null
    }
}

inline fun <reified T> Settings.getSerializableOrNull(key: String, deserializer: DeserializationStrategy<T>): T? = getStringOrNull(key)?.let {
    try {
        SettingsJson.decodeFromString(deserializer, it)
    } catch (e: Exception) {
        Napier.e(tag = "Settings", message = "Failed to deserialize ${T::class.qualifiedName}", throwable = e)
        null
    }
}

inline fun <reified T> Settings.getSerializable(key: String, default: T): T =
    getSerializableOrNull(key) ?: default

inline fun <reified T> Settings.getSerializable(key: String, deserializer: DeserializationStrategy<T>, default: T): T =
    getSerializableOrNull(key, deserializer) ?: default