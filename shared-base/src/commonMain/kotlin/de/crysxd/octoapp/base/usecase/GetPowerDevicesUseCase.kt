package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.power.PowerDevice
import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Suppress("EXPERIMENTAL_API_USAGE")
class GetPowerDevicesUseCase constructor(
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintProvider: OctoPrintProvider
) : UseCase2<GetPowerDevicesUseCase.Params, PowerDeviceList>() {

    override suspend fun doExecute(param: Params, logger: Logger): PowerDeviceList = withContext(Dispatchers.SharedIO) {
        val settings = octoPrintRepository.get(param.instanceId)?.settings ?: if (param.allowNetwork) {
            octoPrintProvider.octoPrint(param.instanceId).settingsApi.getSettings()
        } else {
            logger.e("Not allowed to use network but settings not available")
            return@withContext emptyList()
        }

        val devices = octoPrintProvider.octoPrint(param.instanceId).powerDevicesApi.getDevices(settings)

        // Emit without power state
        val result = devices.map { Pair<PowerDevice, PowerState>(it, PowerState.Unknown) }
            .filter { param.onlyGetDeviceWithUniqueId == null || param.onlyGetDeviceWithUniqueId == it.first.uniqueId }
            .filter { it.first.capabilities.containsAll(param.requiredCapabilities) }
            .toMap()
            .toMutableMap()

        // If we should query power state do so and emit a second value
        if (param.queryState) {
            // Use withContext to split the stream in parallel
            result.keys.forEach {
                try {
                    result[it] = when (it.isOn()) {
                        true -> PowerState.On
                        false -> PowerState.Off
                        null -> PowerState.Unknown
                    }
                } catch (e: Exception) {
                    logger.e(e = e, message = "Failed to query state for $it")
                    result[it] = PowerState.Unknown
                }
            }
        }

        return@withContext result.toList()
    }

    data class Params(
        val instanceId: String? = null,
        val queryState: Boolean,
        val allowNetwork: Boolean = true,
        val onlyGetDeviceWithUniqueId: String? = null,
        val requiredCapabilities: List<PowerDevice.Capability> = emptyList()
    )

    sealed class PowerState : CommonParcelable {
        @CommonParcelize
        object On : PowerState()

        @CommonParcelize
        object Off : PowerState()

        @CommonParcelize
        object Unknown : PowerState()
    }
}

typealias PowerDeviceList = List<Pair<PowerDevice, GetPowerDevicesUseCase.PowerState>>
