package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.commands.BedCommand
import de.crysxd.octoapp.engine.models.commands.ChamberCommand
import de.crysxd.octoapp.engine.models.commands.ToolCommand

class SetTemperatureOffsetUseCase(
    octoPrintProvider: OctoPrintProvider,
    getCurrentPrinterProfileUseCase: GetCurrentPrinterProfileUseCase,
) : BaseChangeTemperaturesUseCase(
    octoPrintProvider = octoPrintProvider,
    getCurrentPrinterProfileUseCase = getCurrentPrinterProfileUseCase
) {
    override fun createBedCommand(temperature: Float) = BedCommand.SetTemperatureOffset(temperature)
    override fun createChamberCommand(temperature: Float) = ChamberCommand.SetTemperatureOffset(temperature)
    override fun createToolCommand(temperature: Map<String, Float>) = ToolCommand.SetTemperatureOffset(temperature)
}
