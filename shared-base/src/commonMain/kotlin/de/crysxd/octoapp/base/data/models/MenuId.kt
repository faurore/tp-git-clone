package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.base.utils.texts.getString


enum class MenuId {

    MainMenu,
    PrintWorkspace,
    PrePrintWorkspace,
    Widget,
    Other;

    fun canPin(canRunWithAppInBackground: Boolean) = (this != Widget || canRunWithAppInBackground) && this != Other

    val label
        get() = when (this) {
            MainMenu -> getString("menu_controls___main")
            PrintWorkspace -> getString("menu_controls___print_workspace")
            PrePrintWorkspace -> getString("menu_controls___prepare_workspace")
            Widget -> getString("menu_controls___widget")
            Other -> getString("string.menu_controls___other")
        }
}