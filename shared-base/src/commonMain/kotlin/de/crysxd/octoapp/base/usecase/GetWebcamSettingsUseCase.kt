package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.framework.isHlsStreamUrl
import de.crysxd.octoapp.engine.framework.isSpaghettiDetectiveUrl
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.config.hostNameOrIp
import de.crysxd.octoapp.sharedcommon.http.framework.UPNP_ADDRESS_PREFIX
import de.crysxd.octoapp.sharedcommon.http.framework.extractAndRemoveBasicAuth
import de.crysxd.octoapp.sharedcommon.http.framework.resolve
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedcommon.http.framework.withHost
import io.ktor.http.Url
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.withContext

class GetWebcamSettingsUseCase(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
    private val localDnsResolver: CachedDns,
    private val getActiveHttpUrlUseCase: GetActiveHttpUrlUseCase,
) : UseCase2<OctoPrintInstanceInformationV3, Flow<List<ResolvedWebcamSettings>>>() {

    override suspend fun doExecute(param: OctoPrintInstanceInformationV3, logger: Logger): Flow<List<ResolvedWebcamSettings>> {
        val activeWebUrlFlow = getActiveHttpUrlUseCase.execute(param)

        // Get settings
        val settingsFlow = octoPrintRepository.instanceInformationFlow(param.id)
            .distinctUntilChangedBy { it?.settings }
            .map { it?.settings ?: octoPrintProvider.octoPrint().settingsApi.getSettings() }
            .distinctUntilChanged()

        // Compile webcam settings
        return activeWebUrlFlow.distinctUntilChanged().combine(settingsFlow) { activeWebUrl, settings ->
            logger.d("Compiling webcam settings for $activeWebUrl")

            // Add all webcams from multicam plugin
            val webcamSettings = mutableListOf<WebcamSettings>()
            settings.plugins.multiCamSettings?.let { webcamSettings.addAll(it.profiles) }

            // If no webcams were added, use default settings object
            if (webcamSettings.isEmpty()) {
                webcamSettings.add(settings.webcam)
            }

            // If active URL is Spaghetti Tunnel, we need to use the dedicated Spaghetti stack
            val newSettings = when {
                activeWebUrl.isSpaghettiDetectiveUrl() -> buildSpaghettiSettings(webcamSettings)
                else -> buildSettings(logger, webcamSettings, activeWebUrl)
            }

            logger.d("Settings: $newSettings")
            newSettings
        }
    }

    private fun buildSpaghettiSettings(webcamSettings: List<WebcamSettings>) = webcamSettings.mapIndexed { index, ws ->
        ResolvedWebcamSettings.SpaghettiCamSettings(
            webcamSettings = ws,
            webcamIndex = index,
        )
    }

    private suspend fun buildSettings(logger: Logger, webcamSettings: List<WebcamSettings>, activeWebUrl: Url): List<ResolvedWebcamSettings> {
        val newSettings = webcamSettings.mapNotNull { ws ->
            suspend fun Url.toWebcamSettings() = extractAndRemoveBasicAuth().let {
                if (it.first.isHlsStreamUrl()) {
                    ResolvedWebcamSettings.HlsSettings(url = it.first.resolveLocalDns(), webcamSettings = ws, basicAuth = it.second)
                } else {
                    ResolvedWebcamSettings.MjpegSettings(url = this, webcamSettings = ws)
                }
            }

            try {
                val streamUrl = ws.streamUrl ?: return@mapNotNull null
                when {
                    // Stream URL
                    streamUrl.startsWith("http") && streamUrl.toUrlOrNull() != null -> ws.streamUrl!!.toUrl().toWebcamSettings()

                    // RTSP URL
                    streamUrl.startsWith("rtsp://") -> {
                        val originalPrefix = "rtsp://"
                        val fakePrefix = "http://"
                        val fakeHttpUrl = (fakePrefix + ws.streamUrl!!.removePrefix(originalPrefix)).toUrl()
                        val (url, basicAuth) = fakeHttpUrl.extractAndRemoveBasicAuth()
                        ResolvedWebcamSettings.RtspSettings(
                            url = originalPrefix + url.resolveLocalDns().toString().removePrefix(fakePrefix),
                            webcamSettings = ws,
                            basicAuth = basicAuth
                        )
                    }

                    // No HTTP/S, no RTSP. Let's assume it's a HTTP path and resolve it
                    else -> activeWebUrl.resolve(streamUrl).toWebcamSettings()
                }
            } catch (e: Exception) {
                logger.e("Failed to resolve URL", e)
                null
            }
        }

        OctoAnalytics.setUserProperty(
            OctoAnalytics.UserProperty.WebCamAvailable,
            when {
                newSettings.any { it is ResolvedWebcamSettings.RtspSettings } -> "rtsp"
                newSettings.any { it is ResolvedWebcamSettings.HlsSettings } -> "hls"
                newSettings.any { it is ResolvedWebcamSettings.MjpegSettings } -> "mjpeg"
                else -> "false"
            }
        )

        return newSettings
    }

    private suspend fun Url.resolveLocalDns() = if (host.endsWith(".local") || host.startsWith(UPNP_ADDRESS_PREFIX)) {
        withContext(Dispatchers.SharedIO) {
            withHost(localDnsResolver.lookup(host).first().hostNameOrIp())
        }
    } else {
        this
    }
}
