package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.commands.JobCommand

class TogglePausePrintJobUseCase(
    private val octoPrintProvider: OctoPrintProvider
) : UseCase2<TogglePausePrintJobUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        octoPrintProvider.octoPrint(param.instanceId).jobApi.executeJobCommand(JobCommand.TogglePauseCommand)
    }

    data class Params(
        val instanceId: String? = null
    )
}