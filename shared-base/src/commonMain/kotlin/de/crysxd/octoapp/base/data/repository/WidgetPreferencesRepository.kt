package de.crysxd.octoapp.base.data.repository

import de.crysxd.octoapp.base.data.models.WidgetPreferences
import de.crysxd.octoapp.base.io.SettingsStore
import de.crysxd.octoapp.base.io.getSerializableOrNull
import de.crysxd.octoapp.base.io.putSerializable
import io.github.aakira.napier.Napier
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class WidgetPreferencesRepository(
    settingsStore: SettingsStore,
) {
    private val tag = "WidgetPreferencesRepository"
    private val settings = settingsStore.forNameSpace("controls")

    companion object {
        const val LIST_PRINT = "print"
        const val LIST_PREPARE = "prepare"
        const val LIST_CONNECT = "connect"
    }

    private val flows = mutableMapOf<String, MutableStateFlow<WidgetPreferences?>>()

    init {
        getFlow(LIST_PRINT).value = getWidgetOrder(LIST_PRINT)
        getFlow(LIST_PREPARE).value = getWidgetOrder(LIST_PREPARE)
        getFlow(LIST_CONNECT).value = getWidgetOrder(LIST_CONNECT)
    }

    fun getWidgetOrder(listId: String): WidgetPreferences? =
        settings.getSerializableOrNull<WidgetPreferences>(listId)

    fun getWidgetOrderFlow(listId: String) = getFlow(listId).asStateFlow()

    private fun getFlow(listId: String) = flows.getOrPut(listId) { MutableStateFlow(getWidgetOrder(listId)) }

    fun setWidgetOrder(listId: String, preferences: WidgetPreferences) {
        Napier.i(tag = tag, message = "Updating widget preferences for $listId: $preferences")
        settings.putSerializable(listId, preferences)
        getFlow(listId).value = preferences
    }

}
