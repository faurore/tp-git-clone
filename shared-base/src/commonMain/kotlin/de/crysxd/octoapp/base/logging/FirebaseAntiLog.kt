package de.crysxd.octoapp.base.logging

import io.github.aakira.napier.LogLevel
import kotlinx.datetime.TimeZone

object FirebaseAntiLog : BaseAntiLog() {

    override val name = "FirebaseAntilLog"
    override val writeTime = false
    override val writeExceptionAsText = false
    override val minPriority = LogLevel.INFO
    override val timeZone get() = TimeZone.currentSystemDefault()
    override val lineLength: Int = Int.MAX_VALUE
    private var adapter: Adapter? = null

    fun initAdapter(adapter: Adapter) {
        this.adapter = adapter
    }

    override fun writeLine(line: String) {
        adapter?.log(line)
    }

    override fun writeException(t: Throwable) {
        adapter?.log(t)
    }

    interface Adapter {
        fun log(line: String)
        fun log(t: Throwable)
    }
}