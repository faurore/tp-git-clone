package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.engine.exceptions.RemoteServiceConnectionBrokenException
import de.crysxd.octoapp.engine.framework.isOctoEverywhereUrl
import io.ktor.http.Url

class RemoteAccessOctoEverywhereViewModelCore(instanceId: String) : RemoteAccessBaseViewModelCore(instanceId) {

    override val tag = "RemoteAccessOctoEverywhereViewModelCore"
    override val remoteServiceName = RemoteServiceConnectionBrokenException.REMOTE_SERVICE_OCTO_EVERYWHERE
    suspend fun getLoginUrl(): String? = getLoginUrl(GetRemoteServiceConnectUrlUseCase.RemoteService.OctoEverywhere)
    override fun Url.isMyUrl() = isOctoEverywhereUrl()
}