package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.exceptions.UnknownHostException
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.engine.exceptions.PrinterHttpsException
import de.crysxd.octoapp.engine.exceptions.WebSocketUpgradeFailedException
import de.crysxd.octoapp.engine.models.event.Event
import de.crysxd.octoapp.sharedcommon.exceptions.BasicAuthRequiredException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.IpAddress
import de.crysxd.octoapp.sharedcommon.http.config.X509Certificate
import de.crysxd.octoapp.sharedcommon.http.config.assertPortOpen
import de.crysxd.octoapp.sharedcommon.http.config.assertReachable
import de.crysxd.octoapp.sharedcommon.http.config.hostNameOrIp
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3
import io.ktor.http.Url
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.async
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.isActive
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import kotlinx.coroutines.withTimeoutOrNull
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import okio.IOException
import kotlin.time.Duration.Companion.seconds

class TestFullNetworkStackUseCase(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
    private val getWebcamSnapshotUseCase: GetWebcamSnapshotUseCase,
    private val getWebcamSettingsUseCase: GetWebcamSettingsUseCase,
    private val localDnsResolver: CachedDns,
    private val httpClientSettings: HttpClientSettings,
) : UseCase2<TestFullNetworkStackUseCase.Target, TestFullNetworkStackUseCase.Finding>() {

    companion object {
        private val PING_TIMEOUT = 2.seconds
        private val SOCKET_TIMEOUT = 3.seconds
    }

    override suspend fun doExecute(param: Target, logger: Logger): Finding = withContext(Dispatchers.SharedIO) {
        var webUrl: String? = null

        try {
            //region See if it magically works (on iOS this triggers the local network permission dialog)
            if (param is Target.OctoPrint) param.webUrl.toUrlOrNull()?.let { url ->
                testOctoPrint(logger, url, param, url.host).let {
                    if (it is Finding.InvalidApiKey || it is Finding.OctoPrintReady) {
                        return@withContext it
                    }
                }
            }
            //endregion
            //region Extract webUrl
            val webcamSettings = (param as? Target.Webcam)?.let {
                val instance = requireNotNull(octoPrintRepository.get(param.instanceId)) { "Unable to find OctoPrint for id ${param.instanceId}" }
                getWebcamSettingsUseCase.execute(instance).first()[param.webcamIndex] as? ResolvedWebcamSettings.MjpegSettings ?: throw WebcamUnsupportedException()
            }


            webUrl = when {
                param is Target.OctoPrint -> param.webUrl
                webcamSettings?.url?.toString() == null -> return@withContext Finding.EmptyUrl()
                else -> webcamSettings.url.toString()
            }
            //endregion
            //region Parse URL
            logger.i("Testing URL syntax")
            val (baseUrl, host) = try {
                val url = webUrl.toUrl()
                url to url.host
            } catch (e: Exception) {
                return@withContext Finding.InvalidUrl(input = webUrl, exception = e)
            }
            logger.i("Passed")
            //endregion
            //region Test DNS
            logger.i("Testing DNS resolution")
            val (ips, dnsFinding) = testDns(host = host, webUrl = baseUrl, logger = logger)
            dnsFinding?.let { return@withContext it }
            ips.takeUnless { it.isEmpty() } ?: throw RuntimeException("IP should be set if no finding was returned")
            logger.i("Passed")
            //endregion
            val results = ips.map { ip ->
                async(Dispatchers.SharedIO) {
                    //region Test reachability
                    logger.i("Testing reachability for $ip")
                    val reachable = testReachability(host = host, ip = ip, webUrl = baseUrl, logger = logger)
                    logger.i(if (reachable == null) "Passed for $ip" else "Failed for $ip")
                    //endregion
                    //region Test port open
                    if (!this.coroutineContext.isActive) return@async reachable
                    logger.i("Testing port access for $ip")
                    val portOpen = testPortOpen(host = host, ip = ip, webUrl = baseUrl, logger = logger)
                    logger.i(if (portOpen == null) "Passed for $ip" else "Failed for $ip")

                    // Return reachability issue or port open issue if reachable
                    // This setup will ignore reachability issues in case the port is open
                    // Some servers can't be pinged, this solves the issue
                    if (portOpen == null) null else reachable ?: portOpen
                    //endregion
                }.also {
                    //region Error handling
                    it.invokeOnCompletion { t ->
                        if (t != null) {
                            logger.i("Cancelling checks for $ip, caused by ${t::class.simpleName}")
                        }
                    }
                    //endregion
                }
            }.let { results ->
                //region Await first, cancel others
                while (results.any { !it.isCompleted }) {
                    delay(1000)
                    val anyNull = results.filter { it.isCompleted }.any { it.await() == null }
                    if (anyNull) {
                        logger.i("One did pass, cancelling others")
                        break
                    }
                }

                results.map {
                    if (it.isCompleted) {
                        it.await()
                    } else {
                        it.cancel()
                        it.cancel(CancellationException("No longer needed"))
                        null
                    }
                }
                //endregion
            }

            //region Return potential error from results
            if (results.all { it != null }) {
                return@withContext results.first()!!
            }
            //endregion
            //region Test Access
            when (param) {
                is Target.OctoPrint -> testOctoPrint(
                    logger = logger,
                    webUrl = baseUrl,
                    target = param,
                    host = host
                )

                is Target.Webcam -> testWebcam(
                    logger = logger,
                    host = host,
                    webcamSettings = requireNotNull(webcamSettings) { "Settings must not be null, tested before" },
                    instanceId = param.instanceId,
                    webcamIndex = param.webcamIndex,
                )
            }
            //endregion
        } catch (e: Exception) {
            Finding.UnexpectedIssue(
                webUrl = webUrl?.toUrlOrNull(),
                exception = e
            )
        }
    }

    private suspend fun testOctoPrint(logger: Logger, webUrl: Url, target: Target.OctoPrint, host: String): Finding {
        // Test HTTP(S) access
        // Using the full stack here, just to be sure that the stack can also resolve the DNS
        // (should though as using same resolver)
        logger.i("Testing HTTP(S) connection")
        testHttpAccess(webUrl = webUrl, host = host, logger = logger)?.let { return it }
        logger.i("Passed")

        // Test that we actually are talking to an OctoPrint
        logger.i("Testing API key")
        testApiKeyValid(webUrl = webUrl, host = host, apiKey = target.apiKey, logger = logger)?.let { return it }
        logger.i("Passed.")

        // Test the websocket
        logger.i("Test web socket is working")
        testWebSocket(webUrl = webUrl, apiKey = target.apiKey, host = host, logger = logger)?.let { return it }
        logger.i("Passed")

        return Finding.OctoPrintReady(webUrl = webUrl, apiKey = target.apiKey)
    }

    private suspend fun testWebcam(
        logger: Logger,
        webcamSettings: ResolvedWebcamSettings.MjpegSettings,
        instanceId: String?,
        webcamIndex: Int,
        host: String
    ) = try {
        withTimeoutOrNull(30000) {
            //region Webcam
            logger.i("Test webcam")
            var startTime = Instant.DISTANT_PAST
            val frames = 20
            val frame = MjpegConnection3(
                streamUrl = webcamSettings.url.toString().toUrl(),
                name = "test",
                throwExceptions = true,
                httpSettings = httpClientSettings,
            ).load().mapNotNull { it as? MjpegConnection3.MjpegSnapshot.Frame }.onEach {
                startTime = startTime.takeIf { it > Instant.DISTANT_PAST } ?: Clock.System.now()
            }.take(frames).toList().last()
            val endTime = Clock.System.now()
            val fps = 1000 / ((endTime - startTime).inWholeMilliseconds / frames.toFloat())
            logger.i("Passed ($fps FPS)")
            //endregion
            //region Data saver
            val (snpashot, snapshotError) = try {
                getWebcamSnapshotUseCase.execute(
                    GetWebcamSnapshotUseCase.Params(
                        instanceId = instanceId,
                        webcamIndex = webcamIndex,
                        illuminateIfPossible = true,
                        maxSize = 720,
                        interval = null
                    )
                ).first() to null
            } catch (e: Exception) {
                null to e
            }
            //endregion

            Finding.WebcamReady(
                webUrl = webcamSettings.url.toString().toUrl(),
                fps = fps,
                image = frame.frame,
                dataSaverException = snapshotError,
                dataSaverImage = snpashot?.bitmap,
                dataSaverSource = snpashot?.source,
            )
        } ?: Finding.NoImage(webUrl = webcamSettings.url.toString().toUrl(), host = host)
        Finding.NoImage(webUrl = webcamSettings.url.toString().toUrl(), host = host)
    } catch (e: PrinterApiException) {
        logger.w(e)
        when (e.responseCode) {
            404 -> Finding.NotFound(
                host = host,
                webUrl = webcamSettings.url.toString().toUrl(),
            )

            else -> Finding.UnexpectedHttpIssue(
                webUrl = webcamSettings.url.toString().toUrl(),
                exception = e,
                host = host
            )
        }
    } catch (e: BasicAuthRequiredException) {
        logger.w(e)
        Finding.BasicAuthRequired(
            host = host,
            userRealm = e.userRealm,
            webUrl = webcamSettings.url.toString().toUrl(),
        )
    } catch (e: IOException) {
        logger.w(e)
        if (e.message?.contains("Connection broken", ignoreCase = true) == true) {
            Finding.NoImage(webUrl = webcamSettings.url.toString().toUrl(), host = host)
        } else {
            Finding.UnexpectedHttpIssue(webUrl = webcamSettings.url.toString().toUrl(), host = host, exception = e)
        }
    } catch (e: Exception) {
        Finding.UnexpectedIssue(webcamSettings.url.toString().toUrl(), e)
    }

    private fun CoroutineScope.testDns(host: String, webUrl: Url, logger: Logger): Pair<List<IpAddress>, Finding?> = try {
        val ips = localDnsResolver.lookup(host)
        if (ips.isEmpty()) throw UnknownHostException("Received no IPs for $host")
        ips to null
    } catch (e: UnknownHostException) {
        if (coroutineContext.isActive) logger.w(e)
        if (host.endsWith(".local") || host.endsWith(".home") || host.endsWith(".lan")) {
            emptyList<IpAddress>() to Finding.LocalDnsFailure(host = host, webUrl = webUrl)
        } else {
            emptyList<IpAddress>() to Finding.DnsFailure(host = host, webUrl = webUrl)
        }
    }

    private suspend fun testReachability(host: String, ip: IpAddress, webUrl: Url, logger: Logger): Finding? = try {
        ip.assertReachable(PING_TIMEOUT)
        null
    } catch (e: Exception) {
        if (currentCoroutineContext().isActive) logger.w(e)
        Finding.HostNotReachable(
            host = host,
            ip = ip.hostNameOrIp(),
            timeoutMs = PING_TIMEOUT.inWholeMilliseconds,
            webUrl = webUrl
        )
    }

    private suspend fun testPortOpen(host: String, ip: IpAddress, webUrl: Url, logger: Logger): Finding? = try {
        ip.assertPortOpen(webUrl.port, timeout = SOCKET_TIMEOUT)
        null
    } catch (e: Exception) {
        logger.w(e)
        Finding.PortClosed(
            host = host,
            port = webUrl.port,
            webUrl = webUrl
        )
    }

    private suspend fun testHttpAccess(webUrl: Url, host: String, logger: Logger): Finding? = try {
        val octoPrint = octoPrintProvider.createAdHocOctoPrint(OctoPrintInstanceInformationV3(id = "adhoc", webUrl = webUrl.toString().toUrl(), apiKey = "notanapikey"))
        try {
            octoPrint.userApi.getCurrentUser()
            null
        } catch (e: PrinterApiException) {
            if (e.responseCode == 404 || e.responseCode == 302) {
                Finding.NotFound(webUrl = webUrl, host = host)
            } else {
                Finding.UnexpectedHttpIssue(webUrl = webUrl, host = host, exception = IOException("Unexpected HTTP response code ${e.responseCode}"))
            }
        }
    } catch (e: PrinterHttpsException) {
        logger.w(e)
        Finding.HttpsNotTrusted(
            webUrl = webUrl,
            host = host,
            certificate = e.certificate,
            weakHostnameVerificationRequired = e.weakHostnameVerificationRequired,
        )
    } catch (e: BasicAuthRequiredException) {
        logger.w(e)
        Finding.BasicAuthRequired(
            host = host,
            userRealm = e.userRealm,
            webUrl = webUrl,
        )
    } catch (e: PrinterApiException) {
        logger.w(e)
        if (e.responseCode == 404) {
            Finding.NotFound(webUrl = webUrl, host = host)
        } else {
            Finding.UnexpectedHttpIssue(
                webUrl = webUrl,
                exception = e,
                host = host,
            )
        }
    } catch (e: Exception) {
        logger.w(e)
        Finding.UnexpectedHttpIssue(
            webUrl = webUrl,
            exception = e,
            host = host,
        )
    }

    private suspend fun testApiKeyValid(webUrl: Url, host: String, apiKey: String, logger: Logger): Finding? = try {
        val octoPrint = octoPrintProvider.createAdHocOctoPrint(OctoPrintInstanceInformationV3(id = "adhoc", webUrl = webUrl.toString().toUrl(), apiKey = apiKey))
        val isApiKeyValid = octoPrint.userApi.getCurrentUser().isGuest.not()
        if (isApiKeyValid) {
            null
        } else {
            Finding.InvalidApiKey(webUrl = webUrl, host = host)
        }
    } catch (e: PrinterApiException) {
        logger.w(e)
        if (e.responseCode == 404) {
            Finding.NotFound(webUrl = webUrl, host = host)
        } else {
            Finding.UnexpectedHttpIssue(
                webUrl = webUrl,
                exception = e,
                host = host,
            )
        }
    } catch (e: Exception) {
        logger.w(e)
        Finding.UnexpectedHttpIssue(
            webUrl = webUrl,
            exception = e,
            host = host,
        )
    }

    private suspend fun testWebSocket(webUrl: Url, apiKey: String, host: String, logger: Logger) = try {
        withTimeout(3000) {
            val instance = OctoPrintInstanceInformationV3(id = "adhoc", webUrl = webUrl.toString().toUrl(), apiKey = apiKey)
            when (val event = octoPrintProvider.createAdHocOctoPrint(instance).eventSource.eventFlow("test").filter { it !is Event.MessageReceived }.first()) {
                is Event.Connected -> null
                is Event.Disconnected -> when (event.exception) {
                    is WebSocketUpgradeFailedException -> Finding.WebSocketUpgradeFailed(
                        webUrl = webUrl,
                        host = host,
                        webSocketUrl = (event.exception as WebSocketUpgradeFailedException).webSocketUrl.toString(),
                        responseCode = (event.exception as WebSocketUpgradeFailedException).responseCode
                    )

                    else -> Finding.UnexpectedIssue(
                        webUrl = webUrl,
                        exception = event.exception ?: RuntimeException("Received unexpected null exception (1)")
                    )
                }
                else -> Finding.UnexpectedIssue(webUrl = webUrl, exception = RuntimeException("Received unexpected ${event::class.simpleName} (2)"))
            }
        }
    } catch (e: TimeoutCancellationException) {
        logger.w(e)
        Finding.UnexpectedIssue(webUrl = webUrl, Exception("Web socket test timed out"))
    }

    // URL is a String here because it might come directly from user input. This way we can also test URL syntax
    sealed class Target {

        data class Webcam(
            val instanceId: String,
            val webcamIndex: Int,
        ) : Target()

        data class OctoPrint(val webUrl: String, val apiKey: String) : Target()
    }

    sealed class Finding {
        abstract val webUrl: Url?

        data class EmptyUrl(
            override val webUrl: Url? = null,
        ) : Finding()

        data class InvalidUrl(
            override val webUrl: Url? = null,
            val input: String,
            val exception: Exception,
        ) : Finding()

        data class LocalDnsFailure(
            override val webUrl: Url,
            val host: String
        ) : Finding()

        data class DnsFailure(
            override val webUrl: Url,
            val host: String
        ) : Finding()

        data class HostNotReachable(
            override val webUrl: Url,
            val host: String,
            val ip: String,
            val timeoutMs: Long
        ) : Finding()

        data class PortClosed(
            override val webUrl: Url,
            val host: String,
            val port: Int
        ) : Finding()

        data class BasicAuthRequired(
            override val webUrl: Url,
            val host: String,
            val userRealm: String
        ) : Finding()

        data class HttpsNotTrusted(
            override val webUrl: Url,
            val host: String,
            val certificate: X509Certificate? = null,
            val weakHostnameVerificationRequired: Boolean = false,
        ) : Finding()

        data class NotFound(
            override val webUrl: Url,
            val host: String,
        ) : Finding()

        data class UnexpectedHttpIssue(
            override val webUrl: Url,
            val host: String,
            val exception: Throwable
        ) : Finding()

        data class ServerIsNotOctoPrint(
            override val webUrl: Url,
            val host: String,
        ) : Finding()

        data class InvalidApiKey(
            override val webUrl: Url,
            val host: String,
        ) : Finding()

        data class UnexpectedIssue(
            override val webUrl: Url?,
            val exception: Throwable
        ) : Finding()

        data class WebSocketUpgradeFailed(
            override val webUrl: Url,
            val host: String,
            val webSocketUrl: String,
            val responseCode: Int,
        ) : Finding()

        data class NoImage(
            override val webUrl: Url,
            val host: String,
        ) : Finding()

        data class OctoPrintReady(
            override val webUrl: Url,
            val apiKey: String,
        ) : Finding()

        data class WebcamReady(
            override val webUrl: Url,
            val dataSaverImage: Image?,
            val dataSaverSource: GetWebcamSnapshotUseCase.SnapshotSource?,
            val dataSaverException: Exception?,
            val fps: Float,
            val image: Image
        ) : Finding()
    }

    class WebcamUnsupportedException : IllegalArgumentException("Only MJPEG webcams are supported")

}