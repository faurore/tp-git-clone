package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.data.models.RemoteConnectionFailure
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.SetAlternativeWebUrlUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.exceptions.UserMessageException
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.map

abstract class RemoteAccessBaseViewModelCore(private val instanceId: String) {

    protected abstract val tag: String
    protected abstract val remoteServiceName: String
    private val getUrlUseCase = SharedBaseInjector.get().getRemoteServiceConnectUrlUseCase()
    private val setAlternativeWebUrlUseCase = SharedBaseInjector.get().setAlternativeWebUrlUseCase()
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository

    val connectedState = printerConfigRepository.instanceInformationFlow(instanceId)
        .distinctUntilChangedBy { it?.alternativeWebUrl to it?.remoteConnectionFailure }
        .map { config ->
            State(
                connected = config?.alternativeWebUrl?.isMyUrl() == true,
                url = config?.alternativeWebUrl?.let(::modifyUrl)?.takeIf { it.isMyUrl() }?.toString(),
                failure = config?.remoteConnectionFailure?.takeIf { it.remoteServiceName == remoteServiceName },
            )
        }

    open fun modifyUrl(url: Url): Url = url

    abstract fun Url.isMyUrl(): Boolean

    protected suspend fun getLoginUrl(remoteService: GetRemoteServiceConnectUrlUseCase.RemoteService): String? = try {
        val params = GetRemoteServiceConnectUrlUseCase.Params(
            remoteService = remoteService,
            instanceId = instanceId
        )

        when (val res = getUrlUseCase.execute(params)) {
            is GetRemoteServiceConnectUrlUseCase.Result.Error -> {
                ExceptionReceivers.dispatchException(RemoteServiceLoginException(res.errorMessage, res.exception))
                null
            }

            is GetRemoteServiceConnectUrlUseCase.Result.Success -> res.url
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to get OctoEverywhere login URL", throwable = e)
        ExceptionReceivers.dispatchException(e)
        null
    }

    suspend fun disconnect() = applyUrl(url = "", user = "", password = "", skipTest = false)

    protected open suspend fun applyUrl(url: String, user: String, password: String, skipTest: Boolean): Boolean = try {
        val params = SetAlternativeWebUrlUseCase.Params(
            instanceId = instanceId,
            webUrl = url,
            username = user,
            password = password,
            bypassChecks = skipTest
        )
        when (val res = setAlternativeWebUrlUseCase.execute(params)) {
            is SetAlternativeWebUrlUseCase.Result.Failure -> throw RemoteServiceLoginException(userMessage = res.errorMessage, cause = res.exception)
            SetAlternativeWebUrlUseCase.Result.Success -> true
        }
    } catch (e: Exception) {
        Napier.e(tag = tag, message = "Failed to disconnect OctoEverywhere", throwable = e)
        ExceptionReceivers.dispatchException(e)
        false
    }

    data class State(
        val connected: Boolean,
        val url: String?,
        val failure: RemoteConnectionFailure?,
    )

    class RemoteServiceLoginException(override val userMessage: String, cause: Throwable) : SuppressedIllegalStateException(userMessage, cause), UserMessageException


}