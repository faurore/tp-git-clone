package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.data.models.FileListState
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.searchInTree
import de.crysxd.octoapp.base.ext.sorted
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.models.files.FileOrigin
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.update

@OptIn(ExperimentalCoroutinesApi::class)
class FileSearchViewModelCore(private val instanceId: String) {

    private val loadFilesUseCase = SharedBaseInjector.get().loadFilesUseCase()

    private val loadTrigger = MutableStateFlow(LoadTrigger(id = 0, skipCache = false))
    private val searchTerm = MutableStateFlow("")
    val searchState = loadTrigger
        .filterNotNull()
        .flatMapLatest {
            loadFilesUseCase.execute(
                param = LoadFilesUseCase.Params.All(
                    instanceId = instanceId,
                    fileOrigin = FileOrigin.Local,
                    skipCache = it.skipCache
                )
            )
        }.combine(searchTerm) { file, term ->
            file to term
        }.map { (file, term) ->
            when (file) {
                is FileListState.Error -> FlowState.Error(file.exception)
                is FileListState.Loaded -> FlowState.Ready(
                    file.file.searchInTree(term).sorted().let {
                        SearchResult(
                            files = it.mapNotNull { it as? FileObject.File },
                            folders = it.mapNotNull { it as? FileObject.Folder }
                        )
                    }
                )
                FileListState.Loading -> FlowState.Loading()
            }
        }.catch {
            ExceptionReceivers.dispatchException(it)
            emit(FlowState.Error(it))
        }.flowOn(Dispatchers.Default)

    fun search(term: String, skipCache: Boolean) {
        if (skipCache) {
            loadTrigger.update { LoadTrigger(id = it.id + 1, skipCache = skipCache) }
        }

        searchTerm.value = term
    }

    data class SearchResult(
        val folders: List<FileObject.Folder>,
        val files: List<FileObject.File>,
    )

    private data class LoadTrigger(
        val id: Int,
        val skipCache: Boolean,
    )
}