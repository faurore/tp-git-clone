package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.OctoAnalytics
import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.commands.FileCommand
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.octoprint.OctoPlugins

class StartPrintJobUseCase(
    private val octoPrintProvider: OctoPrintProvider,
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPreferences: OctoPreferences,
) : UseCase2<StartPrintJobUseCase.Params, StartPrintJobUseCase.Result>() {

    override suspend fun doExecute(param: Params, logger: Logger): Result {
        val octoprint = octoPrintProvider.octoPrint(param.instanceId)
        val instance = octoPrintRepository.get(param.instanceId)
        val settings = instance?.settings ?: octoprint.settingsApi.getSettings()
        val materialManagerAvailable = octoprint.materialsApi.isMaterialManagerAvailable(settings)
        val timelapseConfigRequired = octoPreferences.askForTimelapseBeforePrinting

        // If a material manager is present and the selection was not confirmed, we need material selection
        // If the MMU2 plugin is installed, we skip this step as the MMU2 plugin will ask for the material
        if (materialManagerAvailable && !param.materialSelectionConfirmed && instance?.hasPlugin(OctoPlugins.Mmu2FilamentSelect) != true) {
            return Result.MaterialSelectionRequired
        }

        if (timelapseConfigRequired && !param.timelapseConfigConfirmed) {
            return Result.TimelapseConfigRequired
        }

        OctoAnalytics.logEvent(OctoAnalytics.Event.PrintStartedByApp)
        octoPrintProvider.octoPrint(param.instanceId).filesApi.executeFileCommand(param.file, FileCommand.SelectFile(print = true))
        return Result.PrintStarted
    }

    data class Params(
        val file: FileObject.File,
        val materialSelectionConfirmed: Boolean,
        val timelapseConfigConfirmed: Boolean,
        val instanceId: String? = null
    )

    sealed class Result {
        object PrintStarted : Result()
        object MaterialSelectionRequired : Result()
        object TimelapseConfigRequired : Result()
    }
}