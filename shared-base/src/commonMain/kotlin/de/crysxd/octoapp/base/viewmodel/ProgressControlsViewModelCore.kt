package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.data.models.FileListState
import de.crysxd.octoapp.base.data.models.ProgressWidgetSettings
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.utils.FlowState
import de.crysxd.octoapp.engine.models.event.Message
import de.crysxd.octoapp.engine.models.files.FileObject
import de.crysxd.octoapp.engine.octoprint.dto.message.CompanionPluginMessage
import io.github.aakira.napier.Napier
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart

@OptIn(ExperimentalCoroutinesApi::class)
class ProgressControlsViewModelCore(val instanceId: String) {

    private val tag = "ProgressControlsViewModelCore"
    private val printerProvider = SharedBaseInjector.get().octoPrintProvider
    private val preferences = SharedBaseInjector.get().preferences
    private val loadFileUseCase = SharedBaseInjector.get().loadFilesUseCase()

    private val currentMessageFlow = printerProvider.passiveCurrentMessageFlow(
        tag = "progress/current",
        instanceId = instanceId
    )

    private val pluginMessageFlow = printerProvider.passiveCachedMessageFlow(
        tag = "progress/plugin",
        instanceId = instanceId,
        clazz = CompanionPluginMessage::class
    ).onStart { emit(null) }

    private val activeFile = currentMessageFlow.distinctUntilChangedBy { it.job?.file }.flatMapLatest {
        val activeFile = it.job?.file ?: return@flatMapLatest flowOf(FlowState.Loading())
        val params = LoadFilesUseCase.Params.ForPath(fileOrigin = activeFile.origin, path = activeFile.path, instanceId = instanceId, skipCache = false)
        loadFileUseCase.execute(params).map { s ->
            when (s) {
                is FileListState.Error -> FlowState.Error(s.exception)
                is FileListState.Loaded -> FlowState.Ready(s.file as FileObject.File)
                FileListState.Loading -> FlowState.Loading()
            }
        }
    }.catch {
        FlowState.Error<FlowState<FileObject.File>>(it)
        Napier.e(tag = tag, message = "Getting file failed", throwable = it)
    }

    private val preferencesFlow = preferences.updatedFlow2.map {
        it.progressWidgetSettings
    }

    val state = combine(currentMessageFlow, pluginMessageFlow, preferencesFlow, activeFile) { current, plugin, settings, file ->
        State(
            current = current,
            activeFile = file,
            plugin = plugin,
            settings = settings
        )
    }

    data class State(
        val current: Message.Current,
        val activeFile: FlowState<FileObject.File>,
        val plugin: CompanionPluginMessage?,
        val settings: ProgressWidgetSettings
    )
}