package de.crysxd.octoapp.base.data.models

import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize
import kotlinx.serialization.Serializable

@Serializable
@CommonParcelize
data class WidgetPreferences(
    val listId: String,
    val items: List<WidgetType>,
    val hidden: List<WidgetType>,
) : CommonParcelable {

    constructor(listId: String, list: List<Pair<WidgetType, Boolean>>) : this(
        listId,
        list.map { it.first },
        list.mapNotNull { it.first.takeIf { _ -> it.second } },
    )

    fun prepare(list: List<WidgetType>) = list.sortedBy {
        items.indexOf(it).takeIf { it >= 0 } ?: Int.MAX_VALUE
    }.associateWith {
        hidden.contains(it)
    }
}
