package de.crysxd.octoapp.base.network

import de.crysxd.octoapp.sharedcommon.http.config.Dns
import kotlinx.datetime.Instant
import kotlinx.serialization.Serializable

interface CachedDns : Dns {

    fun addCacheEntry(entry: Entry)

    @Serializable
    data class Entry(
        val hostname: String,
        val resolvedIpString: List<String>,
        val validUntil: Instant = Instant.DISTANT_FUTURE,
    )

    companion object {
        val Noop = object : CachedDns {
            override fun addCacheEntry(entry: Entry) = Unit
            override fun lookup(hostname: String) = throw IllegalStateException("This DNS can't be used")
        }
    }
}