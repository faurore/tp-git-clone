package de.crysxd.octoapp.base.data.models

import kotlinx.serialization.Serializable

@Serializable
data class OctoEverywhereConnection(
    val connectionId: String,
    val apiToken: String,
    val bearerToken: String,
    val basicAuthUser: String,
    val basicAuthPassword: String,
)