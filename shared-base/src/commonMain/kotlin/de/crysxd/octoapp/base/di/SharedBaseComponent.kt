package de.crysxd.octoapp.base.di

import de.crysxd.octoapp.base.OctoPreferences
import de.crysxd.octoapp.base.data.repository.ExtrusionHistoryRepository
import de.crysxd.octoapp.base.data.repository.FileListRepository
import de.crysxd.octoapp.base.data.repository.GcodeHistoryRepository
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.data.repository.PinnedMenuItemRepository
import de.crysxd.octoapp.base.data.repository.SerialCommunicationLogsRepository
import de.crysxd.octoapp.base.data.repository.TemperatureDataRepository
import de.crysxd.octoapp.base.data.repository.TutorialsRepository
import de.crysxd.octoapp.base.data.repository.WidgetPreferencesRepository
import de.crysxd.octoapp.base.io.FileManager
import de.crysxd.octoapp.base.io.SettingsStore
import de.crysxd.octoapp.base.logging.SensitiveDataMask
import de.crysxd.octoapp.base.network.CachedDns
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.base.usecase.ApplyWebcamTransformationsUseCase
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.CreateBugReportUseCase
import de.crysxd.octoapp.base.usecase.DiscoverOctoPrintUseCase
import de.crysxd.octoapp.base.usecase.GetActiveHttpUrlUseCase
import de.crysxd.octoapp.base.usecase.GetAppLanguageUseCase
import de.crysxd.octoapp.base.usecase.GetCurrentPrinterProfileUseCase
import de.crysxd.octoapp.base.usecase.GetPowerDevicesUseCase
import de.crysxd.octoapp.base.usecase.GetRemoteServiceConnectUrlUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSettingsUseCase
import de.crysxd.octoapp.base.usecase.GetWebcamSnapshotUseCase
import de.crysxd.octoapp.base.usecase.HandleAutomaticLightEventUseCase
import de.crysxd.octoapp.base.usecase.HandleOctoEverywhereAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.HandleSpaghettiDetectiveAppPortalSuccessUseCase
import de.crysxd.octoapp.base.usecase.LoadFilesUseCase
import de.crysxd.octoapp.base.usecase.RequestApiAccessUseCase
import de.crysxd.octoapp.base.usecase.SetAlternativeWebUrlUseCase
import de.crysxd.octoapp.base.usecase.SetTargetTemperaturesUseCase
import de.crysxd.octoapp.base.usecase.SetTemperatureOffsetUseCase
import de.crysxd.octoapp.base.usecase.StartPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.usecase.UpdateInstanceCapabilitiesUseCase
import de.crysxd.octoapp.base.usecase.UpdateNgrokTunnelUseCase
import de.crysxd.octoapp.sharedcommon.di.BaseComponent
import de.crysxd.octoapp.sharedcommon.di.SharedCommonComponent
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.KeyStoreProvider
import org.koin.core.component.get
import org.koin.core.component.inject


class SharedBaseComponent(
    sharedCommonComponent: SharedCommonComponent,
    sharedBaseModule: SharedBaseModule = SharedBaseModule(),
    repositoryModule: RepositoryModule = RepositoryModule(),
    loggingModule: LoggingModule = LoggingModule(),
    networkModule: NetworkModule,
    useCaseModule: UseCaseModule = UseCaseModule()
) : BaseComponent(
    dependencies = listOf(
        sharedCommonComponent,
    ),
    modules = listOf(
        sharedBaseModule,
        repositoryModule,
        loggingModule,
        networkModule,
        useCaseModule,
    )
) {
    // SharedBaseModule
    val settings: SettingsStore by inject()
    val fileManager: FileManager by inject()

    // RepositoryModule
    val extrusionHistoryRepository: ExtrusionHistoryRepository by inject()
    val pinnedMenuItemRepository: PinnedMenuItemRepository by inject()
    val gcodeHistoryRepository: GcodeHistoryRepository by inject()
    val printerConfigRepository: OctoPrintRepository by inject()
    val widgetPreferencesRepository: WidgetPreferencesRepository by inject()
    val preferences: OctoPreferences by inject()
    val fileListRepository: FileListRepository by inject()
    val serialCommunicationLogsRepository: SerialCommunicationLogsRepository by inject()
    val temperatureDataRepository: TemperatureDataRepository by inject()
    val tutorialsRepository: TutorialsRepository by inject()

    // NetworkModule
    val octoPrintProvider: OctoPrintProvider by inject()
    val dnsResolver: CachedDns by inject()
    val keyStoreProvider: KeyStoreProvider by inject()
    fun httpClientSettings(): HttpClientSettings = get()

    // LoggingModule
    val sensitiveDataMask: SensitiveDataMask by inject()

    // UseCases
    fun getWebcamSettingsUseCase(): GetWebcamSettingsUseCase = get()
    fun getActiveHttpUrlUseCase(): GetActiveHttpUrlUseCase = get()
    fun applyWebcamTransformationsUseCase(): ApplyWebcamTransformationsUseCase = get()
    fun getPowerDevicesUseCase(): GetPowerDevicesUseCase = get()
    fun handleAutomaticLightEventUseCase(): HandleAutomaticLightEventUseCase = get()
    fun testFullNetworkStackUseCase(): TestFullNetworkStackUseCase = get()
    fun discoverOctoPrintUseCase(): DiscoverOctoPrintUseCase = get()
    fun requestApiAccessUseCase(): RequestApiAccessUseCase = get()
    fun getAppLanguageUseCase(): GetAppLanguageUseCase = get()
    fun updateInstanceCapabilitiesUseCase(): UpdateInstanceCapabilitiesUseCase = get()
    fun getWebcamSnapshotUseCase(): GetWebcamSnapshotUseCase = get()
    fun togglePausePrintJobUseCase(): TogglePausePrintJobUseCase = get()
    fun cancelPrintJobUseCase(): CancelPrintJobUseCase = get()
    fun setTargetTemperaturesUseCase(): SetTargetTemperaturesUseCase = get()
    fun setTemperatureOffsetUseCase(): SetTemperatureOffsetUseCase = get()
    fun getCurrentPrinterProfileUseCase(): GetCurrentPrinterProfileUseCase = get()
    fun loadFilesUseCase(): LoadFilesUseCase = get()
    fun startPrintJobUseCase(): StartPrintJobUseCase = get()
    fun createBugReportUseCase(): CreateBugReportUseCase = get()
    fun getRemoteServiceConnectUrlUseCase(): GetRemoteServiceConnectUrlUseCase = get()
    fun handleOctoEverywhereAppPortalSuccessUseCase(): HandleOctoEverywhereAppPortalSuccessUseCase = get()
    fun handleSpaghettiDetectiveAppPortalSuccessUseCase(): HandleSpaghettiDetectiveAppPortalSuccessUseCase = get()
    fun setAlternativeWebUrlUseCase(): SetAlternativeWebUrlUseCase = get()
    fun updateNgrokTunnelUseCase(): UpdateNgrokTunnelUseCase = get()
}
