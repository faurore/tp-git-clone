package de.crysxd.octoapp.base.io

import de.crysxd.octoapp.sharedcommon.Platform
import okio.FileSystem
import okio.Path.Companion.toPath

expect val DefaultFileSystem: FileSystem

class FileManager(
    private val platform: Platform,
    private val fileSystem: FileSystem = DefaultFileSystem,
) {

    fun forNameSpace(nameSpace: String) = Delegate(nameSpace)

    inner class Delegate(private val nameSpace: String) {
        private fun getPath(name: String) = platform.cacheDirectory.toPath()
            .resolve(platform.appId)
            .resolve(nameSpace)
            .resolve(name)

        fun createCacheFile(name: String) = getPath(name = name).let { path ->
            path.parent?.let { fileSystem.createDirectories(it) }
            fileSystem.openReadWrite(path)
        }

        fun openCacheFile(name: String) = getPath(name = name).let { path ->
            fileSystem.openReadOnly(path)
        }

        fun exists(name: String) = fileSystem.exists(getPath(name = name))

        fun metadataOrNull(name: String) = fileSystem.metadataOrNull(getPath(name = name))
    }
}