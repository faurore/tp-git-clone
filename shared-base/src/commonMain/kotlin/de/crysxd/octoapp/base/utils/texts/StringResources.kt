package de.crysxd.octoapp.base.utils.texts

expect fun getString(id: String, vararg formatArgs: Any): String

expect fun CharSequence.parseHtml(): CharSequence
