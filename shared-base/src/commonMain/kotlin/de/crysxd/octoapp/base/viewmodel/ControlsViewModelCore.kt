package de.crysxd.octoapp.base.viewmodel

import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.CancelPrintJobUseCase
import de.crysxd.octoapp.base.usecase.TogglePausePrintJobUseCase
import de.crysxd.octoapp.base.utils.ExceptionReceivers
import kotlinx.coroutines.flow.map

class ControlsViewModelCore(private val instanceId: String) {

    private val tag = "ControlsViewModelCore"
    private val octoPrintProvider = SharedBaseInjector.get().octoPrintProvider
    private val cancelPrintJobUseCase = SharedBaseInjector.get().cancelPrintJobUseCase()
    private val togglePausePrintJobUseCase = SharedBaseInjector.get().togglePausePrintJobUseCase()

    val state = octoPrintProvider.passiveCurrentMessageFlow(tag = "controls-vm/state", instanceId = instanceId).map {
        when {
            it.state.flags.isPrinting() -> State.Print(
                progress = it.progress?.completion?.toInt() ?: 0,
                pausing = it.state.flags.pausing,
                paused = it.state.flags.paused,
                cancelling = it.state.flags.cancelling,
            )
            it.state.flags.isOperational() -> State.Prepare
            else -> State.Connect
        }
    }

    suspend fun cancelPrint() = ExceptionReceivers.runWithErrorMessage(tag = tag) {
        cancelPrintJobUseCase.execute(
            CancelPrintJobUseCase.Params(restoreTemperatures = false, instanceId = instanceId)
        )
    }

    suspend fun togglePause() = ExceptionReceivers.runWithErrorMessage(tag = tag) {
        togglePausePrintJobUseCase.execute(
            TogglePausePrintJobUseCase.Params(instanceId = instanceId)
        )
    }

    sealed class State {
        object Connect : State()
        object Prepare : State()
        data class Print(val progress: Int, val pausing: Boolean, val paused: Boolean, val cancelling: Boolean) : State()
    }
}