package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.data.models.ResolvedWebcamSettings
import de.crysxd.octoapp.base.data.repository.OctoPrintRepository
import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.asOctoPrint
import de.crysxd.octoapp.engine.exceptions.PrinterApiException
import de.crysxd.octoapp.engine.models.settings.WebcamSettings
import de.crysxd.octoapp.sharedcommon.exceptions.SuppressedIllegalStateException
import de.crysxd.octoapp.sharedcommon.ext.SharedIO
import de.crysxd.octoapp.sharedcommon.http.DefaultHttpClient
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.framework.forLogging
import de.crysxd.octoapp.sharedcommon.http.framework.resolve
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import de.crysxd.octoapp.sharedcommon.http.framework.withBasicAuth
import de.crysxd.octoapp.sharedcommon.http.framework.withHost
import de.crysxd.octoapp.sharedcommon.utils.asVersion
import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image
import de.crysxd.octoapp.sharedexternalapis.mjpeg.JpegDecoder
import de.crysxd.octoapp.sharedexternalapis.mjpeg.MjpegConnection3
import io.ktor.client.request.get
import io.ktor.client.request.head
import io.ktor.client.statement.bodyAsChannel
import io.ktor.http.Url
import io.ktor.util.toByteArray
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.distinctUntilChangedBy
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.isActive

@OptIn(ExperimentalCoroutinesApi::class)
class GetWebcamSnapshotUseCase(
    private val octoPrintRepository: OctoPrintRepository,
    private val octoPrintProvider: OctoPrintProvider,
    private val getActiveHttpUrlUseCase: GetActiveHttpUrlUseCase,
    private val getWebcamSettingsUseCase: GetWebcamSettingsUseCase,
    private val applyWebcamTransformationsUseCase: ApplyWebcamTransformationsUseCase,
    private val handleAutomaticLightEventUseCase: HandleAutomaticLightEventUseCase,
    private val httpSettings: HttpClientSettings,
) : UseCase2<GetWebcamSnapshotUseCase.Params, Flow<GetWebcamSnapshotUseCase.Snapshot>>() {

    override suspend fun doExecute(param: Params, logger: Logger): Flow<Snapshot> {
        var illuminated = false
        return octoPrintRepository.instanceInformationFlow(param.instanceId)
            .map { requireNotNull(it) { "OctoPrint not found" } }
            .flatMapLatest { instance -> getActiveHttpUrlUseCase.execute(instance).map { it to instance } }
            .distinctUntilChangedBy { it.first.hashCode() + it.second.settings.hashCode() }
            .flatMapLatest { getWebcamSettingsUseCase.execute(it.second).map { settings -> Triple(it.first, it.second, settings) } }
            .flatMapLatest { input ->
                require(input.third.isNotEmpty()) { throw SuppressedIllegalStateException("Webcam not configured, zero settings") }
                logger.d("Creating snapshot stream for input: $input")

                // Bit weird structure, but allows us to only get it once and only get it if we need it
                // composeOctoPrintSnapshotUrl triggers a network request, so we need to be cautious
                var octoPrintSnapshotUrl: Url? = null
                suspend fun getOctoPrintSnapshotUrl() = octoPrintSnapshotUrl ?: composeOctoPrintSnapshotUrl(logger, input.first, input.second)
                    .also { octoPrintSnapshotUrl = it }

                val companionVersion = input.second.settings?.plugins?.octoAppCompanion?.version
                val mjpegSettings = input.third[param.webcamIndex] as? ResolvedWebcamSettings.MjpegSettings
                val octoPrintSnapshotSettings = mjpegSettings?.webcamSettings ?: input.third.firstOrNull()?.webcamSettings

                when {
                    // We prefer to use our plugin
                    companionVersion != null && companionVersion.asVersion() >= "1.0.11".asVersion() -> createCompanionSnapshotFlow(
                        instanceId = input.second.id,
                        octoPrintSnapshotUrl = ::getOctoPrintSnapshotUrl,
                        webcamIndex = param.webcamIndex,
                        octoPrintSnapshotSettings = octoPrintSnapshotSettings,
                        mjpegSettings = mjpegSettings,
                        logger = logger,
                        interval = param.interval,
                        maxSize = param.maxSize,
                    )

                    // Second best choice is the snapshot URL
                    getOctoPrintSnapshotUrl() != null -> createOctoPrintSnapshotFlow(
                        octoPrintSnapshotUrl = octoPrintSnapshotUrl!!,
                        octoPrintSnapshotSettings = octoPrintSnapshotSettings,
                        mjpegSettings = mjpegSettings,
                        logger = logger,
                        interval = param.interval,
                        maxSize = param.maxSize,
                    )

                    // Last resort is using MJPEG
                    mjpegSettings != null -> createMjpegOctoPrintSnapshotFlow(
                        mjpegSettings = mjpegSettings,
                        logger = logger,
                        maxSize = param.maxSize,
                        interval = param.interval,
                    )

                    // No snapshot available :(
                    else -> throw SuppressedIllegalStateException("No snapshot URL is available, please install the OctoApp Companion plugin and set up a snapshot URL in the webcam settings!")
                }
            }.onStart {
                try {
                    if (param.illuminateIfPossible) {
                        illuminated = handleAutomaticLightEventUseCase.executeBlocking(
                            HandleAutomaticLightEventUseCase.Event.WebcamVisible(source = "webcam-snapshot-uc", instanceId = param.instanceId)
                        )
                        // Slight delay so a single snapshot is nicely lit
                        delay(500)
                    }
                } catch (e: Exception) {
                    logger.e("Error while handling automatic", e)
                }
            }.onCompletion {
                try {
                    if (illuminated) {
                        // Execute blocking as a normal execute switches threads causing the task never to be done as the current scope
                        // is about to be terminated
                        handleAutomaticLightEventUseCase.executeBlocking(
                            HandleAutomaticLightEventUseCase.Event.WebcamGone(source = "webcam-snapshot-uc", instanceId = param.instanceId, delayAction = true)
                        )
                    }
                } catch (e: Exception) {
                    logger.e("Error while handling automatic", e)
                }
            }.flowOn(Dispatchers.SharedIO)
    }

    private suspend fun composeOctoPrintSnapshotUrl(
        logger: Logger,
        activeUrl: Url,
        instance: OctoPrintInstanceInformationV3
    ): Url? {
        val rawSnapshotUrl = instance.settings?.webcam?.snapshotUrl ?: run {
            logger.w("No native snapshot URL available for ${instance.id}")
            return null
        }

        // OctoPi default value, reconstruct to a known working value as this one won't do
        val snapshotHttpUrl = if (rawSnapshotUrl == "http://127.0.0.1:8080/?action=snapshot") {
            "http://127.0.0.1/webcam/?action=snapshot".toUrl()
        } else {
            requireNotNull(rawSnapshotUrl.toUrlOrNull() ?: instance.webUrl.resolve(rawSnapshotUrl)) {
                SuppressedIllegalStateException("Unable to build snapshot URL from parts: webUrl='${instance.webUrl.forLogging()}' snapshotUrl='$rawSnapshotUrl'")
            }
        }

        // Replace hosts we can't resolve with something we can
        // Also make sure to carry over the basic auth
        val resolvedHost = when (snapshotHttpUrl.host) {
            "localhost",
            "127.0.0.1" -> Triple(activeUrl.host, activeUrl.user, activeUrl.password)
            instance.webUrl.host -> Triple(activeUrl.host, activeUrl.user, activeUrl.password)
            else -> Triple(snapshotHttpUrl.host, snapshotHttpUrl.user, snapshotHttpUrl.password)
        }

        // Compose URL
        val url = snapshotHttpUrl.withHost(resolvedHost.first).withBasicAuth(user = resolvedHost.second, password = resolvedHost.third)

        // Let's test it!
        logger.i("Composed URL, now testing: $url")
        val success = try {
            createHttpClient()
                .head(url.toString())
                .status.value
            true
        } catch (e: Throwable) {
            if (e is PrinterApiException || e.cause is PrinterApiException) {
                // Some HTTP response is still OK
                true
            } else {
                // Not sure what went wrong
                logger.e("Composed URL failed with exception ${e::class.simpleName}: ${e.message}")
                false
            }
        }

        return if (success) {
            logger.i("Composed URL is fine: $url")
            url
        } else {
            null
        }
    }

    private fun createCompanionSnapshotFlow(
        instanceId: String,
        webcamIndex: Int,
        octoPrintSnapshotUrl: suspend () -> Url?,
        octoPrintSnapshotSettings: WebcamSettings?,
        mjpegSettings: ResolvedWebcamSettings.MjpegSettings?,
        logger: Logger,
        interval: Long?,
        maxSize: Int,
    ) = flow {
        val octoPrint = octoPrintProvider.octoPrint(instanceId = instanceId)
        val decoder = JpegDecoder(usePool = interval != null, logTag = "JpegDecoder", maxImageSize = maxSize)
        logger.d("Using companion snapshot stream")
        suspend fun emitSnapshot(): Boolean = try {
            logger.v("Loading snapshot")
            val bytes = octoPrint.asOctoPrint().octoAppCompanionApi.getWebcamSnapshot(maxSize = maxSize, webcamIndex = webcamIndex).toByteArray()
            val image = decoder.decode(bytes, bytes.size)

            emit(image)
            true
        } catch (e: PrinterApiException) {
            if (e.responseCode == 406) {
                false
            } else {
                throw e
            }
        }

        interval?.let {
            while (currentCoroutineContext().isActive) {
                if (emitSnapshot()) {
                    // Success
                    delay(interval)
                } else {
                    // Image was not available, use shortened delay and try again
                    delay(interval.coerceAtMost(2000L))
                }
            }
        } ?: emitSnapshot()
    }.onStart {
        logger.i("Starting companion snapshot flow")
    }.map {
        Snapshot(it, false, SnapshotSource.CompanionPlugin)
    }.onCompletion {
        logger.i("Stopping companion snapshot flow")
    }.catch {
        logger.e("Error while loading snapshot", it)
        // Can we fallback on the OctoPrint snapshot flow?
        logger.e("Failure in companion snapshot flow. Trying to fall back on OctoPrint snapshot flow: ${it::class.simpleName}: ${it.message}")
        val url = octoPrintSnapshotUrl()

        when {
            url != null && octoPrintSnapshotSettings != null -> {
                logger.e("Can fall back on OctoPrint snapshots, executing fallback")
                emitAll(
                    createOctoPrintSnapshotFlow(
                        logger = logger,
                        octoPrintSnapshotSettings = octoPrintSnapshotSettings,
                        octoPrintSnapshotUrl = url,
                        mjpegSettings = mjpegSettings,
                        interval = interval,
                        maxSize = maxSize,
                        advertiseCompanion = false,
                    )
                )
            }

            mjpegSettings != null -> {
                logger.e("Can fall back on MJPEG snapshots, executing fallback")
                emitAll(
                    createMjpegOctoPrintSnapshotFlow(
                        logger = logger,
                        mjpegSettings = mjpegSettings,
                        interval = interval,
                        maxSize = maxSize,
                    )
                )
            }

            else -> {
                logger.e("Unable to fall back")
                throw it
            }
        }
    }

    private fun createOctoPrintSnapshotFlow(
        logger: Logger,
        octoPrintSnapshotSettings: WebcamSettings?,
        mjpegSettings: ResolvedWebcamSettings.MjpegSettings?,
        octoPrintSnapshotUrl: Url,
        interval: Long?,
        maxSize: Int,
        advertiseCompanion: Boolean = true,
    ) = flow {
        logger.d("Using OctoPrint snapshot stream with: $mjpegSettings")
        val client = createHttpClient()
        val decoder = JpegDecoder(usePool = interval != null, logTag = "JpegDecoder", maxImageSize = maxSize)
        suspend fun emitSnapshot() {
            val bytes = client.get(octoPrintSnapshotUrl).bodyAsChannel().toByteArray()
            emit(decoder.decode(bytes, bytes.size))
        }
        interval?.let {
            while (currentCoroutineContext().isActive) {
                emitSnapshot()
                delay(interval)
            }
        } ?: emitSnapshot()
    }.map {
        octoPrintSnapshotSettings?.let { s -> applyWebcamTransformationsUseCase.execute(ApplyWebcamTransformationsUseCase.Params(frame = it, settings = s)) } ?: it
    }.map {
        Snapshot(it, advertiseCompanion, SnapshotSource.OctoPrintSnapshot(url = octoPrintSnapshotUrl))
    }.onStart {
        logger.i("Starting OctoPrint snapshot flow")
    }.onCompletion {
        logger.i("Stopping OctoPrint snapshot flow")
    }.catch {
        logger.e("Error while loading snapshot", it)
        // Can we fallback on the MJPEG flow?
        mjpegSettings?.let { settings ->
            logger.e("Failure in OctoPrint snapshot flow, falling back on MJPEG snapshot flow: ${it::class.simpleName}: ${it.message}")
            emitAll(
                createMjpegOctoPrintSnapshotFlow(
                    logger = logger,
                    mjpegSettings = settings,
                    interval = interval,
                    maxSize = maxSize,
                    advertiseCompanion = advertiseCompanion,
                )
            )
        } ?: throw it
    }

    private fun createMjpegOctoPrintSnapshotFlow(
        logger: Logger,
        mjpegSettings: ResolvedWebcamSettings.MjpegSettings,
        interval: Long?,
        maxSize: Int,
        advertiseCompanion: Boolean = true,
    ) = flow {
        logger.d("Using MJPEG snapshot stream with: $mjpegSettings")
        suspend fun emitSnapshot() {
            val connection = MjpegConnection3(mjpegSettings.url, name = "get-webcam-snapshot", throwExceptions = true, maxSize = maxSize, httpSettings = httpSettings)
            val frame = connection.load().mapNotNull { it as? MjpegConnection3.MjpegSnapshot.Frame }.first()
            emit(frame)
        }

        interval?.let {
            while (currentCoroutineContext().isActive) {
                emitSnapshot()
                delay(interval)
            }
        } ?: emitSnapshot()
    }.map {
        applyWebcamTransformationsUseCase.execute(ApplyWebcamTransformationsUseCase.Params(frame = it.frame, settings = mjpegSettings.webcamSettings))
    }.map {
        Snapshot(it, advertiseCompanion, SnapshotSource.MjpegSnapshot(url = mjpegSettings.url))
    }.onStart {
        logger.i("Starting MJPEG snapshot flow")
    }.onCompletion {
        logger.i("Stopping MJPEG snapshot flow")
    }

    private fun createHttpClient() = DefaultHttpClient(
        settings = httpSettings,
        logTag = "KTOR/GetWebcamSnapshotUseCase",
        config = { expectSuccess = true }
    )

    data class Snapshot(
        val bitmap: Image,
        val advertiseCompanion: Boolean,
        val source: SnapshotSource,
    )

    sealed class SnapshotSource {
        object CompanionPlugin : SnapshotSource()
        data class OctoPrintSnapshot(val url: Url) : SnapshotSource()
        data class MjpegSnapshot(val url: Url) : SnapshotSource()
    }

    data class Params(
        val instanceId: String?,
        val webcamIndex: Int = 0,
        val illuminateIfPossible: Boolean = true,
        val interval: Long?,
        val maxSize: Int,
    )
}