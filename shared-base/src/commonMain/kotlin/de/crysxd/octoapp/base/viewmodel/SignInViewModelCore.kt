package de.crysxd.octoapp.base.viewmodel

import com.benasher44.uuid.uuid4
import de.crysxd.octoapp.base.billing.BillingManager
import de.crysxd.octoapp.base.billing.FEATURE_QUICK_SWITCH
import de.crysxd.octoapp.base.data.models.NetworkService
import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.usecase.RequestApiAccessUseCase
import de.crysxd.octoapp.base.usecase.TestFullNetworkStackUseCase
import de.crysxd.octoapp.base.usecase.execute
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import de.crysxd.octoapp.sharedcommon.http.framework.toUrlOrNull
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.mapNotNull
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import kotlinx.datetime.Instant
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@OptIn(ExperimentalCoroutinesApi::class)
class SignInViewModelCore(private val allowNetworkInSplash: Boolean) {

    companion object {
        private val mainProcessDuration = 3.5.seconds
        private val splashUntil = Clock.System.now() + mainProcessDuration
    }

    private val tag = "SignInViewModelCore"
    private val printerConfigRepository = SharedBaseInjector.get().printerConfigRepository
    private val discoverServicesUseCase = SharedBaseInjector.get().discoverOctoPrintUseCase()
    private val testFullNetworkStackUseCase = SharedBaseInjector.get().testFullNetworkStackUseCase()
    private val requestApiAccessUseCase = SharedBaseInjector.get().requestApiAccessUseCase()
    private val sensitiveDataMask = SharedBaseInjector.get().sensitiveDataMask

    private val mutableState = MutableStateFlow<Pair<Int, State>>(0 to State.Initial)
    val state = mutableState.asStateFlow()
        .flatMapLatest { (_, state) ->
            when (state) {
                is State.Initial -> flowOf(State.Initial)

                is State.Splash,
                is State.Discover -> createDiscoverFlow()

                is State.Probe -> createProbeFlow(webUrl = state.urlString, reusedInstanceId = state.reusedInstanceId, apiKey = state.apiKey)
                is State.AccessRequest -> createAccessRequestFlow(webUrl = state.url, reusedInstanceId = state.reusedInstanceId)

                is State.AccessRequestFailed,
                is State.Success -> flowOf(state)
            }
        }.catch {
            Napier.e(tag = tag, message = "Non-contained exception caught, resetting", throwable = it)
            emit(State.Initial)
        }

    private suspend fun createDiscoverFlow() = flow {
        val discoverFlow = discoverServicesUseCase.execute()
        val configFlow = printerConfigRepository.allInstanceInformationFlow()
        val quickSwitchFlow = BillingManager.isFeatureEnabledFlow(FEATURE_QUICK_SWITCH)
        val startedAt = Clock.System.now()

        val activeUntil = startedAt + mainProcessDuration
        val splashFlow = configFlow.flatMapLatest { configs ->
            flow {
                // Skip splash if time is over or we already have some instances connected
                val splashTime = splashUntil - Clock.System.now()
                if (splashTime > 0.seconds && configs.isEmpty()) {
                    emit(true)
                    delay(splashTime)
                }

                emit(false)
            }
        }

        val splashDiscoverFlow = combine(splashFlow, configFlow, quickSwitchFlow) { i, j, k -> Triple(i, j, k) }
            .flatMapLatest { (splash, configs, hasQuickSwitch) ->
                // We return the real discover flow if we are no longer in splash, allowed to use network in splash (Android yes, iOS no) or we already have configs
                // which means we skip splash
                if (!splash || allowNetworkInSplash || configs.isNotEmpty()) {
                    // Add little extra delay to let splash finish properly in UI in case we were not allowed to network before
                    if (!allowNetworkInSplash) {
                        delay(500.milliseconds)
                    }

                    // Filter out instances that are already connected. If quick switch is disabled, still list them
                    discoverFlow.map { (services) ->
                        services.filterAlreadyKnown(configs, hasQuickSwitch)
                    }
                } else {
                    // We are not allowed to connect discover flow yet, will trigger network access during splash
                    flowOf(emptyList())
                }
            }

        val combined = combine(splashFlow, splashDiscoverFlow, configFlow, quickSwitchFlow) { splash, discoverServices, configs, hasQuickSwitch ->
            when {
                splash -> State.Splash(until = activeUntil)
                else -> State.Discover(services = discoverServices, configs = configs.values.sortedBy { it.label }.toList(), hasQuickSwitch = hasQuickSwitch)
            }
        }

        emitAll(combined)
    }

    private fun List<NetworkService>.filterAlreadyKnown(configs: Map<String, OctoPrintInstanceInformationV3>, hasQuickSwitch: Boolean) = filter { service ->
        configs.none { it.value.webUrl == service.webUrl.toUrlOrNull() } || !hasQuickSwitch
    }

    private suspend fun createProbeFlow(webUrl: String, reusedInstanceId: String?, apiKey: String?) = flow {
        emit(State.Probe(urlString = webUrl, finding = null, reusedInstanceId = reusedInstanceId, apiKey = apiKey))
        val start = Clock.System.now()

        val finding = try {
            testFullNetworkStackUseCase.execute(TestFullNetworkStackUseCase.Target.OctoPrint(webUrl, apiKey = apiKey ?: ""))
        } catch (e: Exception) {
            TestFullNetworkStackUseCase.Finding.UnexpectedIssue(webUrl = null, exception = e)
        }

        // Let the test take at least x time so the user sees we are very BUSY :D
        delay(mainProcessDuration - (Clock.System.now() - start))

        when (finding) {
            is TestFullNetworkStackUseCase.Finding.InvalidApiKey -> {
                // If the API key is invalid, we passed. Move to access request
                moveToState(State.AccessRequest(finding.webUrl, reusedInstanceId = reusedInstanceId, loginUrl = null))
            }

            is TestFullNetworkStackUseCase.Finding.OctoPrintReady -> {
                moveToState(
                    State.Success(
                        config = OctoPrintInstanceInformationV3(
                            id = reusedInstanceId ?: uuid4().toString(),
                            webUrl = finding.webUrl,
                            apiKey = finding.apiKey,
                        )
                    )
                )
            }

            else -> {
                emit(State.Probe(urlString = webUrl, finding = finding, reusedInstanceId = reusedInstanceId, apiKey = apiKey))
            }
        }
    }

    private suspend fun createAccessRequestFlow(webUrl: Url, reusedInstanceId: String?) = flow {
        emit(State.AccessRequest(url = webUrl, reusedInstanceId = reusedInstanceId, loginUrl = null))

        val nested = requestApiAccessUseCase.execute(
            param = RequestApiAccessUseCase.Params(webUrl)
        ).mapNotNull { state ->
            val (emit, move) = when (state) {
                is RequestApiAccessUseCase.State.AccessGranted -> null to State.Success(
                    config = OctoPrintInstanceInformationV3(
                        webUrl = webUrl,
                        id = reusedInstanceId ?: uuid4().toString(),
                        apiKey = state.apiKey
                    )
                )

                RequestApiAccessUseCase.State.Failed -> null to State.AccessRequestFailed(
                    url = webUrl,
                    reusedInstanceId = reusedInstanceId
                )

                is RequestApiAccessUseCase.State.Pending -> State.AccessRequest(
                    url = webUrl,
                    reusedInstanceId = reusedInstanceId,
                    loginUrl = state.authUrl?.toUrl(),
                ) to null
            }

            move?.let { moveToState(it) }
            emit
        }.onStart {
            Napier.i(tag = tag, message = "Starting request access flow")
        }.onCompletion {
            Napier.i(tag = tag, message = "Completing request access flow")
        }

        emitAll(nested)
    }

    fun start(repairForInstanceId: String?) = if (repairForInstanceId != null) {
        printerConfigRepository.get(repairForInstanceId)?.let { config ->
            Napier.i(tag = tag, message = "Starting in repair mode for $repairForInstanceId")
            moveToState(State.Probe(urlString = config.webUrl.toString(), reusedInstanceId = repairForInstanceId, finding = null, apiKey = config.apiKey))
        } ?: let {
            val message = "Started in repair mode for $repairForInstanceId but instance was not found!"
            Napier.e(tag = tag, message = message, throwable = IllegalStateException(message))
            moveToState(State.Splash(splashUntil))
        }
    } else {
        moveToState(State.Splash(splashUntil))
    }

    fun activate(config: OctoPrintInstanceInformationV3) {
        // Try to upsert an existing instance in case we only updated the API key
        val new = printerConfigRepository.getAll().firstOrNull { it.webUrl == config.webUrl }?.copy(apiKey = config.apiKey) ?: config
        printerConfigRepository.setActive(new.copy(lastModifiedAt = Clock.System.now().toEpochMilliseconds()), trigger = "sign-in")
    }

    fun delete(configId: String) {
        printerConfigRepository.remove(configId)
    }

    fun moveToState(state: State) {
        runBlocking {
            sensitiveDataMask.registerWebUrl(state.urlString?.toUrlOrNull())
        }

        Napier.i(tag = tag, message = "Moving to state: $state")
        mutableState.update { (id, _) ->
            (id + 1) to state
        }
    }

    sealed class State(open val urlString: String?) {
        object Initial : State(null)
        data class Splash(
            val until: Instant
        ) : State(null)

        data class Discover(
            val services: List<NetworkService>,
            val configs: List<OctoPrintInstanceInformationV3>,
            val hasQuickSwitch: Boolean
        ) : State(null)

        data class AccessRequest(
            val url: Url,
            val reusedInstanceId: String?,
            val loginUrl: Url?
        ) : State(url.toString())

        data class AccessRequestFailed(
            val url: Url,
            val reusedInstanceId: String?
        ) : State(url.toString())

        data class Success(
            val config: OctoPrintInstanceInformationV3
        ) : State(null)

        data class Probe(
            override val urlString: String,
            val apiKey: String?,
            val reusedInstanceId: String?,
            val finding: TestFullNetworkStackUseCase.Finding?
        ) : State(null)
    }
}