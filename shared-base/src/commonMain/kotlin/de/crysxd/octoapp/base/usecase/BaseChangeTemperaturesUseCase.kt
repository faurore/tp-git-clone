package de.crysxd.octoapp.base.usecase

import de.crysxd.octoapp.base.network.OctoPrintProvider
import de.crysxd.octoapp.engine.models.commands.BedCommand
import de.crysxd.octoapp.engine.models.commands.ChamberCommand
import de.crysxd.octoapp.engine.models.commands.ToolCommand
import de.crysxd.octoapp.sharedcommon.CommonParcelable
import de.crysxd.octoapp.sharedcommon.CommonParcelize

abstract class BaseChangeTemperaturesUseCase(
    private val octoPrintProvider: OctoPrintProvider,
    private val getCurrentPrinterProfileUseCase: GetCurrentPrinterProfileUseCase,
) : UseCase2<BaseChangeTemperaturesUseCase.Params, Unit>() {

    override suspend fun doExecute(param: Params, logger: Logger) {
        val profile = getCurrentPrinterProfileUseCase.execute(GetCurrentPrinterProfileUseCase.Params(param.instanceId))
        param.temps.filter { it.temperature != null }.forEach {
            when {
                it.component.startsWith("tool") -> {
                    octoPrintProvider.octoPrint(param.instanceId).printerApi.executeToolCommand(
                        createToolCommand(mapOf(it.component to (it.temperature ?: 0f)))
                    )
                }

                it.component == "bed" && profile.heatedBed -> {
                    octoPrintProvider.octoPrint(param.instanceId).printerApi.executeBedCommand(
                        createBedCommand(it.temperature ?: 0f)
                    )
                }

                it.component == "chamber" && profile.heatedChamber -> {
                    octoPrintProvider.octoPrint(param.instanceId).printerApi.executeChamberCommand(
                        createChamberCommand(it.temperature ?: 0f)
                    )
                }
            }
        }
    }

    abstract fun createBedCommand(temperature: Float): BedCommand
    abstract fun createChamberCommand(temperature: Float): ChamberCommand
    abstract fun createToolCommand(temperature: Map<String, Float>): ToolCommand

    data class Params(
        val temps: List<Temperature>,
        val instanceId: String? = null,
    ) {
        constructor(temp: Temperature, instanceId: String? = null) : this(listOf(temp), instanceId)
    }

    @CommonParcelize
    data class Temperature(
        val temperature: Float?,
        val component: String
    ) : CommonParcelable
}