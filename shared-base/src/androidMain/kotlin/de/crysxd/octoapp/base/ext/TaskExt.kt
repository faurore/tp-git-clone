package de.crysxd.octoapp.base.ext

import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

suspend fun <T> Task<T>.blockingAwait(): T = withContext(Dispatchers.IO) {
    Tasks.await(this@blockingAwait)
}