package de.crysxd.octoapp.base.network.dnssd

import android.content.Context
import android.content.pm.PackageManager.NameNotFoundException
import android.os.Build
import androidx.core.content.pm.PackageInfoCompat
import com.google.android.gms.common.GoogleApiAvailability.GOOGLE_PLAY_SERVICES_PACKAGE
import de.crysxd.octoapp.base.OctoConfig
import de.crysxd.octoapp.base.OctoConfigField
import io.github.aakira.napier.Napier
import java.net.InetAddress

interface DnsSd {

    companion object {
        private var instance: DnsSd? = null

        fun create(context: Context): DnsSd {
            instance?.let { return@create it }

            // Google rolled out proper DNS-SD support via a Google Play Services update in late 2021. We check here that we have Android 12 and a GPS that supports .local
            // domains. If both are good we use the newer implementation as the LegacyDnsSd sometimes crashes on Android 12 and up.
            val hasAndroid12 = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S
            val playServiceVersion = try {
                PackageInfoCompat.getLongVersionCode(context.packageManager.getPackageInfo(GOOGLE_PLAY_SERVICES_PACKAGE, 0))
            } catch (e: NameNotFoundException) {
                // Play Services not installed
                0
            } catch (e: Exception) {
                Napier.e(tag = "DnsSd", message = "Failed to check Play Services", throwable = e)
                0
            }
            val hasRecentPlayServices = playServiceVersion >= 224312044
            val newAndroidDnsEnabled = OctoConfig.getBoolean(OctoConfigField.AndroidNewDnsSdEnabled)

            val i = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S && hasAndroid12 && hasRecentPlayServices && newAndroidDnsEnabled) {
                Napier.w(tag = "DnsSd", message = "Using AndroidDnsSd: playServiceVersion=$playServiceVersion")
                AndroidDnsSd(context)
            } else {
                Napier.w(tag = "DnsSd", message = "Using LegacyDnsSd: playServiceVersion=$playServiceVersion newAndroidDnsEnabled=$newAndroidDnsEnabled")
                LegacyDnsSd()
            }

            instance = i
            return i
        }
    }

    suspend fun browse(
        regType: String,
        serviceFound: (ServiceData) -> Unit,
        serviceLost: (ServiceData) -> Unit,
        failure: (Throwable) -> Unit,
    ): Operation

    suspend fun resolve(
        service: ServiceData,
        resolved: (Service) -> Unit,
        failure: (Throwable) -> Unit,
    ): Operation

    fun createServiceData(hostName: String): ServiceData?

    interface Operation {
        fun stop()
    }

    interface ServiceData {
        val description: String
    }

    data class Service(
        val label: String,
        val webUrl: String,
        val port: Int,
        val host: InetAddress,
        val hostname: String,
    )
}
