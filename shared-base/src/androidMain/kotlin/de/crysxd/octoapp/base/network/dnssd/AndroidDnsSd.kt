package de.crysxd.octoapp.base.network.dnssd

import android.content.Context
import android.net.nsd.NsdManager
import android.net.nsd.NsdServiceInfo
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.content.getSystemService
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.network.CachedDns
import io.github.aakira.napier.Napier
import kotlinx.coroutines.sync.Mutex
import java.net.InetAddress
import java.net.UnknownHostException

@RequiresApi(Build.VERSION_CODES.S)
class AndroidDnsSd(context: Context) : DnsSd {

    companion object {
        private var instanceCounter = 0
    }

    private val tag = "AndroidDnsSd/${instanceCounter++}"
    private val nsdManager = context.getSystemService<NsdManager>()
    private val lock = Mutex()

    override suspend fun browse(
        regType: String,
        serviceFound: (DnsSd.ServiceData) -> Unit,
        serviceLost: (DnsSd.ServiceData) -> Unit,
        failure: (Throwable) -> Unit
    ): DnsSd.Operation {
        if (nsdManager == null) {
            failure(IllegalStateException("NsdManager not available"))
            return object : DnsSd.Operation {
                override fun stop() = Unit
            }
        } else {
            val discoveryListener = object : NsdManager.DiscoveryListener {

                // Called as soon as service discovery begins.
                override fun onDiscoveryStarted(regType: String) {
                    Napier.d(tag = tag, message = "Service discovery started")
                }

                override fun onServiceFound(service: NsdServiceInfo) {
                    // A service was found! Do something with it.
                    Napier.d(tag = tag, message = "Service discovery success $service")
                    serviceFound(AndroidServiceData(service))
                }

                override fun onServiceLost(service: NsdServiceInfo) {
                    Napier.d(tag = tag, message = "service lost: $service")
                }

                override fun onDiscoveryStopped(serviceType: String) {
                    Napier.d(tag = tag, message = "Discovery stopped : $serviceType")
                }

                override fun onStartDiscoveryFailed(serviceType: String, errorCode: Int) {
                    Napier.d(tag = tag, message = "Discovery failed : Error code:$errorCode")
                    nsdManager.stopServiceDiscovery(this)
                    failure(Exception("NsdManager failed with error code $errorCode"))
                }

                override fun onStopDiscoveryFailed(serviceType: String, errorCode: Int) {
                    Napier.d(tag = tag, message = "Discovery failed : Error code:$errorCode")
                    nsdManager.stopServiceDiscovery(this)
                    failure(Exception("NsdManager failed with error code $errorCode"))
                }
            }

            nsdManager.discoverServices(
                regType,
                NsdManager.PROTOCOL_DNS_SD,
                discoveryListener
            )

            return object : DnsSd.Operation {
                override fun stop() = nsdManager.stopServiceDiscovery(discoveryListener)
            }
        }
    }

    override fun createServiceData(hostName: String): DnsSd.ServiceData? = null

    override suspend fun resolve(service: DnsSd.ServiceData, resolved: (DnsSd.Service) -> Unit, failure: (Throwable) -> Unit): DnsSd.Operation {
        require(service is AndroidServiceData)
        if (nsdManager == null) {
            failure(IllegalStateException("NsdManager not available"))
            return object : DnsSd.Operation {
                override fun stop() = Unit
            }
        } else {
            val resolveListener = object : NsdManager.ResolveListener {
                override fun onResolveFailed(service: NsdServiceInfo, errorCode: Int) {
                    Napier.d(tag = tag, message = "Resolve failed : Error code:$errorCode")
                    failure(Exception("NsdManager failed with error code $errorCode"))
                    lock.unlock()
                }

                override fun onServiceResolved(service: NsdServiceInfo) {
                    Napier.d(tag = tag, message = "Resolve success : $service")
                    requireNotNull(service.host)

                    val hostNames = listOf(
                        service.host.hostName.removeSuffix(".home") + ".local",
                        service.host.hostName,
                    ).distinct()

                    val hostName = hostNames.firstOrNull {
                        try {
                            InetAddress.getByName(it)
                            true
                        } catch (e: UnknownHostException) {
                            false
                        }
                    } ?: return failure(Exception("Unusable result: $hostNames"))

                    SharedBaseInjector.get().dnsResolver.addCacheEntry(
                        CachedDns.Entry(
                            hostname = hostName,
                            resolvedIpString = listOfNotNull(service.host.hostAddress),
                        )
                    )

                    resolved(
                        DnsSd.Service(
                            label = service.serviceName,
                            host = service.host,
                            hostname = hostName,
                            port = service.port,
                            webUrl = "http://$hostName:${service.port}/"
                        )
                    )
                    lock.unlock()
                }
            }

            lock.lock()
            Napier.i(tag = tag, message = "Resolving ${service.info}")
            nsdManager.resolveService(
                service.info,
                resolveListener
            )

            return object : DnsSd.Operation {
                override fun stop() {
                    Napier.d(tag = tag, message = "Stopped for ${service.info}")
                    lock.unlock()
                }
            }
        }
    }

    data class AndroidServiceData(
        val info: NsdServiceInfo,
    ) : DnsSd.ServiceData {
        override val description = "${info.serviceType} ${info.serviceName}"
    }
}