package de.crysxd.octoapp.base.network.wear

import android.content.Context
import de.crysxd.octoapp.base.utils.measureTime
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector.Proxy
import io.github.aakira.napier.Napier
import io.ktor.http.Url
import java.net.InetAddress

class WearOsProxySelector(context: Context) : ProxySelector {

    private val tag = "WearOsProxySelector"
    private val entryPoint = WearOsSocksProxyEntryPoint(context)
    private val needsProxyCache = mutableSetOf<String>()

    override fun selectProxy(url: Url) = measureTime("proxy_selection") {
        try {
            // Get the local address of the proxy entry point
            val localProxy = entryPoint.socketAddress?.let {
                Proxy.SocksProxy(host = it.hostString, port = it.port)
            } ?: return@measureTime let {
                Napier.i(tag = tag, message = "No Socks proxy available!")
                listOf(Proxy.NoProxy)
            }

            // Do we need a proxy for this host? Check the cache and try a ping otherwise
            val needsProxy = try {
                if (needsProxyCache.contains(url.host)) {
                    true
                } else {
                    !InetAddress.getByName(url.host).isReachable(500)
                }
            } catch (e: Exception) {
                true
            }

            // If we need a proxy, prefer the local SOCKS, if not prefer the direct route
            if (needsProxy) {
                needsProxyCache.add(url.host)
                Napier.d(tag = tag, message = "Using proxy for $url")

                listOf(
                    localProxy,
                    Proxy.NoProxy
                )
            } else {
                listOf(
                    Proxy.NoProxy,
                    localProxy,
                )
            }
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "Failed to select proxy")
            listOf(Proxy.NoProxy)
        }
    }


    override fun connectFailed(url: Url, exception: Throwable) {
        Napier.w(tag = tag, message = "Failure with proxy at $url reported: ${exception.message} (${exception::class.simpleName})")
    }
}