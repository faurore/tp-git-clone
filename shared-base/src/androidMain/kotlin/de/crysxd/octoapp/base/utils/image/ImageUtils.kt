package de.crysxd.octoapp.base.utils.image

import android.graphics.Bitmap
import android.graphics.Matrix
import de.crysxd.octoapp.sharedexternalapis.mjpeg.Image

actual fun Image.applyTransformations(rotate90: Boolean, flipH: Boolean, flipV: Boolean): Image = if (flipV || flipH || rotate90) {
    val matrix = Matrix()

    if (rotate90) {
        matrix.postRotate(-90f)
    }

    matrix.postScale(
        if (flipH) -1f else 1f,
        if (flipV) -1f else 1f,
        width / 2f,
        height / 2f
    )

    Bitmap.createBitmap(this, 0, 0, width, height, matrix, true)
} else {
    this
}