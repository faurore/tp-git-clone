package de.crysxd.octoapp.base.network

import android.content.Context
import de.crysxd.octoapp.sharedcommon.http.config.KeyStoreProvider
import io.ktor.http.Url
import java.io.File
import java.security.KeyStore
import java.security.cert.Certificate
import java.util.UUID

class SslKeyStoreHandler(context: Context) : KeyStoreProvider {

    private val password = context.packageName.toCharArray()
    private val keyStoreFile = File(context.filesDir, "ssl_certs.ks")
    private val sharedPreferences = context.getSharedPreferences("ssl_handler", Context.MODE_PRIVATE)

    fun storeCertificate(cert: Certificate) {
        val ks = loadKeyStore() ?: createKeyStore().also { it.load(null) }
        ks.setCertificateEntry(UUID.randomUUID().toString(), cert)
        keyStoreFile.outputStream().use {
            ks.store(it, password)
        }
    }

    fun enforceWeakVerificationForHost(url: Url) {
        sharedPreferences.edit().putBoolean(url.host, true).apply()
    }

    override fun getWeakVerificationForHosts() = sharedPreferences.all.keys
        .map { it to sharedPreferences.getBoolean(it, false) }
        .filter { it.second }
        .map { it.first }

    override fun loadKeyStore() = if (keyStoreFile.exists()) {
        createKeyStore().also { ks ->
            keyStoreFile.inputStream().use {
                ks.load(it, password)
            }
        }
    } else {
        null
    }

    private fun createKeyStore() = KeyStore.getInstance(KeyStore.getDefaultType())

}