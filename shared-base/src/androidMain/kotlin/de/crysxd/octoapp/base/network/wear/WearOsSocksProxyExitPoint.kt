package de.crysxd.octoapp.base.network.wear

import com.google.android.gms.wearable.ChannelClient
import com.google.android.gms.wearable.ChannelIOException
import com.google.android.gms.wearable.Wearable
import com.google.android.gms.wearable.WearableListenerService
import de.crysxd.octoapp.base.di.SharedBaseInjector
import de.crysxd.octoapp.base.ext.blockingAwait
import de.crysxd.octoapp.base.utils.AppScope
import de.crysxd.octoapp.base.utils.closeQuietly
import de.crysxd.octoapp.base.utils.requireString
import io.github.aakira.napier.Napier
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.io.InputStream
import java.io.OutputStream
import java.net.InetAddress
import java.net.Socket
import java.util.concurrent.Executors

class WearOsSocksProxyExitPoint : WearableListenerService() {

    private val dns = SharedBaseInjector.get().dnsResolver

    companion object {
        // Service is started and stopped, so we need to share this between all instances
        private val proxyJobs = mutableMapOf<String, Job>()
    }

    private val tag = "WearOsSocksProxyExitPoint"
    private val channelClient by lazy { Wearable.getChannelClient(this) }
    private val proxyChannelPath by lazy { requireString("rpc_channel_path___socks_proxy") }
    private val ioThreadPool = Executors.newCachedThreadPool()

    override fun onChannelOpened(channel: ChannelClient.Channel) {
        super.onChannelOpened(channel)

        // We only know proxy channels
        if (!channel.path.startsWith(proxyChannelPath)) {
            return Napier.w(tag = tag, message = "Unknown channel opened to ${channel.path}")
        }

        proxyJobs[channel.path] = runProxy(channel)
        Napier.i(tag = tag, message = "[${channel.path}] Channel open, starting proxy (${proxyJobs.size} sessions now active)")
    }

    override fun onChannelClosed(channel: ChannelClient.Channel, p1: Int, p2: Int) {
        super.onChannelClosed(channel, p1, p2)
        proxyJobs.remove(channel.path)?.cancel()
        Napier.i(tag = tag, message = "[${channel.path}] Channel closed, stopping proxy (${proxyJobs.size} sessions still active)")
    }


    private fun runProxy(channel: ChannelClient.Channel) = AppScope.launch(Dispatchers.IO) {
        var s: Socket? = null
        try {
            val channelInput = channelClient.getInputStream(channel).blockingAwait()
            val channelOutput = channelClient.getOutputStream(channel).blockingAwait()
            Napier.v(tag = tag, message = "[${channel.path}] Opened input/output streams")
            val (socket, socketInput, socketOutput) = doSocks5handshake(channel, channelInput, channelOutput)

            s = socket
            Napier.v(tag = tag, message = "[${channel.path}] Handshake completed")

            val input = ioThreadPool.submit {
                try {
                    Napier.v(tag = tag, message = "[${channel.path}] Copying channel input to socket output")
                    socketInput.copyTo(channelOutput)
                } catch (e: ChannelIOException) {
                    // Channel closed
                    return@submit
                } finally {
                    channelClient.close(channel)
                }
            }

            val output = ioThreadPool.submit {
                try {
                    Napier.v(tag = tag, message = "[${channel.path}] Copying socket input to channel output")
                    channelInput.copyTo(socketOutput)
                } catch (e: ChannelIOException) {
                    // Channel closed
                    return@submit
                } finally {
                    channelClient.close(channel)
                }
            }

            Napier.i(tag = tag, message = "[${channel.path}] Copying input/output from socket to channel")

            input.get()
            output.get()
        } catch (e: Exception) {
            channelClient.close(channel)
            s?.closeQuietly()
            proxyJobs.remove(channel.path)?.cancel()
            if (e is CancellationException) {
                Napier.v(tag = tag, message = "[${channel.path}] Session ended, closing channel (${proxyJobs.size} active sessions)", throwable = e)
            } else {
                Napier.e(tag = tag, message = "[${channel.path}] Exception in proxy process, closing channel (${proxyJobs.size} active sessions)", throwable = e)
            }
        }
    }

    private fun doSocks5handshake(channel: ChannelClient.Channel, channelInput: InputStream, channelOutput: OutputStream): Triple<Socket, InputStream, OutputStream> {
        val buf = ByteArray(256)

        // Read initial request, disregard auth methods as we do not need any
        channelInput.read(buf, 0, 2)
        require(buf[0] == 0x5.toByte()) { "[${channel.path}] Not a SOCKS5 connection (1)" }
        val authMethodCount = buf[1].toInt()
        channelInput.read(buf, 0, authMethodCount)
        require(buf.take(authMethodCount).any { it == 0x0.toByte() }) { "[${channel.path}] No authentication mode not supported" }

        // Acknowledge
        channelOutput.write(byteArrayOf(0x5 /* SOCKS version */, 0x0 /* No auth */))
        channelOutput.flush()

        // Read connect request
        channelInput.read(buf, 0, 4)
        require(buf[0] == 0x5.toByte()) { "[${channel.path}] Not a SOCKS5 connection (2)" }
        require(buf[1] == 0x1.toByte()) { "[${channel.path}] Unsupported command ${buf[1]}, can only establish TCP connection" }
        require(buf[2] == 0x0.toByte()) { "[${channel.path}] Invalid byte" }
        val address = when (buf[3]) {
            // IPv4
            0x1.toByte() -> {
                channelInput.read(buf, 0, 4)
                InetAddress.getByAddress(buf.take(4).toByteArray())
            }

            // Host
            0x3.toByte() -> {
                channelInput.read(buf, 0, 1)
                val length = buf[0].toInt()
                channelInput.read(buf, 0, length)
                val host = String(buf.take(length).toByteArray())
                Napier.v("[${channel.path}] Read host $host")
                dns.lookup(host).first()
            }

            // IPv6
            0x4.toByte() -> {
                channelInput.read(buf, 0, 16)
                InetAddress.getByAddress(buf.take(16).toByteArray())
            }

            // Something else...
            else -> throw IllegalStateException("[${channel.path}] Unsupported target type ${buf[3]}")
        }
        channelInput.read(buf, 0, 2)
        fun byte2int(b: Byte) = b.toInt() and 0xff
        val port = byte2int(buf[0]) * 256 + byte2int(buf[1])
        Napier.v("[${channel.path}] Read target as $address:$port")

        // Connect
        val socket = Socket(address, port)
        try {
            val socketOutput = socket.getOutputStream()
            val socketInput = socket.getInputStream()

            // Acknowledge connection
            val localIp = socket.localAddress.address
            val localPort = socket.localPort
            val localIpType = when (localIp.size) {
                // IPv4
                4 -> 0x1.toByte()

                // IPv6
                16 -> 0x4.toByte()

                // Unsupported
                else -> throw java.lang.IllegalStateException("[${channel.path}] Unsupported local address")
            }
            channelOutput.write(byteArrayOf(0x5 /* SOCKS version */, 0x0 /* Request granted */, 0x0 /* Reserved, always 0x0 */, localIpType))
            channelOutput.write(localIp)
            channelOutput.write(byteArrayOf(localPort.shr(8).toByte(), (localPort and 0xff).toByte()))
            channelOutput.flush()

            Napier.i("[${channel.path}] SOCKS5 handshake completed, connected to ${socket.remoteSocketAddress}")
            return Triple(socket, socketInput, socketOutput)
        } catch (e: Exception) {
            Napier.e(tag = tag, throwable = e, message = "[${channel.path}] Failure in handshake/connection")
            socket.closeQuietly()
            throw e
        }
    }

    private fun InputStream.copyTo(out: OutputStream, bufferSize: Int = DEFAULT_BUFFER_SIZE): Long {
        var bytesCopied: Long = 0
        val buffer = ByteArray(bufferSize)
        var bytes = read(buffer)
        while (bytes >= 0) {
            out.write(buffer, 0, bytes)
            out.flush()
            bytesCopied += bytes
            bytes = read(buffer)
        }
        return bytesCopied
    }
}