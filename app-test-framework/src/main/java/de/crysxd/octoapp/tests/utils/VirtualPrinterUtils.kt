package de.crysxd.octoapp.tests.utils

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.engine.framework.urlFromPath
import de.crysxd.octoapp.engine.models.commands.ConnectionCommand
import de.crysxd.octoapp.engine.octoprint.http.setJsonBody
import io.ktor.client.request.post
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import java.util.concurrent.TimeoutException

object VirtualPrinterUtils {
    private val provider get() = BaseInjector.get().octoPrintProvider()

    fun OctoPrintInstanceInformationV3.setVirtualPrinterEnabled(enabled: Boolean) = runBlocking {
        Timber.i("Setting virtual printer enabled=$enabled")
        BaseInjector.get().octoPrintProvider().createAdHocOctoPrint(this@setVirtualPrinterEnabled).genericRequest { baseUrl, httpClient ->
            httpClient.post {
                urlFromPath(baseUrl = baseUrl, "api", "settings")
                setJsonBody("{  \"plugins\": {   \"virtual_printer\": {     \"enabled\": $enabled } }}")
            }
        }
    }

    fun OctoPrintInstanceInformationV3.connectPrinter() = runBlocking {
        Timber.i("Connecting printer to ${webUrl}")
        val octoprint = provider.createAdHocOctoPrint(this@connectPrinter)
        octoprint.connectionApi.executeConnectionCommand(ConnectionCommand.Connect(port = "VIRTUAL"))
        val end = System.currentTimeMillis() + 30_000
        do {
            delay(500)
            if (System.currentTimeMillis() > end) {
                throw TimeoutException("Unable to connect printer within 30s")
            }
        } while (octoprint.connectionApi.getConnection().current.port == null)
        Timber.i("Connected printer to ${webUrl}")
    }

    fun OctoPrintInstanceInformationV3.disconnectPrinter() = runBlocking {
        Timber.i("Disconnecting printer from ${webUrl}")
        val octoprint = provider.createAdHocOctoPrint(this@disconnectPrinter)
        octoprint.connectionApi.executeConnectionCommand(ConnectionCommand.Disconnect)

        val end = System.currentTimeMillis() + 30_000
        do {
            delay(500)
            if (System.currentTimeMillis() > end) {
                throw TimeoutException("Unable to disconnect printer within 30s")
            }
        } while (octoprint.connectionApi.getConnection().current.port != null)
        Timber.i("Disconnected printer from ${webUrl}")
    }
}