package de.crysxd.octoapp.tests.rules

import de.crysxd.octoapp.base.data.models.OctoPrintInstanceInformationV3
import de.crysxd.octoapp.base.di.BaseInjector
import de.crysxd.octoapp.tests.utils.VirtualPrinterUtils.disconnectPrinter
import de.crysxd.octoapp.tests.utils.VirtualPrinterUtils.setVirtualPrinterEnabled
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement
import timber.log.Timber
import kotlin.time.Duration.Companion.seconds

class IdleTestEnvironmentRule(private vararg val envs: OctoPrintInstanceInformationV3) : TestRule {

    override fun apply(base: Statement, description: Description) = object : Statement() {
        override fun evaluate() {
            makeIdle()
            base.evaluate()
        }
    }

    fun makeIdle() {
        val timeout = 15.seconds

        try {
            runBlocking {
                withTimeout(timeout) {
                    envs.map { it to BaseInjector.get().octoPrintProvider().createAdHocOctoPrint(it) }.forEach { (info, octoprint) ->
                        //region Safe mode check
                        Timber.i("Ensuring ${info.webUrl} is not in safe mode")
                        val safeMode = octoprint.systemApi.getSystemInfo().safeMode
                        require(!safeMode) { "${info.webUrl} is in safe mode, cannot proceed with test!" }
                        //endregion
                        //region Disconnect printer
                        Timber.i("Making ${info.webUrl} idle")
                        info.disconnectPrinter()
                        //endregion
                        //region Turn virtual printer on
                        Timber.i("Turning virtual printer on for ${info.webUrl}")
                        info.setVirtualPrinterEnabled(true)
                        //endregion
                    }
                }
            }
        } catch (e: TimeoutCancellationException) {
            Timber.e("Failed to idle ${envs.map { it.webUrl }} within $timeout")
        } catch (e: Exception) {
            throw java.lang.IllegalStateException("Failed to idle environments: ${envs.joinToString { it.webUrl.toString() }} (${e::class.simpleName}: ${e.message}", e)
        }
    }
}