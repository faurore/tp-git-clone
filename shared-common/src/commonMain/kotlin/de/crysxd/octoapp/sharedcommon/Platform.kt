package de.crysxd.octoapp.sharedcommon

expect class Platform {
    val appVersion: String
    val appBuild: Int
    val appId: String
    val lowPowerDevice: Boolean
    val platformVersion: String
    val platformName: String
    val cacheDirectory: String
    val debugBuild: Boolean
    val deviceName: String
    val deviceModel: String
    val deviceLanguage: String
}