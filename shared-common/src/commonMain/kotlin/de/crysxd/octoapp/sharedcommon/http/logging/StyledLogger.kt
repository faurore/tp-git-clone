package de.crysxd.octoapp.sharedcommon.http.logging

import io.github.aakira.napier.Napier
import io.ktor.client.plugins.logging.Logger

class StyledLogger(private val logTag: String) : Logger {

    override fun log(message: String) {
        var method: String? = null
        var response: String? = null
        var from: String? = null
        var request: String? = null
        var failedResponse: String? = null

        val lines = message.split("\n").filter { s ->
            when {
                s.startsWith("RESPONSE: ") -> {
                    response = s.removePrefix("RESPONSE: ")
                    false
                }

                s.startsWith("FROM: ") -> {
                    from = s.removePrefix("FROM: ")
                    false
                }

                s.startsWith("METHOD: ") -> {
                    method = s.substring(startIndex = s.indexOf("=") + 1, endIndex = s.length - 1)
                    false
                }

                s.startsWith("REQUEST: ") -> {
                    request = s.removePrefix("REQUEST: ")
                    false
                }

                s.startsWith("REQUEST ") -> {
                    failedResponse = s.removePrefix("REQUEST ")
                    false
                }

                s == "COMMON HEADERS" -> false
                s == "CONTENT HEADERS" -> false
                s.startsWith("BODY Content-Type") -> false

                else -> {
                    true
                }
            }
        }

        if (request != null) {
            Napier.i(tag = logTag, message = "==> $method $request")
        } else if (failedResponse != null) {
            Napier.i(tag = logTag, message = "<== $failedResponse")
        } else {
            Napier.i(tag = logTag, message = "<== $response $from")
        }

        var verbose = false
        val text = StringBuilder()
        lines.forEach { s ->
            if (s == "BODY START") {
                verbose = true
                Napier.d(tag = logTag, message = text.toString())
                text.clear()
            }

            when {
                verbose -> text.appendLine("    $s")
                else -> text.appendLine("    ${s.removePrefix("->")}")
            }
        }

        if (verbose) {
            Napier.v(tag = logTag, message = text.toString())
        } else {
            Napier.d(tag = logTag, message = text.toString())
        }
    }
}