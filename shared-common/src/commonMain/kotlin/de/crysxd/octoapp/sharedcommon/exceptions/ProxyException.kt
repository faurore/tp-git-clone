package de.crysxd.octoapp.sharedcommon.exceptions

import io.ktor.http.Url
import kotlinx.coroutines.CancellationException

/*
 * ProxyException is a construct to ensure data privacy in logs and crash reports. ProxyExceptions is a replacement for the
 * original exception and can be used for low-level network errors. ProxyException will srub the error message and the error message
 * of any cause while keeping the original stack trace and exception structure.
 *
 * Certain exceptions like CancelledException or InterruptedException are deemed save and are not proxied as they are essential for
 * the program flow.
 */
class ProxyException private constructor(val original: Throwable, webUrl: Url) : NetworkException(
    originalCause = original.cause,
    userFacingMessage = original.message ?: "No error message",
    technicalMessage = "Proxy for ${original::class.simpleName}: ${original.message} [url=$webUrl]",
    webUrl = webUrl,
) {
    companion object {
        private fun isAcceptedException(original: Throwable?) = original is CancellationException

        fun create(original: Throwable, webUrl: Url) = if (isAcceptedException(original)) {
            original
        } else {
            ProxyException(original, webUrl)
        }
    }
}