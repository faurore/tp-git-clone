package de.crysxd.octoapp.sharedcommon.http.config

import io.ktor.client.plugins.logging.LogLevel as KtorLevel

sealed class LogLevel(val ktorLevel: KtorLevel) {
    object Production : LogLevel(ktorLevel = KtorLevel.INFO)
    object Verbose : LogLevel(ktorLevel = KtorLevel.HEADERS)
    object Debug : LogLevel(ktorLevel = KtorLevel.ALL)
}