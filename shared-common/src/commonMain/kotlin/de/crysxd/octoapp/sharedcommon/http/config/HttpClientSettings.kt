package de.crysxd.octoapp.sharedcommon.http.config

data class HttpClientSettings(
    val timeouts: Timeouts = Timeouts(),
    val logLevel: LogLevel = LogLevel.Production,
    val dns: Dns? = null,
    val proxySelector: ProxySelector? = null,
    val keyStore: KeyStoreProvider? = null,
)