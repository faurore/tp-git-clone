package de.crysxd.octoapp.sharedcommon.utils

import kotlinx.coroutines.delay
import kotlinx.datetime.Clock
import kotlin.time.Duration

suspend inline fun <T> runForAtLeast(duration: Duration, block: () -> T): T {
    val start = Clock.System.now()
    val res = block()
    val end = Clock.System.now()
    delay(duration - (end - start))
    return res
}