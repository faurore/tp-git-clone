package de.crysxd.octoapp.sharedcommon.http.framework

import de.crysxd.octoapp.sharedcommon.http.config.Timeouts
import io.github.aakira.napier.Napier
import io.ktor.client.HttpClient
import io.ktor.client.plugins.ConnectTimeoutException
import io.ktor.client.plugins.HttpClientPlugin
import io.ktor.client.plugins.HttpSend
import io.ktor.client.plugins.plugin
import io.ktor.client.statement.HttpReceivePipeline
import io.ktor.util.AttributeKey
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


// This is a stop-block for request timeouts. iOS is a bit stupid with timeouts, we assume a connection timeout if no bytes are received within a certain timeout

private const val Tag = "KTOR/Timeout"

class SoftTimeouts(private val timeouts: Timeouts) {

    private fun handle(scope: HttpClient) {
        scope.receivePipeline.intercept(HttpReceivePipeline.After) { response ->
            response.call.request.attributes.getOrNull(ConnectionEstablishedListenerAttributeKey)?.onConnectionEstablished()
            proceedWith(response)
        }

        scope.installSoftTimeoutExtension(timeouts = timeouts)
    }

    object Plugin : HttpClientPlugin<SoftTimeoutScope, SoftTimeouts> {
        override val key: AttributeKey<SoftTimeouts> = AttributeKey("SoftTimeouts")

        override fun prepare(block: SoftTimeoutScope.() -> Unit): SoftTimeouts {
            val scope = object : SoftTimeoutScope {
                override var timeouts: Timeouts = Timeouts()
            }
            scope.block()
            return SoftTimeouts(scope.timeouts)
        }

        override fun install(plugin: SoftTimeouts, scope: HttpClient) {
            plugin.handle(scope)
        }
    }
}

interface SoftTimeoutScope {
    var timeouts: Timeouts
}


private fun HttpClient.installSoftTimeoutExtension(timeouts: Timeouts) = plugin(HttpSend).intercept { request ->
    if (attributes.getOrNull(DisabledConnectionEstiablishedTimeout) != true) {
        val connectionTimeoutJob = launch(request.executionContext) {
            val timeout = timeouts.connectionTimeout
            delay(timeout)
            Napier.e(tag = Tag, message = "reportConnectionTimeoutJob was not cancelled, reporting connection timeout for ${request.url.buildString()} after $timeout")
            throw ConnectTimeoutException(url = request.url.buildString(), timeout = timeout.inWholeMilliseconds, cause = null)
        }

        val listener = ConnectionEstablishedListener {
            Napier.v(tag = Tag, message = "Cancel timeout job for ${request.url.buildString()}")
            connectionTimeoutJob.cancel()
        }

        request.attributes.put(ConnectionEstablishedListenerAttributeKey, listener)
    } else {
        Napier.v(tag = Tag, message = "Disabling for request to ${request.url.buildString()}")
    }

    execute(request)
}

private val ConnectionEstablishedListenerAttributeKey = AttributeKey<ConnectionEstablishedListener>("ConnectionDoneListener")
val DisabledConnectionEstiablishedTimeout = AttributeKey<Boolean>("DisabledConnectionEstiablishedTimeout")

fun interface ConnectionEstablishedListener {
    fun onConnectionEstablished()
}

