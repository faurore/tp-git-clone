package de.crysxd.octoapp.sharedcommon.exceptions


open class SuppressedIllegalStateException(message: String? = null, cause: Throwable? = null) : IllegalStateException(message, cause), SuppressedException