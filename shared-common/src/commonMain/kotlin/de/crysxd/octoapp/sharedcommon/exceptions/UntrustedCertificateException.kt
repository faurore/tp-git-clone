package de.crysxd.octoapp.sharedcommon.exceptions

import de.crysxd.octoapp.sharedcommon.http.config.X509Certificate

class UntrustedCertificateException(
    val certificate: X509Certificate,
    val weakHostnameVerificationRequired: Boolean,
) : Exception("The certificate was not trusted")