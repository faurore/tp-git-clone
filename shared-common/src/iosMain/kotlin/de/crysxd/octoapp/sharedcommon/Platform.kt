package de.crysxd.octoapp.sharedcommon

import platform.Foundation.NSBundle
import platform.Foundation.NSCachesDirectory
import platform.Foundation.NSFileManager
import platform.Foundation.NSLocale
import platform.Foundation.NSUserDomainMask
import platform.Foundation.preferredLanguages
import platform.UIKit.UIDevice

actual class Platform {
    actual val appVersion by lazy {
        NSBundle.mainBundle.objectForInfoDictionaryKey("CFBundleShortVersionString") as String
    }

    actual val appBuild by lazy {
        (NSBundle.mainBundle.objectForInfoDictionaryKey("CFBundleVersion") as String).toInt()
    }

    actual val appId by lazy {
        requireNotNull(NSBundle.mainBundle.bundleIdentifier) { "bundleIdentifier is null" }
    }

    actual val platformVersion by lazy {
        UIDevice.currentDevice.systemVersion
    }

    actual val platformName by lazy {
        UIDevice.currentDevice.systemName
    }

    actual val cacheDirectory by lazy {
        requireNotNull(
            NSFileManager.defaultManager.URLForDirectory(
                directory = NSCachesDirectory,
                inDomain = NSUserDomainMask,
                appropriateForURL = null,
                error = null,
                create = false
            )?.absoluteURL?.path
        ) {
            "Cache directory was null"
        }
    }
    actual val lowPowerDevice = false

    actual val debugBuild = kotlin.native.Platform.isDebugBinary

    actual val deviceName: String = UIDevice.currentDevice.name

    actual val deviceModel: String = UIDevice.currentDevice.localizedModel

    actual val deviceLanguage: String
        get() = NSLocale.preferredLanguages.first().toString()
}