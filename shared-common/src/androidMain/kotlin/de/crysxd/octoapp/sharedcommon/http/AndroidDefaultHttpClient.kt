package de.crysxd.octoapp.sharedcommon.http

import android.annotation.SuppressLint
import de.crysxd.octoapp.sharedcommon.exceptions.UntrustedCertificateException
import de.crysxd.octoapp.sharedcommon.http.config.HttpClientSettings
import de.crysxd.octoapp.sharedcommon.http.config.KeyStore
import de.crysxd.octoapp.sharedcommon.http.config.X509Certificate
import io.ktor.client.HttpClient
import io.ktor.client.HttpClientConfig
import io.ktor.client.engine.okhttp.OkHttp
import io.ktor.http.Url
import okhttp3.Dns
import okhttp3.OkHttpClient
import okhttp3.internal.tls.OkHostnameVerifier
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Proxy
import java.net.Proxy.NO_PROXY
import java.net.Proxy.Type
import java.net.ProxySelector
import java.net.SocketAddress
import java.net.URI
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.util.concurrent.TimeUnit
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager
import de.crysxd.octoapp.sharedcommon.http.config.Dns as Dns2
import de.crysxd.octoapp.sharedcommon.http.config.ProxySelector as ProxySelector2

internal actual fun SpecificDefaultHttpClient(settings: HttpClientSettings, config: HttpClientConfig<*>.() -> Unit) = HttpClient(OkHttp) {
    engine {
        preconfigured = OkHttpClient.Builder()
            .pingInterval(settings.timeouts.webSocketPingPongTimeout.inWholeMilliseconds, TimeUnit.MILLISECONDS)
            .proxySelector(settings.proxySelector)
            .dns(settings.dns)
            .selfSignedTrustManager(
                keyStore = settings.keyStore?.loadKeyStore(),
                weakVerificationHosts = settings.keyStore?.getWeakVerificationForHosts() ?: emptyList(),
            )
            .build()
    }

    config()
}

// Suppress warning. We fall back on default implementation except for pinned certs from specified hosts.
@SuppressLint("CustomX509TrustManager")
fun OkHttpClient.Builder.selfSignedTrustManager(
    keyStore: KeyStore?,
    weakVerificationHosts: List<String>,
): OkHttpClient.Builder {
    val sslContext = SSLContext.getInstance("SSL")
    val tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
    tmf.init(keyStore)
    val systemTrustManager = tmf.trustManagers.firstNotNullOf { it as? X509TrustManager }

    val hostnameVerifier = HostnameVerifier { host, session ->
        when {
            // Rely on system for default
            OkHostnameVerifier.verify(host, session) -> true

            // If this host is not weakly verified, system has the last word
            weakVerificationHosts.none { it.equals(host, ignoreCase = true) } -> false

            // Host is weakly verified, rely on subjectDN and issuerDN. System relies on subjectAltName which is missing
            else -> (session.peerCertificates.first() as? X509Certificate)?.let { cert ->
                cert.subjectDN.name.contains("CN=$host", ignoreCase = true) ||
                        cert.issuerDN.name.contains("CN=$host", ignoreCase = true)
            } ?: false
        }
    }

    val selfSignedTrustManager = object : X509TrustManager {
        override fun checkServerTrusted(chain: Array<out X509Certificate>, authType: String) {
            try {
                // Rely on system for default
                systemTrustManager.checkServerTrusted(chain, authType)
            } catch (e: CertificateException) {
                // If system declines, check if we can trust the public cert coming from this host
                // We need weak hostname verification if the SAN field is missing
                val leaf = chain[0]
                throw UntrustedCertificateException(
                    certificate = leaf,
                    weakHostnameVerificationRequired = leaf.subjectAlternativeNames.sumOf { it.size } == 0,
                )
            }
        }

        override fun getAcceptedIssuers(): Array<out X509Certificate>? {
            return systemTrustManager.acceptedIssuers
        }

        override fun checkClientTrusted(chain: Array<out X509Certificate>, authType: String) {
            systemTrustManager.checkClientTrusted(chain, authType)
        }
    }

    sslContext.init(null, arrayOf(selfSignedTrustManager), SecureRandom())
    return this
        .sslSocketFactory(sslContext.socketFactory, selfSignedTrustManager)
        .hostnameVerifier(hostnameVerifier)
}

fun OkHttpClient.Builder.dns(dns: Dns2?): OkHttpClient.Builder = dns?.let { nested ->
    object : Dns {
        override fun lookup(hostname: String) = nested.lookup(hostname)
    }
}?.let {
    dns(it)
} ?: this

fun OkHttpClient.Builder.proxySelector(selector: ProxySelector2?): OkHttpClient.Builder = selector?.let { nested ->
    object : ProxySelector() {
        override fun select(url: URI): MutableList<Proxy> = nested.selectProxy(Url(url.toString())).map {
            when (it) {
                ProxySelector2.Proxy.NoProxy -> NO_PROXY
                is ProxySelector2.Proxy.SocksProxy -> Proxy(Type.SOCKS, InetSocketAddress(it.host, it.port))
            }
        }.toMutableList()

        override fun connectFailed(url: URI, p1: SocketAddress, e: IOException) {
            nested.connectFailed(Url(url.toString()), e)
        }
    }
}?.let {
    proxySelector(it)
} ?: this