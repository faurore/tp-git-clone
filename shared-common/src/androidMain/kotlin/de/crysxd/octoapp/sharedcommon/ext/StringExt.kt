package de.crysxd.octoapp.sharedcommon.ext

import java.util.Locale
import java.lang.String.format as format2

actual fun String.format(vararg args: Any): String = format2(Locale.ENGLISH, this, *args)