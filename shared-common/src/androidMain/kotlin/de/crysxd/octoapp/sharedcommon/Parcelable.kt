package de.crysxd.octoapp.sharedcommon

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

actual typealias CommonParcelize = Parcelize
actual typealias CommonParcelable = Parcelable