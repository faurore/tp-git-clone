package de.crysxd.octoapp.help.plugins

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.updatePadding
import androidx.navigation.fragment.navArgs
import com.google.android.material.tabs.TabLayoutMediator
import de.crysxd.baseui.BaseFragment
import de.crysxd.baseui.InsetAwareScreen
import de.crysxd.baseui.ext.requireOctoActivity
import de.crysxd.baseui.menu.main.OctoPrintMenu
import de.crysxd.baseui.utils.CollapsibleToolbarTabsHelper
import de.crysxd.octoapp.base.data.models.Announcement
import de.crysxd.octoapp.help.R
import de.crysxd.octoapp.help.databinding.HelpPluginsLibraryFragmentBinding
import de.crysxd.octoapp.help.di.injectViewModel
import de.crysxd.octoapp.sharedcommon.http.framework.toUrl
import java.util.concurrent.TimeUnit

class PluginsLibraryFragment : BaseFragment(), InsetAwareScreen {
    override val viewModel by injectViewModel<PluginsLibraryViewModel>()
    private lateinit var binding: HelpPluginsLibraryFragmentBinding
    private var previousMediator: TabLayoutMediator? = null
    private val helper = CollapsibleToolbarTabsHelper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        postponeEnterTransition(1000, TimeUnit.MILLISECONDS)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = HelpPluginsLibraryFragmentBinding.inflate(inflater, container, false).also {
        binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = PluginsLibraryPagerAdapter()
        binding.viewPager.adapter = adapter
        binding.appBarLayout.title.setText(R.string.plugin_library___title)
        binding.appBarLayout.subtitle.setText(R.string.plugin_library___description)

        if (OctoPrintMenu.shouldAnnounceCompanion()) {
            binding.appBarLayout.announcement.checkVisible(
                Announcement(
                    id = "companion_plugin_annoucement_in_library",
                    text = { getString(de.crysxd.baseui.R.string.main_menu___companion_accouncement___subtitle) },
                    actionText = { getString(R.string.plugin_library___plugin_page) },
                    actionUri = { "https://plugins.octoprint.org/plugins/octoapp/".toUrl() },
                    backgroundColor = R.color.menu_style_settings_background,
                    foregroundColor = R.color.dark_text,
                    canHide = false,
                )
            )
        }

        helper.install(
            octoActivity = requireOctoActivity(),
            binding = binding.appBarLayout,
            viewLifecycleOwner = viewLifecycleOwner,
        )

        viewModel.pluginsIndex.observe(viewLifecycleOwner) {
            startPostponedEnterTransition()
            helper.markTabsCreated()
            createTabs(it)
            adapter.index = it

            binding.appBarLayout.tabs.post {
                val selectedCategory = navArgs<PluginsLibraryFragmentArgs>().value.category?.takeIf { c -> c.isNotBlank() }
                val selectedIndex = it.categories.indexOfFirst { c -> c.id == selectedCategory }.takeIf { i -> i >= 0 } ?: 0
                binding.appBarLayout.tabs.selectTab(binding.appBarLayout.tabs.getTabAt(selectedIndex))
            }
        }
    }

    private fun createTabs(index: PluginsLibraryViewModel.PluginsIndex) {
        previousMediator?.detach()
        previousMediator = TabLayoutMediator(binding.appBarLayout.tabs, binding.viewPager) { tab, position ->
            tab.text = index.categories[position].name
        }
        previousMediator?.attach()
    }

    override fun handleInsets(insets: Rect) {
        helper.handleInsets(insets)
        binding.root.updatePadding(bottom = insets.bottom)
    }
}
