package de.crysxd.octoapp.help.tutorials

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.distinctUntilChanged
import androidx.lifecycle.viewModelScope
import de.crysxd.baseui.BaseViewModel
import de.crysxd.octoapp.base.data.repository.TutorialsRepository
import de.crysxd.octoapp.sharedexternalapis.tutorials.model.Tutorial
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.Date

class TutorialsViewModel(
    private val tutorialsRepository: TutorialsRepository,
) : BaseViewModel() {

    private val mutableViewState = MutableLiveData<ViewState>(ViewState.Loading)
    val viewState = mutableViewState.distinctUntilChanged()

    init {
        reloadPlaylist()
    }

    fun reloadPlaylist(skipCache: Boolean = false) = viewModelScope.launch(coroutineExceptionHandler) {
        try {
            mutableViewState.postValue(ViewState.Loading)
            mutableViewState.postValue(
                ViewState.Data(
                    videos = tutorialsRepository.getTutorials(skipCache),
                    seenUpUntil = Date(tutorialsRepository.getTutorialsSeenUpUntil().toEpochMilliseconds())
                )
            )
            tutorialsRepository.markTutorialsSeen()
        } catch (e: Exception) {
            mutableViewState.postValue(ViewState.Error)
            Timber.e(e)
        }
    }

    sealed class ViewState {
        object Loading : ViewState()
        object Error : ViewState()
        data class Data(val videos: List<Tutorial>, val seenUpUntil: Date) : ViewState()
    }
}
